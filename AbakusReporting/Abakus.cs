﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.IO;
using System.Data;
using System.IO.Compression;
using SolutionHelper;
using WhirlygigSolutionHelper;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace AbakusReporting
{
    public class Abakus
    {
        Company company;
        Data Processor = new Data();
        mediaLavidgeEntities12 db = new mediaLavidgeEntities12();
        dataAbakusAccount abakusAccount = new dataAbakusAccount();

        string _server;
        string _filelocation;
        string _ftpUser;
        string _ftpPassword;
        string csvExportCompleted;
        string csvExportFailed;
        string csvExportWaiting;
        string csvExport;
        string tableName;

        public bool completeFile;
        public bool duplicate;

        public Abakus()
        {
            tableName = "dataAbakusReportingData";

            csvExportCompleted = ConfigurationManager.AppSettings["csvExportCompleted"];
            csvExportFailed = ConfigurationManager.AppSettings["csvExportFailed"];
            csvExport = ConfigurationManager.AppSettings["csvExport"];
            csvExportWaiting = ConfigurationManager.AppSettings["csvExportWaiting"];

            _server = ConfigurationManager.AppSettings["FtpServer"];
            _filelocation = ConfigurationManager.AppSettings["FileLocation"];
        }

        public void Run()
        {
            GetFiles();
            ExactFiles();
            ProcessFiles();
        }

        private void ExactFiles()
        {
            DirectoryInfo directorySelected = new DirectoryInfo(_filelocation);

            foreach (FileInfo fileToDecompress in directorySelected.GetFiles("*.gz"))
            {
                Decompress(fileToDecompress);
                fileToDecompress.Delete();
            }
        }

        private Company GetCompany(string fullName)
        {
            Company[] companys = (from m in db.Companies where m.IsActive == true && m.Abakus_Keyword != null select m).ToArray();

            foreach (Company company in companys)
            {
                if (fullName.ToLower().ContainsAny(company.Abakus_Keyword.ToLower().Split(',')))
                {
                    Data processor = new Data(company.DatabaseName, company.Login, company.Password, company.Server);

                    Processor = processor;

                    abakusAccount = (from m in db.dataAbakusAccounts where m.IsActive == true && m.CompanyId == company.Id select m).FirstOrDefault();

                    return company;
                }
            }

            return null;
        }

        /// <summary>
        /// Sets the processor object
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        private void SetProcessor(Company company)
        {
            abakusAccount = (from m in db.dataAbakusAccounts where m.IsActive == true && m.CompanyId == company.Id select m).FirstOrDefault();

            Data processor = new Data(company.DatabaseName, company.Login, company.Password, company.Server);

            Processor = processor;
             
        }

        private void ProcessFiles()
        {
            try
            {
                DirectoryInfo d = new DirectoryInfo(_filelocation + @"\Process");
                FileInfo[] infos = d.GetFiles();

                int idx = 0;

                foreach (FileInfo f in infos.Where(m => m.FullName.ContainsAny("_abakus_")).OrderBy(n => n.Length))
                {
                    DataTable dt = null;

                    idx++;

                    try
                    {
                        Reset();

                        company = GetCompany(f.Name);

                        if (company == null)
                        {
                            MoveFileForLaterProcessing(idx, f);
                        }
                        else
                        {
                            ProcessDatatable(f);
                        }
                    }
                    catch (Exception ex)
                    {
                        SolutionHelper.Error.LogError(ex, "AbakusServiceHandler.ProcessFiles:" + Processor.DatabaseName);
                    }
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "AbakusServiceHandler.ProcessFiles1:" + Processor.DatabaseName);
            }
        }

        internal void ProcessDatatable(FileInfo file)
        {
            DataTable dt = ExtractDatatable(file);

            if (dt != null)
            {
                ColumnChecker(dt);
                CleanData(dt);
                ProcessFile(file, dt);
            }
            else
                throw new Exception("Datatable is null.");
        }

        private bool ProcessFile(FileInfo file, DataTable dt)
        {
            bool processed = ProcessReport(dt);

            if (processed)
            {
                file.MoveTo(csvExportCompleted + @"\" + file.Name.Replace(file.Extension, " ") + System.DateTime.Now.ToString().Replace(":", ".").Replace("/", "") + file.Extension);
            }
            else if (duplicate || completeFile)
            {
                string path = csvExportCompleted + @"\" + file.Name.Replace(file.Extension, " Duplicate ") + System.DateTime.Now.ToString().Replace(":", ".").Replace("/", "") + file.Extension;
                file.MoveTo(path);
            }
            else
            {
                file.MoveTo(csvExportFailed + @"\" + file.Name);
            }

            return processed;
        }

        private bool ProcessReport(DataTable dt)
        {
            bool processed = false;
            DateTime today = System.DateTime.Now.Date;
            DateTime[] reports = GetReportDates(tableName, dt);

            DataSet ds = dt.ToDataSetDateSplit();

            foreach (DataTable datatab in ds.Tables)
            {
                DateTime tablename = DateTime.Parse(datatab.TableName.ToString());

                if (reports.Contains(tablename))
                {
                    bool comparedata = CompareReportDataAgainstDatabaseData(datatab);

                    if (comparedata)
                        completeFile = true;
                    else
                    {
                        completeFile = false;
                        return false;
                    }

                    continue;
                }
                else if (tablename == today)
                {
                    continue;
                }
                else
                {
                    completeFile = false;
                }

                processed = ProcessAbakusDatatable(datatab);

                if (!processed)
                    return processed;
            }

            return processed;
        }

        private bool ProcessAbakusDatatable(DataTable dt)
        {
            try
            { 
                string[] columnCol = GetColumnsFromDatabase(tableName, false);

                SqlConnection conn = new SqlConnection(Processor.ConnectionString);

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);

                    bulkCopy.BatchSize = 10000;
                    bulkCopy.BulkCopyTimeout = 50;

                    foreach (DataColumn dc in dt.Columns)
                    {
                        string dbCol = "";

                        foreach (string c in columnCol)
                        {
                            if (dc.ColumnName.ToLower() == c.ToLower())
                            {
                                dbCol = c;
                                break;
                            }
                        }

                        if (dbCol == "")
                            throw new Exception("Column not matching: " + dc.ColumnName.ToString());

                        SqlBulkCopyColumnMapping mapping = new SqlBulkCopyColumnMapping(dc.ColumnName, dbCol);

                        bulkCopy.ColumnMappings.Add(mapping);
                    }

                    bulkCopy.DestinationTableName = tableName;

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 2000;
                    conn.Open();

                    bulkCopy.WriteToServer(dt);

                    return true;
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "AbakusServiceHandler.DataTableToDatabase:" + Processor.DatabaseName);
            }

            return false;
        }
        private string[] GetColumnsFromDatabase(string tableName, bool isToLower = true)
        {
            string query = "";
            query += "DECLARE @columns varchar(MAX) ";
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(MAX), COLUMN_NAME) ";
            query += String.Format("FROM (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{0}') as tab ", tableName);
            query += "select @columns ";

            DataSet ds = Processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString();

            string[] columnCol;

            if (isToLower)
                columnCol = columns.ToLower().Split(',');
            else
                columnCol = columns.Split(',');

            return columnCol;
        }

        private void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(String.Format("{0} Rows have been copied.", e.RowsCopied.ToString()));
        }

        /// <summary>
        /// This is to compare the total records and the sum of the impressions against the database. 
        /// </summary>
        /// <param name="datatab"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private bool CompareReportDataAgainstDatabaseData(DataTable datatab)
        {
            try
            {

                string query = String.Format(@"SELECT count(*) Total, sum([Conversions]) SumConv FROM [dbo].[{0}] where AsOfDate =  '{1}'", tableName, datatab.TableName);

                DataSet ds = Processor.RunQuery(query);

                if (ds != null)
                {
                    if (ds.Tables.Count != 0)
                    {
                        int total = int.Parse(ds.Tables[0].Rows[0]["Total"].ToString());

                        int _total = datatab.Rows.Count;

                        if (total == _total)
                            return true;
                    }
                }

            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }

        private DateTime[] GetReportDates(string tableName, DataTable dt)
        {
            List<DateTime> dates = new List<DateTime>();

            string query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[{0}] where [AsOfDate] is not null", tableName);

            DataSet ds = Processor.RunQuery(query);

            if (ds != null) // Add date to list of date
                if (ds.Tables.Count != 0) // Add date to list of date
                    foreach (DataRow dr in ds.Tables[0].Rows)
                        if (dr[0].ToString() != "")
                            dates.Add(DateTime.Parse(dr[0].ToString())); // Date is a non nullable date type

            return dates.ToArray();
        }

        /// <summary>
        /// Clean the data based on the column type define in the AddColumn method 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private DataTable CleanData(DataTable dt)
        {
            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName.ToUpper() == "Conversion Timestamp".ToUpper())
                    {
                        DateTime date = DateTime.Parse(dr[col.ColumnName].ToString());

                        string adfs = date.Date.ToShortDateString();

                        dr["AsOfDate"] = date.Date.ToShortDateString();
                    }
                    if (col.ColumnName.ToUpper() == "Conversion Tag Id".ToUpper())
                    {
                        var fdsafda = dr[col.ColumnName];
                         if (dr[col.ColumnName].ToString() == "N/A")
                            dr[col.ColumnName] = 0;
                    }
                }
            }
            return dt;
        }

        /// <summary>
        /// Check if all the columns are created in the database for the reporting table. If the column doesnt exist it will be added.
        /// </summary>
        /// <param name="dt"></param>
        private void ColumnChecker(DataTable dt)
        {
            string[] columnCol = GetColumnsFromDatabase();
            DateTime today = System.DateTime.Now;


            foreach (DataColumn dc in dt.Columns)
            {
                if (dc.ColumnName.ContainsAny("* ", "#"))
                    dc.ColumnName = dc.ColumnName.Replace("* ", "").Replace("#", "");

                if (dc.ColumnName.ToLower() == "asofdate")
                    continue;

                if (dc.ColumnName.ToLower() == "player")
                    dc.ColumnName = "Placement Id";
                else if (dc.ColumnName.ToLower() == "cookieid")
                    dc.ColumnName = "Cookie Id";
                else if (dc.ColumnName.ToLower() == "conversiontagid")
                    dc.ColumnName = "Conversion Tag Id";
                else if (dc.ColumnName.ToLower() == "conversiontimestamp")
                    dc.ColumnName = "Conversion Timestamp";
                else if (dc.ColumnName.ToLower() == "conversionname")
                    dc.ColumnName = "Conversion Name";
                else
                    dc.ColumnName = dc.ColumnName.ToProperCase();

                if (!columnCol.Contains(dc.ColumnName.ToLower()))
                    AddColumn(tableName, dc.ColumnName);
            }

            if (!dt.Columns.Contains("AsOfDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("AsOfDate", typeof(System.String));
                dt.Columns.Add(newColumn);
            }

            if (!dt.Columns.Contains("Modified Date"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("Modified Date", typeof(System.DateTime));
                newColumn.DefaultValue = today.ToString();
                dt.Columns.Add(newColumn);
            }

            bool bolChecker = DebugHelper.CheckColumnsAgainstDatabase(Processor, dt, tableName);
        }

        /// <summary>
        /// Due to the columns needing to be dynamic this method is designed to add additional column to the table
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        private void AddColumn(string table, string column)
        {
            column = column.ToProperCase();

            string query = "ALTER TABLE {0} ADD [{1}] {2}";

            if (column.ToUpper().ContainsAny("CLICKS", "POST-IMPRESSION CONVERSIONS", "POST-CLICK CONVERSIONS", "TOTAL CONVERSIONS", "VIDEO STARTED"))
                query = string.Format(query, table, column, "int NULL");
            else if (column.ToUpper().ContainsAny("VIDEO FULLY PLAYED RATE", "TOTAL REVENUE"))
                query = string.Format(query, table, column, "[decimal](18, 2) NULL");
            else
                query = string.Format(query, table, column, "[nvarchar](250)");

            Processor.RunQuery(query);
        }

        private string[] GetColumnsFromDatabase(bool isToLower = true)
        {
            string query = "";
            query += "DECLARE @columns varchar(MAX) ";
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(MAX), COLUMN_NAME) ";
            query += String.Format("FROM (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{0}') as tab ", tableName);
            query += "select @columns ";

            DataSet ds = Processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString();

            string[] columnCol;

            if (isToLower)
                columnCol = columns.ToLower().Split(',').Select(x => x.Trim()).ToArray();
            else
                columnCol = columns.Split(',').Select(x => x.Trim()).ToArray(); 

            return columnCol;
        }

        private DataTable ExtractDatatable(FileInfo file)
        {
            DataTable dt = new DataTable();

            dt = CsvToDatabase.GetDataTabletFromCSVTSVFile(file.FullName.ToString());

            return dt;
        }

        private void Reset()
        {
            company = null;
            Processor = null;

            duplicate = false;
            completeFile = false;

            _ftpUser = "";
            _ftpPassword = "";
        }

        private void MoveFileForLaterProcessing(int idx, FileInfo f)
        {
            FileInfo fInfo = new FileInfo(csvExportWaiting + "/" + f.Name);

            if (!fInfo.Exists)
                f.MoveTo(csvExportWaiting + "/" + f.Name.Replace(f.Extension, "").ToString() + f.Extension);
            else
                f.MoveTo(csvExportWaiting + "/" + f.Name.Replace(f.Extension, " Duplicated " + idx.ToString() + f.Extension));
        }

        private void GetFiles()
        {
            foreach (Company company in db.Companies.Where(x => x.IsActive == true && x.Abakus_Keyword != null))
            {
                Reset();
                SetProcessor(company);

                DateTime date = System.DateTime.Now.AddDays(-1).Date;
                DateTime StartDate = date.AddDays(-14);  // DateTime.Parse("12/28/2016");  //  
                DateTime EndDate = date; //   DateTime.Parse("6/30/2016"); // 

                DateTime[] reports = GetReportDates(StartDate);

                string[] list = GetDirectory();

                for (date = StartDate; date.Date <= EndDate.Date; date = date.AddDays(1))  /*    LOOP THROUGH LOGIC */
                {
                    if (!reports.Contains(date))
                    { 
                        string d = date.ToString("yyyy-MM-dd");
                        string str = list.Where(x => x.Contains(company.Abakus_Keyword) && x.Contains(d)).FirstOrDefault() ?? "";

                        if (!String.IsNullOrEmpty(str))
                            DownloadFile(_server + @"/" + str);
                    }
                }
            }
        }

        /// <summary>
        /// Get distinct AsOfDate dates from tables
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private DateTime[] GetReportDates(DateTime date)
        {
            List<DateTime> dates = new List<DateTime>();

            string query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[{0}] where asofdate >= '{1}'", tableName, date);

            DataSet ds = Processor.RunQuery(query);

            if (ds != null) // Add date to list of date
                if (ds.Tables.Count != 0) // Add date to list of date
                    foreach (DataRow dr in ds.Tables[0].Rows)
                        dates.Add(DateTime.Parse(dr[0].ToString())); // Date is a non nullable date type

            return dates.ToArray();
        }

        /// <summary>
        /// Get a list of all files in the ftp folder
        /// </summary>
        /// <returns></returns>
        private string[] GetDirectory()
        {
            FtpWebRequest directoryListRequest = (FtpWebRequest)WebRequest.Create("ftp://ftp.abakus.me");
            directoryListRequest.Method = WebRequestMethods.Ftp.ListDirectory;
            directoryListRequest.Credentials = new NetworkCredential("asu", "8Bu=!aGx68xzt$v#");

            using (FtpWebResponse directoryListResponse = (FtpWebResponse)directoryListRequest.GetResponse())
            {
                using (StreamReader directoryListResponseReader = new StreamReader(directoryListResponse.GetResponseStream()))
                {
                    string responseString = directoryListResponseReader.ReadToEnd();
                    string[] results = responseString.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);
                    return results;
                }
            }
        }

        /// <summary>
        /// Download file from ftp folder
        /// </summary>
        /// <param name="RemoteFtpPath"></param>
        private void DownloadFile(string RemoteFtpPath)
        {
            string result = "";

            // Get the object used to communicate with the server.  
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(RemoteFtpPath);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            // This example assumes the FTP site uses asu logon.  
            request.Credentials = new NetworkCredential(abakusAccount.Login, abakusAccount.Password);

            try
            {
                using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                {
                    Stream responseStream = response.GetResponseStream();

                    try
                    {
                        string locations = _filelocation + RemoteFtpPath.Replace(@"ftp://ftp.abakus.me/", @"\");

                        if (File.Exists(locations))
                            return;

                        FileStream fileStream = File.Create(locations);


                        byte[] b = null;

                        using (MemoryStream ms = new MemoryStream())
                        {
                            int count = 0;
                            do
                            {
                                byte[] buf = new byte[1024];
                                count = responseStream.Read(buf, 0, 1024);
                                ms.Write(buf, 0, count);
                            } while (responseStream.CanRead && count > 0);
                            b = ms.ToArray();
                        }

                        fileStream.Write(b, 0, b.Length);
                        fileStream.Close();

                    }
                    catch (Exception ex)
                    {
                        SolutionHelper.Error.LogError(ex, "AbakusServiceHandler.DownloadFile:" + ex.Message);
                    }

                    response.Close();
                }

            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "AbakusServiceHandler.DownloadFile2:" + ex.Message);
            }

        }

        private static void Decompress(FileInfo fileToDecompress)
        {
            using (FileStream originalFileStream = fileToDecompress.OpenRead())
            {
                string currentFileName = fileToDecompress.FullName;
                string newFileName = currentFileName.Remove(currentFileName.Length - fileToDecompress.Extension.Length);

                string locations = newFileName.Replace(@"\Files\", @"\Files\Process\");

                if (File.Exists(locations))
                    return;

                using (FileStream decompressedFileStream = File.Create(locations))
                {
                    using (GZipStream decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                    {
                        decompressionStream.CopyTo(decompressedFileStream);
                        Console.WriteLine("Decompressed: {0}", fileToDecompress.Name);
                    }
                }
            }
        }
    }
}
