﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbakusReporting
{
    class Program
    {
        static void Main(string[] args)
        {
            RunAbakus();
        }

        private static void RunAbakus()
        {
            Console.WriteLine("RunAbakus : *******************************************" + System.DateTime.Now);
            DateTime startTime = System.DateTime.Now;

            Abakus a = new Abakus();
            a.Run();

            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("Total Time : " + endTime.Subtract(startTime).TotalMinutes);
        }
    }
}
