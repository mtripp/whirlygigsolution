﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BingReporting
{
    public class Program
    {
        static void Main(string[] args)
        {
            RunCustomReport();
        }
        private static void RunCustomReport()
        {
            Console.WriteLine("RunCustomBingReport : " + System.DateTime.Now);
            DateTime startTime = System.DateTime.Now;

            BingReport service = new BingReport();
            service.Process();

            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("Total Time : " + endTime.Subtract(startTime).TotalMinutes);
        }
    }
}
