﻿using OpenPop.Mime;
using OpenPop.Pop3;
using SolutionHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using WhirlygigSolutionHelper;

namespace ConversantReporting
{
    public class ConversantReport
    {
        Data Processor = new Data();
       mediaConversantEntities db = new mediaConversantEntities();
        Company company = new Company();

        string server;
        string accountname;
        string accountpassword;

        public ConversantReport()
        {
            server = ConfigurationManager.AppSettings["EmailAccountServer"];
            accountname = ConfigurationManager.AppSettings["EmailAccountName"];
            accountpassword = ConfigurationManager.AppSettings["EmailAccountPassword"];
        }

        public void Run()
        {
            ProcessConversantReport();
        }

        private bool ProcessConversantReport(DataTable dt)
        {
            try
            {
                SqlConnection conn = new SqlConnection(Processor.ConnectionString);

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);

                    bulkCopy.BatchSize = 10000;
                    bulkCopy.BulkCopyTimeout = 500;
                     
                    foreach (DataColumn dc in dt.Columns)
                    {
                        if (dc.ColumnName == "Date")
                        {
                            SqlBulkCopyColumnMapping mapping = new SqlBulkCopyColumnMapping(dc.ColumnName, "AsOfDate");

                            bulkCopy.ColumnMappings.Add(mapping);
                        }
                        else
                        {
                            SqlBulkCopyColumnMapping mapping = new SqlBulkCopyColumnMapping(dc.ColumnName, dc.ColumnName);

                            bulkCopy.ColumnMappings.Add(mapping);
                        }
                    }

                    bulkCopy.DestinationTableName = "dataConversantReportingData";

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 200;
                    conn.Open();

                    bulkCopy.WriteToServer(dt);

                    return true;
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "AnalyticsDataServiceHandler.DataTableToDatabase:");
            }
            return false;
        }

        private bool ProcessMessage(Message message)
        {
            try
            {
                string url = GetUrlFromDownload(message);

                if (url == "")
                    return false;

                DataTable dt = ExtractReport(url);

                if (dt != null)
                {
                    UpdateDateColumn(dt);
                    ColumnChecker(dt);

                    bool processed = ProcessConversantReport(dt);

                    return processed;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "SizmekMDXReporting.ProcessMessage:");
            }

            return false;
        }

        private string GetUrlFromDownload(Message message)
        {
            try
            {
                string pattern = @"\b(?:https?://|www\.)\S+\b";
                 // string pattern = @"\b(?:https?://|www\.)\S+\b";

                /*
                 *   \b       -matches a word boundary (spaces, periods..etc)
                 *   (?:      -define the beginning of a group, the ?: specifies not to capture the data within this group.
                 *   https?://  - Match http or https (the '?' after the "s" makes it optional)
                 *   |        -OR
                 *   www\.    -literal string, match www. (the \. means a literal ".")
                 *   )        -end group
                 *   \S+      -match a series of non-whitespace characters.
                 *   \b       -match the closing word boundary.
                 * 
                 */

                Regex r = new Regex(pattern, RegexOptions.IgnoreCase);

                string result = System.Text.Encoding.UTF8.GetString(message.FindFirstPlainTextVersion().Body);
                // Match the regular expression pattern against a text string.
                Match m = r.Match(result);

                Console.WriteLine("Url: {0}", m.Value.Replace("=3D", "="));

                // 3D are the hex digits corresponding to ='s ASCII value (61).
                string url = m.Value.Replace("=3D", "=");
            
                return url;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "GetUrlFromDownload Conversant");
                return "";
            }
        }

        private DataTable ExtractReport(string url)
        {
            try
            {
                //XmlTextReader rssReader = new XmlTextReader(url.ToString());
                //XmlDocument rssDoc = new XmlDocument();

                WebRequest wrGETURL;
                wrGETURL = WebRequest.Create(url);

                Stream objStream;
                var objNew = wrGETURL.GetResponse();
                objStream = wrGETURL.GetResponse().GetResponseStream();
                StreamReader objReader = new StreamReader(objStream, Encoding.UTF8);
                WebResponse wr = wrGETURL.GetResponse();
                Stream receiveStream = wr.GetResponseStream();
                StreamReader reader = new StreamReader(receiveStream, Encoding.UTF8);
                string content = reader.ReadToEnd();
                 
                DataTable dt = new DataTable();

                string[] tableData = content.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                var col = from cl in tableData[0].Split(",".ToCharArray())
                          select new DataColumn(cl);
                dt.Columns.AddRange(col.ToArray());

                (from st in tableData.Skip(1)
                 select dt.Rows.Add(st.Split(",".ToCharArray()))).ToList();

                return dt;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "ExtractReport Conversant");
            }

            return null;
        }

        private Company GetCompany(string subject)
        {
            Company[] companies = (from m in db.Companies where m.IsActive == true select m).ToArray();

            foreach (Company company in companies)
            {
                if (subject.ToLower().ContainsAny(company.Conversant_Keyword.ToLower().Split(',')))
                {
                    Data processor = new Data(company.DatabaseName, company.Login, company.Password, company.Server); 

                    Processor = processor;
                    return company;
                }
            }

            return null;
        }

        /// <summary>
        /// Check if all the columns are created in the database for the reporting table. If the column doesnt exist it will be added.
        /// </summary>
        /// <param name="dt"></param>
        private void ColumnChecker(DataTable dt)
        {
            string[] columnCol = GetColumnsNameCollection();

            foreach (DataColumn dc in dt.Columns)
            {
                if (!columnCol.Contains(dc.ColumnName.ToLower()))
                    AddColumn("dataConversantReportingData", dc.ColumnName);
            }

            if (!dt.Columns.Contains("ModifiedDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                newColumn.DefaultValue = System.DateTime.Now.ToString("d");
                dt.Columns.Add(newColumn);
            }
        }

        private string[] GetColumnsNameCollection()
        {
            string query = "";
            query += "DECLARE @columns varchar(8000) ";
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(8000), COLUMN_NAME) ";
            query += "FROM (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'dataConversantReportingData') as tab ";
            query += "select @columns ";


            DataSet ds = Processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString();

            string[] columnCol = columns.ToLower().Split(',');
            return columnCol;
        }

        private void UpdateDateColumn(DataTable dt)
        {
            if (dt.Columns.Contains("Date"))
                dt.Columns["Date"].ColumnName = "AsOfDate"; 
        }

        /// <summary>
        /// Due to the columns needing to be dynamic this method is designed to add additional column to the table
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        private void AddColumn(string table, string column)
        {
            string query = "ALTER TABLE {0} ADD [{1}] {2}";

            if (column.ToUpper().ContainsAny("* CLICKS", "POST-IMPRESSION CONVERSIONS", "POST-CLICK CONVERSIONS", "TOTAL CONVERSIONS", "VALUE", "IMPRESSIONS DELIVERED"))
                query = string.Format(query, table, column, "int NULL");
            else
                query = string.Format(query, table, column, "[nvarchar](250)");

            Processor.RunQuery(query);
        }

        private void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(String.Format("{0} Rows have been copied.", e.RowsCopied.ToString()));
        }

        /// <summary>
        /// Example showing:
        ///  - how to fetch all messages from a POP3 server
        /// </summary>
        /// <param name="hostname">Hostname of the server. For example: pop3.live.com</param>
        /// <param name="port">Host port to connect to. Normally: 110 for plain POP3, 995 for SSL POP3</param>
        /// <param name="useSsl">Whether or not to use SSL to connect to server</param>
        /// <param name="username">Username of the user on the server</param>
        /// <param name="password">Password of the user on the server</param>
        ///  http://hpop.sourceforge.net/
        /// <returns>All Messages on the POP3 server</returns>
        private void ProcessConversantReport()
        {
            try
            {
                using (Pop3Client client = new Pop3Client())
                {
                    client.Connect(server, 110, false);
                    client.Authenticate(accountname, accountpassword);

                    int messageCount = client.GetMessageCount();

                    List<Message> allMessages = new List<Message>(messageCount);

                    for (int i = messageCount; i > 0; i--)
                    {
                        Console.WriteLine("Processing " + i + " of " + messageCount);

                        Message message = client.GetMessage(i);
                        company = null;
                        if (message.Headers.Subject.Contains("Conversant Report"))
                        {
                            company = GetCompany(message.Headers.Subject.ToString());

                            if (company == null)
                                continue;

                            if (ProcessMessage(message))
                            {
                                client.DeleteMessage(i);
                            }
                            else
                                SolutionHelper.Error.LogError("SizmekMDXReporting.OpenPopHandler.DeleteMessage");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.OpenPopHandler: " + ex.Message);
            }
        }
    }
}

 