﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConversantReporting
{
    class Program
    {
        public static void Main(string[] args)
        {
            RunCustomReport();
        }
        private static void RunCustomReport()
        {
            Console.WriteLine("RunCustomConversantReport : " + System.DateTime.Now);
            DateTime startTime = System.DateTime.Now;

            ConversantReport service = new ConversantReport();
            service.Run();
            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("Total Time : " + endTime.Subtract(startTime).TotalMinutes);
        }
    }
}
