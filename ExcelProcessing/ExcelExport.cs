﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelProcessing
{
    public class ExcelExport
    {
        public static DataTable GetDataTabletXlsSearchFlatExcel(string csv_file_path)
        {
            try
            {
                var connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0};Extended Properties='Excel 8.0;IMEX=1;HDR=NO;TypeGuessRows=0;ImportMixedTypes=Text'", csv_file_path);

                OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + csv_file_path + @";Extended Properties=""Excel 8.0;IMEX=1;HDR=NO;TypeGuessRows=0;ImportMixedTypes=Text""");

                OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Search_Overview_FlatExcel$]", connection);
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);

                DataSet ds = new DataSet();
                ds.Tables.Add("xlsImport", "Excel");
                da.Fill(ds, "xlsImport");

                DataTable dt = ds.Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                string error = "";
            }

            return null;
        }
        public static DataTable GetDataTabletXls(string csv_file_path)
        {
            try
            {
                var connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0};Extended Properties='Excel 8.0;IMEX=1;HDR=NO;TypeGuessRows=0;ImportMixedTypes=Text'", csv_file_path);

                OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + csv_file_path + @";Extended Properties=""Excel 8.0;IMEX=1;HDR=NO;TypeGuessRows=0;ImportMixedTypes=Text""");

                OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Sheet1$]", connection);
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);

                DataSet ds = new DataSet();
                ds.Tables.Add("xlsImport", "Excel");
                da.Fill(ds, "xlsImport");

                DataTable dt = ds.Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                string error = "";
            }

            return null;
        }
    }
}
