﻿using ExcelProcessing;
using SolutionHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhirlygigSolutionHelper;

namespace SizmekMDXReporting.EmailProcessing
{
    public class FileProcessing
    {
        mediaLavidgeEntities1 db = new mediaLavidgeEntities1();

        Company company;

        Data Processor = new Data();

        string csvExportCompleted;
        string csvExportFailed;
        string csvExportWaiting;
        string csvExport;

        bool deleteReport;

        public FileProcessing()
        {
            csvExportCompleted = ConfigurationManager.AppSettings["csvExportCompleted"];
            csvExportFailed = ConfigurationManager.AppSettings["csvExportFailed"];
            csvExportWaiting = ConfigurationManager.AppSettings["csvExportWaiting"];
            csvExport = ConfigurationManager.AppSettings["csvExport"];
        }

        public void RunProcessingFiles()
        {
            try
            {
                DirectoryInfo d = new DirectoryInfo(csvExport);
                FileInfo[] infos = d.GetFiles();

                int idx = 0;

                foreach (FileInfo f in infos.Where(m => m.FullName.ContainsAny("Sizmek Custom Report -", "Sizmek Daily Search -", "Sizmek Uniques - ")).OrderBy(n => n.Length))
                {
                    DataTable dt = null;
                    Result result = new Result();

                    string tableNam = "";
                    deleteReport = false;
                    idx++;

                    try
                    {
                        company = GetCompany(f.Name);

                        if (company == null)
                        {
                            MoveFileForLaterProcessing(idx, f);
                        }
                        else
                        { 
                            if (f.FullName.ContainsAny("Sizmek Custom Report"))
                                tableNam = "dataSizmekDataFeedReportingData";
                            if (f.FullName.ContainsAny("Sizmek Daily Search"))
                                tableNam = "dataSizmekSearchReportingData";
                            if (f.FullName.ContainsAny("Sizmek Uniques"))
                                tableNam = "dataSizmekUniquesReportingData";
                             
                            CsvToDatabase csvToDatabase = new CsvToDatabase(tableNam, f.FullName, Processor);

                            long size = f.Length / 1000;

                            if ((f.Length / 1000) > 50000) // TODO temp
                            {
                                result = ProcessLargeFiles(csvToDatabase, f);

                            }
                            else
                            {
                                dt = GetDataTableFromXls(f.FullName);

                                if (dt == null || dt.Rows.Count == 0 || dt.Columns.Count <= 1)
                                    result.result = false;
                                else
                                {
                                    if (f.FullName.ContainsAny("Sizmek Daily Search"))
                                    {
                                        FormatDataTable(dt);
                                    }

                                    result.result = process.ProcessMessageDataFeed(dt, tableNam);

                                    result.duplicate = process.deleteEmail;
                                }
                            }

                            if (result.result)
                                f.MoveTo(csvExportCompleted + @"\" + f.Name);
                            else if (result.duplicate || deleteReport)
                            {
                                string path = csvExportCompleted + @"\" + f.Name.Replace(f.Extension, " Duplicate ") + System.DateTime.Now.ToString().Replace(":", ".").Replace("/", "") + f.Extension;
                                f.MoveTo(path);
                            }
                            else
                                f.MoveTo(csvExportFailed + @"\" + f.Name);
                        }

                    }
                    catch (Exception ex)
                    {
                        SolutionHelper.Error.LogError("SizmekMDXReporting.FileProcessing.RunProcessingFiles:" + f.Name + " - " + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.FileProcessing.RunProcessingFiles - " + ex.Message);
            }
        }

        private DataTable GetDataTableFromXls(string csv_file_path)
        {
            string sheetName = "Sheet1";

            if(csv_file_path.Contains("Search_Overview"))
                sheetName = "Search_Overview_FlatExcel"; 

            try
            {
                var connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0};Extended Properties='Excel 8.0;IMEX=1;HDR=NO;TypeGuessRows=0;ImportMixedTypes=Text'", csv_file_path);

                OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + csv_file_path + @";Extended Properties=""Excel 8.0;IMEX=1;HDR=NO;TypeGuessRows=0;ImportMixedTypes=Text""");
                 
                OleDbCommand cmd = new OleDbCommand(String.Format("SELECT * FROM [{0}$]", sheetName), connection);
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);


                DataSet ds = new DataSet();
                ds.Tables.Add("xlsImport", "Excel");
                da.Fill(ds, "xlsImport");

                DataTable dt = ds.Tables[0];

                return dt;
            }
            catch (Exception ex)
            {

            }

            return null;
        } 

        private DataTable FormatDataTable(DataTable dt)
        {

            int intRow = 15;

            for (int counter = 0; intRow > 0; intRow--)
            {
                dt.Rows[counter].Delete();
                counter++;
            }

            dt.AcceptChanges();



            foreach (DataColumn col in dt.Columns)
            {
                string colName = dt.Rows[0][col].ToString();

                if (colName == "")
                {
                    dt.Columns.Remove(col);
                    dt.AcceptChanges();
                    break;
                }
                else
                    dt.Columns[col.ColumnName.ToString()].ColumnName = colName.Trim();
            }

            dt.Rows[0].Delete();

            dt.Rows[dt.Rows.Count - 1].Delete();
            dt.Rows[dt.Rows.Count - 2].Delete();
            dt.AcceptChanges();

            return dt;
        }

        private void MoveFileForLaterProcessing(int idx, FileInfo f)
        {
            FileInfo fInfo = new FileInfo(csvExportWaiting + "/" + f.Name);

            if (!fInfo.Exists)
                f.MoveTo(csvExportWaiting + "/" + f.Name.Replace(f.Extension, "").ToString() + f.Extension);
            else
                f.MoveTo(csvExportWaiting + "/" + f.Name.Replace(f.Extension, " Duplicated " + idx.ToString() + f.Extension));
        }

        private Result ProcessLargeFiles(CsvToDatabase csvToDatabase, FileInfo f)
        {
            Result result = new Result();
            result.result = csvToDatabase.Run();
            result.duplicate = csvToDatabase._duplicate;

            return result;
        }

        private void Reset(out bool result, out bool deleteReport)
        {
            company = null;
            Processor = null;

            result = false;
            deleteReport = false;
        }

        private Company GetCompany(string fullName)
        {
            Company[] companys = (from m in db.Companies select m).ToArray();

            foreach (Company company in companys)
            {
                if (fullName.ToLower().ContainsAny(company.Sizemek_Keyword.ToLower().Split(',')))
                {
                    Data processor = new Data(company.DatabaseName, company.Login, company.Password);

                    Processor = processor;
                    return company;
                }
            }

            return null;
        }

        /// <summary>
        /// Move back to processing folder if company name is established
        /// </summary>
        internal void RunWaitingFiles()
        {
            Console.WriteLine("Checking waiting files");
            try
            {
                DirectoryInfo d = new DirectoryInfo(csvExportWaiting);
                FileInfo[] infos = d.GetFiles();


                foreach (
                    FileInfo f in infos.Where(m => m.FullName.ContainsAny(
                    "Sizmek Custom Report -", 
                    "Sizmek Daily Search -",
                    "Sizmek Uniques - ")))
                {
                    try
                    {
                        company = GetCompany(f.Name);

                        if (company == null)
                            continue;
                        else
                            f.MoveTo(csvExport + "/" + f.Name);
                    }
                    catch (Exception ex)
                    {
                        SolutionHelper.Error.LogError("SizmekMDXReporting.FileProcessing.RunProcessingFiles:" + f.Name + " - " + ex.Message);
                    }
                }

            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.FileProcessing.RunWaitingFiles:" + ex.Message);
            }

            Console.WriteLine("Checking waiting files Finished");
        }

        class Result
        {
            public bool result { get; set; }
            public bool duplicate { get; set; }
        }
    }
}
