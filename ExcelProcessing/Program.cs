﻿using SizmekMDXReporting.EmailProcessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 
Please find the Sizmek MDX API reporting credentials for mtripp@lavidge.com below:

Username: mtripp@lavidge.com
Password: Password01
API Key: 5bd8a44e-5971-4475-bfd2-0c542840d8da
 
As a reminder – here is a link to our API documentation - http://platform.mediamind.com/Eyeblaster.MediaMind.API.Doc/

Test advertiser 152256

*/

namespace SizmekMDXReporting
{
    public class Program
    {
        public static void Main(string[] args)
        {  
            RunFileProcessingData();
        }
                  
        private static void RunFileProcessingData()
        {
            Console.WriteLine("RunFileProcessingData : *******************************************" + System.DateTime.Now);
            DateTime startTime = System.DateTime.Now;

            FileProcessing service = new FileProcessing();
            service.RunWaitingFiles();
            service.RunProcessingFiles();
            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("Total Time : " + endTime.Subtract(startTime).TotalMinutes);
        } 
    }
}

