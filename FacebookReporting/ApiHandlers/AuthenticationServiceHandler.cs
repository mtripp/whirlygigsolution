﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace FacebookReporting
{
    /// <summary>
    /// This class is a utility class to handle authentication for tests
    /// 
    ///   ATUrZA0f5IMBANVfaRFuBS46IkvCXTRLDJ83G9GpYuJcybq8UwT8t5p2MFluiQnu2q7SJj6CWYpuZAzewI1X5Vz0JsrSMjwjX8JUvkOsrQ9xxlA3fiyVrmZB7YCstjVWU0lZAypeZCUTNSIiPCQAr0f6TBQ2qML65Qm3ft42SQZDZD
    ///        https://graph.facebook.com/oauth/access_token?client_id=1359742037386371&client_secret=bf9ed67a42f5bfd1c4783e35925d0cb1&grant_type=client_credentials 
    /// 
    /// </summary>
    public class AuthenticationServiceHandler : IDisposable
    {
        public void Run(){}

        public string UserSecurityToken { get; set; } 

        public AuthenticationServiceHandler()
            : this(ConfigurationManager.AppSettings["AppID"],
                   ConfigurationManager.AppSettings["AppSecret"],
                   ConfigurationManager.AppSettings["ApiUrl"] + "access_token?")
        {  
        }

        /// <summary>
        /// Authenticates the user's credentials on the Trade Desk API and creates a session which its 
        /// identifier is kept in the UserSecurityToken property.
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <param name="url"></param>
        public AuthenticationServiceHandler(string clientid, string clientsecret, string url)
        { 
            try
            {
                string _url = string.Format(@"{0}client_id={1}&client_secret={2}&grant_type=client_credentials", url, clientid, clientsecret);

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(_url);
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                Stream stream = resp.GetResponseStream();
                // Pipes the stream to a higher level stream reader with the required encoding format. 
                StreamReader readStream = new StreamReader(stream, Encoding.UTF8);
                UserSecurityToken = readStream.ReadToEnd();
                UserSecurityToken = UserSecurityToken.Replace("access_token=", "");

            }
            catch (Exception ex)
            {

                string ems = ex.Message;
                return;
            }
        }

        /// <summary>
        /// Clears the current session on the MediaMind API.
        /// </summary>
        public void Dispose()
        {
            UserSecurityToken = "";
        }
         
        [DataContract]
        public class Response
        {
            [DataMember(Name = "access_token")]
            public string access_token { get; set; }
        }
    }
}
