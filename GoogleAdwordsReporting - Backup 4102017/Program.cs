﻿using Google.Api.Ads.AdWords.v201605;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;






/// <summary>
/// https://adwords.google.com - Login using imedia@lavidge.com
/// </summary>
namespace GoogleAdwordsReporting
{
    public class Program
    {
        static void Main(string[] args)
        {
            DownloadCriteriaReport download = new DownloadCriteriaReport(); 
            download.RunReport(ReportDefinitionReportType.ADGROUP_PERFORMANCE_REPORT); 
            download.RunReport(ReportDefinitionReportType.AD_PERFORMANCE_REPORT);
        }
    }
}
