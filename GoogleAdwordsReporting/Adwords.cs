﻿using OpenPop.Mime;
using OpenPop.Pop3;
using SolutionHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using WhirlygigSolutionHelper;


namespace GoogleAdwordsReporting
{
    public class Adwords
    {
        AdwordsEntities db = new AdwordsEntities();

        Data Processor = new Data();
        Company company;
        bool debug;

        string server;
        string accountname;
        string accountpassword;
        private const string tableName = "dataAdWordsReportingData";
        private bool deleteEmail;
        private bool reimportData;

        public Adwords()
        {
            debug = bool.Parse(ConfigurationManager.AppSettings["debug"].ToString());
            server = ConfigurationManager.AppSettings["EmailAccountServer"];
            accountname = ConfigurationManager.AppSettings["EmailAccountName"];
            accountpassword = ConfigurationManager.AppSettings["EmailAccountPassword"];
        }


        /// <summary>
        /// Example showing:
        ///  - how to fetch all messages from a POP3 server
        /// </summary>
        /// <param name="hostname">Hostname of the server. For example: pop3.live.com</param>
        /// <param name="port">Host port to connect to. Normally: 110 for plain POP3, 995 for SSL POP3</param>
        /// <param name="useSsl">Whether or not to use SSL to connect to server</param>
        /// <param name="username">Username of the user on the server</param>
        /// <param name="password">Password of the user on the server</param>
        ///  http://hpop.sourceforge.net/
        /// <returns>All Messages on the POP3 server</returns>
        public void ProcessAdwordsReport()
        { 
            try
            {
                restart: // This goto was implemented due to a Pop3Client timeout. Connection to the mail server needs to be reauthenticated between each message
                DateTime startTime = System.DateTime.Now;

                using (Pop3Client client = new Pop3Client())
                {
                    client.Connect(server, 110, false);
                    client.Authenticate(accountname, accountpassword);

                    int messageCount = client.GetMessageCount();

                    List<Message> allMessages = new List<Message>(messageCount);

                    if (messageCount == 0)
                        return;

                    for (int i = messageCount; i > 0; i--)
                    {
                        Console.WriteLine("Processing " + i + " of " + messageCount);

                        Reset();

                        Message message = client.GetMessage(i);

                        if (message.Headers.Subject == null)
                            continue;

                        if (message.Headers.Subject.ContainsAny(ConfigurationManager.AppSettings["EmailSubject"].Split(',')))
                        {
                            bool result = false;

                            result = ProcessData(message);

                            if (result || deleteEmail)
                            {
                                client.DeleteMessage(i);
                                goto restart;
                            }
                        }

                        TimeSpan span = System.DateTime.Now - startTime;

                        if (span.TotalMinutes > 3)
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("Adwords.OpenPopHandler: " + ex.Message.ToString());
            }
        }

        private bool ProcessData(Message message)
        {
            bool result = false;

            Console.WriteLine("Processing: " + message.Headers.Subject.ToString());

            try
            {
                company = GetCompany(message.Headers.Subject.ToString());
                
                if (company == null)   
                    return false;

                if (company.CompanyName == "GOYFF")     // This is due to a corrupt file. The 'Account Id' is partially loaded throughout the file.
                {
                    deleteEmail = true;
                    return false;
                }

                if (company != null)
                {

                    result = ProcessMessage(message);

                    Console.WriteLine("Finished: " + message.Headers.Subject.ToString());

                    return result;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in processing: " + message.Headers.Subject.ToString() + ex.Message.ToString());
                SolutionHelper.Error.LogError(ex, "FacebookReporting.ProcessAnalytics:");
            }

            return result;
        }

        private bool ProcessMessage(Message message)
        {
            try
            {
                List<MessagePart> attachments = message.FindAllAttachments();

                Stream stream = new MemoryStream(attachments[0].Body);

                DataTable dt = new DataTable();

                foreach (MessagePart attachment in attachments)
                { 
                    if (attachments[0].FileName.Contains(".pdf"))
                    {
                        deleteEmail = true;
                        return false;
                    } 
                    if (attachments[0].FileName.Contains(".zip"))
                        stream = stream.ToStreamFromZip();
                    if (attachments[0].FileName.Contains(".tsv"))
                        dt = stream.ToDataTableTsv();
                    else if (attachments[0].FileName.Contains(".csv"))
                        dt = stream.ToDataTableCsv();

                    if (dt.Columns[0].ColumnName.ToString().ToLower().ContainsAny("data was updated last", "refreshing...", "ERROR: AUTHORIZATIONERROR".ToLower()))
                    {
                        deleteEmail = true;
                        return false;
                    } 
                }
                 
                bool processed = ProcessDatatable(dt);

                return processed;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "AdwordsReporting.ProcessMessage:");
            }

            return false;
        }

        public static bool ExportFile(Message msg, string csvExport)
        {
            try
            {
                List<MessagePart> attachments = msg.FindAllAttachments();

                string file = "";
                string filename = "";
                string ext = " " + System.DateTime.Now.ToString().Replace(":", "").Replace("/", "");

                Stream stream = new MemoryStream(attachments[0].Body);

                DataTable dt = new DataTable();

                if (attachments[0].FileName.Contains(".zip"))
                    stream = stream.ToStreamFromZip();
                if (attachments[0].FileName.Contains(".tsv"))
                {
                    filename = attachments[0].FileName.Replace(".tsv", ext + ".tsv");
                    dt = stream.ToDataTableTsv();
                    file = dt.ToDataSetExportTsv();
                }
                else if (attachments[0].FileName.Contains(".csv"))
                {
                    filename = attachments[0].FileName.Replace(".csv", ext + ".csv");
                    dt = stream.ToDataTableCsv();
                    file = dt.ToDataSetExportCsv();
                }

                if (dt != null)
                {
                    File.WriteAllText(csvExport + @"\Adwords Report - " + filename, file.ToString());
                    return true;
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "AdwordsReporting.ExportFile: " + msg.Headers.Subject.ToString());
                throw ex;
            }

            return false;
        }

        public bool ProcessDatatable(DataTable dt)
        {
            bool processed = false;
            DateTime today = System.DateTime.Now.Date;
            DateTime[] reports = GetReportDates(tableName); // TODO: Replace with date checker from Sizkek code

            ColumnChecker(dt, tableName);

            DataSet ds = dt.ToDataSetDateSplit();

            foreach (DataTable datatab in ds.Tables)
            {
                string name = datatab.TableName.ToString();
                DateTime dateTime;

                if (!DateTime.TryParse(name, out dateTime))
                {
                    deleteEmail = true; // The purpose of this is to make sure the email is deleted if the only datatable in the dataset is empty
                    continue;
                }
                else
                    deleteEmail = false;

                DateTime tablename = DateTime.Parse(name);

                if (reports.Contains(tablename))
                {
                    bool comparedata = CompareReportDataAgainstDatabaseData(datatab);

                    if (comparedata)
                    {
                        deleteEmail = true;
                        continue;
                    }
                    else if (reimportData)
                    {
                        DeleteOldData(datatab);
                    }
                    else
                    {
                        deleteEmail = false;
                        return false;
                    }
                }
                else if (tablename == today)
                {
                    continue;
                }
                else
                {
                    deleteEmail = false;
                }

                CleanData(datatab);

                processed = ProcessAdwordsDatatable(datatab, tableName);

                if (!processed)
                    return processed;
            }

            return processed;
        }
        private bool ProcessAdwordsDatatable(DataTable dt, string tableName)
        {
            try
            {
                SqlConnection conn = new SqlConnection(Processor.ConnectionString);

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);

                    bulkCopy.BatchSize = 10000;
                    bulkCopy.BulkCopyTimeout = 500;

                    foreach (DataColumn dc in dt.Columns)
                    {
                        SqlBulkCopyColumnMapping mapping = new SqlBulkCopyColumnMapping(dc.ColumnName, dc.ColumnName);

                        bulkCopy.ColumnMappings.Add(mapping);
                    }

                    bulkCopy.DestinationTableName = tableName;

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 2000;
                    conn.Open();

                    bulkCopy.WriteToServer(dt);

                    return true;
                }
            }
            catch (Exception ex)
                {
                SolutionHelper.Error.LogError(ex, "AdwordsProcessing.DataTableToDatabase:" + Processor.DatabaseName);
                bool bolChecker = DebugHelper.CheckColumnsAgainstDatabase(Processor, dt, tableName);

            }

            return false;
        }

        #region Internal Methods
        private void DeleteOldData(DataTable dt)
        {
            try
            {
                string query = String.Format(@"delete FROM [dbo].[{0}] where AsOfDate = '{1}'", tableName, dt.TableName);

                Processor.RunQuery(query);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         
        private DateTime[] GetReportDates(string tableName)
        {
            List<DateTime> dates = new List<DateTime>();

            string query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[{0}] where [AsOfDate] is not null", tableName);

            DataSet ds = Processor.RunQuery(query);

            if (ds != null) // Add date to list of date
                if (ds.Tables.Count != 0) // Add date to list of date
                    foreach (DataRow dr in ds.Tables[0].Rows)
                        dates.Add(DateTime.Parse(dr[0].ToString())); // Date is a non nullable date type

            return dates.ToArray();
        }

        private void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(String.Format("{0} Rows have been copied.", e.RowsCopied.ToString()));
        }


        /// <summary>
        /// This is to compare the total records and the sum of the impressions against the database. 
        /// </summary>
        /// <param name="datatab"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private bool CompareReportDataAgainstDatabaseData(DataTable datatab)
        {
            try
            {

                string query = String.Format(@"SELECT count(*) Total, sum([Impressions]) SumImp FROM [dbo].[{0}] where AsOfDate =  '{1}'", tableName, datatab.TableName);

                DataSet ds = Processor.RunQuery(query);

                if (ds != null)
                {
                    if (ds.Tables.Count != 0)
                    {
                        int _sumImp = 0;
                        int total = int.Parse(ds.Tables[0].Rows[0]["Total"].ToString());
                        int sumImp = int.Parse(ds.Tables[0].Rows[0]["SumImp"].ToString());

                        foreach (DataRow dr in datatab.Rows)
                        {
                            _sumImp += int.Parse(dr["Impressions"].ToString());
                        }

                        int _total = datatab.Rows.Count;

                        if (total == _total && sumImp == _sumImp)
                            return true;
                        else
                            reimportData = true;
                    }
                }

            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }

        /// <summary>
        /// Check if all the columns are created in the database for the reporting table. If the column doesnt exist it will be added.
        /// </summary>
        /// <param name="dt"></param>
        private void ColumnChecker(DataTable dt, string tableName)
        {
            try
            {
                string[] columnCol = GetColumnsFromDatabase(tableName);

                foreach (DataColumn dc in dt.Columns)
                {
                    //if (dc.ColumnName.Contains("* "))
                    //    dc.ColumnName = dc.ColumnName.Replace("* ", "");

                    dc.ColumnName = dc.ColumnName.ToProperCase();

                    if (dc.ColumnName.ToLower() == "date")
                    {
                        dc.ColumnName = "AsOfDate";
                        continue;
                    }

                    if (!columnCol.Contains(dc.ColumnName.ToLower()))
                    {
                        if (!dc.ColumnName.Contains("Duplicate"))
                            AddColumn(tableName, dc.ColumnName);
                    }
                }

                DateTime today = System.DateTime.Now;

                if (!dt.Columns.Contains("ModifiedDate"))
                {
                    System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                    newColumn.DefaultValue = today.ToString();
                    dt.Columns.Add(newColumn);
                }

                if (!dt.Columns.Contains("AdWordsClientCustomersID") && columnCol.Contains("AdWordsClientCustomersID".ToLower()))
                {
                    System.Data.DataColumn newColumn = new System.Data.DataColumn("AdWordsClientCustomersID", typeof(System.Int64));
                    dt.Columns.Add(newColumn);
                }

            }
            catch (Exception ex)
            {
                bool bolChecker = DebugHelper.CheckColumnsAgainstDatabase(Processor, dt, tableName);
                throw ex;
            }
        }

        private string[] GetColumnsFromDatabase(string tableName)
        {
            string query = "";
            query += "DECLARE @columns varchar(8000) ";
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(8000), COLUMN_NAME) ";
            query += String.Format("FROM (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{0}') as tab ", tableName);
            query += "select @columns ";

            DataSet ds = Processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString();

            string[] columnCol = columns.ToLower().Split(',');

            return columnCol;
        }

        /// <summary>
        /// Clean the data based on the column type define in the AddColumn method 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private DataTable CleanData(DataTable dt)
        {
            string remove = "";

            foreach (DataRow dr in dt.Rows)
                foreach (DataColumn col in dt.Columns)
                    if (col.ColumnName.ToUpper().ContainsAny("CROSS-DEVICE CONVERSIONS", "Video Impressions".ToUpper()))
                        if (String.IsNullOrEmpty(dr[col.ColumnName].ToString()) || dr[col.ColumnName].ToString().ToUpper() == "NULL" || dr[col.ColumnName].ToString().Contains(" --"))
                            dr[col.ColumnName] = 0;

            foreach (DataColumn col in dt.Columns)
                if (col.ColumnName.ToUpper().Contains("DUPLICATE"))
                    remove = col.ColumnName;

            if (remove != "")
            {
                dt.Columns.Remove(remove);
                dt.AcceptChanges();
            }

            if (!dt.Columns.Contains("Account Id"))
                return dt;

            if (!dt.Columns.Contains("AdWordsClientCustomersID"))
                return dt;

            int idx = 0;

            var selectedDb = new AdwordsEntities();

            Data processor = new Data(company.DatabaseName, company.Login, company.Password, company.Server);

            selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

            List<dataAdWordsClientCustomer> ClientCustomerIds = new List<dataAdWordsClientCustomer>();

            try
            {
                // some companies do not need this table due to multiple daily files with with diffent accounts
                // This is to ensure we can determine that we can insert data with the same date as long as the dataset is unique in respect to the account

                ClientCustomerIds = (from m in selectedDb.dataAdWordsClientCustomers select m).ToList<dataAdWordsClientCustomer>();
            }
            catch (Exception)
            { return dt; }


            foreach (DataRow dr in dt.Rows)
            {
                dataAdWordsClientCustomer client = ClientCustomerIds.Where(x => x.ClientCustomerId.Replace("-", "") == dr["Account Id"].ToString()).FirstOrDefault();

                if (client == null)
                {
                    dataAdWordsClientCustomer ad = new dataAdWordsClientCustomer();
                    ad.ClientCustomerId = dr["Account Id"].ToString();
                    ad.ClientCustomerName = dr["Account"].ToString();
                    ad.CompanyId = int.Parse(company.Id.ToString());

                    selectedDb.dataAdWordsClientCustomers.Add(ad);
                    selectedDb.SaveChanges();
                    ClientCustomerIds.Add(ad);

                    client = ad;
                }

                dr["AdWordsClientCustomersID"] = client.ID;
            }

            dt.Columns.Remove("Account Id");

            return dt;
        }

        /// <summary>
        /// Due to the columns needing to be dynamic this method is designed to add additional column to the table
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        private void AddColumn(string table, string column)
        {
            column = column.ToProperCase();

            string query = "ALTER TABLE {0} ADD [{1}] {2}";

            if (column.ToUpper().ContainsAny("CLICKS", "IMPRESSIONS"))
            {
                if (!column.ToUpper().ContainsAny("COST"))
                    query = string.Format(query, table, column, "[int] NULL");
            }

            if (column.ToUpper().ContainsAny("RATE", "COST", "CTR", "CPC", "CONVERSIONS"))
                query = string.Format(query, table, column, "decimal(21, 4) NULL");

            if (column.ToUpper().ContainsAny("TIME"))
                query = string.Format(query, table, column, "datetime NULL");

            if (query == "ALTER TABLE {0} ADD [{1}] {2}")
                query = string.Format(query, table, column, "[nvarchar](250)");

            Processor.RunQuery(query);
        }

        private Company GetCompany(string _companyString)
        {
            Company[] companys = (from m in db.Companies where m.Adwords_Keyword != null && m.IsActive == true select m).ToArray();

            foreach (Company company in companys)
            {
                if (company.Adwords_Keyword == null)
                    continue;

                if (_companyString.ToLower().ContainsAny(company.Adwords_Keyword.ToLower().Split(',')))
                {
                    Data processor = new Data();
                    processor.DatabaseName = company.DatabaseName;
                    processor.UserName = company.Login;
                    processor.Password = company.Password;
                    processor.Server = company.Server;

                    Processor = processor;
                    return company;
                }
            }

            return null;
        }

        private void Reset()
        {
            company = null;
            Processor = null;
            deleteEmail = false;
        }
        #endregion

    }
}
