﻿
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.Util.Reports;
using Google.Api.Ads.AdWords.v201702;
using Google.Api.Ads.Common.Util.Reports;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using SolutionHelper;
using System.Data.SqlClient;
using System.Configuration;
using WhirlygigSolutionHelper;
using Google.Api.Ads.Common.Lib;
using GoogleAdwordsReporting;

namespace GoogleAdwordsReporting
{

    public class DownloadCriteriaReport
    {
        AdwordsEntities db = new AdwordsEntities();

        Data Processor = new Data();
        Company company;
        bool debug;

        public DownloadCriteriaReport()
        {
            debug = bool.Parse(ConfigurationManager.AppSettings["debug"].ToString());
        }


        /// <summary>
        /// Run report based on the report type
        /// </summary>
        /// <param name="reportDefinitionReportType"></param>
        public void RunReport(ReportDefinitionReportType reportDefinitionReportType)
        {
            try
            {
                List<string> fieldsDefinition = new List<string>();
                string reportName = "";

                if (reportDefinitionReportType == ReportDefinitionReportType.ADGROUP_PERFORMANCE_REPORT)
                {
                    reportName = "ADGROUP_PERFORMANCE_REPORT";
                    fieldsDefinition = (from m in db.dataAdWordsCampaignColumns select m.Column_Names).ToList();
                }
                else if (reportDefinitionReportType == ReportDefinitionReportType.AD_PERFORMANCE_REPORT)
                {
                    reportName = "AD_PERFORMANCE_REPORT";
                    fieldsDefinition = (from m in db.dataAdWordsCampaignSegmentsColumns select m.Column_Names).ToList();
                }

                dataAdWordsClientCustomer[] clients = (from m in db.dataAdWordsClientCustomers where m.CompanyId != null select m).OrderByDescending(m => m.ClientCustomerName).ToArray();

                foreach (dataAdWordsClientCustomer client in clients)
                {
                    company = GetCompany(client);



                    if (company != null)
                        Execute(reportDefinitionReportType, fieldsDefinition.ToArray(), reportName, client);
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "DownloadCriteriaReport.RunReport:" + reportDefinitionReportType.ToString());
            }
        }

        /// <summary>
        /// Get company based on the client and where or not the client has been linked to a company in the lavidge database.
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        private Company GetCompany(dataAdWordsClientCustomer client)
        {
            if (client.CompanyId != null)
            {
                Company com = (from m in db.Companies select m).Where(x => x.Id == client.CompanyId).FirstOrDefault();

                if (com != null)
                {
                    Data processor = new Data(com.DatabaseName, com.Login, com.Password,com.Server);
                    Processor = processor;
                    return com;
                }
            }
            return null;
        }

        /// <summary>
        /// Runs the code against SDK.
        /// </summary>
        /// <param name="user">The AdWords user.</param>
        /// <param name="fileName">The file to which the report is downloaded.
        /// </param>                                                                                                              
        private void Execute(ReportDefinitionReportType reportDefinitionReportType, string[] fieldsDefinition, string reportName, dataAdWordsClientCustomer Client)
        {


           // TestNewApi();
            

            CampaignService camp = new CampaignService();

            DateTime[] reports = GetReportDates(reportDefinitionReportType, Client);
            DateTime now = System.DateTime.Now.AddDays(-1);
            DateTime today = System.DateTime.Now.Date;
            DateTime date = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
            DateTime StartDate = DateTime.Parse("2/5/2017");//  date.AddDays(-45);  DateTime.Parse("7/1/2016");
            DateTime EndDate = date; // DateTime.Parse("9/28/2016");    


            if (debug)
            {
                if (company.Id ==3) // Target specific company
                    now = now;
                else
                    now = now;// return;
            }

            /*    LOOP THROUGH LOGIC */
            for (date = StartDate; date.Date <= EndDate.Date; date = date.AddDays(1))
            {
                if (reports.Contains(date))
                    continue;

                Console.WriteLine(Client.ClientCustomerName + " - " + date.ToString());

                ReportDefinition definition = new ReportDefinition()
                {
                    reportName = reportName,
                    reportType = ReportDefinitionReportType.CRITERIA_PERFORMANCE_REPORT,
                    downloadFormat = DownloadFormat.CSV,
                    dateRangeType = ReportDefinitionDateRangeType.CUSTOM_DATE,
                    //dateRangeType = ReportDefinitionDateRangeType.ALL_TIME,    // USE TO GET ALL HISTORICAL DATA - COMMENT dateRange parameter from selector and get rid of the Break at the end

                    selector = new Selector()
                    {
                        fields = fieldsDefinition,
                        dateRange = new DateRange { min = date.ToString(@"yyyyMMdd"), max = date.ToString(@"yyyyMMdd") },
                        predicates = new Predicate[] { Predicate.In("Status", new string[] { "ENABLED", "PAUSED" }) }
                    },
                };

                if (reportDefinitionReportType == ReportDefinitionReportType.ADGROUP_PERFORMANCE_REPORT)
                {
                    definition.selector.predicates = null;
                    definition.reportType = ReportDefinitionReportType.ADGROUP_PERFORMANCE_REPORT;
                }


                AdWordsUser user = new AdWordsUser();
                // Optional: You can also skip the report headers, column headers and
                // report summary etc. to make the report parsing simpler.
                (user.Config as AdWordsAppConfig).SkipColumnHeader = false;
                (user.Config as AdWordsAppConfig).SkipReportHeader = true;
                (user.Config as AdWordsAppConfig).SkipReportSummary = true;

                (user.Config as AdWordsAppConfig).ClientCustomerId = Client.ClientCustomerId; 

                try
                {
                    ReportUtilities utilities = new ReportUtilities(user,"v201702", definition); 
                    //     utilities.User.OAuthProvider.ClientId = "412898024701-k06iaibuvr9da6tgl20k2mqpq7725hv9.apps.googleusercontent.com";
                    //       utilities.User.OAuthProvider.ClientSecret = "aysc5QHNq0iLTBsVhiftmfbI";
                    //     utilities.User.OAuthProvider. = "1/yLVf78YVKxa9YJgBYNDeO_nvoAZCjzOu0_vjgjTOQ8U";
                      
                    using (ReportResponse response = utilities.GetResponse())
                    {
                        Stream stream = response.Stream;

                        DataSet ds = ConvertStreamToDataSet(stream, Client); // Conversions are happening here   TODO: separate responcibilities

                        if (ds == null)
                        {

                            Console.Write(": No results");
                            continue;
                        }

                        foreach (DataTable dt in ds.Tables)
                        {
                            DateTime tablename = DateTime.Parse(dt.TableName.ToString());

                            if (reports.Contains(tablename) || tablename == today)
                                continue;

                            if (reportDefinitionReportType == ReportDefinitionReportType.ADGROUP_PERFORMANCE_REPORT)
                            {
                                BulkConvertAdgroupPerformanceReport(dt, Client);
                            }
                            else if (reportDefinitionReportType == ReportDefinitionReportType.AD_PERFORMANCE_REPORT)
                            {
                                BulkConvertAdPerformanceReport(dt, Client);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SolutionHelper.Error.LogError(ex, "GoogleAdwordsReporting.Execute: " + reportName + " - " + Client.ClientCustomerName);
                }
                // break;
            }
        }

        private void TestNewApi()
        {


            ReportDefinition definition = new ReportDefinition()
            {
                reportName = "Last 7 days CRITERIA_PERFORMANCE_REPORT",
                reportType = ReportDefinitionReportType.CRITERIA_PERFORMANCE_REPORT,
                downloadFormat = DownloadFormat.GZIPPED_CSV,
                dateRangeType = ReportDefinitionDateRangeType.LAST_7_DAYS,

                selector = new Selector()
                {
                    fields = new string[] {"CampaignId", "AdGroupId", "Id", "CriteriaType", "Criteria",
              "FinalUrls", "Clicks", "Impressions", "Cost"},
                    predicates = new Predicate[] {
            Predicate.In("Status", new string[] {"ENABLED", "PAUSED"})
          }
                },
            };

            AdWordsUser user = new AdWordsUser();
            // Optional: You can also skip the report headers, column headers and
            // report summary etc. to make the report parsing simpler.
            (user.Config as AdWordsAppConfig).SkipColumnHeader = false;
            (user.Config as AdWordsAppConfig).SkipReportHeader = true;
            (user.Config as AdWordsAppConfig).SkipReportSummary = true;

            (user.Config as AdWordsAppConfig).ClientCustomerId = "132-630-0588";

            // Optional: Include zero impression rows.
            (user.Config as AdWordsAppConfig).IncludeZeroImpressions = true;

            // Optional: You can also skip the report headers, column headers and
            // report summary etc. to make the report parsing simpler.
            // (user.Config as AdWordsAppConfig).SkipColumnHeader = true;
            // (user.Config as AdWordsAppConfig).SkipReportHeader = true;
            // (user.Config as AdWordsAppConfig).SkipReportSummary = true;

            string filePath = "C:/" + Path.DirectorySeparatorChar + "fdsafdasfdsasdfdsa";

            try
            {
                ReportUtilities utilities = new ReportUtilities(user, "v201702", definition);
                using (ReportResponse response = utilities.GetResponse())
                {
                    response.Save(filePath);
                }
                Console.WriteLine("Report was downloaded to '{0}'.", filePath);
            }
            catch (Exception e)
            {
                throw new System.ApplicationException("Failed to download report.", e);
            }
        }

        private DataSet ConvertStreamToDataSet(Stream stream, dataAdWordsClientCustomer client)
        {
            DataTable dt = stream.ToDataTableCsv();

            if (dt == null || dt.Rows.Count == 0)
                return null;

            dt = ColumnChecker(dt, client);

            DataSet des = dt.ToDataSetDateSplit();

            return des;
        }

        private DateTime[] GetReportDates(ReportDefinitionReportType reportDefinitionReportType, dataAdWordsClientCustomer client)
        {
            List<DateTime> dates = new List<DateTime>();

            string query = "";

            if (reportDefinitionReportType == ReportDefinitionReportType.ADGROUP_PERFORMANCE_REPORT)
                query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[dataAdwordsReportingdata] where [AdWordsClientCustomersID] = '{0}'", client.ID);
            else if (reportDefinitionReportType == ReportDefinitionReportType.AD_PERFORMANCE_REPORT)
                query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[dataAdWordsCampaignSegments] where [AdWordsClientCustomersID] = '{0}'", client.ID);

            DataSet ds = Processor.RunQuery(query);

            if (ds != null) // Add date to list of date
                if (ds.Tables.Count != 0) // Add date to list of date
                    foreach (DataRow dr in ds.Tables[0].Rows)
                        dates.Add(DateTime.Parse(dr[0].ToString())); // Date is a non nullable date type

            return dates.ToArray();
        }
         
        private void BulkConvertAdgroupPerformanceReport(DataTable dt, dataAdWordsClientCustomer Client)
        {

            try
            {
                SqlConnection conn = new SqlConnection(Processor.ConnectionString);

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);

                    bulkCopy.BatchSize = 20000;

                    bulkCopy.BulkCopyTimeout = 500;

                    #region Mappings Region
                    SqlBulkCopyColumnMapping mapping1 = new SqlBulkCopyColumnMapping("AD GROUP ID", "AdGroupID");
                    SqlBulkCopyColumnMapping mapping2 = new SqlBulkCopyColumnMapping("Campaign Id", "CampaignID");
                    SqlBulkCopyColumnMapping mapping3 = new SqlBulkCopyColumnMapping("ACCOUNT", "AccountName");
                    SqlBulkCopyColumnMapping mapping5 = new SqlBulkCopyColumnMapping("CAMPAIGN", "CampaignName");
                    SqlBulkCopyColumnMapping mapping4 = new SqlBulkCopyColumnMapping("AD GROUP", "AdGroupName");
                    SqlBulkCopyColumnMapping mapping6 = new SqlBulkCopyColumnMapping("CLICKS", "Clicks");
                    SqlBulkCopyColumnMapping mapping7 = new SqlBulkCopyColumnMapping("IMPRESSIONS", "Impressions");
                    SqlBulkCopyColumnMapping mapping71 = new SqlBulkCopyColumnMapping("COST", "Cost");
                    SqlBulkCopyColumnMapping mapping72 = new SqlBulkCopyColumnMapping("Cost Orig.", "Cost Orig.");
                    SqlBulkCopyColumnMapping mapping8 = new SqlBulkCopyColumnMapping("AVG. POSITION", "AveragePosition");
                    SqlBulkCopyColumnMapping mapping10 = new SqlBulkCopyColumnMapping("ALL CONV.", "AllConversions");
                    SqlBulkCopyColumnMapping mapping11 = new SqlBulkCopyColumnMapping("CROSS-DEVICE CONV.", "CrossDeviceConversions");
                    SqlBulkCopyColumnMapping mapping13 = new SqlBulkCopyColumnMapping("VIEW-THROUGH CONV.", "ViewThroughConversions");
                    SqlBulkCopyColumnMapping mapping14 = new SqlBulkCopyColumnMapping("DEVICE", "Device");
                    SqlBulkCopyColumnMapping mapping15 = new SqlBulkCopyColumnMapping("AsOfDate", "AsOfDate");
                    SqlBulkCopyColumnMapping mapping16 = new SqlBulkCopyColumnMapping("ModifiedDate", "ModifiedDate");
                    SqlBulkCopyColumnMapping mapping17 = new SqlBulkCopyColumnMapping("AdWordsClientCustomersID", "AdWordsClientCustomersID");

                    bulkCopy.ColumnMappings.Add(mapping1);
                    bulkCopy.ColumnMappings.Add(mapping2);
                    bulkCopy.ColumnMappings.Add(mapping3);
                    bulkCopy.ColumnMappings.Add(mapping4);
                    bulkCopy.ColumnMappings.Add(mapping5);
                    bulkCopy.ColumnMappings.Add(mapping6);
                    bulkCopy.ColumnMappings.Add(mapping7);
                    bulkCopy.ColumnMappings.Add(mapping71);
                    bulkCopy.ColumnMappings.Add(mapping8);
                    bulkCopy.ColumnMappings.Add(mapping10);
                    bulkCopy.ColumnMappings.Add(mapping11);
                    bulkCopy.ColumnMappings.Add(mapping13);
                    bulkCopy.ColumnMappings.Add(mapping14);
                    bulkCopy.ColumnMappings.Add(mapping15);
                    bulkCopy.ColumnMappings.Add(mapping16);
                    bulkCopy.ColumnMappings.Add(mapping17);
                    bulkCopy.ColumnMappings.Add(mapping72);
                    #endregion

                    bulkCopy.DestinationTableName = "dataAdWordsReportingData";

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 20000;
                    conn.Open();

                    bulkCopy.WriteToServer(dt);
                    Console.WriteLine(String.Format("{0} Rows have been copied.", dt.Rows.Count));
                }
            }

            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "GoogleAdwordsReporting.BulkConvertAdgroupPerformanceReport: " + Client.ClientCustomerName);
            }
        }

        private DataTable ColumnChecker(DataTable dt, dataAdWordsClientCustomer Client)
        {

            if (!dt.Columns.Contains("ModifiedDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                newColumn.DefaultValue = System.DateTime.Now;
                dt.Columns.Add(newColumn);

            }

            if (!dt.Columns.Contains("AdWordsClientCustomersID"))
            {
                System.Data.DataColumn newColumn2 = new System.Data.DataColumn("AdWordsClientCustomersID", typeof(System.Int64));
                newColumn2.DefaultValue = Client.ID;
                dt.Columns.Add(newColumn2);
            }

            foreach (DataColumn dc in dt.Columns)
            {
                if (dc.ColumnName.ToLower() == "date" || dc.ColumnName.ToLower() == "day")
                {
                    dc.ColumnName = "AsOfDate";
                    continue;
                }
            }

            if (dt.Columns.Contains("COST"))
            {
                if (!dt.Columns.Contains("Cost Orig."))
                {
                    System.Data.DataColumn newColumn = new System.Data.DataColumn("Cost Orig.", typeof(Decimal));
                    newColumn.DefaultValue = 0;
                    dt.Columns.Add(newColumn);
                }

                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["COST"] == null || dr["COST"].ToString() == "" || dr["COST"].ToString() == "0")
                        dr["COST"] = "0";
                    else
                    {
                        dr["COST Orig."] = dr["COST"];
                        dr["COST"] = decimal.Parse(dr["COST"].ToString()) / 1000000;
                    }
                }
            }
            return dt;
        }

        private void BulkConvertAdPerformanceReport(DataTable dt, dataAdWordsClientCustomer Client)
        {
            try
            {
                Company com = (from m in db.Companies select m).Where(x => x.Id == Client.CompanyId).FirstOrDefault();

                if (com == null)
                    throw new System.InvalidOperationException("This company is not setup.");

                Data processor = new Data(com.DatabaseName, com.Login, com.Password,com.Server);

                SqlConnection conn = new SqlConnection(processor.ConnectionString);


                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);

                    bulkCopy.BatchSize = 20000;
                    bulkCopy.BulkCopyTimeout = 500;


                    #region Mappings Region

                    SqlBulkCopyColumnMapping mapping1 = new SqlBulkCopyColumnMapping("AD GROUP ID", "AdGroupID");
                    SqlBulkCopyColumnMapping mapping2 = new SqlBulkCopyColumnMapping("CAMPAIGN ID", "CampaignID");
                    SqlBulkCopyColumnMapping mapping3 = new SqlBulkCopyColumnMapping("CLICK TYPE", "ClickType");
                    SqlBulkCopyColumnMapping mapping4 = new SqlBulkCopyColumnMapping("CONVERSION CATEGORY", "ConversionCategoryName");
                    SqlBulkCopyColumnMapping mapping5 = new SqlBulkCopyColumnMapping("CAMPAIGN", "Campaign");
                    SqlBulkCopyColumnMapping mapping6 = new SqlBulkCopyColumnMapping("CONVERSION NAME", "ConversionTypeName");
                    SqlBulkCopyColumnMapping mapping7 = new SqlBulkCopyColumnMapping("KEYWORD / PLACEMENT", "Criteria");
                    SqlBulkCopyColumnMapping mapping8 = new SqlBulkCopyColumnMapping("CRITERIA TYPE", "CriteriaType");
                    SqlBulkCopyColumnMapping mapping9 = new SqlBulkCopyColumnMapping("AsOfDate", "AsOfDate");
                    SqlBulkCopyColumnMapping mapping10 = new SqlBulkCopyColumnMapping("KEYWORD ID", "KeywordID");
                    SqlBulkCopyColumnMapping mapping11 = new SqlBulkCopyColumnMapping("QUALITY SCORE", "QualityScore");
                    SqlBulkCopyColumnMapping mapping12 = new SqlBulkCopyColumnMapping("ModifiedDate", "ModifiedDate");
                    SqlBulkCopyColumnMapping mapping13 = new SqlBulkCopyColumnMapping("AdWordsClientCustomersID", "AdWordsClientCustomersID");

                    bulkCopy.ColumnMappings.Add(mapping1);
                    bulkCopy.ColumnMappings.Add(mapping2);
                    bulkCopy.ColumnMappings.Add(mapping3);
                    bulkCopy.ColumnMappings.Add(mapping4);
                    bulkCopy.ColumnMappings.Add(mapping5);
                    bulkCopy.ColumnMappings.Add(mapping6);
                    bulkCopy.ColumnMappings.Add(mapping7);
                    bulkCopy.ColumnMappings.Add(mapping8);
                    bulkCopy.ColumnMappings.Add(mapping9);
                    bulkCopy.ColumnMappings.Add(mapping10);
                    bulkCopy.ColumnMappings.Add(mapping11);
                    bulkCopy.ColumnMappings.Add(mapping12);
                    bulkCopy.ColumnMappings.Add(mapping13);

                    #endregion

                    bulkCopy.DestinationTableName = "dataAdWordsCampaignSegments";

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 20000;
                    conn.Open();

                    bulkCopy.WriteToServer(dt);
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "GoogleAdwordsReporting.BulkConvertAdPerformanceReport: " + Client.ClientCustomerName);
            }
        }

        private void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(String.Format("{0} Rows have been copied.", e.RowsCopied.ToString()));
        }
















        /// <summary>
        /// Does the OAuth2 authorization for installed applications.
        /// </summary>
        /// <param name="user">The AdWords user.</param>
        private static void DoAuth2Authorization(AdWordsUser user)
        {
            // Since we are using a console application, set the callback url to null.
            user.Config.OAuth2RedirectUri = null;
            AdsOAuthProviderForApplications oAuth2Provider =
                (user.OAuthProvider as AdsOAuthProviderForApplications);
            // Get the authorization url.
            string authorizationUrl = oAuth2Provider.GetAuthorizationUrl();
            Console.WriteLine("Open a fresh web browser and navigate to \n\n{0}\n\n. You will be " +
                "prompted to login and then authorize this application to make calls to the " +
                "AdWords API. Once approved, you will be presented with an authorization code.",
                authorizationUrl);

            // Accept the OAuth2 authorization code from the user.
            Console.Write("Enter the authorization code :");
            string authorizationCode = Console.ReadLine();

            // Fetch the access and refresh tokens.
            oAuth2Provider.FetchAccessAndRefreshTokens(authorizationCode);
        }
    }
}
