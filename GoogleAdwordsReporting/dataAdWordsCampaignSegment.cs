//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GoogleAdwordsReporting
{
    using System;
    using System.Collections.Generic;
    
    public partial class dataAdWordsCampaignSegment
    {
        public long ID { get; set; }
        public Nullable<long> CampaignID { get; set; }
        public Nullable<long> AdGroupID { get; set; }
        public Nullable<long> KeywordID { get; set; }
        public string Campaign { get; set; }
        public string QualityScore { get; set; }
        public string ConversionCategoryName { get; set; }
        public string ConversionTypeName { get; set; }
        public string ClickType { get; set; }
        public Nullable<System.DateTime> AsOfDate { get; set; }
        public string CriteriaType { get; set; }
        public string Criteria { get; set; }
        public Nullable<int> AdWordsClientCustomersID { get; set; }
    }
}
