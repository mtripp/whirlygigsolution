//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GoogleAdwordsReporting
{
    using System;
    using System.Collections.Generic;
    
    public partial class dataAdWordsClientCustomer
    {
        public int ID { get; set; }
        public string ClientCustomerId { get; set; }
        public string ClientCustomerName { get; set; }
        public Nullable<int> CompanyId { get; set; }
    }
}
