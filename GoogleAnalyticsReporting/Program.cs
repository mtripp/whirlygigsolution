﻿using System;
using System.Collections.Generic;
using System.Linq;  
using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;
using Google.Apis.Services;
using Google.Apis.Auth.OAuth2;
using System.Threading;
using Google.Apis.Util.Store;
using System.Configuration;
using System.Data; 
using System.Data.SqlClient; 

namespace GoogleAnalyticsReporting
{
    public class Program
    {
        WhirlygigEntities db = new WhirlygigEntities();

        public void Process()
        {
            string analyticsClientId = ConfigurationManager.AppSettings["AnalyticsClientId"].ToString();
            string analyticsClientSecret = ConfigurationManager.AppSettings["AnalyticsClientSecret"].ToString();

            string[] AccountIds = (from m in db.dataAnalyticsProfileLists select m.AccountId).ToArray();

            if (AccountIds.Count() == 0)
                return;
                
            UserCredential credential;

            credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                         new ClientSecrets { ClientId = analyticsClientId, ClientSecret = analyticsClientSecret  },
                         new[] { AnalyticsService.Scope.AnalyticsReadonly },
                         "user",
                         CancellationToken.None,
                         new FileDataStore("Analytics.Auth.Store")).Result;

            AnalyticsService service = new AnalyticsService(
                               new BaseClientService.Initializer()
                               {
                                   HttpClientInitializer = credential,
                                   ApplicationName = "Analytics API sample",
                               });
              
            ManagementResource.AccountsResource.ListRequest AccountListRequest = service.Management.Accounts.List();

            Accounts AccountList = AccountListRequest.Execute();

            for (var i = 0; i <= AccountList.Items.Count() - 1; i++)
            {
                Account account = AccountList.Items[i];

                ManagementResource.WebpropertiesResource.ListRequest WebPropertyListRequest = service.Management.Webproperties.List(account.Id);
                Webproperties WebPropertyList = WebPropertyListRequest.Execute();

                Console.WriteLine("Processing: " + account.Name + " " + i.ToString() + " of " + AccountList.Items.Count());

                for (var g = 0; g <= WebPropertyList.Items.Count() - 1; g++)
                { 
                    // Check if the AccountId is stored in the database
                    if (AccountIds.Contains(WebPropertyList.Items[g].Id) || true) // TODO: Test only  || true)
                    {
                        Webproperty webProperty = WebPropertyList.Items[g];
                        Console.WriteLine("Processing: " + account.Name + " - " + webProperty.Name + " " + g.ToString() + " of " + WebPropertyList.Items.Count());
                         
                        ManagementResource.ProfilesResource.ListRequest ProfileListRequest = service.Management.Profiles.List(account.Id, webProperty.Id);
                        Profiles ProfileList = ProfileListRequest.Execute();

                        /// List of profiles. Pulling first for brevity           
                        Profile profile = ProfileList.Items[0];

                        ManagementResource.GoalsResource.ListRequest GoalListRequest = service.Management.Goals.List(account.Id, webProperty.Id, profile.Id);
                        Goals GoalList = GoalListRequest.Execute();

                        //Now you have a list of goals for that profile.    If you are going to be querying goal metrics from the core reporting API you will need to know the goal number in order to query them out.

                        //example:  You can’t query the metric ga:goalXXStarts   directly you will need to replace the XX with the number of the goal you got back from the Goals.List

                        //Segments List
                        ManagementResource.SegmentsResource.ListRequest SegmentListRequest = service.Management.Segments.List();
                        Segments SegmentList = SegmentListRequest.Execute();

                        List<string> SegmentNameList = new List<string>();

                        // Build list of Segment Names
                        for (int seg = 0; seg < SegmentList.Items.Count(); seg++)
                            SegmentNameList.Add(SegmentList.Items[seg].Name.ToString());

                        //Segments arnt attached to account in anyway they are attached to the current authenticated user.  So you don’t need to send any account information with the query.

                        //You can also manage users with the Managment API but in order to do so you will need to add another scope to your OAuth connection above.
                        //AnalyticsService.Scope.AnalyticsReadonly,AnalyticsService.Scope.AnalyticsManageUsers
                         
                        string analyticsProfileMetrics = ConfigurationManager.AppSettings["AnalyticsProfileMetrics"].ToString();
                        string analyticsProfileDimensions = ConfigurationManager.AppSettings["AnalyticsProfileDimensions"].ToString();
                         
                        string startdate = System.DateTime.Now.AddDays(-5).ToString("yyyy-MM-dd");
                        string enddate = System.DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");

                        DataResource.GaResource.GetRequest request = service.Data.Ga.Get("ga:" + profile.Id, startdate, enddate, analyticsProfileMetrics);
                        request.Dimensions = analyticsProfileDimensions;
                        request.QuotaUser = "MyQuotaUser";
                        request.MaxResults = 100000;
                        //request.Segment = SegmentList.Items[47].SegmentId.ToString();
                         
                        int totalResults = 0;   // holder for the totalResults that the query will return
                        int rowcnt = 0;         // Holds the row we are curently on.
                         
                        do
                        {
                            try
                            {
                                GaData list = request.Execute();  // Make the request

                                DataTable dt = ExtractDataTable(list);

                                if (dt != null)
                                    ProcessGoogleAnalyticsReport(dt); // ConvertToAnalyticsData(dt, request.Segment);
                                 
                                string a = "";
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("An error occurred: " + e.Message);
                                totalResults = 0;
                            }
                        } while (request.StartIndex <= totalResults);

                    }
                }
            } 
        } 
          
        private static DataTable ExtractDataTable(GaData list)
        {
            DataTable dt = new DataTable();

            // Create columns for datatable
            foreach (var header in list.ColumnHeaders)
                dt.Columns.Add(header.Name.ToString().Replace("ga:", "").ToLower());


            if (list.Rows == null)
                return null;

            // Insert data from GaData
            foreach (var row in list.Rows)
            {
                // Create a new row for datatable
                System.Data.DataRow dr = dt.NewRow();

                // Load data into row
                for (int idx = 0; idx < row.Count(); idx++)
                    dr[idx] = row[idx];

                // Add row to the datatable
                dt.Rows.Add(dr);
            }
            return dt;
        }

        private void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(String.Format("{0} Rows have been copied.", e.RowsCopied.ToString()));
        }

        /// <summary>
        /// Convert datatable to Database
        /// </summary>
        /// <param name="dt"></param>
        private bool ProcessGoogleAnalyticsReport(DataTable dt)
        {
            try
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["WhirlygigDatabase"]);

                AddConvertColumns(dt);

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);

                    bulkCopy.BatchSize = 10000;
                    bulkCopy.BulkCopyTimeout = 500;

                    foreach (DataColumn dc in dt.Columns)
                    {
                        SqlBulkCopyColumnMapping mapping = new SqlBulkCopyColumnMapping(dc.ColumnName, dc.ColumnName);

                        bulkCopy.ColumnMappings.Add(mapping);
                    }

                    bulkCopy.DestinationTableName = "dataAnalyticsData";

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 200;
                    conn.Open();

                    bulkCopy.WriteToServer(dt);

                    return true;
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "GoogleAnalyticsReporting.ProcessAnalyticsReport:");
            }

            return false;
        }

        private static void AddConvertColumns(DataTable dt)
        {
            if (!dt.Columns.Contains("modifieddate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("modifieddate", typeof(System.String));
                newColumn.DefaultValue = System.DateTime.Now.ToString();
                dt.Columns.Add(newColumn);
            }

            foreach (DataRow row in dt.Rows)
            {
                DateTime ddt = DateTime.ParseExact(row["date"].ToString(), "yyyyMMdd",
                              System.Globalization.CultureInfo.InvariantCulture);

                row["date"] = ddt.ToShortDateString();
            }
        }
    }
}
