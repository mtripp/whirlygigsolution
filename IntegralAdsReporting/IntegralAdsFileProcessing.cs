﻿using SolutionHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhirlygigSolutionHelper;

namespace IntegralAdsReporting
{
    public class IntegralAdsFileProcessing
    {
        mediaIntegralAdsEntities db = new mediaIntegralAdsEntities();

        Company company;

        Data Processor = new Data();

        string csvExportCompleted;
        string csvExportFailed;
        string csvExportWaiting;
        string csvExport;

        bool deleteReport;

        public IntegralAdsFileProcessing()
        {
            csvExportCompleted = ConfigurationManager.AppSettings["csvExportCompleted"];
            csvExportFailed = ConfigurationManager.AppSettings["csvExportFailed"];
            csvExportWaiting = ConfigurationManager.AppSettings["csvExportWaiting"];
            csvExport = ConfigurationManager.AppSettings["csvExport"];
        }

        public bool ExportToWaiting(DataTable dt, string fileName )
        {
            try
            {
                string file = "";
 
                file = dt.ToDataSetExportCsv();

                File.WriteAllText(csvExportWaiting + @"\Intergral Ads Report - " + fileName, file.ToString());
                return true;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.ProcessDataFeed.ToDataTableCsv Big File" + ex.Message);
                return false;
            }
        }

        public void Process()
        {
            try
            {
                DirectoryInfo d = new DirectoryInfo(csvExport);
                FileInfo[] infos = d.GetFiles();

                foreach (FileInfo f in infos.Where(m => m.FullName.Contains("Sizmek Custom Report -")).OrderBy(n => n.Length))
                {
                    try
                    {
                        //company = GetCompany(f.Name);
                        //Console.WriteLine(f.Name);

                        //if (company == null)
                        //{
                        //    f.MoveTo(csvExportWaiting + "/" + f.Name);
                        //    continue;
                        //}
                         

                        //DataTable dt = null;
                          
                        //dt = CsvToDatabase.GetDataTabletFromCSVTSVFile(f.FullName);

                        //if (dt == null || dt.Rows.Count == 0 || dt.Columns.Count <= 1)
                        //    result.result = false;
                        //else
                        //{
                        //    result.result = process.ProcessMessageDataFeed(dt);
                        //    result.duplicate = process.deleteEmail;
                        //} 

                        //if (result.result)
                        //    f.MoveTo(csvExportCompleted + "/" + f.Name);
                        //else if (result.duplicate || deleteReport)
                        //    f.MoveTo(csvExportCompleted + "/" + f.Name.Replace(f.Extension, " Duplicate").ToString() + f.Extension);
                        //else
                        //    f.MoveTo(csvExportFailed + "/" + f.Name);
                    }
                    catch (Exception ex)
                    {
                        SolutionHelper.Error.LogError("SizmekMDXReporting.FileProcessing.RunProcessingFiles:" + f.Name + " - " + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.FileProcessing.RunProcessingFiles - " + ex.Message);
            }
        }

        private Company GetCompany(string fileName)
        {
            Company[] companies = (from m in db.Companies where m.IntegralAds_Keyword != null select m).ToArray();

            foreach (Company company in companies)
            {
                if (fileName.ToLower().ContainsAny(company.IntegralAds_Keyword.ToLower().Split(',')))
                {
                    Data processor = new Data();
                    processor.DatabaseName = company.DatabaseName;
                    processor.UserName = company.Login;
                    processor.Password = company.Password;

                    Processor = processor;
                    return company;
                }
            }

            return null;
        }
    }
}
