﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegralAdsReporting
{
    public class IntegralAdsMain
    {
        static void Main(string[] args)
        {
            RunCustomReport();
            RunFileReports();
        }
        private static void RunCustomReport()
        {
            Console.WriteLine("RunIntegralAdsReport : " + System.DateTime.Now);
            DateTime startTime = System.DateTime.Now;

            IntegralAdsReport process = new IntegralAdsReport();
            process.Process();
            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("Total Time : " + endTime.Subtract(startTime).TotalMinutes);
        }
        private static void RunFileReports()
        {
            Console.WriteLine("RunIntegralRunFileReportst : " + System.DateTime.Now);
            DateTime startTime = System.DateTime.Now;

            IntegralAdsFileProcessing process = new IntegralAdsFileProcessing();
            process.Process();
            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("Total Time : " + endTime.Subtract(startTime).TotalMinutes);
        }
    }
}
