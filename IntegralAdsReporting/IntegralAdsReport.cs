﻿using Excel;
using OpenPop.Mime;
using OpenPop.Pop3;
using SolutionHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using WhirlygigSolutionHelper;

namespace IntegralAdsReporting
{
    public class IntegralAdsReport
    {
        Data Processor = new Data();
        mediaIntegralAdsEntities db = new mediaIntegralAdsEntities();
        Company company = new Company();

        string server;
        string accountname;
        string accountpassword;
        private string tableName = "dataIntegralAdsReportingData";
        private bool deleteEmail;

        string csvExportCompleted;
        string csvExportFailed;
        string csvExportWaiting;
        string csvExport;

        public IntegralAdsReport()
        {
            server = ConfigurationManager.AppSettings["EmailAccountServer"];
            accountname = ConfigurationManager.AppSettings["EmailAccountName"];
            accountpassword = ConfigurationManager.AppSettings["EmailAccountPassword"];
            csvExportCompleted = ConfigurationManager.AppSettings["csvExportCompleted"];
            csvExportFailed = ConfigurationManager.AppSettings["csvExportFailed"];
            csvExportWaiting = ConfigurationManager.AppSettings["csvExportWaiting"];
            csvExport = ConfigurationManager.AppSettings["csvExport"];
        }

        internal void Process()
        {
            ProcessIntegralAdsReport();
        }

        /// <summary>
        /// Example showing:
        ///  - how to fetch all messages from a POP3 server
        /// </summary>
        /// <param name="hostname">Hostname of the server. For example: pop3.live.com</param>
        /// <param name="port">Host port to connect to. Normally: 110 for plain POP3, 995 for SSL POP3</param>
        /// <param name="useSsl">Whether or not to use SSL to connect to server</param>
        /// <param name="username">Username of the user on the server</param>
        /// <param name="password">Password of the user on the server</param>
        ///  http://hpop.sourceforge.net/
        /// <returns>All Messages on the POP3 server</returns>
        private void ProcessIntegralAdsReport()
        {
            try
            {
                using (Pop3Client client = new Pop3Client())
                {
                    client.Connect(server, 110, false);
                    client.Authenticate(accountname, accountpassword);

                    int messageCount = client.GetMessageCount();

                    List<Message> allMessages = new List<Message>(messageCount);

                    if (messageCount == 0)
                        return;

                    for (int i = messageCount; i > 0; i--)
                    {
                        Message message = client.GetMessage(i);
                        bool results = true;


                        if (message.Headers.Subject.Contains("Daily Lavidge IAS Report"))
                        {
                            List<MessagePart> attachments = message.FindAllAttachments();

                            string fileName = "";

                            foreach (MessagePart attachment in attachments)
                            {
                                deleteEmail = false;
                                company = null;
                                Processor = null;

                                if (attachment.FileName.ContainsAny(".csv", ".xlsx"))
                                {
                                    fileName = attachment.FileName;

                                    if (fileName == "")
                                        throw new Exception("What is this file");

                                    company = GetCompany(fileName);

                                    bool result = false;

                                    if (company == null)
                                        result = ExportMessage(attachment);
                                    else
                                        result = ProcessMessage(attachment);

                                    if (!result && !deleteEmail)
                                    {
                                        results = false;
                                        SolutionHelper.Error.LogError("ProcessIntegralAdsReport.Processing Error: " + fileName);
                                    }
                                }
                            }

                            if (results)
                                client.DeleteMessage(i);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("ProcessIntegralAdsReport.OpenPopHandler: " + ex.Message);
            }
        }

        private Company GetCompany(string fileName)
        {
            Company[] companies = (from m in db.Companies where m.IntegralAds_Keyword != null && m.IsActive == true select m).ToArray();

            foreach (Company company in companies)
            {
                if (fileName.ToLower().ContainsAny(company.IntegralAds_Keyword.ToLower().Split(',')))
                {
                    Data processor = new Data();
                    processor.DatabaseName = company.DatabaseName;
                    processor.UserName = company.Login;
                    processor.Password = company.Password;
                    processor.Server = company.Server;

                    Processor = processor;
                    return company;
                }
            }

            return null;
        }

        private bool ExportMessage(MessagePart attachment)
        {
            DateTime today = System.DateTime.Now.Date;
            bool processed = false;

            try
            {
                DataTable dt = ExtractDataTableFromCsv(attachment);

                if (dt == null  )
                    return false;

                if (dt.Rows.Count == 0)
                {
                    deleteEmail = true; 
                    return false;
                }

                CleanData(dt);


                foreach (DataColumn dc in dt.Columns)
                    if (dc.ColumnName.ToLower() == "date" || dc.ColumnName.ToLower() == "asofdate")
                        dc.ColumnName = "AsOfDate";


                DataSet ds = dt.ToDataSetDateSplit();

                foreach (DataTable datatab in ds.Tables)
                {
                    IntegralAdsFileProcessing process = new IntegralAdsFileProcessing();

                    datatab.TableName = datatab.Rows[0]["AsOfDate"].ToString().Replace(" 00:00:00", "");

                    processed = process.ExportToWaiting(datatab, attachment.FileName);

                    if (!processed)
                        return false;
                }

                return processed;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "ProcessIntegralAdsReport.ExportMessage:");
            }

            return false;
        }

        private bool ProcessMessage(MessagePart attachment)
        {
            DateTime today = System.DateTime.Now.Date;
            bool processed = false;

            try
            {
                DataTable dt = ExtractDataTableFromCsv(attachment);

                if (dt == null)
                    return false;

                DateTime[] reports = GetReportDates(tableName);

                ColumnChecker(dt);
                if(dt.Rows.Count == 0)
                {
                    deleteEmail = true;
                    return false;
                }

                DataSet ds = dt.ToDataSetDateSplit();


                foreach (DataTable datatab in ds.Tables)
                {
                    DateTime tablename = DateTime.Parse(datatab.TableName.ToString());

                    if (reports.Contains(tablename))
                    {
                        bool comparedata = CompareReportDataAgainstDatabaseData(datatab, tableName);

                        if (comparedata)
                            deleteEmail = true;
                        else
                        {
                            deleteEmail = false;
                            return false;
                        }

                        continue;
                    }
                    else if (tablename == today)
                    {
                        continue;
                    }
                    else
                    {
                        deleteEmail = false;
                    }

                    CleanData(dt);

                    processed = ProcessIntegralAdsReport(dt);

                    if (!processed)
                        return processed;
                }

                return processed;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "ProcessIntegralAdsReport.ProcessMessage:");
            }

            return false;
        }

        private bool CompareReportDataAgainstDatabaseData(DataTable datatab, string tableName)
        {
            string imps = "";

            try
            {

                string query = String.Format(@"SELECT count(*) Total, sum([In-View Impressions]) SumImp FROM [dbo].[{0}] where AsOfDate =  '{1}'", tableName, datatab.TableName);

                DataSet ds = Processor.RunQuery(query);

                if (ds != null)
                {
                    if (ds.Tables.Count != 0)
                    {
                        int _sumImp = 0;
                        int total = int.Parse(ds.Tables[0].Rows[0]["Total"].ToString());
                        int sumImp = int.Parse(ds.Tables[0].Rows[0]["SumImp"].ToString());

                        foreach (DataRow dr in datatab.Rows)
                        {
                            imps = dr["In-View Impressions"].ToString();
                            _sumImp += int.Parse(dr["In-View Impressions"].ToString().Replace(",", "").Replace("\"", ""));
                        }

                        int _total = datatab.Rows.Count;

                        if (total == _total && sumImp == _sumImp)
                            return true;
                    }
                }

            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }

        private DateTime[] GetReportDates(object tableName)
        {
            List<DateTime> dates = new List<DateTime>();

            string query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[{0}] where [AsOfDate] is not null", tableName);

            DataSet ds = Processor.RunQuery(query);

            if (ds != null) // Add date to list of date
                if (ds.Tables.Count != 0) // Add date to list of date
                    foreach (DataRow dr in ds.Tables[0].Rows)
                        dates.Add(DateTime.Parse(dr[0].ToString())); // Date is a non nullable date type

            return dates.ToArray();
        }

        private DataTable ExtractDataTableFromCsv(Message message)
        {
            try
            {
                List<MessagePart> attachments = message.FindAllAttachments();

                foreach (MessagePart attachment in attachments)
                {
                    if (attachment.FileName.Contains(".csv"))
                    {
                        Stream stream = new MemoryStream(attachment.Body);

                        var ms = new MemoryStream();

                        DataTable dt = stream.ToDataTableCsv();

                        return dt;
                    }
                }

            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "IntegralAdsReporting.ExtractDataTableFromCsv:");
            }

            return null;
        }

        private DataTable ExtractDataTableFromCsv(MessagePart attachment)
        {
            try
            {
                if (attachment.FileName.Contains(".csv"))
                {
                    Stream stream = new MemoryStream(attachment.Body);

                    var ms = new MemoryStream();

                    DataTable dt = stream.ToDataTableCsv();

                    return dt;
                }

            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "IntegralAdsReporting.ExtractDataTableFromCsv:");
            }

            return null;
        }

        private bool ProcessIntegralAdsReport(DataTable dt)
        {
            try
            {
                SqlConnection conn = new SqlConnection(Processor.ConnectionString);

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);

                    bulkCopy.BatchSize = 100000;
                    bulkCopy.BulkCopyTimeout = 500;

                    foreach (DataColumn dc in dt.Columns)
                    {
                        SqlBulkCopyColumnMapping mapping = new SqlBulkCopyColumnMapping(dc.ColumnName, dc.ColumnName);

                        bulkCopy.ColumnMappings.Add(mapping);
                    }

                    bulkCopy.DestinationTableName = tableName;

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 20000;
                    conn.Open();

                    bulkCopy.WriteToServer(dt);

                    return true;
                }
            }
            catch (Exception ex)
            {
                bool bolChecker = DebugHelper.CheckColumnsAgainstDatabase(Processor, dt, tableName);

                SolutionHelper.Error.LogError(ex, "ProcessIntegralAdsReport.ProcessIntegralAdsReport:" + Processor.DatabaseName);
            }
            return false;
        }
        private void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(String.Format("{0} Rows have been copied.", e.RowsCopied.ToString()));
        }

        #region  ColumnCheckerCleanData

        /// <summary>
        /// Check if all the columns are created in the database for the reporting table. If the column doesnt exist it will be added base on custom criterias.
        /// </summary>
        /// <param name="dt"></param>
        private void ColumnChecker(DataTable dt)
        {
            string[] columnCol = GetColumnsFromDatabase(tableName);

            foreach (DataColumn dc in dt.Columns)
            {
                if (dc.ColumnName.Contains("* "))
                    dc.ColumnName = dc.ColumnName.Replace("* ", "");
                if (dc.ColumnName.Contains("_"))
                    dc.ColumnName = dc.ColumnName.Replace("_", " ");
                if (dc.ColumnName.Contains("("))
                    dc.ColumnName = dc.ColumnName.Substring(0, int.Parse(dc.ColumnName.IndexOf("(").ToString()));

                dc.ColumnName = dc.ColumnName.Trim().ToProperCase();

                if (dc.ColumnName.ToLower() == "date" || dc.ColumnName.ToLower() == "asofdate")
                {
                    dc.ColumnName = "AsOfDate";
                    continue;
                }
                if (dc.ColumnName.ToLower() == "modifieddate")
                {
                    dc.ColumnName = "ModifiedDate";
                    continue;
                }

                if (!columnCol.Contains(dc.ColumnName.ToLower()))
                {
                    AddColumn(tableName, dc.ColumnName);
                }
            }

            DateTime today = System.DateTime.Now;

            if (!dt.Columns.Contains("ModifiedDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                newColumn.DefaultValue = today.ToString();
                dt.Columns.Add(newColumn);
            }
        }

        private string[] GetColumnsFromDatabase(string tableName, bool isToLower = true)
        {
            string query = "";
            query += "DECLARE @columns varchar(MAX) ";
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(MAX), COLUMN_NAME) ";
            query += String.Format("FROM (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{0}') as tab ", tableName);
            query += "select @columns ";

            DataSet ds = Processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString();

            string[] columnCol;

            if (isToLower)
                columnCol = columns.ToLower().Split(',').Select(x => x.Trim()).ToArray();  
            else
                columnCol = columns.Split(',').Select(x => x.Trim()).ToArray();

            return columnCol;
        }

        private string[] GetColumnsNameCollection()
        {
            string query = "";
            query += "DECLARE @columns varchar(8000) ";
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(8000), COLUMN_NAME) ";
            query += "FROM (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'dataIntegralAdsReportingData') as tab ";
            query += "select @columns ";

            DataSet ds = Processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString();

            string[] columnCol = columns.ToLower().Split(',');
            return columnCol;
        }
        /// <summary>
        /// Due to the columns needing to be dynamic this method is designed to add additional column to the table
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        private void AddColumn(string table, string column)
        {
            string query = "ALTER TABLE {0} ADD [{1}] {2}";

            if (column.ToUpper().ContainsAny("IMPRESSIONS", "(SECONDS)"))
                query = string.Format(query, table, column, "int NULL");
            else if (column.ToUpper().ContainsAny("DATE"))
                query = string.Format(query, table, column, "[DateTime] NULL");
            else if (column.ToUpper().ContainsAny("RATE", "%"))
                query = string.Format(query, table, column, "Decimal (18,4) NULL");
            else
                query = string.Format(query, table, column, "int NULL");

            Processor.RunQuery(query);
        }

        /// <summary>
        /// Clean the data based on the column type define in the AddColumn method 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private DataTable CleanData(DataTable dt)
        {
            int idx = 0;
            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName.ToUpper().ContainsAny("IMPRESSIONS", "(SECONDS)", "IMPRESSION", "NON VIEWABLE", "NON VIEWABLE"))
                        if (String.IsNullOrEmpty(dr[col.ColumnName].ToString()) || dr[col.ColumnName].ToString().ToUpper() == "NULL")
                            dr[col.ColumnName] = 0;

                    if (col.ColumnName.ToUpper().ContainsAny("RATE", "%", "%"))
                    {
                        if (String.IsNullOrEmpty(dr[col.ColumnName].ToString())
                            || dr[col.ColumnName].ToString().ToUpper() == "NULL"
                            || dr[col.ColumnName].ToString().ToUpper() == "0"
                            || dr[col.ColumnName].ToString().ToUpper() == "NAN")
                            dr[col.ColumnName] = "0.0";
                    }

                    if (dr[col.ColumnName].ToString().ToUpper() == "NULL")
                        dr[col.ColumnName] = 0;



                }
                idx++;
            }
            return dt;
        }

        #endregion

        #region  DeprecatedCode

        /// <summary>
        /// Extract datatable from email message attachment
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private DataTable ExtractDataTableFromExcel(Message message)
        {
            try
            {

                List<MessagePart> attachments = message.FindAllAttachments();

                Stream stream = new MemoryStream(attachments[0].Body);

                var ms = new MemoryStream();

                IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

                DataSet AsDataSet = excelReader.AsDataSet();

                DataTable dt = AsDataSet.Tables[0];

                dt.Columns.RemoveAt(0); // Remove empty column

                //if (dt.Columns.Count == 0) // This means report is empty
                //    continue;

                for (int i = 0; i <= 17; i++)
                    dt.Rows.Remove(dt.Rows[0]); //Remove empty rows

                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    string col = dt.Rows[0][i].ToString();  // Rename the columns based on the data from row 1

                    if (col == "")
                        continue;

                    dt.Columns[i].ColumnName = col;
                }

                foreach (var coln in dt.Columns.Cast<DataColumn>().ToArray()) // Remove remaining colums
                {
                    if (dt.AsEnumerable().All(dr => dr.IsNull(coln)))
                        dt.Columns.Remove(coln);
                }

                dt.Rows.Remove(dt.Rows[0]); //Remove first row with column names 

                for (int i = dt.Rows.Count - 1; i >= 0; i--) //Remove empty rows
                {
                    if (dt.Rows[i][1] == DBNull.Value)
                        dt.Rows[i].Delete();
                }

                dt.AcceptChanges();

                #region CleanData
                foreach (DataRow dr in dt.Rows)
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        if (col.ColumnName.ToUpper().Contains("IMPRESSIONS"))
                        {
                            if (dr[col.ColumnName].ToString() == "")
                            {
                                dr[col.ColumnName] = 0;
                            }
                        }

                        if (col.ColumnName.ToUpper().ContainsAny("RATE", "%"))
                        {
                            if (dr[col.ColumnName].ToString() == "")
                            {
                                dr[col.ColumnName] = "0.0";
                            }
                        }
                    }
                }
                #endregion

                if (dt.Columns.Contains("Date"))
                    dt.Columns["Date"].ColumnName = "AsOfDate";

                return dt;

            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "ProcessIntegralAdsReport.ExtractExcelDataTable:");
            }
            return null;
        }

        #endregion
    }
}
