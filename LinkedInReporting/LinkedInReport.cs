﻿using OpenPop.Mime;
using OpenPop.Pop3;
using SolutionHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhirlygigSolutionHelper;

namespace LinkedInReporting
{
    public class LinkedInReport
    {
        Data Processor = new Data();
        Company company = new Company();

        mediaLinkedInEntities db = new mediaLinkedInEntities();

        string server;
        string accountname;
        string accountpassword;
        private const string tableName = "dataLinkedInReportingData";
        private bool deleteEmail;
        bool reimportData;

        public LinkedInReport()
        {
            server = ConfigurationManager.AppSettings["EmailAccountServer"];
            accountname = ConfigurationManager.AppSettings["EmailAccountName"];
            accountpassword = ConfigurationManager.AppSettings["EmailAccountPassword"];
        }

        /// <summary>
        /// Example showing:
        ///  - how to fetch all messages from a POP3 server
        /// </summary>
        /// <param name="hostname">Hostname of the server. For example: pop3.live.com</param>
        /// <param name="port">Host port to connect to. Normally: 110 for plain POP3, 995 for SSL POP3</param>
        /// <param name="useSsl">Whether or not to use SSL to connect to server</param>
        /// <param name="username">Username of the user on the server</param>
        /// <param name="password">Password of the user on the server</param>
        ///  http://hpop.sourceforge.net/
        /// <returns>All Messages on the POP3 server</returns>
        public void Run()
        {
            string status = "";

            try
            {
                restart: // This goto was implemented due to a Pop3Client timeout. Connection to the mail server needs to be reauthenticated between each message

                do // LOOP AND REFRESH POP3 CLIENT
                {
                    DateTime startTime = System.DateTime.Now;

                    status = "Keep running";
                    Console.WriteLine(status);

                    using (Pop3Client client = new Pop3Client())
                    {
                        client.Connect(server, 110, false);
                        client.Authenticate(accountname, accountpassword);

                        int messageCount = client.GetMessageCount();

                        List<Message> allMessages = new List<Message>(messageCount);

                        if (messageCount == 0)
                            return;

                        for (int i = messageCount; i > 0; i--)
                        {
                            if (i == 1)
                                status = "Stop"; // Once the last email passes status should remain "Stop"
                            else
                                status = "Keep running";

                            Console.WriteLine("Processing " + i + " of " + messageCount);

                            company = null;
                            Processor = null;
                            deleteEmail = false;

                            Message message = client.GetMessage(i);

                            if (message.Headers.Subject == null)
                                continue;

                            if (message.Headers.Subject.ContainsAny(ConfigurationManager.AppSettings["EmailSubject"].Split(',')))
                            {
                                bool result = false;

                                result = ProcessData(message);

                                if (result || deleteEmail)
                                {
                                    client.DeleteMessage(i);
                                    goto restart;
                                }
                            }

                            TimeSpan span = System.DateTime.Now - startTime;

                            if (span.TotalMinutes > 3)
                                break;
                        }
                    }
                } while (status == "Keep running"); // DONT STOP UNLESS ERROR
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("LinkedInReport.OpenPopHandler: " + ex.Message.ToString());
            }
        }
        private bool ProcessData(Message message)
        {
            bool result = false;

            Console.WriteLine("Processing: " + message.Headers.Subject.ToString());

            try
            {
                company = GetCompany(message.Headers.Subject.ToString());

                if (company != null)
                {
                    result = ProcessMessage(message);

                    Console.WriteLine("Finished: " + message.Headers.Subject.ToString());

                    return result;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in processing: " + message.Headers.Subject.ToString() + ex.Message.ToString());
                SolutionHelper.Error.LogError(ex, "LinkedInReport.ProcessAnalytics:");
            }

            return result;
        }

        private bool ProcessMessage(Message message)
        {
            try
            {
                List<MessagePart> attachments = message.FindAllAttachments();

                Stream stream = new MemoryStream(attachments[0].Body);

                DataTable dt = new DataTable();

                if (attachments[0].FileName.Contains(".zip"))
                    stream = stream.ToStreamFromZip();
                if (attachments[0].FileName.Contains(".tsv"))
                    dt = stream.ToDataTableTsv();
                else if (attachments[0].FileName.Contains(".csv"))
                    dt = stream.ToDataTableCsv();

                if (
                        dt.Columns[0].ColumnName.ToString().ToLower().Contains("data was updated last") ||
                        dt.Columns[0].ColumnName.ToString().ToLower().Contains("no data found")
                    )
                {
                    deleteEmail = true;
                    return false;
                }

                bool processed = ProcessDatatable(dt);

                return processed;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "LinkedInReport.ProcessMessage:");
            }

            return false;
        }

        public bool ProcessDatatable(DataTable dt)
        {
            try
            { 
                bool processed = false;
                DateTime today = System.DateTime.Now.Date;
                DateTime[] reports = GetReportDates(tableName);

                ColumnChecker(dt, tableName);

                if (deleteEmail)
                    return processed;

                DataSet ds = dt.ToDataSetDateSplit();

                foreach (DataTable datatab in ds.Tables)
                {
                    DateTime dateValue;
                    reimportData = false;

                    if (!DateTime.TryParse(datatab.TableName.ToString(), out dateValue))
                        continue;

                    if (reports.Contains(dateValue))
                    {
                        bool comparedata = CompareReportDataAgainstDatabaseData(datatab);

                        if (comparedata)
                        {
                            deleteEmail = true;
                            continue;
                        }
                        else if (reimportData)
                        {
                            DeleteOldData(datatab);
                        }
                        else
                        {
                            deleteEmail = false;
                            return false;
                        }
                    }
                    else if (dateValue == today)
                    {
                        continue;
                    }
                    else
                    {
                        deleteEmail = false;
                    }

                    processed = ProcessLinkedInDatatable(datatab, tableName);

                    if (!processed)
                        return processed;
                } 
                return processed;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool ProcessLinkedInDatatable(DataTable datatab, string tableName)
        {
            try
            {
                string[] columnCol = GetColumnsFromDatabase(tableName, false);

                SqlConnection conn = new SqlConnection(Processor.ConnectionString);

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);

                    bulkCopy.BatchSize = 10000;
                    bulkCopy.BulkCopyTimeout = 500;

                    foreach (DataColumn dc in datatab.Columns)
                    {
                        string dbCol = "";

                        foreach (string c in columnCol)
                        {
                            if (dc.ColumnName == c)
                            {
                                dbCol = c;
                                break;
                            }

                        }

                        if (dbCol == "")
                        {
                            throw new Exception("Column not matching: " + dc.ColumnName.ToString());
                        }

                        SqlBulkCopyColumnMapping mapping = new SqlBulkCopyColumnMapping(dc.ColumnName, dc.ColumnName);

                        bulkCopy.ColumnMappings.Add(mapping);
                    }

                    bulkCopy.DestinationTableName = tableName;

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 2000;
                    conn.Open();

                    bulkCopy.WriteToServer(datatab);

                    return true;
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "ProcessLinkedInDatatable:" + Processor.DatabaseName);
            }

            return false;
        }

        #region FunctionMethods

        /// <summary>
        /// Check if all the columns are created in the database for the reporting table. If the column doesnt exist it will be added.
        /// </summary>
        /// <param name="dt"></param>
        private void ColumnChecker(DataTable dt, string tableName)
        {
            string[] columnCol = GetColumnsFromDatabase(tableName);

            foreach (DataColumn dc in dt.Columns)
            {
                if (dc.ColumnName.Contains("* "))
                    dc.ColumnName = dc.ColumnName.Replace("* ", "");

                dc.ColumnName = dc.ColumnName.ToProperCase();

                if (dc.ColumnName.ToLower() == "date")
                {
                    dc.ColumnName = "AsOfDate";
                    continue;
                }

                if (dc.ColumnName.ToLower() == "Creative Direct Sponsored Content Name".ToLower())
                {
                    dc.ColumnName = "Dsc Name";
                }

                if (!columnCol.Contains(dc.ColumnName.ToLower()))
                {
                    AddColumn(tableName, dc.ColumnName);
                }
            }

            DateTime today = System.DateTime.Now;

            if (!dt.Columns.Contains("ModifiedDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                newColumn.DefaultValue = today.ToString();
                dt.Columns.Add(newColumn);
            }

            bool bolChecker = DebugHelper.CheckColumnsAgainstDatabase(Processor, dt, tableName);
        }

        private string[] GetColumnsFromDatabase(string tableName)
        {
            string query = "";
            query += "DECLARE @columns varchar(8000) ";
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(8000), COLUMN_NAME) ";
            query += String.Format("FROM (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{0}') as tab ", tableName);
            query += "select @columns ";

            DataSet ds = Processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString();

            string[] columnCol = columns.ToLower().Split(',').Select(x => x.Trim()).ToArray(); 

            return columnCol;
        }

        /// <summary>
        /// This is to compare the total records and the sum of the impressions against the database. 
        /// </summary>
        /// <param name="datatab"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private bool CompareReportDataAgainstDatabaseData(DataTable datatab)
        {
            try
            {

                string query = String.Format(@"SELECT count(*) Total, sum([Impressions]) SumImp FROM [dbo].[{0}] where AsOfDate =  '{1}'", tableName, datatab.TableName);

                DataSet ds = Processor.RunQuery(query);

                if (ds != null)
                {
                    if (ds.Tables.Count != 0)
                    {
                        int _sumImp = 0;
                        int total = int.Parse(ds.Tables[0].Rows[0]["Total"].ToString());
                        int sumImp = int.Parse(ds.Tables[0].Rows[0]["SumImp"].ToString());

                        foreach (DataRow dr in datatab.Rows)
                        {
                            _sumImp += int.Parse(dr["Impressions"].ToString());
                        }

                        int _total = datatab.Rows.Count;

                        if (total == _total && sumImp == _sumImp)
                            return true;
                        else
                            reimportData = true;
                    }
                }

            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }

        /// <summary>
        /// Get company from the subject line in the message
        /// </summary>
        /// <param name="_subject"></param>
        /// <returns></returns>
        private Company GetCompany(string _subject)
        {
            Company[] companys = (from m in db.Companies where m.IsActive == true select m).ToArray();

            foreach (Company company in companys)
            {
                if (company.LinkedIn_Keyword == null)
                    continue;

                if (_subject.ToLower().ContainsAny(company.LinkedIn_Keyword.ToLower().Split(',')))
                {
                    Data processor = new Data();
                    processor.DatabaseName = company.DatabaseName;
                    processor.UserName = company.Login;
                    processor.Password = company.Password;
                    processor.Server = company.Server;

                    Processor = processor;
                    return company;
                }
            }

            return null;
        }

        private void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(String.Format("{0} Rows have been copied.", e.RowsCopied.ToString()));
        }

        /// <summary>
        /// This method returns a disinctive list of the asofdates from the database.
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns>DateTime[]</returns>
        private DateTime[] GetReportDates(string tableName)
        {
            List<DateTime> dates = new List<DateTime>();

            string query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[{0}] where [AsOfDate] is not null", tableName);

            DataSet ds = Processor.RunQuery(query);

            if (ds != null) // Add date to list of date
                if (ds.Tables.Count != 0) // Add date to list of date
                    foreach (DataRow dr in ds.Tables[0].Rows)
                        dates.Add(DateTime.Parse(dr[0].ToString())); // Date is a non nullable date type

            return dates.ToArray();
        }

        /// <summary>
        /// This method deletes old data from the specified table to allow a reimport of the new data.
        /// </summary>
        /// <param name="dt"></param>
        private void DeleteOldData(DataTable dt)
        {
            try
            {
                string query = String.Format(@"delete FROM [dbo].[{0}] where AsOfDate = '{1}'", tableName, dt.TableName);

                Processor.RunQuery(query);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Due to the columns needing to be dynamic this method is designed to add additional column to the table
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        private void AddColumn(string table, string column)
        {
            column = column.ToProperCase();

            string query = "ALTER TABLE {0} ADD [{1}] {2}";

            if (column.ToUpper().ContainsAny("CLICKS", "IMPRESSIONS", "CONVERSIONS", "OPENS", "CREATIVE ID"))
            {
                if (!column.ToUpper().ContainsAny("COST"))
                    query = string.Format(query, table, column, "[int] NULL");
            }

            if (column.ToUpper().ContainsAny("RATE", "COST", "CTR", "CPC"))
                query = string.Format(query, table, column, "decimal(21, 4) NULL");

            if (column.ToUpper().ContainsAny("TIME"))
                query = string.Format(query, table, column, "datetime NULL");

            if (query == "ALTER TABLE {0} ADD [{1}] {2}")
                query = string.Format(query, table, column, "[varchar](250)");

            Processor.RunQuery(query);
        }

        private string[] GetColumnsFromDatabase(string tableName, bool isToLower = true)
        {
            string query = "";
            query += "DECLARE @columns varchar(MAX) ";
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(MAX), COLUMN_NAME) ";
            query += String.Format("FROM (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{0}') as tab ", tableName);
            query += "select @columns ";

            DataSet ds = Processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString();

            string[] columnCol;

            if (isToLower)
                columnCol = columns.ToLower().Split(',').Select(x => x.Trim()).ToArray();  
            else
                columnCol = columns.Split(',').Select(x => x.Trim()).ToArray();  

            return columnCol;
        }

        #endregion
    }
}
