﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedInReporting
{
    public class Program
    {
        static void Main(string[] args)
        {
            LinkedInReport report = new LinkedInReport();
            report.Run();
        }
    }
}
