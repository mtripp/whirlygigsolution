﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WhirlygigSolutionHelper;

namespace MediaConsoleApp
{
    public partial class BreakoutForm : Form
    {
        private Company company;
        private Data processor;
        private int BreakoutId = 0;
        private mediaEntities selectedDb = new mediaEntities();

        public BreakoutForm(Company company, Data processor, int breakoutId = 0)
        {
            this.company = company;
            this.processor = processor;
            BreakoutId = breakoutId;

            InitializeComponent(); 
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            if (BreakoutId == 0)
            {
                PlacementBreakOut breakout = new PlacementBreakOut();
                breakout.Active = chActive.Checked;
                breakout.Prefix = txtPrefix.Text;
                breakout.Title = txtTitle.Text;
                breakout.Order = int.Parse(dbOrder.Text);

                selectedDb.PlacementBreakOuts.Add(breakout);
            }
            else
            {
                PlacementBreakOut breakout = (from m in selectedDb.PlacementBreakOuts where m.Id == BreakoutId select m).FirstOrDefault();
                breakout.Active = chActive.Checked;
                breakout.Prefix = txtPrefix.Text;
                breakout.Title = txtTitle.Text;
            }

            selectedDb.SaveChanges();

            this.Close();
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void txtTitle_TextChanged(object sender, KeyEventArgs e)
        {
            string title = txtTitle.Text.Replace("a", "").Replace("e", "").Replace("i", "").Replace("o", "").Replace("u", "");

            try
            {
                txtPrefix.Text = title.ToLower().Substring(0, 2);
            }
            catch (Exception)
            {
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BreakoutForm_Load(object sender, EventArgs e)
        { 
            selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

            if (BreakoutId != 0)
            {
                PlacementBreakOut breakout = (from m in selectedDb.PlacementBreakOuts where m.Id == BreakoutId select m).FirstOrDefault();

                chActive.Checked = breakout.Active ?? true;
                txtPrefix.Text = breakout.Prefix ?? "";
                txtTitle.Text = breakout.Title ?? "";
                dbOrder.Text = breakout.Order.ToString() ?? "5";

                gbBreakOut.Text = breakout.Title;
            }

        }
    }
}
