﻿namespace MediaConsoleApp
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgPlacements = new System.Windows.Forms.DataGridView();
            this.PlacementId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlacementEdit = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataSizmekPlacementsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mediaLavidgeDataSet1 = new MediaConsoleApp.mediaLavidgeDataSet1();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnCreate = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.dgBreakout = new System.Windows.Forms.DataGridView();
            this.BreakOutId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BreakOutTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BreakOutPrefix = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BreakOutOrder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BreakOutActive = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.placementBreakOutsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mediaLavidgeDataSet = new MediaConsoleApp.mediaLavidgeDataSet();
            this.tbMain = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tbBreakout = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.gbBreakOutEdit = new System.Windows.Forms.GroupBox();
            this.txtBreakoutId = new System.Windows.Forms.TextBox();
            this.dbOrder = new System.Windows.Forms.ComboBox();
            this.chActive = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtPrefix = new System.Windows.Forms.TextBox();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.btnAddNew = new System.Windows.Forms.Button();
            this.btnDetailsCancel = new System.Windows.Forms.Button();
            this.btnDetailsEdit = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDetailId = new System.Windows.Forms.TextBox();
            this.txtDetailAbbrev = new System.Windows.Forms.TextBox();
            this.txtDetailValue = new System.Windows.Forms.TextBox();
            this.dgBreakOutDetails = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlacementDetailId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlacementDetailTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlacementDetailAbbrev = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.placementBreakOutDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mediaLavidgeDataSet2 = new MediaConsoleApp.mediaLavidgeDataSet2();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbImpressions = new System.Windows.Forms.ComboBox();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.btnCreatePlacements = new System.Windows.Forms.Button();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.cbMediaChannel = new System.Windows.Forms.ComboBox();
            this.cbPlaceType = new System.Windows.Forms.ComboBox();
            this.cbAdvertisers = new System.Windows.Forms.ComboBox();
            this.cbCampaign = new System.Windows.Forms.ComboBox();
            this.cbSections = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cbSites = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.companyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dbCompanySelector = new System.Windows.Forms.ToolStripComboBox();
            this.lblLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.dataSizmekPlacementsTableAdapter1 = new MediaConsoleApp.mediaLavidgeDataSet1TableAdapters.dataSizmekPlacementsTableAdapter();
            this.placementBreakOutDetailsTableAdapter1 = new MediaConsoleApp.mediaLavidgeDataSet2TableAdapters.PlacementBreakOutDetailsTableAdapter();
            this.placementBreakOutsTableAdapter1 = new MediaConsoleApp.mediaLavidgeDataSetTableAdapters.PlacementBreakOutsTableAdapter();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPlacements)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSizmekPlacementsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mediaLavidgeDataSet1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBreakout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.placementBreakOutsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mediaLavidgeDataSet)).BeginInit();
            this.tbMain.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tbBreakout.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.gbBreakOutEdit.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBreakOutDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.placementBreakOutDetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mediaLavidgeDataSet2)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.dgPlacements);
            this.groupBox3.Location = new System.Drawing.Point(6, 290);
            this.groupBox3.MinimumSize = new System.Drawing.Size(695, 218);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(695, 258);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Placement";
            // 
            // dgPlacements
            // 
            this.dgPlacements.AllowUserToAddRows = false;
            this.dgPlacements.AllowUserToDeleteRows = false;
            this.dgPlacements.AutoGenerateColumns = false;
            this.dgPlacements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPlacements.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PlacementId,
            this.PlacementEdit,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.dgPlacements.DataSource = this.dataSizmekPlacementsBindingSource;
            this.dgPlacements.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgPlacements.Location = new System.Drawing.Point(3, 16);
            this.dgPlacements.Name = "dgPlacements";
            this.dgPlacements.ReadOnly = true;
            this.dgPlacements.RowHeadersWidth = 10;
            this.dgPlacements.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPlacements.Size = new System.Drawing.Size(689, 239);
            this.dgPlacements.TabIndex = 2;
            this.dgPlacements.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPlacements_CellContentClick);
            // 
            // PlacementId
            // 
            this.PlacementId.DataPropertyName = "Id";
            this.PlacementId.HeaderText = "Id";
            this.PlacementId.Name = "PlacementId";
            this.PlacementId.ReadOnly = true;
            this.PlacementId.Visible = false;
            // 
            // PlacementEdit
            // 
            this.PlacementEdit.HeaderText = "Edit";
            this.PlacementEdit.Name = "PlacementEdit";
            this.PlacementEdit.ReadOnly = true;
            this.PlacementEdit.Text = "Edit";
            this.PlacementEdit.Width = 40;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "PlacementId";
            this.dataGridViewTextBoxColumn3.HeaderText = "PlacementId";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "PlacementName";
            this.dataGridViewTextBoxColumn4.HeaderText = "PlacementName";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "PackageName";
            this.dataGridViewTextBoxColumn5.HeaderText = "PackageName";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataSizmekPlacementsBindingSource
            // 
            this.dataSizmekPlacementsBindingSource.DataMember = "dataSizmekPlacements";
            this.dataSizmekPlacementsBindingSource.DataSource = this.mediaLavidgeDataSet1;
            // 
            // mediaLavidgeDataSet1
            // 
            this.mediaLavidgeDataSet1.DataSetName = "mediaLavidgeDataSet1";
            this.mediaLavidgeDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnCreate);
            this.groupBox2.Controls.Add(this.btnEdit);
            this.groupBox2.Controls.Add(this.dgBreakout);
            this.groupBox2.Location = new System.Drawing.Point(8, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(693, 311);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "BreakOut";
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(612, 282);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(75, 23);
            this.btnCreate.TabIndex = 7;
            this.btnCreate.Text = "New";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(3, 282);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 23);
            this.btnEdit.TabIndex = 6;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // dgBreakout
            // 
            this.dgBreakout.AllowUserToAddRows = false;
            this.dgBreakout.AllowUserToDeleteRows = false;
            this.dgBreakout.AutoGenerateColumns = false;
            this.dgBreakout.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBreakout.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BreakOutId,
            this.BreakOutTitle,
            this.BreakOutPrefix,
            this.BreakOutOrder,
            this.BreakOutActive});
            this.dgBreakout.DataSource = this.placementBreakOutsBindingSource;
            this.dgBreakout.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgBreakout.Location = new System.Drawing.Point(3, 16);
            this.dgBreakout.MultiSelect = false;
            this.dgBreakout.Name = "dgBreakout";
            this.dgBreakout.ReadOnly = true;
            this.dgBreakout.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgBreakout.Size = new System.Drawing.Size(687, 260);
            this.dgBreakout.TabIndex = 0;
            this.dgBreakout.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBreakout_RowLeave);
            // 
            // BreakOutId
            // 
            this.BreakOutId.DataPropertyName = "Id";
            this.BreakOutId.HeaderText = "Id";
            this.BreakOutId.Name = "BreakOutId";
            this.BreakOutId.ReadOnly = true;
            this.BreakOutId.Visible = false;
            // 
            // BreakOutTitle
            // 
            this.BreakOutTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BreakOutTitle.DataPropertyName = "Title";
            this.BreakOutTitle.HeaderText = "Title";
            this.BreakOutTitle.Name = "BreakOutTitle";
            this.BreakOutTitle.ReadOnly = true;
            // 
            // BreakOutPrefix
            // 
            this.BreakOutPrefix.DataPropertyName = "Prefix";
            this.BreakOutPrefix.HeaderText = "Prefix";
            this.BreakOutPrefix.Name = "BreakOutPrefix";
            this.BreakOutPrefix.ReadOnly = true;
            // 
            // BreakOutOrder
            // 
            this.BreakOutOrder.DataPropertyName = "Order";
            this.BreakOutOrder.HeaderText = "Order";
            this.BreakOutOrder.Name = "BreakOutOrder";
            this.BreakOutOrder.ReadOnly = true;
            // 
            // BreakOutActive
            // 
            this.BreakOutActive.DataPropertyName = "Active";
            this.BreakOutActive.HeaderText = "Active";
            this.BreakOutActive.Name = "BreakOutActive";
            this.BreakOutActive.ReadOnly = true;
            // 
            // placementBreakOutsBindingSource
            // 
            this.placementBreakOutsBindingSource.DataMember = "PlacementBreakOuts";
            this.placementBreakOutsBindingSource.DataSource = this.mediaLavidgeDataSet;
            // 
            // mediaLavidgeDataSet
            // 
            this.mediaLavidgeDataSet.DataSetName = "mediaLavidgeDataSet";
            this.mediaLavidgeDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tbMain
            // 
            this.tbMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMain.Controls.Add(this.tabPage1);
            this.tbMain.Controls.Add(this.tabPage2);
            this.tbMain.Controls.Add(this.tabPage3);
            this.tbMain.Enabled = false;
            this.tbMain.Location = new System.Drawing.Point(12, 52);
            this.tbMain.Name = "tbMain";
            this.tbMain.SelectedIndex = 0;
            this.tbMain.Size = new System.Drawing.Size(712, 578);
            this.tbMain.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tbBreakout);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(704, 552);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "BreakOuts";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tbBreakout
            // 
            this.tbBreakout.Controls.Add(this.tabPage4);
            this.tbBreakout.Controls.Add(this.tabPage5);
            this.tbBreakout.Location = new System.Drawing.Point(8, 323);
            this.tbBreakout.Name = "tbBreakout";
            this.tbBreakout.SelectedIndex = 0;
            this.tbBreakout.Size = new System.Drawing.Size(696, 191);
            this.tbBreakout.TabIndex = 4;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.gbBreakOutEdit);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(688, 165);
            this.tabPage4.TabIndex = 0;
            this.tabPage4.Text = "Edit / New";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // gbBreakOutEdit
            // 
            this.gbBreakOutEdit.Controls.Add(this.txtBreakoutId);
            this.gbBreakOutEdit.Controls.Add(this.dbOrder);
            this.gbBreakOutEdit.Controls.Add(this.chActive);
            this.gbBreakOutEdit.Controls.Add(this.btnSave);
            this.gbBreakOutEdit.Controls.Add(this.btnCancel);
            this.gbBreakOutEdit.Controls.Add(this.txtPrefix);
            this.gbBreakOutEdit.Controls.Add(this.txtTitle);
            this.gbBreakOutEdit.Controls.Add(this.label5);
            this.gbBreakOutEdit.Controls.Add(this.label4);
            this.gbBreakOutEdit.Controls.Add(this.label3);
            this.gbBreakOutEdit.Controls.Add(this.label2);
            this.gbBreakOutEdit.Controls.Add(this.label1);
            this.gbBreakOutEdit.Enabled = false;
            this.gbBreakOutEdit.Location = new System.Drawing.Point(6, 7);
            this.gbBreakOutEdit.Name = "gbBreakOutEdit";
            this.gbBreakOutEdit.Size = new System.Drawing.Size(693, 151);
            this.gbBreakOutEdit.TabIndex = 4;
            this.gbBreakOutEdit.TabStop = false;
            this.gbBreakOutEdit.Text = "Breakout";
            // 
            // txtBreakoutId
            // 
            this.txtBreakoutId.Enabled = false;
            this.txtBreakoutId.Location = new System.Drawing.Point(420, 125);
            this.txtBreakoutId.Name = "txtBreakoutId";
            this.txtBreakoutId.Size = new System.Drawing.Size(38, 20);
            this.txtBreakoutId.TabIndex = 7;
            this.txtBreakoutId.Visible = false;
            // 
            // dbOrder
            // 
            this.dbOrder.FormattingEnabled = true;
            this.dbOrder.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30"});
            this.dbOrder.Location = new System.Drawing.Point(65, 86);
            this.dbOrder.Name = "dbOrder";
            this.dbOrder.Size = new System.Drawing.Size(65, 21);
            this.dbOrder.TabIndex = 3;
            // 
            // chActive
            // 
            this.chActive.AutoSize = true;
            this.chActive.Checked = true;
            this.chActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chActive.Location = new System.Drawing.Point(64, 116);
            this.chActive.Name = "chActive";
            this.chActive.Size = new System.Drawing.Size(15, 14);
            this.chActive.TabIndex = 4;
            this.chActive.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(602, 122);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(495, 122);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtPrefix
            // 
            this.txtPrefix.Location = new System.Drawing.Point(65, 52);
            this.txtPrefix.Name = "txtPrefix";
            this.txtPrefix.Size = new System.Drawing.Size(188, 20);
            this.txtPrefix.TabIndex = 2;
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(65, 17);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(293, 20);
            this.txtTitle.TabIndex = 1;
            this.txtTitle.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTitle_KeyUp);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(398, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Id";
            this.label5.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Active";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Order";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Prefix";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Title";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.btnAddNew);
            this.tabPage5.Controls.Add(this.btnDetailsCancel);
            this.tabPage5.Controls.Add(this.btnDetailsEdit);
            this.tabPage5.Controls.Add(this.label7);
            this.tabPage5.Controls.Add(this.label6);
            this.tabPage5.Controls.Add(this.txtDetailId);
            this.tabPage5.Controls.Add(this.txtDetailAbbrev);
            this.tabPage5.Controls.Add(this.txtDetailValue);
            this.tabPage5.Controls.Add(this.dgBreakOutDetails);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(688, 165);
            this.tabPage5.TabIndex = 1;
            this.tabPage5.Text = "Values";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // btnAddNew
            // 
            this.btnAddNew.Location = new System.Drawing.Point(607, 136);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(75, 23);
            this.btnAddNew.TabIndex = 2;
            this.btnAddNew.Text = "Save / Add";
            this.btnAddNew.UseVisualStyleBackColor = true;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // btnDetailsCancel
            // 
            this.btnDetailsCancel.Location = new System.Drawing.Point(526, 136);
            this.btnDetailsCancel.Name = "btnDetailsCancel";
            this.btnDetailsCancel.Size = new System.Drawing.Size(75, 23);
            this.btnDetailsCancel.TabIndex = 6;
            this.btnDetailsCancel.Text = "Cancel";
            this.btnDetailsCancel.UseVisualStyleBackColor = true;
            this.btnDetailsCancel.Visible = false;
            this.btnDetailsCancel.Click += new System.EventHandler(this.btnDetailsCancel_Click);
            // 
            // btnDetailsEdit
            // 
            this.btnDetailsEdit.Location = new System.Drawing.Point(399, 136);
            this.btnDetailsEdit.Name = "btnDetailsEdit";
            this.btnDetailsEdit.Size = new System.Drawing.Size(75, 23);
            this.btnDetailsEdit.TabIndex = 6;
            this.btnDetailsEdit.Text = "Edit";
            this.btnDetailsEdit.UseVisualStyleBackColor = true;
            this.btnDetailsEdit.Click += new System.EventHandler(this.btnDetailsEdit_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(396, 44);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "BreakOut Abbrev: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(396, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "BreakOut Value:";
            // 
            // txtDetailId
            // 
            this.txtDetailId.Location = new System.Drawing.Point(495, 67);
            this.txtDetailId.Name = "txtDetailId";
            this.txtDetailId.Size = new System.Drawing.Size(48, 20);
            this.txtDetailId.TabIndex = 1;
            this.txtDetailId.Visible = false;
            // 
            // txtDetailAbbrev
            // 
            this.txtDetailAbbrev.Location = new System.Drawing.Point(495, 41);
            this.txtDetailAbbrev.Name = "txtDetailAbbrev";
            this.txtDetailAbbrev.Size = new System.Drawing.Size(187, 20);
            this.txtDetailAbbrev.TabIndex = 1;
            // 
            // txtDetailValue
            // 
            this.txtDetailValue.Location = new System.Drawing.Point(495, 18);
            this.txtDetailValue.Name = "txtDetailValue";
            this.txtDetailValue.Size = new System.Drawing.Size(187, 20);
            this.txtDetailValue.TabIndex = 1;
            this.txtDetailValue.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtDetailValue_KeyUp);
            // 
            // dgBreakOutDetails
            // 
            this.dgBreakOutDetails.AllowUserToAddRows = false;
            this.dgBreakOutDetails.AllowUserToDeleteRows = false;
            this.dgBreakOutDetails.AutoGenerateColumns = false;
            this.dgBreakOutDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBreakOutDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.PlacementDetailId,
            this.PlacementDetailTitle,
            this.PlacementDetailAbbrev});
            this.dgBreakOutDetails.DataSource = this.placementBreakOutDetailsBindingSource;
            this.dgBreakOutDetails.Location = new System.Drawing.Point(6, 9);
            this.dgBreakOutDetails.MultiSelect = false;
            this.dgBreakOutDetails.Name = "dgBreakOutDetails";
            this.dgBreakOutDetails.ReadOnly = true;
            this.dgBreakOutDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgBreakOutDetails.Size = new System.Drawing.Size(383, 150);
            this.dgBreakOutDetails.TabIndex = 0;
            this.dgBreakOutDetails.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBreakOutDetails_RowLeave);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // PlacementDetailId
            // 
            this.PlacementDetailId.DataPropertyName = "PlacementBreakOutId";
            this.PlacementDetailId.HeaderText = "Id";
            this.PlacementDetailId.Name = "PlacementDetailId";
            this.PlacementDetailId.ReadOnly = true;
            this.PlacementDetailId.Visible = false;
            // 
            // PlacementDetailTitle
            // 
            this.PlacementDetailTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PlacementDetailTitle.DataPropertyName = "Title";
            this.PlacementDetailTitle.HeaderText = "Title";
            this.PlacementDetailTitle.Name = "PlacementDetailTitle";
            this.PlacementDetailTitle.ReadOnly = true;
            // 
            // PlacementDetailAbbrev
            // 
            this.PlacementDetailAbbrev.DataPropertyName = "Abbrev";
            this.PlacementDetailAbbrev.HeaderText = "Abbrev";
            this.PlacementDetailAbbrev.Name = "PlacementDetailAbbrev";
            this.PlacementDetailAbbrev.ReadOnly = true;
            // 
            // placementBreakOutDetailsBindingSource
            // 
            this.placementBreakOutDetailsBindingSource.DataMember = "PlacementBreakOutDetails";
            this.placementBreakOutDetailsBindingSource.DataSource = this.mediaLavidgeDataSet2;
            // 
            // mediaLavidgeDataSet2
            // 
            this.mediaLavidgeDataSet2.DataSetName = "mediaLavidgeDataSet2";
            this.mediaLavidgeDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(704, 552);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Placements";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.cbImpressions);
            this.groupBox1.Controls.Add(this.dtpEnd);
            this.groupBox1.Controls.Add(this.btnCreatePlacements);
            this.groupBox1.Controls.Add(this.dtpStart);
            this.groupBox1.Controls.Add(this.cbMediaChannel);
            this.groupBox1.Controls.Add(this.cbPlaceType);
            this.groupBox1.Controls.Add(this.cbAdvertisers);
            this.groupBox1.Controls.Add(this.cbCampaign);
            this.groupBox1.Controls.Add(this.cbSections);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.cbSites);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(695, 284);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sizmek Media Plan Requirements";
            // 
            // cbImpressions
            // 
            this.cbImpressions.FormattingEnabled = true;
            this.cbImpressions.Items.AddRange(new object[] {
            "TBD",
            "Unlimited"});
            this.cbImpressions.Location = new System.Drawing.Point(130, 187);
            this.cbImpressions.Name = "cbImpressions";
            this.cbImpressions.Size = new System.Drawing.Size(121, 21);
            this.cbImpressions.TabIndex = 7;
            this.cbImpressions.Text = "Unlimited";
            // 
            // dtpEnd
            // 
            this.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEnd.Location = new System.Drawing.Point(130, 161);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(97, 20);
            this.dtpEnd.TabIndex = 6;
            // 
            // btnCreatePlacements
            // 
            this.btnCreatePlacements.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnCreatePlacements.Location = new System.Drawing.Point(573, 255);
            this.btnCreatePlacements.Name = "btnCreatePlacements";
            this.btnCreatePlacements.Size = new System.Drawing.Size(116, 23);
            this.btnCreatePlacements.TabIndex = 10;
            this.btnCreatePlacements.Text = "Create Placements";
            this.btnCreatePlacements.UseVisualStyleBackColor = true;
            this.btnCreatePlacements.Click += new System.EventHandler(this.btnCreatePlacements_Click);
            // 
            // dtpStart
            // 
            this.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpStart.Location = new System.Drawing.Point(130, 135);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(97, 20);
            this.dtpStart.TabIndex = 5;
            // 
            // cbMediaChannel
            // 
            this.cbMediaChannel.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbMediaChannel.FormattingEnabled = true;
            this.cbMediaChannel.Location = new System.Drawing.Point(130, 241);
            this.cbMediaChannel.Name = "cbMediaChannel";
            this.cbMediaChannel.Size = new System.Drawing.Size(265, 21);
            this.cbMediaChannel.TabIndex = 9;
            // 
            // cbPlaceType
            // 
            this.cbPlaceType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbPlaceType.FormattingEnabled = true;
            this.cbPlaceType.Items.AddRange(new object[] {
            "InBanner",
            "InGame",
            "InStreamVideo",
            "InStreamVideoTracking",
            "Mobile",
            "OutOfBanner",
            "TrackingType"});
            this.cbPlaceType.Location = new System.Drawing.Point(130, 214);
            this.cbPlaceType.Name = "cbPlaceType";
            this.cbPlaceType.Size = new System.Drawing.Size(265, 21);
            this.cbPlaceType.TabIndex = 8;
            this.cbPlaceType.Text = "InBanner";
            // 
            // cbAdvertisers
            // 
            this.cbAdvertisers.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbAdvertisers.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbAdvertisers.FormattingEnabled = true;
            this.cbAdvertisers.Location = new System.Drawing.Point(130, 55);
            this.cbAdvertisers.Name = "cbAdvertisers";
            this.cbAdvertisers.Size = new System.Drawing.Size(265, 21);
            this.cbAdvertisers.TabIndex = 2;
            // 
            // cbCampaign
            // 
            this.cbCampaign.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbCampaign.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCampaign.FormattingEnabled = true;
            this.cbCampaign.Location = new System.Drawing.Point(130, 82);
            this.cbCampaign.Name = "cbCampaign";
            this.cbCampaign.Size = new System.Drawing.Size(265, 21);
            this.cbCampaign.TabIndex = 3;
            // 
            // cbSections
            // 
            this.cbSections.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbSections.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbSections.FormattingEnabled = true;
            this.cbSections.Location = new System.Drawing.Point(130, 109);
            this.cbSections.Name = "cbSections";
            this.cbSections.Size = new System.Drawing.Size(265, 21);
            this.cbSections.TabIndex = 4;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 165);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "End Date:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 190);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(105, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "Booked Impressions:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 244);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(102, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Media Buy Channel:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 58);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 13);
            this.label16.TabIndex = 6;
            this.label16.Text = "Advertisers:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 217);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(87, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Placement Type:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 85);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "Campaign:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 138);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Start Date:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 112);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Section:";
            // 
            // cbSites
            // 
            this.cbSites.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbSites.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbSites.FormattingEnabled = true;
            this.cbSites.Location = new System.Drawing.Point(130, 28);
            this.cbSites.Name = "cbSites";
            this.cbSites.Size = new System.Drawing.Size(265, 21);
            this.cbSites.TabIndex = 1;
            this.cbSites.SelectedIndexChanged += new System.EventHandler(this.cbSites_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Site / Network:";
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(704, 552);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "References";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.companyToolStripMenuItem,
            this.dbCompanySelector,
            this.lblLoad});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(736, 27);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // companyToolStripMenuItem
            // 
            this.companyToolStripMenuItem.Name = "companyToolStripMenuItem";
            this.companyToolStripMenuItem.Size = new System.Drawing.Size(77, 23);
            this.companyToolStripMenuItem.Text = "Company: ";
            // 
            // dbCompanySelector
            // 
            this.dbCompanySelector.Name = "dbCompanySelector";
            this.dbCompanySelector.Size = new System.Drawing.Size(250, 23);
            this.dbCompanySelector.SelectedIndexChanged += new System.EventHandler(this.dbCompanySelector_SelectedIndexChanged);
            // 
            // lblLoad
            // 
            this.lblLoad.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblLoad.Name = "lblLoad";
            this.lblLoad.Size = new System.Drawing.Size(45, 23);
            this.lblLoad.Text = "Load";
            this.lblLoad.Click += new System.EventHandler(this.lblLoad_Click);
            // 
            // dataSizmekPlacementsTableAdapter1
            // 
            this.dataSizmekPlacementsTableAdapter1.ClearBeforeFill = true;
            // 
            // placementBreakOutDetailsTableAdapter1
            // 
            this.placementBreakOutDetailsTableAdapter1.ClearBeforeFill = true;
            // 
            // placementBreakOutsTableAdapter1
            // 
            this.placementBreakOutsTableAdapter1.ClearBeforeFill = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(528, 31);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 642);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.tbMain);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(752, 680);
            this.Name = "Main";
            this.Text = "Main";
            this.Load += new System.EventHandler(this.Main_Load);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPlacements)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSizmekPlacementsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mediaLavidgeDataSet1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgBreakout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.placementBreakOutsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mediaLavidgeDataSet)).EndInit();
            this.tbMain.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tbBreakout.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.gbBreakOutEdit.ResumeLayout(false);
            this.gbBreakOutEdit.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBreakOutDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.placementBreakOutDetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mediaLavidgeDataSet2)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgPlacements;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgBreakout;
        private System.Windows.Forms.TabControl tbMain;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem companyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lblLoad;
        private System.Windows.Forms.ToolStripComboBox dbCompanySelector;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.GroupBox gbBreakOutEdit;
        private System.Windows.Forms.ComboBox dbOrder;
        private System.Windows.Forms.CheckBox chActive;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtPrefix;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.TextBox txtBreakoutId;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Title;
        private System.Windows.Forms.DataGridViewTextBoxColumn Prefix;
        private System.Windows.Forms.DataGridViewTextBoxColumn Order;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn placementIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn placementNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn packageNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.TabControl tbBreakout;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView dgBreakOutDetails;
        private System.Windows.Forms.Button btnAddNew;
        private System.Windows.Forms.Button btnDetailsCancel;
        private System.Windows.Forms.Button btnDetailsEdit;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDetailId;
        private System.Windows.Forms.TextBox txtDetailAbbrev;
        private System.Windows.Forms.TextBox txtDetailValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlacementDetailsId;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlacementBreakOutId;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlacementDetailsTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlacementDetailsAbbrev;
        private System.Windows.Forms.Button btnCreatePlacements;
        private MediaConsoleApp.DataSets.PlacementBreakOutsDataSet placementBreakOutsDataSet;
        private MediaConsoleApp.DataSets.PlacementBreakOutsDataSetTableAdapters.PlacementBreakOutsTableAdapter placementBreakOutsTableAdapter;
        private MediaConsoleApp.DataSets.PlacementDataSet placementDataSet;
        private MediaConsoleApp.DataSets.PlacementDataSetTableAdapters.dataSizmekPlacementsTableAdapter dataSizmekPlacementsTableAdapter;
        private MediaConsoleApp.DataSets.placementDetailsDataSet placementDetailsDataSet;
        private MediaConsoleApp.DataSets.placementDetailsDataSetTableAdapters.PlacementBreakOutDetailsTableAdapter placementBreakOutDetailsTableAdapter;
        private mediaLavidgeDataSet mediaLavidgeDataSet;
        private System.Windows.Forms.BindingSource placementBreakOutsBindingSource;
        private mediaLavidgeDataSetTableAdapters.PlacementBreakOutsTableAdapter placementBreakOutsTableAdapter1;
        private mediaLavidgeDataSet1 mediaLavidgeDataSet1;
        private System.Windows.Forms.BindingSource dataSizmekPlacementsBindingSource;
        private mediaLavidgeDataSet1TableAdapters.dataSizmekPlacementsTableAdapter dataSizmekPlacementsTableAdapter1;
        private System.Windows.Forms.DataGridViewTextBoxColumn BreakOutId;
        private System.Windows.Forms.DataGridViewTextBoxColumn BreakOutTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn BreakOutPrefix;
        private System.Windows.Forms.DataGridViewTextBoxColumn BreakOutOrder;
        private System.Windows.Forms.DataGridViewCheckBoxColumn BreakOutActive;
        private mediaLavidgeDataSet2 mediaLavidgeDataSet2;
        private System.Windows.Forms.BindingSource placementBreakOutDetailsBindingSource;
        private mediaLavidgeDataSet2TableAdapters.PlacementBreakOutDetailsTableAdapter placementBreakOutDetailsTableAdapter1;
        private System.Windows.Forms.DataGridViewTextBoxColumn placementBreakOutIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn abbrevDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlacementDetailId;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlacementDetailTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlacementDetailAbbrev;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dtpStart;
        private System.Windows.Forms.ComboBox cbSections;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbSites;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private System.Windows.Forms.ComboBox cbMediaChannel;
        private System.Windows.Forms.ComboBox cbPlaceType;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbCampaign;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cbAdvertisers;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cbImpressions;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlacementId;
        private System.Windows.Forms.DataGridViewButtonColumn PlacementEdit;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.Button button1;
    }
}

