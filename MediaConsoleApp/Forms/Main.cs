﻿using SizmekMDXReporting.PlacementService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WhirlygigSolutionHelper;

namespace MediaConsoleApp
{
    public partial class Main : Form
    {
        mediaEntities db = new mediaEntities();
        Company company = new Company();
        Data Processor = new Data(); 
        mediaEntities selectedDb = new mediaEntities();

        #region LoadEvents()
        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            LoadCompanies();
        }

        private void LoadCompanies()
        {
            try
            { 
                Company[] companies = (from m in db.Companies select m).ToArray();

                foreach (Company com in companies) 
                    dbCompanySelector.Items.Add(com.CompanyName); 
            }
            catch (Exception ex)
            {
                // TODO
                string error = ex.Message;
            }  
        }  
         
        private void Reset()
        {
            var selectedDb = new mediaEntities();
            selectedDb.Database.Connection.ConnectionString = Processor.ConnectionString;

            dataSizmekPlacement[] placements = (from m in selectedDb.dataSizmekPlacements select m).ToArray();
            dgPlacements.DataSource = placements;

            PlacementBreakOut[] breakout = (from r in selectedDb.PlacementBreakOuts select r).ToArray();
            dgBreakout.DataSource = breakout;
        }
            
        private void LoadPlacements()
        {
            LoadSites();
            PlacementGridLoad();

            cbCampaign.DataSource = (from camp in selectedDb.dataSizmekCampaigns select camp).OrderBy(m => m.CampaignName).ToArray(); 
            this.cbCampaign.ValueMember = "CampaignId";
            this.cbCampaign.DisplayMember = "CampaignName";

            cbAdvertisers.DataSource = (from adv in selectedDb.dataSizmekAdvertisers select adv).OrderBy(m => m.AdvertiserName).ToArray();
            this.cbAdvertisers.ValueMember = "AdvertiserId";
            this.cbAdvertisers.DisplayMember = "AdvertiserName";

            cbPlaceType.DataSource = (from adv in selectedDb.DataPlacementTypes select adv).ToArray();
            this.cbPlaceType.ValueMember = "PlacementTypeId";
            this.cbPlaceType.DisplayMember = "PlacementTypeName";
        }
        private void PlacementGridLoad()
        { 
            dgPlacements.DataSource = null;

            dgPlacements.DataSource = (from m in selectedDb.dataSizmekPlacements select m).ToArray(); 
        }

        private void LoadSites()
        { 
            cbSites.DataSource = (from site in selectedDb.dataSizmekSites where site.Active == true select site).OrderBy(m => m.SiteName).ToArray();

            this.cbSites.ValueMember = "SiteId";
            this.cbSites.DisplayMember = "SiteName";
        }

        private void LoadBreakouts()
        {
            PlacementBreakOut[] breakout = (from r in selectedDb.PlacementBreakOuts select r).ToArray();
            dgBreakout.DataSource = breakout;
        }

        private void ClearBreakOut()
        {
            chActive.Checked = true;
            txtPrefix.Text =  "";
            txtTitle.Text = "";
            dbOrder.Text = ""; 
            txtBreakoutId.Text = "";
            tbBreakout.Enabled = false;
            gbBreakOutEdit.Enabled = false;
            tbBreakout.SelectTab(0);
        }

        /// <summary>
        /// Loads the break out details from the database.
        /// </summary>
        private void LoadBreakOutDetails()
        {
            try
            {
                int breakoutId = (int.Parse(txtBreakoutId.Text));
                dgBreakOutDetails.DataSource = null;

                PlacementBreakOutDetail[] details = (from m in selectedDb.PlacementBreakOutDetails where m.PlacementBreakOutId == breakoutId select m).ToArray();
                dgBreakOutDetails.DataSource = details;

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void ClearPlacement()
        {
            cbSections.DataSource = null;
            cbSites.DataSource = null;

            dtpStart.Value = DateTime.Today;
            dtpEnd.Value = DateTime.Today.AddYears(1);
        }

        private void ResetDetails()
        {
            btnDetailsCancel.Visible = false;
            btnDetailsEdit.Visible = true;

            txtDetailId.Text = "";
            txtDetailValue.Text = "";
            txtDetailAbbrev.Text = "";
        }

        #endregion

        #region Events
        private void dgBreakout_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            ClearBreakOut();
        }

          
        private void txtTitle_KeyUp(object sender, KeyEventArgs e)
        {

            string title = txtTitle.Text.Replace("a", "").Replace("e", "").Replace("i", "").Replace("o", "").Replace("u", "");

            try
            {
                txtPrefix.Text = title.ToLower().Substring(0, 2);
            }
            catch (Exception)
            {
            }
        }
        private void cbSites_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbSections.DataSource = null;

            try
            {
                int siteId = int.Parse(cbSites.SelectedValue.ToString());
                cbSections.DataSource = (from sec in selectedDb.dataSizmekSections where sec.SiteId == siteId select sec).ToArray();
                this.cbSections.ValueMember = "SectionId";
                this.cbSections.DisplayMember = "SectionName";
            }
            catch (Exception ex)
            {
                string error = ex.Message;//TODO ERROR HANDLE
            }

        }

        private void txtDetailValue_KeyUp(object sender, KeyEventArgs e)
        { 
            string title = txtDetailValue.Text.Replace("a", "").Replace("e", "").Replace("i", "").Replace("o", "").Replace("u", "");
            int idx = 3;

            if (txtTitle.Text.ToLower() == "size")
            {
                txtDetailAbbrev.Text = title.ToLower();
            } else
            {
                try
                {
                    txtDetailAbbrev.Text = title.ToLower().Substring(0, idx); 
                }
                catch (Exception)
                {
                    txtDetailAbbrev.Text = title.ToLower();
                }
            } 
        }

        private void dgBreakOutDetails_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            ResetDetails();
        }

        private void dbCompanySelector_SelectedIndexChanged(object sender, EventArgs e)
        { 
            tbMain.Enabled = false;
        }

        #endregion

        #region ClickEvents
        private void lblLoad_Click(object sender, EventArgs e)
        {

            ClearBreakOut();
            ClearPlacement();

            if (dbCompanySelector.SelectedItem == null)
                return;

            string companyName = dbCompanySelector.SelectedItem.ToString();

            company = (from n in db.Companies where n.CompanyName == companyName select n).FirstOrDefault();

            Data processor = new Data(company.DatabaseName, company.Login, company.Password);
            Processor = processor;

            selectedDb.Database.Connection.ConnectionString = Processor.ConnectionString;

            LoadPlacements(); 

            LoadBreakouts();

            tbMain.Enabled = true;
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            gbBreakOutEdit.Enabled = true;
            tbBreakout.Enabled = true;

            if(dgBreakout.RowCount == 0)
            {
                MessageBox.Show("Please load breakouts.");
                return;
            }


            chActive.Checked = bool.Parse(dgBreakout.CurrentRow.Cells["BreakOutActive"].Value.ToString()) ; 
            txtPrefix.Text = dgBreakout.CurrentRow.Cells["BreakOutPrefix"].Value.ToString();
            txtTitle.Text = dgBreakout.CurrentRow.Cells["BreakOutTitle"].Value.ToString();
            dbOrder.Text = dgBreakout.CurrentRow.Cells["BreakOutOrder"].Value.ToString();
            txtBreakoutId.Text = dgBreakout.CurrentRow.Cells["BreakOutId"].Value.ToString();

            LoadBreakOutDetails();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ClearBreakOut();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            ClearBreakOut();
            gbBreakOutEdit.Enabled = true;
            tbBreakout.Enabled = true;
        }

        private void btnSave_Click_1(object sender, EventArgs e)
        {
            if (!BreakoutValidate())
                return;

            if (txtBreakoutId.Text == "0" || txtBreakoutId.Text == "")
            {
                PlacementBreakOut breakout = new PlacementBreakOut();
                breakout.Active = chActive.Checked;
                breakout.Prefix = txtPrefix.Text;
                breakout.Title = txtTitle.Text;
                breakout.Order = int.Parse(dbOrder.Text);

                selectedDb.PlacementBreakOuts.Add(breakout);
            }
            else
            {
                int id = int.Parse(txtBreakoutId.Text.ToString());

                PlacementBreakOut breakout = (from m in selectedDb.PlacementBreakOuts where m.Id == id select m).FirstOrDefault();
                breakout.Active = chActive.Checked;
                breakout.Prefix = txtPrefix.Text;
                breakout.Title = txtTitle.Text;
                breakout.Order = int.Parse(dbOrder.Text);
            }

            selectedDb.SaveChanges();
            Reset();
        }
        private void btnDetailsEdit_Click(object sender, EventArgs e)
        {
            btnDetailsEdit.Visible = false;
            btnDetailsCancel.Visible = true;

            txtDetailId.Text = dgBreakOutDetails.CurrentRow.Cells["PlacementDetailId"].Value.ToString();  
            txtDetailValue.Text = dgBreakOutDetails.CurrentRow.Cells["PlacementDetailTitle"].Value.ToString(); 
            txtDetailAbbrev.Text = dgBreakOutDetails.CurrentRow.Cells["PlacementDetailAbbrev"].Value.ToString();
        }

        private void btnDetailsCancel_Click(object sender, EventArgs e)
        {
            ResetDetails();
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        { 
            if (!BreakoutDetialValidate())
                return;

            try
            { 
                if (txtDetailId.Text == "0" || txtDetailId.Text == "")
                {
                    PlacementBreakOutDetail breakoutDetail = new PlacementBreakOutDetail();
                    breakoutDetail.Title = txtDetailValue.Text;
                    breakoutDetail.Abbrev = txtDetailAbbrev.Text;
                    breakoutDetail.PlacementBreakOutId = int.Parse(txtBreakoutId.Text);

                    selectedDb.PlacementBreakOutDetails.Add(breakoutDetail);
                }
                else
                {
                    int id = int.Parse(txtDetailId.Text.ToString());

                    PlacementBreakOutDetail breakoutDetail = (from m in selectedDb.PlacementBreakOutDetails where m.PlacementBreakOutId == id select m).FirstOrDefault();
                    breakoutDetail.Title = txtDetailValue.Text;
                    breakoutDetail.Abbrev = txtDetailAbbrev.Text;
                    selectedDb.SaveChanges();
                }

                selectedDb.SaveChanges();

                LoadBreakOutDetails();
                ResetDetails();
            }
            catch (Exception ex)
            {
            }
        }

        private void btnCreatePlacements_Click(object sender, EventArgs e)
        { 
            // (*)	Create package (2) – 
            // placement packages date periods object - can be between 1 and 10 periods (must contain at least 1 period)
            // in our example, only 1 period
            ServingPeriodInfo[] PackageDates = new ServingPeriodInfo[1];
            PackageDates[0] = new ServingPeriodInfo();
            PackageDates[0].StartDate = (APIDateTime)dtpStart.Value; // start date minutes must be - XX:00
            PackageDates[0].EndDate = (APIDateTime)dtpEnd.Value.AddMinutes(59); // end date minutes must be - YY:59
             
            PlacementInfo placementInfo = CreatePlacement(
                int.Parse(cbPlaceType.SelectedValue.ToString()),
                int.Parse(cbCampaign.SelectedValue.ToString()), 
                int.Parse(cbSites.SelectedValue.ToString()), 
                int.Parse(cbSections.SelectedValue.ToString()), "Sample InBannerPlacement", PackageDates,
                BookedImpressionsTypes.BookedImpressionsLimited, 100000);

            
            if (cbImpressions.Text == "TBD")
                placementInfo.BookedImpressionsType = BookedImpressionsTypes.BookedImpressionsTBD;
            else if (cbImpressions.Text == "Unlimited")
                placementInfo.BookedImpressionsType = BookedImpressionsTypes.BookedImpressionsUnlimited;

            PlacementEditor pl = new PlacementEditor(company, int.Parse(cbPlaceType.SelectedValue.ToString()), placementInfo);
            pl.ShowDialog();
             
            PlacementGridLoad();
        }

        private void dgPlacements_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                int placementId = int.Parse(dgPlacements.Rows[e.RowIndex].Cells[2].Value.ToString());

                PlacementEditor pl = new PlacementEditor(company, placementId);
                pl.ShowDialog();

                PlacementGridLoad();
            }

        }

        #endregion

        private bool BreakoutValidate()
        {
            if (txtTitle.Text == "" || txtPrefix.Text == "" || dbOrder.Text == "")
            {
                MessageBox.Show("Please fill out all required information.");
                return false;
            }

            string id = (from m in selectedDb.PlacementBreakOuts where m.Title == txtTitle.Text select m.Id.ToString()).FirstOrDefault();

            if (id != null)
            {
                if (id != txtBreakoutId.Text)
                {
                    MessageBox.Show("Please make sure Breakout value is unique.");
                    return false;
                }
            }
              
            string idPre = (from m in selectedDb.PlacementBreakOuts where m.Prefix == txtPrefix.Text select m.Id.ToString()).FirstOrDefault();

            if (id != null)
            {
                if (idPre != txtBreakoutId.Text)
                {
                    MessageBox.Show("Please make sure Breakout value is unique.");
                    return false;
                }
            }
            return true;
        }

        private bool BreakoutDetialValidate()
        {
            if (txtBreakoutId.Text == "" || int.Parse(txtBreakoutId.Text) == 0)
            {
                MessageBox.Show("Please save current Breakout.");
                return false;
            }

           int breakoutId = int.Parse(txtBreakoutId.Text);

            if (txtDetailValue.Text == "" || txtDetailAbbrev.Text == "")
            {
                MessageBox.Show("Please fill out all required information.");
                return false;
            } 

            string id = (from m in selectedDb.PlacementBreakOutDetails where m.Title == txtDetailValue.Text && m.PlacementBreakOutId == breakoutId select m.Id.ToString()).FirstOrDefault();

            if (id != null)
            {
                if(id != txtDetailId.Text)
                {
                    MessageBox.Show("Please make sure Breakout value is unique.");
                    return false; 
                }
            } 

            string aId = (from ss in selectedDb.PlacementBreakOutDetails where ss.Abbrev == txtDetailAbbrev.Text && ss.PlacementBreakOutId == breakoutId select ss.Id.ToString()).FirstOrDefault();

            if (aId != null)
            {
                if (aId != txtDetailId.Text)
                {
                    MessageBox.Show("Please make sure Breakout abbreviation value is unique.");
                    return false;
                }
            }

            return true;
        }
         
        private PlacementInfo CreatePlacement(int placementTypeId, int campaign, int site, int section, string placementName, ServingPeriodInfo[] dates, BookedImpressionsTypes bookedImpressionsType, int? bookedImpressionsValue)
        {
            PlacementInfo placement = new InBannerPlacementInfo();

            switch ((int)placementTypeId)
            {
                case (int)PlacementTypes.InBanner:
                    placement = new InBannerPlacementInfo(); 
                    break;
                case (int)PlacementTypes.InGame:
                    placement = new InGamePlacementInfo(); 
                    break;
                case (int)PlacementTypes.InStreamVideo:
                    placement = new InStreamVideoPlacementInfo(); 
                    break;
                case (int)PlacementTypes.InStreamVideoTracking:
                    placement = new InStreamVideoTrackingPlacementInfo(); 
                    break;
                case (int)PlacementTypes.Mobile:
                    placement = new MobilePlacementInfo(); 
                    break;
                case (int)PlacementTypes.OutOfBanner:
                    placement = new OutOfBannerPlacementInfo(); 
                    break;
                case (int)PlacementTypes.TrackingType:
                    placement = new TrackingOnlyPlacementInfo(); 
                    break;  
            }

            placement.CampaignID = campaign;
            placement.SiteID = site;
            placement.SiteSectionID = section;
            placement.Dates = dates;
            placement.PlacementName = placementName;
            placement.BookedImpressionsType = bookedImpressionsType;
            placement.BookedImpressionsValue = bookedImpressionsValue;

            return placement;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SeleniumHelper.GetLinkedInReport("");
        }
    }
}
