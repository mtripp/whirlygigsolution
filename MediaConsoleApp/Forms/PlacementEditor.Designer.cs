﻿namespace MediaConsoleApp
{
    partial class PlacementEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnBreakouts = new System.Windows.Forms.Panel();
            this.lbl19 = new System.Windows.Forms.Label();
            this.lbl18 = new System.Windows.Forms.Label();
            this.lbl17 = new System.Windows.Forms.Label();
            this.lbl14 = new System.Windows.Forms.Label();
            this.lbl11 = new System.Windows.Forms.Label();
            this.lbl15 = new System.Windows.Forms.Label();
            this.lbl16 = new System.Windows.Forms.Label();
            this.lbl10 = new System.Windows.Forms.Label();
            this.lbl13 = new System.Windows.Forms.Label();
            this.lbl5 = new System.Windows.Forms.Label();
            this.lbl9 = new System.Windows.Forms.Label();
            this.lbl12 = new System.Windows.Forms.Label();
            this.lbl4 = new System.Windows.Forms.Label();
            this.lbl8 = new System.Windows.Forms.Label();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lbl7 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl6 = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbl0 = new System.Windows.Forms.Label();
            this.gb11 = new System.Windows.Forms.GroupBox();
            this.cb11 = new System.Windows.Forms.ComboBox();
            this.gb0 = new System.Windows.Forms.GroupBox();
            this.cb0 = new System.Windows.Forms.ComboBox();
            this.gb1 = new System.Windows.Forms.GroupBox();
            this.cb1 = new System.Windows.Forms.ComboBox();
            this.gb12 = new System.Windows.Forms.GroupBox();
            this.cb12 = new System.Windows.Forms.ComboBox();
            this.gb10 = new System.Windows.Forms.GroupBox();
            this.cb10 = new System.Windows.Forms.ComboBox();
            this.gb19 = new System.Windows.Forms.GroupBox();
            this.cb19 = new System.Windows.Forms.ComboBox();
            this.gb2 = new System.Windows.Forms.GroupBox();
            this.cb2 = new System.Windows.Forms.ComboBox();
            this.gb13 = new System.Windows.Forms.GroupBox();
            this.cb13 = new System.Windows.Forms.ComboBox();
            this.gb9 = new System.Windows.Forms.GroupBox();
            this.cb9 = new System.Windows.Forms.ComboBox();
            this.gb18 = new System.Windows.Forms.GroupBox();
            this.cb18 = new System.Windows.Forms.ComboBox();
            this.gb3 = new System.Windows.Forms.GroupBox();
            this.cb3 = new System.Windows.Forms.ComboBox();
            this.gb14 = new System.Windows.Forms.GroupBox();
            this.cb14 = new System.Windows.Forms.ComboBox();
            this.gb8 = new System.Windows.Forms.GroupBox();
            this.cb8 = new System.Windows.Forms.ComboBox();
            this.gb17 = new System.Windows.Forms.GroupBox();
            this.cb17 = new System.Windows.Forms.ComboBox();
            this.gb4 = new System.Windows.Forms.GroupBox();
            this.cb4 = new System.Windows.Forms.ComboBox();
            this.gb15 = new System.Windows.Forms.GroupBox();
            this.cb15 = new System.Windows.Forms.ComboBox();
            this.gb7 = new System.Windows.Forms.GroupBox();
            this.cb7 = new System.Windows.Forms.ComboBox();
            this.gb16 = new System.Windows.Forms.GroupBox();
            this.cb16 = new System.Windows.Forms.ComboBox();
            this.gb5 = new System.Windows.Forms.GroupBox();
            this.cb5 = new System.Windows.Forms.ComboBox();
            this.gb6 = new System.Windows.Forms.GroupBox();
            this.cb6 = new System.Windows.Forms.ComboBox();
            this.lbSize = new System.Windows.Forms.ListBox();
            this.gbSize = new System.Windows.Forms.GroupBox();
            this.lblSize = new System.Windows.Forms.Label();
            this.gbMessaging = new System.Windows.Forms.GroupBox();
            this.lbMessaging = new System.Windows.Forms.ListBox();
            this.lblMessaging = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbPlacements = new System.Windows.Forms.TextBox();
            this.btnCopy = new System.Windows.Forms.Button();
            this.btnProcess = new System.Windows.Forms.Button();
            this.pnBreakouts.SuspendLayout();
            this.gb11.SuspendLayout();
            this.gb0.SuspendLayout();
            this.gb1.SuspendLayout();
            this.gb12.SuspendLayout();
            this.gb10.SuspendLayout();
            this.gb19.SuspendLayout();
            this.gb2.SuspendLayout();
            this.gb13.SuspendLayout();
            this.gb9.SuspendLayout();
            this.gb18.SuspendLayout();
            this.gb3.SuspendLayout();
            this.gb14.SuspendLayout();
            this.gb8.SuspendLayout();
            this.gb17.SuspendLayout();
            this.gb4.SuspendLayout();
            this.gb15.SuspendLayout();
            this.gb7.SuspendLayout();
            this.gb16.SuspendLayout();
            this.gb5.SuspendLayout();
            this.gb6.SuspendLayout();
            this.gbSize.SuspendLayout();
            this.gbMessaging.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnBreakouts
            // 
            this.pnBreakouts.AutoScroll = true;
            this.pnBreakouts.Controls.Add(this.lbl19);
            this.pnBreakouts.Controls.Add(this.lbl18);
            this.pnBreakouts.Controls.Add(this.lbl17);
            this.pnBreakouts.Controls.Add(this.lbl14);
            this.pnBreakouts.Controls.Add(this.lbl11);
            this.pnBreakouts.Controls.Add(this.lbl15);
            this.pnBreakouts.Controls.Add(this.lbl16);
            this.pnBreakouts.Controls.Add(this.lbl10);
            this.pnBreakouts.Controls.Add(this.lbl13);
            this.pnBreakouts.Controls.Add(this.lbl5);
            this.pnBreakouts.Controls.Add(this.lbl9);
            this.pnBreakouts.Controls.Add(this.lbl12);
            this.pnBreakouts.Controls.Add(this.lbl4);
            this.pnBreakouts.Controls.Add(this.lbl8);
            this.pnBreakouts.Controls.Add(this.lbl3);
            this.pnBreakouts.Controls.Add(this.lbl7);
            this.pnBreakouts.Controls.Add(this.lbl2);
            this.pnBreakouts.Controls.Add(this.lbl6);
            this.pnBreakouts.Controls.Add(this.lbl1);
            this.pnBreakouts.Controls.Add(this.lbl0);
            this.pnBreakouts.Controls.Add(this.gb11);
            this.pnBreakouts.Controls.Add(this.gb0);
            this.pnBreakouts.Controls.Add(this.gb1);
            this.pnBreakouts.Controls.Add(this.gb12);
            this.pnBreakouts.Controls.Add(this.gb10);
            this.pnBreakouts.Controls.Add(this.gb19);
            this.pnBreakouts.Controls.Add(this.gb2);
            this.pnBreakouts.Controls.Add(this.gb13);
            this.pnBreakouts.Controls.Add(this.gb9);
            this.pnBreakouts.Controls.Add(this.gb18);
            this.pnBreakouts.Controls.Add(this.gb3);
            this.pnBreakouts.Controls.Add(this.gb14);
            this.pnBreakouts.Controls.Add(this.gb8);
            this.pnBreakouts.Controls.Add(this.gb17);
            this.pnBreakouts.Controls.Add(this.gb4);
            this.pnBreakouts.Controls.Add(this.gb15);
            this.pnBreakouts.Controls.Add(this.gb7);
            this.pnBreakouts.Controls.Add(this.gb16);
            this.pnBreakouts.Controls.Add(this.gb5);
            this.pnBreakouts.Controls.Add(this.gb6);
            this.pnBreakouts.Location = new System.Drawing.Point(12, 12);
            this.pnBreakouts.Name = "pnBreakouts";
            this.pnBreakouts.Size = new System.Drawing.Size(441, 327);
            this.pnBreakouts.TabIndex = 1;
            // 
            // lbl19
            // 
            this.lbl19.AutoSize = true;
            this.lbl19.Location = new System.Drawing.Point(446, 1046);
            this.lbl19.Name = "lbl19";
            this.lbl19.Size = new System.Drawing.Size(0, 13);
            this.lbl19.TabIndex = 4;
            this.lbl19.Visible = false;
            // 
            // lbl18
            // 
            this.lbl18.AutoSize = true;
            this.lbl18.Location = new System.Drawing.Point(446, 993);
            this.lbl18.Name = "lbl18";
            this.lbl18.Size = new System.Drawing.Size(0, 13);
            this.lbl18.TabIndex = 4;
            this.lbl18.Visible = false;
            // 
            // lbl17
            // 
            this.lbl17.AutoSize = true;
            this.lbl17.Location = new System.Drawing.Point(446, 940);
            this.lbl17.Name = "lbl17";
            this.lbl17.Size = new System.Drawing.Size(0, 13);
            this.lbl17.TabIndex = 4;
            this.lbl17.Visible = false;
            // 
            // lbl14
            // 
            this.lbl14.AutoSize = true;
            this.lbl14.Location = new System.Drawing.Point(446, 778);
            this.lbl14.Name = "lbl14";
            this.lbl14.Size = new System.Drawing.Size(0, 13);
            this.lbl14.TabIndex = 4;
            this.lbl14.Visible = false;
            // 
            // lbl11
            // 
            this.lbl11.AutoSize = true;
            this.lbl11.Location = new System.Drawing.Point(446, 612);
            this.lbl11.Name = "lbl11";
            this.lbl11.Size = new System.Drawing.Size(0, 13);
            this.lbl11.TabIndex = 4;
            this.lbl11.Visible = false;
            // 
            // lbl15
            // 
            this.lbl15.AutoSize = true;
            this.lbl15.Location = new System.Drawing.Point(446, 834);
            this.lbl15.Name = "lbl15";
            this.lbl15.Size = new System.Drawing.Size(0, 13);
            this.lbl15.TabIndex = 4;
            this.lbl15.Visible = false;
            // 
            // lbl16
            // 
            this.lbl16.AutoSize = true;
            this.lbl16.Location = new System.Drawing.Point(446, 892);
            this.lbl16.Name = "lbl16";
            this.lbl16.Size = new System.Drawing.Size(0, 13);
            this.lbl16.TabIndex = 4;
            this.lbl16.Visible = false;
            // 
            // lbl10
            // 
            this.lbl10.AutoSize = true;
            this.lbl10.Location = new System.Drawing.Point(446, 559);
            this.lbl10.Name = "lbl10";
            this.lbl10.Size = new System.Drawing.Size(0, 13);
            this.lbl10.TabIndex = 4;
            this.lbl10.Visible = false;
            // 
            // lbl13
            // 
            this.lbl13.AutoSize = true;
            this.lbl13.Location = new System.Drawing.Point(446, 730);
            this.lbl13.Name = "lbl13";
            this.lbl13.Size = new System.Drawing.Size(0, 13);
            this.lbl13.TabIndex = 4;
            this.lbl13.Visible = false;
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = true;
            this.lbl5.Location = new System.Drawing.Point(446, 297);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(0, 13);
            this.lbl5.TabIndex = 4;
            this.lbl5.Visible = false;
            // 
            // lbl9
            // 
            this.lbl9.AutoSize = true;
            this.lbl9.Location = new System.Drawing.Point(446, 511);
            this.lbl9.Name = "lbl9";
            this.lbl9.Size = new System.Drawing.Size(0, 13);
            this.lbl9.TabIndex = 4;
            this.lbl9.Visible = false;
            // 
            // lbl12
            // 
            this.lbl12.AutoSize = true;
            this.lbl12.Location = new System.Drawing.Point(446, 677);
            this.lbl12.Name = "lbl12";
            this.lbl12.Size = new System.Drawing.Size(0, 13);
            this.lbl12.TabIndex = 4;
            this.lbl12.Visible = false;
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = true;
            this.lbl4.Location = new System.Drawing.Point(446, 244);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(0, 13);
            this.lbl4.TabIndex = 4;
            this.lbl4.Visible = false;
            // 
            // lbl8
            // 
            this.lbl8.AutoSize = true;
            this.lbl8.Location = new System.Drawing.Point(446, 458);
            this.lbl8.Name = "lbl8";
            this.lbl8.Size = new System.Drawing.Size(0, 13);
            this.lbl8.TabIndex = 4;
            this.lbl8.Visible = false;
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = true;
            this.lbl3.Location = new System.Drawing.Point(446, 196);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(0, 13);
            this.lbl3.TabIndex = 4;
            this.lbl3.Visible = false;
            // 
            // lbl7
            // 
            this.lbl7.AutoSize = true;
            this.lbl7.Location = new System.Drawing.Point(446, 398);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(0, 13);
            this.lbl7.TabIndex = 4;
            this.lbl7.Visible = false;
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.Location = new System.Drawing.Point(446, 143);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(0, 13);
            this.lbl2.TabIndex = 4;
            this.lbl2.Visible = false;
            // 
            // lbl6
            // 
            this.lbl6.AutoSize = true;
            this.lbl6.Location = new System.Drawing.Point(446, 350);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(0, 13);
            this.lbl6.TabIndex = 4;
            this.lbl6.Visible = false;
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Location = new System.Drawing.Point(446, 83);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(0, 13);
            this.lbl1.TabIndex = 4;
            this.lbl1.Visible = false;
            // 
            // lbl0
            // 
            this.lbl0.AutoSize = true;
            this.lbl0.Location = new System.Drawing.Point(446, 25);
            this.lbl0.Name = "lbl0";
            this.lbl0.Size = new System.Drawing.Size(0, 13);
            this.lbl0.TabIndex = 4;
            this.lbl0.Visible = false;
            // 
            // gb11
            // 
            this.gb11.Controls.Add(this.cb11);
            this.gb11.Location = new System.Drawing.Point(3, 593);
            this.gb11.Name = "gb11";
            this.gb11.Size = new System.Drawing.Size(415, 54);
            this.gb11.TabIndex = 2;
            this.gb11.TabStop = false;
            this.gb11.Text = "Breakout";
            this.gb11.Visible = false;
            // 
            // cb11
            // 
            this.cb11.FormattingEnabled = true;
            this.cb11.Location = new System.Drawing.Point(6, 19);
            this.cb11.Name = "cb11";
            this.cb11.Size = new System.Drawing.Size(398, 21);
            this.cb11.TabIndex = 0;
            this.cb11.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // gb0
            // 
            this.gb0.Controls.Add(this.cb0);
            this.gb0.Location = new System.Drawing.Point(3, 3);
            this.gb0.Name = "gb0";
            this.gb0.Size = new System.Drawing.Size(415, 47);
            this.gb0.TabIndex = 2;
            this.gb0.TabStop = false;
            this.gb0.Text = "Breakout";
            this.gb0.Visible = false;
            // 
            // cb0
            // 
            this.cb0.FormattingEnabled = true;
            this.cb0.Location = new System.Drawing.Point(6, 19);
            this.cb0.Name = "cb0";
            this.cb0.Size = new System.Drawing.Size(398, 21);
            this.cb0.TabIndex = 0;
            this.cb0.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // gb1
            // 
            this.gb1.Controls.Add(this.cb1);
            this.gb1.Location = new System.Drawing.Point(3, 56);
            this.gb1.Name = "gb1";
            this.gb1.Size = new System.Drawing.Size(415, 54);
            this.gb1.TabIndex = 2;
            this.gb1.TabStop = false;
            this.gb1.Text = "Breakout";
            this.gb1.Visible = false;
            // 
            // cb1
            // 
            this.cb1.FormattingEnabled = true;
            this.cb1.Location = new System.Drawing.Point(6, 19);
            this.cb1.Name = "cb1";
            this.cb1.Size = new System.Drawing.Size(398, 21);
            this.cb1.TabIndex = 0;
            this.cb1.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // gb12
            // 
            this.gb12.Controls.Add(this.cb12);
            this.gb12.Location = new System.Drawing.Point(3, 653);
            this.gb12.Name = "gb12";
            this.gb12.Size = new System.Drawing.Size(415, 47);
            this.gb12.TabIndex = 2;
            this.gb12.TabStop = false;
            this.gb12.Text = "Breakout";
            this.gb12.Visible = false;
            // 
            // cb12
            // 
            this.cb12.FormattingEnabled = true;
            this.cb12.Location = new System.Drawing.Point(6, 19);
            this.cb12.Name = "cb12";
            this.cb12.Size = new System.Drawing.Size(398, 21);
            this.cb12.TabIndex = 0;
            this.cb12.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // gb10
            // 
            this.gb10.Controls.Add(this.cb10);
            this.gb10.Location = new System.Drawing.Point(3, 540);
            this.gb10.Name = "gb10";
            this.gb10.Size = new System.Drawing.Size(415, 47);
            this.gb10.TabIndex = 2;
            this.gb10.TabStop = false;
            this.gb10.Text = "Breakout";
            this.gb10.Visible = false;
            // 
            // cb10
            // 
            this.cb10.FormattingEnabled = true;
            this.cb10.Location = new System.Drawing.Point(6, 19);
            this.cb10.Name = "cb10";
            this.cb10.Size = new System.Drawing.Size(398, 21);
            this.cb10.TabIndex = 0;
            this.cb10.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // gb19
            // 
            this.gb19.Controls.Add(this.cb19);
            this.gb19.Location = new System.Drawing.Point(3, 1024);
            this.gb19.Name = "gb19";
            this.gb19.Size = new System.Drawing.Size(415, 47);
            this.gb19.TabIndex = 2;
            this.gb19.TabStop = false;
            this.gb19.Text = "Breakout";
            this.gb19.Visible = false;
            // 
            // cb19
            // 
            this.cb19.FormattingEnabled = true;
            this.cb19.Location = new System.Drawing.Point(6, 19);
            this.cb19.Name = "cb19";
            this.cb19.Size = new System.Drawing.Size(398, 21);
            this.cb19.TabIndex = 0;
            this.cb19.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // gb2
            // 
            this.gb2.Controls.Add(this.cb2);
            this.gb2.Location = new System.Drawing.Point(3, 116);
            this.gb2.Name = "gb2";
            this.gb2.Size = new System.Drawing.Size(415, 47);
            this.gb2.TabIndex = 2;
            this.gb2.TabStop = false;
            this.gb2.Text = "Breakout";
            this.gb2.Visible = false;
            // 
            // cb2
            // 
            this.cb2.FormattingEnabled = true;
            this.cb2.Location = new System.Drawing.Point(6, 19);
            this.cb2.Name = "cb2";
            this.cb2.Size = new System.Drawing.Size(398, 21);
            this.cb2.TabIndex = 0;
            this.cb2.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // gb13
            // 
            this.gb13.Controls.Add(this.cb13);
            this.gb13.Location = new System.Drawing.Point(3, 706);
            this.gb13.Name = "gb13";
            this.gb13.Size = new System.Drawing.Size(415, 47);
            this.gb13.TabIndex = 2;
            this.gb13.TabStop = false;
            this.gb13.Text = "Breakout";
            this.gb13.Visible = false;
            // 
            // cb13
            // 
            this.cb13.FormattingEnabled = true;
            this.cb13.Location = new System.Drawing.Point(6, 19);
            this.cb13.Name = "cb13";
            this.cb13.Size = new System.Drawing.Size(398, 21);
            this.cb13.TabIndex = 0;
            this.cb13.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // gb9
            // 
            this.gb9.Controls.Add(this.cb9);
            this.gb9.Location = new System.Drawing.Point(3, 487);
            this.gb9.Name = "gb9";
            this.gb9.Size = new System.Drawing.Size(415, 47);
            this.gb9.TabIndex = 2;
            this.gb9.TabStop = false;
            this.gb9.Text = "Breakout";
            this.gb9.Visible = false;
            // 
            // cb9
            // 
            this.cb9.FormattingEnabled = true;
            this.cb9.Location = new System.Drawing.Point(6, 19);
            this.cb9.Name = "cb9";
            this.cb9.Size = new System.Drawing.Size(398, 21);
            this.cb9.TabIndex = 0;
            this.cb9.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // gb18
            // 
            this.gb18.Controls.Add(this.cb18);
            this.gb18.Location = new System.Drawing.Point(3, 971);
            this.gb18.Name = "gb18";
            this.gb18.Size = new System.Drawing.Size(415, 47);
            this.gb18.TabIndex = 2;
            this.gb18.TabStop = false;
            this.gb18.Text = "Breakout";
            this.gb18.Visible = false;
            // 
            // cb18
            // 
            this.cb18.FormattingEnabled = true;
            this.cb18.Location = new System.Drawing.Point(6, 19);
            this.cb18.Name = "cb18";
            this.cb18.Size = new System.Drawing.Size(398, 21);
            this.cb18.TabIndex = 0;
            this.cb18.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // gb3
            // 
            this.gb3.Controls.Add(this.cb3);
            this.gb3.Location = new System.Drawing.Point(3, 169);
            this.gb3.Name = "gb3";
            this.gb3.Size = new System.Drawing.Size(415, 47);
            this.gb3.TabIndex = 2;
            this.gb3.TabStop = false;
            this.gb3.Text = "Breakout";
            this.gb3.Visible = false;
            // 
            // cb3
            // 
            this.cb3.FormattingEnabled = true;
            this.cb3.Location = new System.Drawing.Point(6, 19);
            this.cb3.Name = "cb3";
            this.cb3.Size = new System.Drawing.Size(398, 21);
            this.cb3.TabIndex = 0;
            this.cb3.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // gb14
            // 
            this.gb14.Controls.Add(this.cb14);
            this.gb14.Location = new System.Drawing.Point(3, 759);
            this.gb14.Name = "gb14";
            this.gb14.Size = new System.Drawing.Size(415, 47);
            this.gb14.TabIndex = 2;
            this.gb14.TabStop = false;
            this.gb14.Text = "Breakout";
            this.gb14.Visible = false;
            // 
            // cb14
            // 
            this.cb14.FormattingEnabled = true;
            this.cb14.Location = new System.Drawing.Point(6, 19);
            this.cb14.Name = "cb14";
            this.cb14.Size = new System.Drawing.Size(398, 21);
            this.cb14.TabIndex = 0;
            this.cb14.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // gb8
            // 
            this.gb8.Controls.Add(this.cb8);
            this.gb8.Location = new System.Drawing.Point(3, 434);
            this.gb8.Name = "gb8";
            this.gb8.Size = new System.Drawing.Size(415, 47);
            this.gb8.TabIndex = 2;
            this.gb8.TabStop = false;
            this.gb8.Text = "Breakout";
            this.gb8.Visible = false;
            // 
            // cb8
            // 
            this.cb8.FormattingEnabled = true;
            this.cb8.Location = new System.Drawing.Point(6, 19);
            this.cb8.Name = "cb8";
            this.cb8.Size = new System.Drawing.Size(398, 21);
            this.cb8.TabIndex = 0;
            this.cb8.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // gb17
            // 
            this.gb17.Controls.Add(this.cb17);
            this.gb17.Location = new System.Drawing.Point(3, 918);
            this.gb17.Name = "gb17";
            this.gb17.Size = new System.Drawing.Size(415, 47);
            this.gb17.TabIndex = 2;
            this.gb17.TabStop = false;
            this.gb17.Text = "Breakout";
            this.gb17.Visible = false;
            // 
            // cb17
            // 
            this.cb17.FormattingEnabled = true;
            this.cb17.Location = new System.Drawing.Point(6, 19);
            this.cb17.Name = "cb17";
            this.cb17.Size = new System.Drawing.Size(398, 21);
            this.cb17.TabIndex = 0;
            this.cb17.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // gb4
            // 
            this.gb4.Controls.Add(this.cb4);
            this.gb4.Location = new System.Drawing.Point(3, 222);
            this.gb4.Name = "gb4";
            this.gb4.Size = new System.Drawing.Size(415, 47);
            this.gb4.TabIndex = 2;
            this.gb4.TabStop = false;
            this.gb4.Text = "Breakout";
            this.gb4.Visible = false;
            // 
            // cb4
            // 
            this.cb4.FormattingEnabled = true;
            this.cb4.Location = new System.Drawing.Point(6, 19);
            this.cb4.Name = "cb4";
            this.cb4.Size = new System.Drawing.Size(398, 21);
            this.cb4.TabIndex = 0;
            this.cb4.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // gb15
            // 
            this.gb15.Controls.Add(this.cb15);
            this.gb15.Location = new System.Drawing.Point(3, 812);
            this.gb15.Name = "gb15";
            this.gb15.Size = new System.Drawing.Size(415, 47);
            this.gb15.TabIndex = 2;
            this.gb15.TabStop = false;
            this.gb15.Text = "Breakout";
            this.gb15.Visible = false;
            // 
            // cb15
            // 
            this.cb15.FormattingEnabled = true;
            this.cb15.Location = new System.Drawing.Point(6, 19);
            this.cb15.Name = "cb15";
            this.cb15.Size = new System.Drawing.Size(398, 21);
            this.cb15.TabIndex = 0;
            this.cb15.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // gb7
            // 
            this.gb7.Controls.Add(this.cb7);
            this.gb7.Location = new System.Drawing.Point(3, 381);
            this.gb7.Name = "gb7";
            this.gb7.Size = new System.Drawing.Size(415, 47);
            this.gb7.TabIndex = 2;
            this.gb7.TabStop = false;
            this.gb7.Text = "Breakout";
            this.gb7.Visible = false;
            // 
            // cb7
            // 
            this.cb7.FormattingEnabled = true;
            this.cb7.Location = new System.Drawing.Point(6, 19);
            this.cb7.Name = "cb7";
            this.cb7.Size = new System.Drawing.Size(398, 21);
            this.cb7.TabIndex = 0;
            this.cb7.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // gb16
            // 
            this.gb16.Controls.Add(this.cb16);
            this.gb16.Location = new System.Drawing.Point(3, 865);
            this.gb16.Name = "gb16";
            this.gb16.Size = new System.Drawing.Size(415, 47);
            this.gb16.TabIndex = 2;
            this.gb16.TabStop = false;
            this.gb16.Text = "Breakout";
            this.gb16.Visible = false;
            // 
            // cb16
            // 
            this.cb16.FormattingEnabled = true;
            this.cb16.Location = new System.Drawing.Point(6, 19);
            this.cb16.Name = "cb16";
            this.cb16.Size = new System.Drawing.Size(398, 21);
            this.cb16.TabIndex = 0;
            this.cb16.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // gb5
            // 
            this.gb5.Controls.Add(this.cb5);
            this.gb5.Location = new System.Drawing.Point(3, 275);
            this.gb5.Name = "gb5";
            this.gb5.Size = new System.Drawing.Size(415, 47);
            this.gb5.TabIndex = 2;
            this.gb5.TabStop = false;
            this.gb5.Text = "Breakout";
            this.gb5.Visible = false;
            // 
            // cb5
            // 
            this.cb5.FormattingEnabled = true;
            this.cb5.Location = new System.Drawing.Point(6, 19);
            this.cb5.Name = "cb5";
            this.cb5.Size = new System.Drawing.Size(398, 21);
            this.cb5.TabIndex = 0;
            this.cb5.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // gb6
            // 
            this.gb6.Controls.Add(this.cb6);
            this.gb6.Location = new System.Drawing.Point(3, 328);
            this.gb6.Name = "gb6";
            this.gb6.Size = new System.Drawing.Size(415, 47);
            this.gb6.TabIndex = 2;
            this.gb6.TabStop = false;
            this.gb6.Text = "Breakout";
            this.gb6.Visible = false;
            // 
            // cb6
            // 
            this.cb6.FormattingEnabled = true;
            this.cb6.Location = new System.Drawing.Point(6, 19);
            this.cb6.Name = "cb6";
            this.cb6.Size = new System.Drawing.Size(398, 21);
            this.cb6.TabIndex = 0;
            this.cb6.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // lbSize
            // 
            this.lbSize.FormattingEnabled = true;
            this.lbSize.Location = new System.Drawing.Point(6, 19);
            this.lbSize.Name = "lbSize";
            this.lbSize.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lbSize.Size = new System.Drawing.Size(219, 147);
            this.lbSize.TabIndex = 5;
            this.lbSize.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // gbSize
            // 
            this.gbSize.Controls.Add(this.lbSize);
            this.gbSize.Controls.Add(this.lblSize);
            this.gbSize.Location = new System.Drawing.Point(464, 12);
            this.gbSize.Name = "gbSize";
            this.gbSize.Size = new System.Drawing.Size(231, 170);
            this.gbSize.TabIndex = 6;
            this.gbSize.TabStop = false;
            this.gbSize.Text = "Size";
            // 
            // lblSize
            // 
            this.lblSize.AutoSize = true;
            this.lblSize.Location = new System.Drawing.Point(51, 91);
            this.lblSize.Name = "lblSize";
            this.lblSize.Size = new System.Drawing.Size(0, 13);
            this.lblSize.TabIndex = 4;
            this.lblSize.Visible = false;
            // 
            // gbMessaging
            // 
            this.gbMessaging.Controls.Add(this.lbMessaging);
            this.gbMessaging.Controls.Add(this.lblMessaging);
            this.gbMessaging.Location = new System.Drawing.Point(464, 196);
            this.gbMessaging.Name = "gbMessaging";
            this.gbMessaging.Size = new System.Drawing.Size(231, 143);
            this.gbMessaging.TabIndex = 6;
            this.gbMessaging.TabStop = false;
            this.gbMessaging.Text = "Messaging";
            // 
            // lbMessaging
            // 
            this.lbMessaging.FormattingEnabled = true;
            this.lbMessaging.Location = new System.Drawing.Point(6, 19);
            this.lbMessaging.Name = "lbMessaging";
            this.lbMessaging.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lbMessaging.Size = new System.Drawing.Size(219, 121);
            this.lbMessaging.TabIndex = 5;
            this.lbMessaging.SelectedIndexChanged += new System.EventHandler(this.BuildPlacementName);
            // 
            // lblMessaging
            // 
            this.lblMessaging.AutoSize = true;
            this.lblMessaging.Location = new System.Drawing.Point(51, 91);
            this.lblMessaging.Name = "lblMessaging";
            this.lblMessaging.Size = new System.Drawing.Size(0, 13);
            this.lblMessaging.TabIndex = 4;
            this.lblMessaging.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.tbPlacements);
            this.groupBox1.Location = new System.Drawing.Point(12, 345);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(690, 175);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Placement Names";
            // 
            // tbPlacements
            // 
            this.tbPlacements.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbPlacements.Location = new System.Drawing.Point(3, 16);
            this.tbPlacements.Multiline = true;
            this.tbPlacements.Name = "tbPlacements";
            this.tbPlacements.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbPlacements.Size = new System.Drawing.Size(684, 156);
            this.tbPlacements.TabIndex = 8;
            // 
            // btnCopy
            // 
            this.btnCopy.Location = new System.Drawing.Point(12, 526);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(75, 23);
            this.btnCopy.TabIndex = 7;
            this.btnCopy.Text = "Copy";
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(624, 526);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(75, 23);
            this.btnProcess.TabIndex = 7;
            this.btnProcess.Text = "Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.button2_Click);
            // 
            // PlacementEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(705, 560);
            this.Controls.Add(this.btnProcess);
            this.Controls.Add(this.btnCopy);
            this.Controls.Add(this.gbSize);
            this.Controls.Add(this.gbMessaging);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pnBreakouts);
            this.MaximumSize = new System.Drawing.Size(721, 848);
            this.MinimumSize = new System.Drawing.Size(721, 585);
            this.Name = "PlacementEditor";
            this.Text = "PlacementEditor";
            this.Load += new System.EventHandler(this.PlacementEditor_Load);
            this.pnBreakouts.ResumeLayout(false);
            this.pnBreakouts.PerformLayout();
            this.gb11.ResumeLayout(false);
            this.gb0.ResumeLayout(false);
            this.gb1.ResumeLayout(false);
            this.gb12.ResumeLayout(false);
            this.gb10.ResumeLayout(false);
            this.gb19.ResumeLayout(false);
            this.gb2.ResumeLayout(false);
            this.gb13.ResumeLayout(false);
            this.gb9.ResumeLayout(false);
            this.gb18.ResumeLayout(false);
            this.gb3.ResumeLayout(false);
            this.gb14.ResumeLayout(false);
            this.gb8.ResumeLayout(false);
            this.gb17.ResumeLayout(false);
            this.gb4.ResumeLayout(false);
            this.gb15.ResumeLayout(false);
            this.gb7.ResumeLayout(false);
            this.gb16.ResumeLayout(false);
            this.gb5.ResumeLayout(false);
            this.gb6.ResumeLayout(false);
            this.gbSize.ResumeLayout(false);
            this.gbSize.PerformLayout();
            this.gbMessaging.ResumeLayout(false);
            this.gbMessaging.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnBreakouts;
        private System.Windows.Forms.GroupBox gb1;
        private System.Windows.Forms.ComboBox cb1;
        private System.Windows.Forms.GroupBox gb10;
        private System.Windows.Forms.ComboBox cb10;
        private System.Windows.Forms.GroupBox gb2;
        private System.Windows.Forms.ComboBox cb2;
        private System.Windows.Forms.GroupBox gb9;
        private System.Windows.Forms.ComboBox cb9;
        private System.Windows.Forms.GroupBox gb3;
        private System.Windows.Forms.ComboBox cb3;
        private System.Windows.Forms.GroupBox gb8;
        private System.Windows.Forms.ComboBox cb8;
        private System.Windows.Forms.GroupBox gb4;
        private System.Windows.Forms.ComboBox cb4;
        private System.Windows.Forms.GroupBox gb7;
        private System.Windows.Forms.ComboBox cb7;
        private System.Windows.Forms.GroupBox gb5;
        private System.Windows.Forms.ComboBox cb5;
        private System.Windows.Forms.GroupBox gb6;
        private System.Windows.Forms.ComboBox cb6;
        private System.Windows.Forms.GroupBox gb11;
        private System.Windows.Forms.ComboBox cb11;
        private System.Windows.Forms.GroupBox gb0;
        private System.Windows.Forms.ComboBox cb0;
        private System.Windows.Forms.GroupBox gb12;
        private System.Windows.Forms.ComboBox cb12;
        private System.Windows.Forms.GroupBox gb19;
        private System.Windows.Forms.ComboBox cb19;
        private System.Windows.Forms.GroupBox gb13;
        private System.Windows.Forms.ComboBox cb13;
        private System.Windows.Forms.GroupBox gb18;
        private System.Windows.Forms.ComboBox cb18;
        private System.Windows.Forms.GroupBox gb14;
        private System.Windows.Forms.ComboBox cb14;
        private System.Windows.Forms.GroupBox gb17;
        private System.Windows.Forms.ComboBox cb17;
        private System.Windows.Forms.GroupBox gb15;
        private System.Windows.Forms.ComboBox cb15;
        private System.Windows.Forms.GroupBox gb16;
        private System.Windows.Forms.ComboBox cb16;
        private System.Windows.Forms.Label lbl19;
        private System.Windows.Forms.Label lbl18;
        private System.Windows.Forms.Label lbl17;
        private System.Windows.Forms.Label lbl14;
        private System.Windows.Forms.Label lbl11;
        private System.Windows.Forms.Label lbl15;
        private System.Windows.Forms.Label lbl16;
        private System.Windows.Forms.Label lbl10;
        private System.Windows.Forms.Label lbl13;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.Label lbl9;
        private System.Windows.Forms.Label lbl12;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.Label lbl8;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lbl7;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lbl6;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbl0;
        private System.Windows.Forms.ListBox lbSize;
        private System.Windows.Forms.GroupBox gbSize;
        private System.Windows.Forms.GroupBox gbMessaging;
        private System.Windows.Forms.ListBox lbMessaging;
        private System.Windows.Forms.Label lblMessaging;
        private System.Windows.Forms.Label lblSize;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.TextBox tbPlacements;
    }
}