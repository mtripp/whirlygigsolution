﻿using SizmekMDXReporting;
using SizmekMDXReporting.PlacementService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WhirlygigSolutionHelper;

namespace MediaConsoleApp
{
    public partial class PlacementEditor : Form
    {
        mediaEntities db = new mediaEntities();
        Company company = new Company();
        Data Processor = new Data();
        PlacementInfo tempPlacement = new PlacementInfo();
        dataSizmekPlacement CurrentPlacement;
        int placementTypeId = new int();
        bool isEdit = false;

        public PlacementEditor(Company com, int _placementId)
        {
            var selectedDb = new mediaEntities();

            company = com;
            Processor = new Data(company.DatabaseName, company.Login, company.Password);

            selectedDb.Database.Connection.ConnectionString = Processor.ConnectionString;

            CurrentPlacement = (from m in selectedDb.dataSizmekPlacements where m.PlacementId == _placementId select m).FirstOrDefault();

            InitializeComponent();
        }

        public PlacementEditor(Company com, int _placementTypeId, PlacementInfo _Placement)
        {
            company = com;
            Data processor = new Data(company.DatabaseName, company.Login, company.Password);

            if (_Placement != null)
                tempPlacement = _Placement;

            placementTypeId = _placementTypeId;

            Processor = processor;
            InitializeComponent();
        }

        private void PlacementEditor_Load(object sender, EventArgs e)
        {
            var selectedDb = new mediaEntities();
            selectedDb.Database.Connection.ConnectionString = Processor.ConnectionString;

            PlacementBreakOut[] breakOuts = (from n in selectedDb.PlacementBreakOuts where n.Title.ToLower() != "messaging" && n.Title.ToLower() != "size" orderby n.Order select n).ToArray();
            LoadBreakouts(breakOuts);

            PlacementBreakOut[] breakOutsMultiChoice = (from n in selectedDb.PlacementBreakOuts where n.Title.ToLower() == "messaging" || n.Title.ToLower() == "size" select n).ToArray();
            LoadBreakOutsMultiChoice(breakOutsMultiChoice);
        }

        private void LoadBreakOutsMultiChoice(PlacementBreakOut[] breakOutsMultiChoice)
        {
            var selectedDb = new mediaEntities();
            selectedDb.Database.Connection.ConnectionString = Processor.ConnectionString;

            PlacementBreakOutDetail[] choices = (from j in selectedDb.PlacementBreakOutDetails select j).ToArray();

            PlacementBreakOut breakOutSize = breakOutsMultiChoice.Where(m => m.Title == "Size").FirstOrDefault();
            PlacementBreakOut breakOutMessaging = breakOutsMultiChoice.Where(m => m.Title == "Messaging").FirstOrDefault();

            lbSize.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOutSize.Id).ToArray(); ;
            this.lbSize.ValueMember = "Abbrev";
            this.lbSize.DisplayMember = "Title";
            this.lblSize.Text = breakOutSize.Prefix;

            lbMessaging.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOutMessaging.Id).ToArray();
            this.lbMessaging.ValueMember = "Abbrev";
            this.lbMessaging.DisplayMember = "Title";
            this.lblMessaging.Text = breakOutMessaging.Prefix;
        }

        private void LoadBreakouts(PlacementBreakOut[] breakOuts)
        {
            var selectedDb = new mediaEntities();
            selectedDb.Database.Connection.ConnectionString = Processor.ConnectionString;

            PlacementBreakOutDetail[] choices = (from j in selectedDb.PlacementBreakOutDetails select j).ToArray();
            PlacementBreakOutLookup[] choiceLookup = null;

            if (CurrentPlacement != null)
            {
                choiceLookup = (from j in selectedDb.PlacementBreakOutLookups where j.PlacementId == CurrentPlacement.PlacementId select j).ToArray();
            }

            if (breakOuts.Length > 0)
            {
                this.gb0.Visible = true;
                this.gb0.Text = breakOuts[0].Title;
                this.lbl0.Text = breakOuts[0].Prefix;
                this.cb0.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOuts[0].Id).ToArray();
                this.cb0.ValueMember = "Abbrev";
                this.cb0.DisplayMember = "Title";
                this.cb0.SelectedText = "";

                if (CurrentPlacement != null)
                {
                    bool found = false;

                    if (choiceLookup != null) // Get selected value based on database entry
                    {
                        PlacementBreakOutLookup choice = choiceLookup.Where(m => m.PlacementBreakOutId == breakOuts[0].Id).FirstOrDefault();
                        if (choice != null)
                        {
                            PlacementBreakOutDetail detail = choices.Where(m => m.Id == choice.PlacementBreakOutDetailsId).FirstOrDefault();

                            if (detail != null)
                            {
                                this.cb0.SelectedValue = detail.Abbrev;
                                this.cb0.Enabled = false;

                                found = true;
                            }
                        }
                    }

                    if (found != true)
                    {
                        // Get selected value based on name
                        string value = GetBreakoutValueByName(CurrentPlacement.PlacementName, breakOuts[0].Prefix);

                        if (value != "")
                        {
                            PlacementBreakOutDetail choice = choices.Where(c => c.Abbrev == value).FirstOrDefault();

                            // Create PlacementBreakOutLookup
                            if (choice != null)
                            {
                                this.cb0.SelectedValue = choice.Abbrev;
                                this.cb0.Enabled = false;

                                PlacementBreakOutLookup addLookUp = new PlacementBreakOutLookup();
                                addLookUp.PlacementId = CurrentPlacement.PlacementId;
                                addLookUp.PlacementBreakOutId = breakOuts[0].Id;
                                addLookUp.PlacementBreakOutDetailsId = choice.Id;

                                selectedDb.PlacementBreakOutLookups.Add(addLookUp);

                                try
                                {

                                selectedDb.SaveChanges();
                                }
                                catch (Exception ex)
                                {
                                    string error = ex.Message;
                                }
                            }
                        }

                    }
                }
            }
            if (breakOuts.Length > 1)
            {
                this.gb1.Visible = true;
                this.gb1.Text = breakOuts[1].Title;
                this.lbl1.Text = breakOuts[1].Prefix;
                this.cb1.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOuts[1].Id).ToArray();
                this.cb1.ValueMember = "Abbrev";
                this.cb1.DisplayMember = "Title";
                this.cb1.SelectedText = "";

                cb1.Update();
            }
            if (breakOuts.Length > 2)
            {
                gb2.Visible = true;
                gb2.Text = breakOuts[2].Title;
                this.lbl2.Text = breakOuts[2].Prefix;
                cb2.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOuts[2].Id).ToArray();
                cb2.ValueMember = "Abbrev";
                cb2.DisplayMember = "Title";
                cb2.SelectedText = "";
            }
            if (breakOuts.Length > 3)
            {
                gb3.Visible = true;
                gb3.Text = breakOuts[3].Title;
                this.lbl3.Text = breakOuts[3].Prefix;
                cb3.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOuts[3].Id).ToArray();
                cb3.ValueMember = "Abbrev";
                cb3.DisplayMember = "Title";
                cb3.SelectedText = "";
            }
            if (breakOuts.Length > 4)
            {
                gb4.Visible = true;
                gb4.Text = breakOuts[4].Title;
                this.lbl4.Text = breakOuts[4].Prefix;
                cb4.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOuts[4].Id).ToArray();
                cb4.ValueMember = "Abbrev";
                cb4.DisplayMember = "Title";
                cb4.SelectedText = "";
            }
            if (breakOuts.Length > 5)
            {
                gb5.Visible = true;
                gb5.Text = breakOuts[5].Title;
                this.lbl5.Text = breakOuts[5].Prefix;
                cb5.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOuts[5].Id).ToArray();
                cb5.ValueMember = "Abbrev";
                cb5.DisplayMember = "Title";
                cb5.SelectedText = "";
            }
            if (breakOuts.Length > 6)
            {
                gb6.Visible = true;
                gb6.Text = breakOuts[6].Title;
                this.lbl6.Text = breakOuts[6].Prefix;
                cb6.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOuts[6].Id).ToArray();
                cb6.ValueMember = "Abbrev";
                cb6.DisplayMember = "Title";
                cb6.SelectedText = "";
            }
            if (breakOuts.Length > 7)
            {
                gb7.Visible = true;
                gb7.Text = breakOuts[7].Title;
                this.lbl7.Text = breakOuts[7].Prefix;
                cb7.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOuts[7].Id).ToArray();
                cb7.ValueMember = "Abbrev";
                cb7.DisplayMember = "Title";
                cb7.SelectedText = "";
            }
            if (breakOuts.Length > 8)
            {
                gb8.Visible = true;
                gb8.Text = breakOuts[8].Title;
                this.lbl8.Text = breakOuts[8].Prefix;
                cb8.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOuts[8].Id).ToArray();
                cb8.ValueMember = "Abbrev";
                cb8.DisplayMember = "Title";
                cb8.SelectedText = "";
            }
            if (breakOuts.Length > 9)
            {
                gb9.Visible = true;
                gb9.Text = breakOuts[9].Title;
                this.lbl9.Text = breakOuts[9].Prefix;
                cb9.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOuts[9].Id).ToArray();
                cb9.ValueMember = "Abbrev";
                cb9.DisplayMember = "Title";
                cb9.SelectedText = "";
            }
            if (breakOuts.Length > 10)
            {
                gb10.Visible = true;
                gb10.Text = breakOuts[10].Title;
                this.lbl10.Text = breakOuts[10].Prefix;
                cb10.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOuts[10].Id).ToArray();
                cb10.ValueMember = "Abbrev";
                cb10.DisplayMember = "Title";
                cb10.SelectedText = "";
            }
            if (breakOuts.Length > 11)
            {
                gb11.Visible = true;
                gb11.Text = breakOuts[11].Title;
                this.lbl11.Text = breakOuts[11].Prefix;
                cb11.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOuts[11].Id).ToArray();
                cb11.ValueMember = "Abbrev";
                cb11.DisplayMember = "Title";
                cb11.SelectedText = "";
            }
            if (breakOuts.Length > 12)
            {
                gb12.Visible = true;
                gb12.Text = breakOuts[12].Title;
                this.lbl12.Text = breakOuts[12].Prefix;
                cb12.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOuts[12].Id).ToArray();
                cb12.ValueMember = "Abbrev";
                cb12.DisplayMember = "Title";
                cb12.SelectedText = "";
            }
            if (breakOuts.Length > 13)
            {
                gb13.Visible = true;
                gb13.Text = breakOuts[13].Title;
                this.lbl13.Text = breakOuts[13].Prefix;
                cb13.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOuts[13].Id).ToArray();
                cb13.ValueMember = "Abbrev";
                cb13.DisplayMember = "Title";
                cb13.SelectedText = "";
            }
            if (breakOuts.Length > 14)
            {
                gb14.Visible = true;
                gb14.Text = breakOuts[14].Title;
                this.lbl14.Text = breakOuts[14].Prefix;
                cb14.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOuts[14].Id).ToArray();
                cb14.ValueMember = "Abbrev";
                cb14.DisplayMember = "Title";
                cb14.SelectedText = "";
            }
            if (breakOuts.Length > 15)
            {
                gb15.Visible = true;
                gb15.Text = breakOuts[15].Title;
                this.lbl15.Text = breakOuts[15].Prefix;
                cb15.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOuts[15].Id).ToArray();
                cb15.ValueMember = "Abbrev";
                cb15.DisplayMember = "Title";
                cb15.SelectedText = "";
            }
            if (breakOuts.Length > 16)
            {
                gb16.Visible = true;
                gb16.Text = breakOuts[16].Title;
                this.lbl16.Text = breakOuts[16].Prefix;
                cb16.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOuts[16].Id).ToArray();
                cb16.ValueMember = "Abbrev";
                cb16.DisplayMember = "Title";
                cb16.SelectedText = "";
            }
            if (breakOuts.Length > 17)
            {
                gb17.Visible = true;
                gb17.Text = breakOuts[17].Title;
                this.lbl17.Text = breakOuts[17].Prefix;
                cb17.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOuts[17].Id).ToArray();
                cb17.ValueMember = "Abbrev";
                cb17.DisplayMember = "Title";
                cb17.SelectedText = "";
            }
            if (breakOuts.Length > 18)
            {
                gb18.Visible = true;
                gb18.Text = breakOuts[18].Title;
                this.lbl18.Text = breakOuts[18].Prefix;
                cb18.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOuts[18].Id).ToArray();
                cb18.ValueMember = "Abbrev";
                cb18.DisplayMember = "Title";
                cb18.SelectedText = "";
            }
            if (breakOuts.Length > 19)
            {
                gb19.Visible = true;
                gb19.Text = breakOuts[19].Title;
                this.lbl19.Text = breakOuts[19].Prefix;
                cb19.DataSource = choices.Where(c => c.PlacementBreakOutId == breakOuts[19].Id).ToArray();
                cb19.ValueMember = "Abbrev";
                cb19.DisplayMember = "Title";
                cb19.SelectedText = "";
            }

            selectedDb.SaveChanges();
        }

        private string GetBreakoutValueByName(string placementName, string prefix)
        {
            string[] breakouts = placementName.Split('_');

            foreach(string breakout in breakouts)
            {
                string[] brkout = breakout.Split('-');

                if (brkout[0] == prefix)
                    return brkout[1];
            }

            return "";

        }

        private void BuildPlacementName(object sender, EventArgs e)
        {
            tbPlacements.Text = "";

            foreach (object itemMessages in lbMessaging.SelectedItems)
            {
                foreach (object itemSize in lbSize.SelectedItems)
                {
                    string PlacementName = "";

                    #region BuildPlacementName

                    if (gb0.Visible == true)
                    {
                        if (cb0.SelectedValue != null)
                        {
                            string value = lbl0.Text.ToString();
                            string index = cb0.SelectedValue.ToString();

                            PlacementName += value + "-" + index + "_";
                        }
                    }
                    if (gb1.Visible == true)
                    {
                        if (cb1.SelectedValue != null)
                        {
                            string value = lbl1.Text.ToString();
                            string index = cb1.SelectedValue.ToString();

                            PlacementName += value + "-" + index + "_";
                        }
                    }
                    if (gb2.Visible == true)
                    {
                        if (cb2.SelectedValue != null)
                        {
                            string value = lbl2.Text.ToString();
                            string index = cb2.SelectedValue.ToString();

                            PlacementName += value + "-" + index + "_";
                        }
                    }
                    if (gb3.Visible == true)
                    {
                        if (cb3.SelectedValue != null)
                        {
                            string value = lbl3.Text.ToString();
                            string index = cb3.SelectedValue.ToString();

                            PlacementName += value + "-" + index + "_";
                        }
                    }
                    if (gb4.Visible == true)
                    {
                        if (cb4.SelectedValue != null)
                        {
                            string value = lbl4.Text.ToString();
                            string index = cb4.SelectedValue.ToString();

                            PlacementName += value + "-" + index + "_";
                        }
                    }
                    if (gb5.Visible == true)
                    {
                        if (cb5.SelectedValue != null)
                        {
                            string value = lbl5.Text.ToString();
                            string index = cb5.SelectedValue.ToString();

                            PlacementName += value + "-" + index + "_";
                        }
                    }
                    if (gb6.Visible == true)
                    {
                        if (cb6.SelectedValue != null)
                        {
                            string value = lbl6.Text.ToString();
                            string index = cb6.SelectedValue.ToString();

                            PlacementName += value + "-" + index + "_";
                        }
                    }
                    if (gb7.Visible == true)
                    {
                        if (cb7.SelectedValue != null)
                        {
                            string value = lbl7.Text.ToString();
                            string index = cb7.SelectedValue.ToString();

                            PlacementName += value + "-" + index + "_";
                        }
                    }
                    if (gb8.Visible == true)
                    {
                        if (cb8.SelectedValue != null)
                        {
                            string value = lbl8.Text.ToString();
                            string index = cb8.SelectedValue.ToString();

                            PlacementName += value + "-" + index + "_";
                        }
                    }
                    if (gb9.Visible == true)
                    {
                        if (cb9.SelectedValue != null)
                        {
                            string value = lbl9.Text.ToString();
                            string index = cb9.SelectedValue.ToString();

                            PlacementName += value + "-" + index + "_";
                        }
                    }
                    if (gb10.Visible == true)
                    {
                        if (cb10.SelectedValue != null)
                        {
                            string value = lbl10.Text.ToString();
                            string index = cb10.SelectedValue.ToString();

                            PlacementName += value + "-" + index + "_";
                        }
                    }
                    if (gb11.Visible == true)
                    {
                        if (cb11.SelectedValue != null)
                        {
                            string value = lbl11.Text.ToString();
                            string index = cb11.SelectedValue.ToString();

                            PlacementName += value + "-" + index + "_";
                        }
                    }
                    if (gb12.Visible == true)
                    {
                        if (cb12.SelectedValue != null)
                        {
                            string value = lbl12.Text.ToString();
                            string index = cb12.SelectedValue.ToString();

                            PlacementName += value + "-" + index + "_";
                        }
                    }
                    if (gb13.Visible == true)
                    {
                        if (cb13.SelectedValue != null)
                        {
                            string value = lbl13.Text.ToString();
                            string index = cb13.SelectedValue.ToString();

                            PlacementName += value + "-" + index + "_";
                        }
                    }
                    if (gb14.Visible == true)
                    {
                        if (cb14.SelectedValue != null)
                        {
                            string value = lbl14.Text.ToString();
                            string index = cb14.SelectedValue.ToString();

                            PlacementName += value + "-" + index + "_";
                        }
                    }
                    if (gb15.Visible == true)
                    {
                        if (cb15.SelectedValue != null)
                        {
                            string value = lbl15.Text.ToString();
                            string index = cb15.SelectedValue.ToString();

                            PlacementName += value + "-" + index + "_";
                        }
                    }
                    if (gb16.Visible == true)
                    {
                        if (cb16.SelectedValue != null)
                        {
                            string value = lbl16.Text.ToString();
                            string index = cb16.SelectedValue.ToString();

                            PlacementName += value + "-" + index + "_";
                        }
                    }
                    if (gb17.Visible == true)
                    {
                        if (cb17.SelectedValue != null)
                        {
                            string value = lbl17.Text.ToString();
                            string index = cb17.SelectedValue.ToString();

                            PlacementName += value + "-" + index + "_";
                        }
                    }
                    if (gb18.Visible == true)
                    {
                        if (cb18.SelectedValue != null)
                        {
                            string value = lbl18.Text.ToString();
                            string index = cb18.SelectedValue.ToString();

                            PlacementName += value + "-" + index + "_";
                        }
                    }
                    if (gb19.Visible == true)
                    {
                        if (cb19.SelectedValue != null)
                        {
                            string value = lbl19.Text.ToString();
                            string index = cb19.SelectedValue.ToString();

                            PlacementName += value + "-" + index + "_";
                        }
                    }

                    #endregion

                    PlacementBreakOutDetail msg = (PlacementBreakOutDetail)itemMessages;
                    PlacementBreakOutDetail size = (PlacementBreakOutDetail)itemSize;

                    PlacementName += lblMessaging.Text + "-" + msg.Abbrev + "_";
                    PlacementName += lblSize.Text + "-" + size.Abbrev + "_";

                    PlacementName = PlacementName.Substring(0, PlacementName.Length - 1);

                    tbPlacements.Text += PlacementName + Environment.NewLine;
                }
            }


        }

        private async void button2_Click(object sender, EventArgs e)
        {
            bool result = true;
            btnProcess.Enabled = false;

            string[] placementNames = tbPlacements.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

            PlacementDataServiceHandler handler = new PlacementDataServiceHandler();

            List<PlacementInfo> colPlacements = new List<PlacementInfo>();

            GetPlacementCollection(result, placementNames, colPlacements);

            PlacementInfo[] processedPlacements = await handler.CreatePlacementsAsync(colPlacements.ToArray());

            if (processedPlacements == null)
                MessageBox.Show("Please contact administrator.");
            else
            {
                ProcessNewPlacements(processedPlacements);

                MessageBox.Show("Placements created successfully.");
                this.Close();
            }

            btnProcess.Enabled = true;
        }

        private void ProcessNewPlacements(PlacementInfo[] placements)
        {
            try
            {
                var selectedDb = new mediaEntities();
                selectedDb.Database.Connection.ConnectionString = Processor.ConnectionString;

                foreach (PlacementInfo placement in placements.Where(x => x.ID > 0))
                {
                    dataSizmekPlacement _placement = new dataSizmekPlacement();
                    _placement.PlacementId = placement.ID;
                    _placement.PlacementName = placement.PlacementName;

                    selectedDb.dataSizmekPlacements.Add(_placement);
                }

                selectedDb.SaveChanges();
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "ProcessNewPlacements");
            }
        }
        private bool GetPlacementCollection(bool result, string[] placementNames, List<PlacementInfo> colPlacements)
        {
            foreach (string placementname in placementNames)
            {
                if (placementname == "")
                    continue;

                PlacementInfo placement = new InBannerPlacementInfo();

                placement = GeneratePlacement(placementname, placement);

                colPlacements.Add(placement);
                result = false;
            }

            return result;
        }

        private PlacementInfo GeneratePlacement(string placementname, PlacementInfo placement)
        {
            int _sizeId = GetBannerSize(placementname);

            switch ((int)placementTypeId)
            {
                case (int)PlacementTypes.InBanner:
                    placement = new InBannerPlacementInfo();
                    (placement as InBannerPlacementInfo).BannerSize = _sizeId;
                    break;
                case (int)PlacementTypes.InGame:
                    placement = new InGamePlacementInfo();
                    break;
                case (int)PlacementTypes.InStreamVideo:
                    placement = new InStreamVideoPlacementInfo();
                    break;
                case (int)PlacementTypes.InStreamVideoTracking:
                    placement = new InStreamVideoTrackingPlacementInfo();
                    break;
                case (int)PlacementTypes.Mobile:
                    placement = new MobilePlacementInfo();
                    break;
                case (int)PlacementTypes.OutOfBanner:
                    placement = new OutOfBannerPlacementInfo();
                    break;
                case (int)PlacementTypes.TrackingType:
                    placement = new TrackingOnlyPlacementInfo();
                    break;
            }

            placement.PlacementName = placementname;
            placement.Status = PlacementStatus.Idle;
            placement.CampaignID = tempPlacement.CampaignID;
            placement.SiteID = tempPlacement.SiteID;
            placement.SiteSectionID = tempPlacement.SiteSectionID;
            placement.Dates = tempPlacement.Dates;
            placement.BookedImpressionsType = tempPlacement.BookedImpressionsType;
            placement.BookedImpressionsValue = tempPlacement.BookedImpressionsValue;
            return placement;
        }

        private int GetBannerSize(string placementName)
        {
            int _sizeId = 0;

            string Abbrev = GetSize(placementName);

            try
            {
                var selectedDb = new mediaEntities();
                selectedDb.Database.Connection.ConnectionString = Processor.ConnectionString;

                PlacementBannerSize banner = (from m in selectedDb.PlacementBannerSizes where m.Abbrev == Abbrev select m).FirstOrDefault();

                if (banner.BannerId < 0)
                {
                    BannerSizeInfo _size = PlacementDataServiceHandler.GetBannerId(banner.Width ?? 0, banner.Height ?? 0);
                    banner.BannerId = _size.ID;
                    selectedDb.SaveChanges();

                    _sizeId = _size.ID;
                }
                else
                    _sizeId = banner.BannerId ?? 0;
            }
            catch (Exception ex)
            {
                // TODO ERROR HANDLING
                throw;
            }

            return _sizeId;
        }
        private string GetSize(string placementName)
        {
            string sizeAbbrev = "";

            string[] colStrings = placementName.Split('_');

            foreach (string colString in colStrings)
            {
                string[] colStr = colString.Split('-');

                if (colStr[0] == "s")
                    return colStr[1];
            }

            return sizeAbbrev;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string placements = tbPlacements.Text;
            Clipboard.SetText(placements);
        }
    }
}
