﻿namespace MediaConsoleApp
{
    partial class Placements
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbBreakout = new System.Windows.Forms.GroupBox();
            this.cbBreakoutChoices = new System.Windows.Forms.ComboBox();
            this.gbBreakout.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbBreakout
            // 
            this.gbBreakout.Controls.Add(this.cbBreakoutChoices);
            this.gbBreakout.Location = new System.Drawing.Point(3, 3);
            this.gbBreakout.Name = "gbBreakout";
            this.gbBreakout.Size = new System.Drawing.Size(506, 62);
            this.gbBreakout.TabIndex = 1;
            this.gbBreakout.TabStop = false;
            this.gbBreakout.Text = "Breakout";
            // 
            // cbBreakoutChoices
            // 
            this.cbBreakoutChoices.FormattingEnabled = true;
            this.cbBreakoutChoices.Location = new System.Drawing.Point(6, 19);
            this.cbBreakoutChoices.Name = "cbBreakoutChoices";
            this.cbBreakoutChoices.Size = new System.Drawing.Size(283, 21);
            this.cbBreakoutChoices.TabIndex = 0;
            // 
            // Placements
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbBreakout);
            this.Name = "Placements";
            this.Size = new System.Drawing.Size(512, 72);
            this.gbBreakout.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbBreakout;
        private System.Windows.Forms.ComboBox cbBreakoutChoices;
    }
}
