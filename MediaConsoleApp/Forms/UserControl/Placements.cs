﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WhirlygigSolutionHelper;


namespace MediaConsoleApp
{
    public partial class Placements : UserControl
    {
        mediaEntities db = new mediaEntities();
        Company company = new Company();
        Data Processor = new Data();

        public Placements(PlacementBreakOut breakOut,Company com,int placementid)
        {
            InitializeComponent();

            var selectedDb = new mediaEntities();

            Data processor = new Data(com.DatabaseName, com.Login, com.Password);

            Processor = processor;

            selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;
             
            gbBreakout.Text = breakOut.Title;

            List<int> detailIds = (from m in selectedDb.PlacementBreakOutDetails where m.PlacementBreakOutId == breakOut.Id select m.Id).ToList();
            PlacementBreakOutDetail[] choices = (from j in selectedDb.PlacementBreakOutDetails where j.PlacementBreakOutId == breakOut.Id select j).ToArray();

            PlacementBreakOutLookup[] lookups = (from f in selectedDb.PlacementBreakOutLookups where f.PlacementId == placementid select f).ToArray();
             
            PlacementBreakOutLookup BreakoutValue;

            foreach (PlacementBreakOutLookup look in lookups)
            {
                if (detailIds.Contains(look.PlacementBreakOutDetailsId ?? 0))
                {
                    BreakoutValue = look;
                }
            }

            cbBreakoutChoices.DataSource = choices;
            cbBreakoutChoices.ValueMember = "Id"; 
            cbBreakoutChoices.DisplayMember = "Title"; 
        }
         
    }
}
