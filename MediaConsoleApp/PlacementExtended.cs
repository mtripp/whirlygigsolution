﻿using SizmekMDXReporting.PlacementService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaConsoleApp
{
    public class PlacementExtended
    {
        public PlacementTypes PlacementTypeEnum { get; set; }
        public PlacementInfo PlacementType { get; set; }
    }
}
