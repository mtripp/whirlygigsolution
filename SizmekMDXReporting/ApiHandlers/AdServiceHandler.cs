﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SizmekMDXReporting.AnalyticsDataService;
using System.Xml;
using System.IO;
using System.Data;
using System.Net;
using System.Data.SqlClient;
using System.Configuration;
using SizmekMDXReporting.AdService;

namespace SizmekMDXReporting
{
    public class AdServiceHandler
    {
        mediaLavidgeEntities db = new mediaLavidgeEntities();

        public void Run()
        {
            using (AuthenticationUtil authenticationUtil = new AuthenticationUtil())
            {
                try
                {
                    AdInfo[] Ads = GetAdInfo(authenticationUtil);

                    if (Ads != null)
                    {
                        UpdateAdList(Ads);
                    }
                }
                catch (Exception ex)
                {
                    SolutionHelper.Error.LogError(ex, "AdServiceHandler.Run");
                }
            }
        }

        private AdInfo[] GetAdInfo(AuthenticationUtil authenticationUtil)
        {
            try
            {
                bool specify = false;
                int count;

                ListPaging Paging = new ListPaging();
                Paging.PageSize = 2;
                Paging.PageIndex = 0;

                AdServiceClient ad = new AdServiceClient();

                AdInfo[] Ads = ad.GetAds(authenticationUtil.UserSecurityToken, null, Paging, false, out count);

                return Ads;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "GetAdInfo");
                return null;
            }
        }

        private void UpdateAdList(AdInfo[] Ads)
        {
            int idx = 0;

            foreach (AdInfo Ad in Ads)
            {
                int id = Ad.ID;
                dataSizmekAd ad = (from m in db.dataSizmekAds where m.Id == id select m).FirstOrDefault();

                if (ad == null)
                {
                    dataSizmekAd a = new dataSizmekAd();
                    a.AdId = Ad.ID;
                    a.AdName = Ad.AdName; 

                    db.dataSizmekAds.Add(a);
                }
                else
                {
                    ad.AdId = Ad.ID;
                    ad.AdName = Ad.AdName;
                }

                if (idx == 25) // Batch trips to the database
                {
                    db.SaveChanges();
                    idx = 0;
                }

                idx++;
            }

            db.SaveChanges();
        }
    }
}
