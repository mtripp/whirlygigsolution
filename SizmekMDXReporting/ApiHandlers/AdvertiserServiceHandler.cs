﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SizmekMDXReporting.AdvertiserService;
using System.Data.Entity;
using SolutionHelper;
using WhirlygigSolutionHelper;
using System.Data;

namespace SizmekMDXReporting
{
    public class AdvertiserServiceHandler
    { 
        mediaLavidgeEntities db = new mediaLavidgeEntities();

        public void Run()
        {
            using (AuthenticationUtil authenticationUtil = new AuthenticationUtil())
            {
                ProcessAdvertisers(authenticationUtil);
                ProcessBrands(authenticationUtil); 
            }
        }

        /// <summary>
        /// Pulling site needs to be run after the analytics process
        /// </summary>
        public void RunProcessSites()
        {
            using (AuthenticationUtil authenticationUtil = new AuthenticationUtil())
            {
                ProcessSites(authenticationUtil);
            }
        }

        private void ProcessAdvertisers(AuthenticationUtil authenticationUtil)
        {
            try
            {
                AdvertiserInfo[] advertisers = GetAdvertisers(authenticationUtil);

                if (advertisers != null)
                {
                    UpdateAdvertisersList(advertisers); 
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "ProcessAdvertisers");
            }
        }

        private void ProcessBrands(AuthenticationUtil authenticationUtil)
        { 
            try
            {
                dataSizmekAdvertiser[] advertisers = (from m in db.dataSizmekAdvertisers where m.CompanyId != null select m).ToArray();

                foreach (dataSizmekAdvertiser advertiser in advertisers)
                {
                    try
                    { 
                        Company com = (from m in db.Companies where m.Id == advertiser.CompanyId && m.IsActive == true select m).FirstOrDefault();

                        if (com == null)
                            continue;

                        BrandInfo[] brands = GetBrands(authenticationUtil, (int)advertiser.AdvertiserId);

                        if (brands != null)
                        {
                            Data processor = new Data(com.DatabaseName, com.Login, com.Password, com.Server);

                            UpdateBrandsList(brands, processor);
                        } 
                        Console.WriteLine(advertiser.AdvertiserName + ": Completed Brands " + brands.Count());
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(advertiser.AdvertiserName + ": Fail - " + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "ProcessBrands");
            }
        }
        private void ProcessSites(AuthenticationUtil authenticationUtil)
        {
            try
            {
                BasicInfo[] sites = GetSites(authenticationUtil);

                if (sites != null)
                {
                    UpdateSitesList(sites);
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "ProcessSites");
            }
        }
        private void ProcessConversionTags(AuthenticationUtil authenticationUtil)
        {
            try
            {
                dataSizmekAdvertiser[] advertisers = (from m in db.dataSizmekAdvertisers where m.CompanyId != null select m).ToArray();

                foreach (dataSizmekAdvertiser advertiser in advertisers)
                {
                    try
                    {
                        Company com = (from m in db.Companies where m.Id == advertiser.CompanyId && m.IsActive == true select m).FirstOrDefault();

                        if (com == null)
                            continue;

                        ConversionTagInfo[] getconversiontags = GetConversionTags(authenticationUtil, (int)advertiser.AdvertiserId);

                        if (getconversiontags != null)
                        {
                            Data processor = new Data(com.DatabaseName, com.Login, com.Password, com.Server); 
                            UpdateConversionTags(getconversiontags, processor);
                        }

                        Console.WriteLine(advertiser.AdvertiserName + ": Completed ConversionTags " + getconversiontags.Count());
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(advertiser.AdvertiserName + ": Fail - " + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "ProcessConversionTags");
            }
        }

        private void UpdateConversionTags(ConversionTagInfo[] conversiontags, Data processor)
        {
            try
            { 
                var selectedDb = new mediaLavidgeEntities();
                selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

                foreach (ConversionTagInfo conversiontag in conversiontags)
                {
                    int id = conversiontag.ID;

                    dataSizmekConversionTag conversionta = (from m in selectedDb.dataSizmekConversionTags where m.ConversionTagId == id select m).FirstOrDefault();

                    if (conversionta == null)
                    {
                        dataSizmekConversionTag conversion = new dataSizmekConversionTag();
                        conversion.AdvertiserId = conversiontag.AdvertiserID;
                        conversion.ConversionTagId = conversiontag.ID;
                        conversion.ConversionTagName = conversiontag.ReportingName;

                        selectedDb.dataSizmekConversionTags.Add(conversion);
                    }
                    else
                    {
                        conversionta.AdvertiserId = conversiontag.AdvertiserID;
                        conversionta.ConversionTagId = conversiontag.ID;
                        conversionta.ConversionTagName = conversiontag.ReportingName;
                    }

                    selectedDb.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                Error.LogError(ex, "UpdateConversionTags:" + processor.DatabaseName);
            }
        }
        private void UpdateBrandsList(BrandInfo[] brands, Data processor)
        { 
            try
            { 
                var selectedDb = new mediaLavidgeEntities();
                selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

                foreach (BrandInfo brand in brands)
                {
                    int id = brand.ID;
                    dataSizmekBrand bran = (from m in selectedDb.dataSizmekBrands where m.BrandId == id select m).FirstOrDefault();

                    if (bran == null)
                    {
                        dataSizmekBrand bra = new dataSizmekBrand();
                        bra.BrandId = brand.ID;
                        bra.BrandName = brand.BrandName;
                        bra.Vertical = brand.Vertical.ToString();
                        bra.AdvertiserId = brand.AdvertiserID;

                        selectedDb.dataSizmekBrands.Add(bra);
                    }
                    else
                    {
                        bran.BrandId = brand.ID;
                        bran.BrandName = brand.BrandName;
                        bran.Vertical = brand.Vertical.ToString();
                        bran.AdvertiserId = brand.AdvertiserID;
                    }

                    selectedDb.SaveChanges(); 
                } 

            }
            catch (Exception ex)
            {
                Error.LogError(ex, "UpdateBrandsList:" + processor.DatabaseName);
            }
        }
        private void UpdateAdvertisersList(AdvertiserInfo[] advertisers)
        {
            int idx = 0;

            foreach (AdvertiserInfo adv in advertisers)
            {
                int id = adv.ID;
                dataSizmekAdvertiser advertiser = (from m in db.dataSizmekAdvertisers where m.AdvertiserId == id select m).FirstOrDefault();

                if (advertiser == null)
                {
                    dataSizmekAdvertiser advs = new dataSizmekAdvertiser();
                    advs.AdvertiserId = adv.ID;
                    advs.AdvertiserName = adv.AdvertiserName;

                    db.dataSizmekAdvertisers.Add(advs);
                }
                else
                {
                    advertiser.AdvertiserId = adv.ID;
                    advertiser.AdvertiserName = adv.AdvertiserName;
                }

                if (idx == 25) // Batch trips to the database
                {
                    db.SaveChanges();
                    idx = 0;
                }

                idx++;
            }

            db.SaveChanges();

            ProcessAdvertisersToIndividualDatabase();
        }
        
        private void ProcessAdvertisersToIndividualDatabase()
        {
          //  string constring = db.Database.Connection.ConnectionString ;

            dataSizmekAdvertiser[] advertisers = (from m in db.dataSizmekAdvertisers where m.CompanyId != null select m).ToArray();
              
            foreach (dataSizmekAdvertiser adv in advertisers)
            {
                try
                {
                    var selectedDb = new mediaLavidgeEntities();
                 
                    Company com = (from m in db.Companies where m.Id == adv.CompanyId && m.IsActive == true select m).FirstOrDefault();

                    if(com != null)
                    {
                        Data processor = new Data(com.DatabaseName, com.Login, com.Password, com.Server);

                        selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

                        dataSizmekAdvertiser advertiser = (from s in selectedDb.dataSizmekAdvertisers where s.AdvertiserId == adv.AdvertiserId select s).FirstOrDefault();

                        if (advertiser == null)
                        {
                            dataSizmekAdvertiser advs = new dataSizmekAdvertiser();
                            advs.AdvertiserId = adv.Id;
                            advs.AdvertiserName = adv.AdvertiserName;

                            selectedDb.dataSizmekAdvertisers.Add(advs);
                        }
                        else
                        {
                            advertiser.AdvertiserId = adv.Id;
                            advertiser.AdvertiserName = adv.AdvertiserName;
                        }

                        selectedDb.SaveChanges(); 
                    } 
                }
                catch (Exception ex)
                {
                    Error.LogError(ex, "ProcessAdvertisersToIndividualDatabase:" + adv.AdvertiserName);
                }
            } 
        }

        private void UpdateSitesList(BasicInfo[] sites)
        {
            int idx = 0;

            DataTable dtSites = GetSiteIntoDataTable(sites);
             
            ReportingData.ProcessSitesReport(dtSites);
        }

        private DataTable GetSiteIntoDataTable(BasicInfo[] sites)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SiteId");
            dt.Columns.Add("SiteName"); 

            foreach (BasicInfo site in sites)
            {
                DataRow row = dt.NewRow();

                row["SiteId"] = site.ID;
                row["SiteName"] = site.DisplayText;
                // add the current row to the DataTable
                dt.Rows.Add(row);
            }

            return dt; 
        }

        private void ProcessSections(dataSizmekSite site)
        {
            Company company = (from m in db.Companies where m.Id == 1 select m).FirstOrDefault();

            PlacementDataServiceHandler.LoadSections(site, company); 
        }

        private BrandInfo[] GetBrands(AuthenticationUtil authenticationUtil, int advId)
        { 
            int count;

            ListPaging Paging = new ListPaging();
            Paging.PageSize = 1000;
            Paging.PageIndex = 0;

            AdvertiserServiceClient advertiser = new AdvertiserServiceClient();

            BrandInfo[] brands = advertiser.GetBrands(authenticationUtil.UserSecurityToken, advId, out count);

            return brands;
        }
        private ConversionTagInfo[] GetConversionTags(AuthenticationUtil authenticationUtil, int advId)
        { 
            int count;

            ListPaging Paging = new ListPaging();
            Paging.PageSize = 1000;
            Paging.PageIndex = 0;

            AdvertiserServiceClient advertiser = new AdvertiserServiceClient();

            ConversionTagInfo[] conversiontags = advertiser.GetConversionTags(authenticationUtil.UserSecurityToken, (UInt32)advId, null, Paging, true, out count);

            return conversiontags;
        }
        private AdvertiserInfo[] GetAdvertisers(AuthenticationUtil authenticationUtil)
        { 
            int count;

            ListPaging Paging = new ListPaging();
            Paging.PageSize = 1000;
            Paging.PageIndex = 0;

            AdvertiserServiceClient advertiser = new AdvertiserServiceClient();

            AdvertiserInfo[] advertisers = advertiser.GetAdvertisers(authenticationUtil.UserSecurityToken, null, Paging, true, out count);



            return advertisers;
        }
        private BasicInfo[] GetSites(AuthenticationUtil authenticationUtil)
        { 
            int count;

            ListPaging Paging = new ListPaging();
            Paging.PageSize = 10000000;
            Paging.PageIndex = 0;

            AdvertiserServiceClient advertiser = new AdvertiserServiceClient();
            GetSitesFilter filter = new GetSitesFilter();
           
             
            BasicInfo[] sites = advertiser.GetSites(authenticationUtil.UserSecurityToken, Paging, null, out count);

            return sites;
        }

    }
}
