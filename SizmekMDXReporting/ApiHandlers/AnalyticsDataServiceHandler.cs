﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SizmekMDXReporting.AnalyticsDataService;
using System.Xml;
using System.IO;
using System.Data;
using System.Net;
using System.Data.SqlClient;
using System.Configuration;
using SolutionHelper;

namespace SizmekMDXReporting
{
    public class AnalyticsDataServiceHandler
    {
        mediaLavidgeEntities db = new mediaLavidgeEntities();

        public void Run()
        {
            using (AuthenticationUtil authenticationUtil = new AuthenticationUtil())
            {
                try
                {

                    dataSizmekAdvertiser[] advertisers = (from m in db.dataSizmekAdvertisers where m.CompanyId != null && m.Active == true select m).OrderByDescending(m => m.AdvertiserName).ToArray();
                     
                    foreach(dataSizmekAdvertiser advertiser in advertisers)
                    {
                        if (advertiser.CompanyId != 4)
                            continue;
                         
                        dataSizmekCampaign[] campaigns = (from m in db.dataSizmekCampaigns where m.AdvertiserId == advertiser.AdvertiserId select m).ToArray();

                        if (campaigns != null && campaigns.Count() != 0)
                        { 
                            GetAnalyticsReport(authenticationUtil, campaigns);
                        } 
                    }

                            GetAnalyticsConversionDataReport(authenticationUtil);
                }
                catch (Exception ex)
                {
                    SolutionHelper.Error.LogError(ex, "AnalyticsDataServiceHandler.Run");
                }
            }
        }

        private void GetAnalyticsConversionDataReport(AuthenticationUtil authenticationUtil)
        {
            dataSizmekCampaign[] campaigns = (from m in db.dataSizmekCampaigns select m).ToArray();

            // TODO ISOLATE CAMPAIGNS

            foreach (dataSizmekCampaign campaign in campaigns)
            {
                try
                {
                    dataSizmekConversionTag[] conversionTags = (from m in db.dataSizmekConversionTags where m.AdvertiserId == campaign.AdvertiserId select m).ToArray();

                    if (conversionTags != null)
                    {
                        foreach (dataSizmekConversionTag conversionTag in conversionTags)
                        {
                            ProcessAnalyticsConversionTagsDataReport(authenticationUtil, campaigns, conversionTag);
                        }
                    }
                }
                catch (Exception ex)
                {
                    SolutionHelper.Error.LogError(ex, "GetAnalyticsConversionDataReport:");
                }
            }
        }

        private void GetAnalyticsReport(AuthenticationUtil authenticationUtil, dataSizmekCampaign[] campaigns)
        {
            DateTime yesterday = System.DateTime.Now.AddDays(-365);
            int idx = 1;

            foreach (dataSizmekCampaign campaign in campaigns)
            { 
                try
                {
                    //if (campaign.CampaignStatus != "CampaignIsRunning")
                    //    continue;

                    if (campaign.CampaignName.Contains("DONTUSE"))
                        continue;

                    if (!campaign.CampaignName.ContainsAny("ASU WPC_Draper U", "ASU WP SM_FY16-17")) // Testing
                        continue;
                     
                    AnalyticsDataServiceClient client = new AnalyticsDataServiceClient();

                    ReportBase baseReport = new ReportBase();

                    DynamicPerformanceReport report = new DynamicPerformanceReport();
                    report.CampaignID  = campaign.CampaignId.ToString();

                    //     PerformanceReport report = new PerformanceReport();
                    //     report.CampaignID = (int)campaign.CampaignId;

                    report.ReportStartDate = new AnalyticsDataService.APIDateTime();

                    report.ReportStartDate.Year = yesterday.Year;

                    report.ReportStartDate.Month = yesterday.Month;

                    report.ReportStartDate.Day = yesterday.Day;

                    report.ReporEndtDate = new AnalyticsDataService.APIDateTime();

                    report.ReporEndtDate.Year = yesterday.Year;

                    report.ReporEndtDate.Month = yesterday.Month;

                    report.ReporEndtDate.Day = yesterday.Day;

                    baseReport = report;
                     
                    client.InitiateReportJob(authenticationUtil.UserSecurityToken, ref baseReport);

                    AnalyticsDataService.JobStatus status = client.GetReportJobStatus(authenticationUtil.UserSecurityToken, baseReport);

                    int counter = 1;

                    while (status != AnalyticsDataService.JobStatus.Completed)
                    {
                        status = client.GetReportJobStatus(authenticationUtil.UserSecurityToken, baseReport);
                        Console.WriteLine(campaign.CampaignName + ": " + status.ToString() + " Attempt - " + counter.ToString());
                        counter++;
                    }

                    string url = client.GetReportAsURL(authenticationUtil.UserSecurityToken, baseReport);

                    Console.WriteLine(campaign.CampaignName + ": " + idx.ToString() + " of " + campaigns.Count().ToString());

                    ExtractReport(url);
                    idx++;
                }
                catch (Exception ex)
                {
                    SolutionHelper.Error.LogError(ex, "GetAnalyticsReport: " + campaign.CampaignName + " - " + campaign.CampaignId.ToString());
                }
            }
        }

        private void ProcessAnalyticsConversionTagsDataReport(AuthenticationUtil authenticationUtil, dataSizmekCampaign[] campaigns, dataSizmekConversionTag conversionTag)
        {
            DateTime yesterday = System.DateTime.Now.AddDays(-1);

            foreach (dataSizmekCampaign campaign in campaigns)
            {
                try
                {

                    AnalyticsDataServiceClient client = new AnalyticsDataServiceClient();

                    ReportBase baseReport = new ReportBase();

                    PerformanceReport report = new PerformanceReport();

                    report.ConversionTagsIDsFilters = conversionTag.ConversionTagId.ToString();

                    report.CampaignID = (int)campaign.CampaignId;

                    report.ReportStartDate = new AnalyticsDataService.APIDateTime();

                    report.ReportStartDate.Year = yesterday.Year;

                    report.ReportStartDate.Month = yesterday.Month;

                    report.ReportStartDate.Day = yesterday.Day;

                    report.ReporEndtDate = new AnalyticsDataService.APIDateTime();

                    report.ReporEndtDate.Year = yesterday.Year;

                    report.ReporEndtDate.Month = yesterday.Month;

                    report.ReporEndtDate.Day = yesterday.Day;

                    baseReport = report;

                    client.InitiateReportJob(authenticationUtil.UserSecurityToken, ref baseReport);

                    AnalyticsDataService.JobStatus status = client.GetReportJobStatus(authenticationUtil.UserSecurityToken, baseReport);

                    while (status != AnalyticsDataService.JobStatus.Completed)
                    {
                        status = client.GetReportJobStatus(authenticationUtil.UserSecurityToken, baseReport);
                    }

                    string url = client.GetReportAsURL(authenticationUtil.UserSecurityToken, baseReport);

                    ExtractReport(url, (int)conversionTag.ConversionTagId);
                }
                catch (Exception ex)
                {
                    SolutionHelper.Error.LogError(ex, "ProcessAnalyticsConversionTagsDataReport: 1");
                }
            }
        }

        /// <summary>
        /// Convert datatable to Database
        /// </summary>
        /// <param name="dt"></param>
        private void ProcessAnalyticsReport(DataTable dt)
        {
            try
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["LavidgeDatabase"]);

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);

                    bulkCopy.BatchSize = 10000;
                    bulkCopy.BulkCopyTimeout = 500;

                    #region Mappings Region
                    SqlBulkCopyColumnMapping mapping1 = new SqlBulkCopyColumnMapping("PlacementID", "PlacementID");
                    SqlBulkCopyColumnMapping mapping2 = new SqlBulkCopyColumnMapping("CampaignId", "CampaignId");
                    SqlBulkCopyColumnMapping mapping3 = new SqlBulkCopyColumnMapping("AdvertiserId", "AdvertiserId");
                    SqlBulkCopyColumnMapping mapping4 = new SqlBulkCopyColumnMapping("PackageID", "PackageID");
                    SqlBulkCopyColumnMapping mapping5 = new SqlBulkCopyColumnMapping("SiteID", "SiteID");
                    SqlBulkCopyColumnMapping mapping6 = new SqlBulkCopyColumnMapping("DeliveryDate", "DeliveryDate");
                    SqlBulkCopyColumnMapping mapping7 = new SqlBulkCopyColumnMapping("LastUpdatingDate", "LastUpdatingDate");
                    SqlBulkCopyColumnMapping mapping8 = new SqlBulkCopyColumnMapping("Impressions", "Impressions");
                    SqlBulkCopyColumnMapping mapping9 = new SqlBulkCopyColumnMapping("Clicks", "Clicks");
                    SqlBulkCopyColumnMapping mapping10 = new SqlBulkCopyColumnMapping("CTR", "CTR");
                    SqlBulkCopyColumnMapping mapping11 = new SqlBulkCopyColumnMapping("MediaCost", "MediaCost");
                    SqlBulkCopyColumnMapping mapping12 = new SqlBulkCopyColumnMapping("TotalRevenue", "TotalRevenue");
                    SqlBulkCopyColumnMapping mapping13 = new SqlBulkCopyColumnMapping("TotalInteractions", "TotalInteractions");
                    SqlBulkCopyColumnMapping mapping14 = new SqlBulkCopyColumnMapping("ImpressionsWithDwell", "ImpressionsWithDwell");
                    SqlBulkCopyColumnMapping mapping15 = new SqlBulkCopyColumnMapping("VisibleImpressions", "VisibleImpressions");
                    SqlBulkCopyColumnMapping mapping16 = new SqlBulkCopyColumnMapping("NonVisibleImpressions", "NonVisibleImpressions");
                    SqlBulkCopyColumnMapping mapping17 = new SqlBulkCopyColumnMapping("TotalConversion", "TotalConversion");
                    SqlBulkCopyColumnMapping mapping18 = new SqlBulkCopyColumnMapping("PostImpressionsConversions", "PostImpressionsConversions");
                    SqlBulkCopyColumnMapping mapping19 = new SqlBulkCopyColumnMapping("PostClickConversions", "PostClickConversions");
                    SqlBulkCopyColumnMapping mapping20 = new SqlBulkCopyColumnMapping("PlacementClassification1", "PlacementClassification1");
                    SqlBulkCopyColumnMapping mapping21 = new SqlBulkCopyColumnMapping("PlacementClassification2", "PlacementClassification2");
                    SqlBulkCopyColumnMapping mapping22 = new SqlBulkCopyColumnMapping("PlacementClassification3", "PlacementClassification3");
                    SqlBulkCopyColumnMapping mapping23 = new SqlBulkCopyColumnMapping("PlacementClassification4", "PlacementClassification4");
                    SqlBulkCopyColumnMapping mapping24 = new SqlBulkCopyColumnMapping("Reserve1", "Reserve1");
                    SqlBulkCopyColumnMapping mapping25 = new SqlBulkCopyColumnMapping("Reserve2", "Reserve2");
                    SqlBulkCopyColumnMapping mapping26 = new SqlBulkCopyColumnMapping("Reserve3", "Reserve3");
                    SqlBulkCopyColumnMapping mapping27 = new SqlBulkCopyColumnMapping("Reserve4", "Reserve4");

                    bulkCopy.ColumnMappings.Add(mapping1);
                    bulkCopy.ColumnMappings.Add(mapping2);
                    bulkCopy.ColumnMappings.Add(mapping3);
                    bulkCopy.ColumnMappings.Add(mapping4);
                    bulkCopy.ColumnMappings.Add(mapping5);
                    bulkCopy.ColumnMappings.Add(mapping6);
                    bulkCopy.ColumnMappings.Add(mapping7);
                    bulkCopy.ColumnMappings.Add(mapping8);
                    bulkCopy.ColumnMappings.Add(mapping9);
                    bulkCopy.ColumnMappings.Add(mapping10);
                    bulkCopy.ColumnMappings.Add(mapping11);
                    bulkCopy.ColumnMappings.Add(mapping12);
                    bulkCopy.ColumnMappings.Add(mapping13);
                    bulkCopy.ColumnMappings.Add(mapping14);
                    bulkCopy.ColumnMappings.Add(mapping15);
                    bulkCopy.ColumnMappings.Add(mapping16);
                    bulkCopy.ColumnMappings.Add(mapping17);
                    bulkCopy.ColumnMappings.Add(mapping18);
                    bulkCopy.ColumnMappings.Add(mapping19);
                    bulkCopy.ColumnMappings.Add(mapping20);
                    bulkCopy.ColumnMappings.Add(mapping21);
                    bulkCopy.ColumnMappings.Add(mapping22);
                    bulkCopy.ColumnMappings.Add(mapping23);
                    bulkCopy.ColumnMappings.Add(mapping24);
                    bulkCopy.ColumnMappings.Add(mapping25);
                    bulkCopy.ColumnMappings.Add(mapping26);
                    bulkCopy.ColumnMappings.Add(mapping27);
                    #endregion

                    bulkCopy.DestinationTableName = "dataSizmekPlacementData";

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 200;
                    conn.Open();

                    bulkCopy.WriteToServer(dt);
                }
            }

            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "SizmekMDXReporting.AnalyticsDataServiceHandler.ProcessAnalyticsReport:" );
            }
        }

        /// <summary>
        /// Convert datatable to Database
        /// </summary>
        /// <param name="dt"></param>
        private void ProcessAnalyticsConversionTagsDataReport(DataTable dt, int conversionTagId)
        {
            try
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["LavidgeDatabase"]);

                dt.Columns.Add("ConversionTagId", typeof(String));

                foreach (DataRow row in dt.Rows)
                    row["ConversionTagId"] = conversionTagId;

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);

                    bulkCopy.BatchSize = 10000;
                    bulkCopy.BulkCopyTimeout = 500;

                    #region Mappings Region
                    SqlBulkCopyColumnMapping mapping1 = new SqlBulkCopyColumnMapping("PlacementID", "PlacementID");
                    SqlBulkCopyColumnMapping mapping2 = new SqlBulkCopyColumnMapping("CampaignId", "CampaignId");
                    SqlBulkCopyColumnMapping mapping3 = new SqlBulkCopyColumnMapping("AdvertiserId", "AdvertiserId");
                    SqlBulkCopyColumnMapping mapping4 = new SqlBulkCopyColumnMapping("PackageID", "PackageID");
                    SqlBulkCopyColumnMapping mapping5 = new SqlBulkCopyColumnMapping("SiteID", "SiteID");
                    SqlBulkCopyColumnMapping mapping6 = new SqlBulkCopyColumnMapping("DeliveryDate", "DeliveryDate");
                    SqlBulkCopyColumnMapping mapping7 = new SqlBulkCopyColumnMapping("LastUpdatingDate", "LastUpdatingDate");
                    SqlBulkCopyColumnMapping mapping8 = new SqlBulkCopyColumnMapping("TotalConversion", "TotalConversion");
                    SqlBulkCopyColumnMapping mapping9 = new SqlBulkCopyColumnMapping("PostImpressionsConversions", "PostImpressionsConversions");
                    SqlBulkCopyColumnMapping mapping10 = new SqlBulkCopyColumnMapping("PostClickConversions", "PostClickConversions");
                    SqlBulkCopyColumnMapping mapping11 = new SqlBulkCopyColumnMapping("ConversionTagId", "ConversionTagId");

                    bulkCopy.ColumnMappings.Add(mapping1);
                    bulkCopy.ColumnMappings.Add(mapping2);
                    bulkCopy.ColumnMappings.Add(mapping3);
                    bulkCopy.ColumnMappings.Add(mapping4);
                    bulkCopy.ColumnMappings.Add(mapping5);
                    bulkCopy.ColumnMappings.Add(mapping6);
                    bulkCopy.ColumnMappings.Add(mapping7);
                    bulkCopy.ColumnMappings.Add(mapping8);
                    bulkCopy.ColumnMappings.Add(mapping9);
                    bulkCopy.ColumnMappings.Add(mapping10);
                    bulkCopy.ColumnMappings.Add(mapping11);
                    #endregion

                    bulkCopy.DestinationTableName = "dataSizmekPlacementConversionTagData";

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 200;
                    conn.Open();

                    bulkCopy.WriteToServer(dt);
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "ProcessAnalyticsConversionTagsDataReport.DataTableToDatabase:");
            }
        }

        void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(String.Format("{0} Rows have been copied.", e.RowsCopied.ToString()));
        }

        private void ExtractReport(string url, int conversiontagid = 0)
        {
            try
            {
                XmlTextReader rssReader = new XmlTextReader(url.ToString());
                XmlDocument rssDoc = new XmlDocument();

                WebRequest wrGETURL;
                wrGETURL = WebRequest.Create(url);

                Stream objStream;
                objStream = wrGETURL.GetResponse().GetResponseStream();
                StreamReader objReader = new StreamReader(objStream, Encoding.UTF8);
                WebResponse wr = wrGETURL.GetResponse();
                Stream receiveStream = wr.GetResponseStream();
                StreamReader reader = new StreamReader(receiveStream, Encoding.UTF8);
                string content = reader.ReadToEnd();
                XmlDocument content2 = new XmlDocument();

                content2.LoadXml(content);
                content2.Save("direct.xml");

                XmlNodeReader xmlReader = new XmlNodeReader(content2);
                DataSet ds = new DataSet();
                ds.ReadXml(xmlReader);


                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count == 1 )
                        {
                            if (conversiontagid > 0)
                                ProcessAnalyticsConversionTagsDataReport(ds.Tables[0], conversiontagid);
                            else
                                ProcessAnalyticsReport(ds.Tables[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "ExtractReport:" + conversiontagid.ToString());
            }
        }
    }
}
