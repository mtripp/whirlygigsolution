﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SizmekMDXReporting.AuthenticationService;
using SolutionHelper;

namespace SizmekMDXReporting
{
    /// <summary>
    /// This class is a utility class to handle authentication for tests
    /// </summary>
    public class AuthenticationUtil : IDisposable
    {
        public string UserSecurityToken { get; set; }

        public AuthenticationUtil(bool debug = false)
            : this(ConfigurationManager.AppSettings["APIUserName"],
                   ConfigurationManager.AppSettings["APIPassword"],
                   ConfigurationManager.AppSettings["APIApplicationKey"], debug)
        {
        }

        /// <summary>
        /// Authenticates the user's credentials on the MediaMind API and creates a session which its 
        /// identifier is kept in the UserSecurityToken property.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="applicationKey"></param>
        public AuthenticationUtil(string username, string password, string applicationKey,bool debug = false)
        {
            try
            {

                //if (bool.Parse(ConfigurationManager.AppSettings["debug"]) || debug)
                //{
                //    username = ConfigurationManager.AppSettings["APIUserNameTEST"];
                //    password = ConfigurationManager.AppSettings["APIPasswordTEST"];
                //    applicationKey = ConfigurationManager.AppSettings["APIApplicationKeyTEST"];
                //} 

                AuthenticationService.AuthenticationService client = new AuthenticationService.AuthenticationService();
                UserSecurityToken = client.ClientLogin(username, password, applicationKey);  

            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "AuthenticationUtil: Password my need to be reset. https://platform.mediamind.com/Eyeblaster.ACM.Web/Login/ForgotPassword.aspx?lang=en-US"); 
            }
        }

        /// <summary>
        /// Clears the current session on the MediaMind API.
        /// </summary>
        public void Dispose()
        {
            AuthenticationService.AuthenticationService client = new AuthenticationService.AuthenticationService();
            client.ClientLogout(UserSecurityToken);
        }


    }
}
