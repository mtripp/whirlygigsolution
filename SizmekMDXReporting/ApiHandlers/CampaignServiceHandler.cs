﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SizmekMDXReporting;
using SizmekMDXReporting.CampaignService;
using WhirlygigSolutionHelper;
using SolutionHelper;

namespace SizmekMDXReporting
{
    public class CampaignServiceHandler
    {
        mediaLavidgeEntities db = new mediaLavidgeEntities();

        public void Run()
        {
            using (AuthenticationUtil authenticationUtil = new AuthenticationUtil())
            {
                try
                {
                    CampaignInfo[] campaigns = GetCampaigns(authenticationUtil);

                    if (campaigns != null)
                    {
                        UpdateCampaignsList(campaigns);
                        UpdateMainCampaignsList(campaigns);
                    }
                }
                catch (Exception ex)
                {
                    SolutionHelper.Error.LogError(ex, "CampaignServiceHandler.Run:");
                }
            }
        }

        private CampaignInfo[] GetCampaigns(AuthenticationUtil authenticationUtil)
        {
            int count;

            ListPaging Paging = new ListPaging();
            Paging.PageSize = 1000;
            Paging.PageIndex = 0;

            CampaignService.CampaignServiceClient campaignServiceClient = new CampaignService.CampaignServiceClient();

            CampaignInfo[] campaignInfoArr = campaignServiceClient.GetCampaigns(authenticationUtil.UserSecurityToken, null, Paging, true, out count);

            return campaignInfoArr;
        }

        private void UpdateCampaignsList(CampaignInfo[] Campaigns)
        {
            int idx = 0;

            dataSizmekAdvertiser[] advertisers = (from m in db.dataSizmekAdvertisers where m.CompanyId != null select m).ToArray();

            foreach (dataSizmekAdvertiser advertiser in advertisers)
            {
                try
                {
                    Company com = (from m in db.Companies where m.Id == advertiser.CompanyId && m.IsActive == true select m).FirstOrDefault();

                    if (com == null)
                        continue;

                    CampaignInfo[] campaigns = Campaigns.Where(x => x.AdvertiserID == advertiser.AdvertiserId).ToArray();

                    Data processor = new Data(com.DatabaseName, com.Login, com.Password, com.Server);

                    var selectedDb = new mediaLavidgeEntities();
                    selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

                    foreach (CampaignInfo campaign in campaigns)
                    {
                        int id = campaign.ID;

                        dataSizmekCampaign campaig = (from m in selectedDb.dataSizmekCampaigns where m.CampaignId == id select m).FirstOrDefault();

                        if (campaig == null)
                        {
                            dataSizmekCampaign campai = new dataSizmekCampaign();
                            campai.CampaignId = campaign.ID;
                            campai.AdvertiserId = campaign.AdvertiserID;
                            campai.BrandId = campaign.BrandID;
                            campai.CampaignStatus = campaign.CampaignStatus.ToString();
                            campai.BookedImpressions = (int)campaign.BookedImpressions;
                            campai.TotalPlacements = (int)campaign.TotalPlacments;
                            campai.ActualImpressions = campaign.CampaignExtendedInfo.ActualImpressions;
                            campai.CampaignName = campaign.Name;

                            selectedDb.dataSizmekCampaigns.Add(campai);
                        }
                        else
                        {
                            campaig.CampaignId = campaign.ID;
                            campaig.AdvertiserId = campaign.AdvertiserID;
                            campaig.BrandId = campaign.BrandID;
                            campaig.CampaignStatus = campaign.CampaignStatus.ToString();
                            campaig.BookedImpressions = (int)campaign.BookedImpressions;
                            campaig.TotalPlacements = (int)campaign.TotalPlacments;
                            campaig.ActualImpressions = campaign.CampaignExtendedInfo.ActualImpressions;
                            campaig.CampaignName = campaign.Name;
                        }

                        if (idx == 25) // Batch trips to the database
                        {
                            selectedDb.SaveChanges();
                            idx = 0;
                        }

                        idx++;
                    }

                    selectedDb.SaveChanges();
                }
                catch (Exception ex)
                {
                    Error.LogError(ex, "UpdateCampaignsList:" + advertiser.AdvertiserName);
                }
            }
        }

        private void UpdateMainCampaignsList(CampaignInfo[] Campaigns)
        {
            int idx = 0;
             
            try
            {
                Company com = (from m in db.Companies where m.Id == 1 select m).FirstOrDefault(); 
                Data processor = new Data(com.DatabaseName, com.Login, com.Password, com.Server);

                var selectedDb = new mediaLavidgeEntities();

                selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

                foreach (CampaignInfo campaign in Campaigns)
                {
                    int id = campaign.ID;

                    dataSizmekCampaign campaig = (from m in selectedDb.dataSizmekCampaigns where m.CampaignId == id select m).FirstOrDefault();

                    if (campaig == null)
                    {
                        dataSizmekCampaign campai = new dataSizmekCampaign();
                        campai.CampaignId = campaign.ID;
                        campai.AdvertiserId = campaign.AdvertiserID;
                        campai.BrandId = campaign.BrandID;
                        campai.CampaignStatus = campaign.CampaignStatus.ToString();
                        campai.BookedImpressions = (int)campaign.BookedImpressions;
                        campai.TotalPlacements = (int)campaign.TotalPlacments;
                        campai.ActualImpressions = campaign.CampaignExtendedInfo.ActualImpressions;
                        campai.CampaignName = campaign.Name;

                        selectedDb.dataSizmekCampaigns.Add(campai);
                    }
                    else
                    {
                        campaig.CampaignId = campaign.ID;
                        campaig.AdvertiserId = campaign.AdvertiserID;
                        campaig.BrandId = campaign.BrandID;
                        campaig.CampaignStatus = campaign.CampaignStatus.ToString();
                        campaig.BookedImpressions = (int)campaign.BookedImpressions;
                        campaig.TotalPlacements = (int)campaign.TotalPlacments;
                        campaig.ActualImpressions = campaign.CampaignExtendedInfo.ActualImpressions;
                        campaig.CampaignName = campaign.Name;
                    }

                    if (idx == 25) // Batch trips to the database
                    {
                        selectedDb.SaveChanges();
                        idx = 0;
                    }

                    idx++;
                }

                selectedDb.SaveChanges();
            }
            catch (Exception ex)
            {
                Error.LogError(ex, "UpdateMainCampaignsList:");
            }
        }

    }
}
