﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SizmekMDXReporting.AnalyticsDataService;
using System.Xml;
using System.IO;
using System.Data;
using System.Net;
using System.Data.SqlClient;
using System.Configuration;
using SizmekMDXReporting.PlacementService;
using WhirlygigSolutionHelper;

namespace SizmekMDXReporting
{
    public class PlacementDataServiceHandler
    { 
        mediaLavidgeEntities db = new mediaLavidgeEntities();
        Company company;
        Data Processor = new Data();

        public void Run()
        {
            using (AuthenticationUtil authenticationUtil = new AuthenticationUtil())
            {
                try
                {
                    PlacementInfo[] placements = GetPlacementInfo(authenticationUtil);
                      
                    if (placements != null)
                    {
                        ProcessPlacements(placements);
                    }
                }
                catch (Exception ex)
                {
                    SolutionHelper.Error.LogError(ex, "PlacementDataServiceHandler.Run");
                }
            }
        }

        public static void LoadSections(dataSizmekSite site, Company company)
        {
            using (AuthenticationUtil authenticationUtil = new AuthenticationUtil())
            { 
                bool specify = false;
                int count;

                ListPaging Paging = new ListPaging();
                Paging.PageSize = 10000;
                Paging.PageIndex = 0;

                PlacementServiceClient placement = new PlacementServiceClient();

                int SiteID = site.SiteId ?? 0;

                if (SiteID == 0)
                    return;

                SiteSectionInfo[] Sections = placement.GetSiteSections(authenticationUtil.UserSecurityToken, Paging, SiteID, out count);

                ProcessSection(Sections, company);

                mediaLavidgeEntities db = new mediaLavidgeEntities();
                Company lavidge = (from m in db.Companies where m.Id == 1 select m).FirstOrDefault();

                ProcessSection(Sections, lavidge); 
            }
        }

        private static void ProcessSection(SiteSectionInfo[] sections, Company company)
        {
            try
            {
                int idx = 0;

                var selectedDb = new mediaLavidgeEntities();

                Data processor = new Data(company.DatabaseName, company.Login, company.Password,company.Server);

                selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;
                 
                foreach (SiteSectionInfo section in sections)
                {
                    int id = section.ID;

                    dataSizmekSection place = (from m in selectedDb.dataSizmekSections where m.SectionId == id select m).FirstOrDefault();

                    if (place == null)
                    {
                        dataSizmekSection sec = new dataSizmekSection();
                        sec.SectionId = section.ID;
                        sec.SectionName = section.Name;
                        sec.SiteId = section.Site;

                        selectedDb.dataSizmekSections.Add(sec);
                    }
                    else
                    {
                        place.SectionId = section.ID;
                        place.SectionName = section.Name;
                        place.SiteId = section.Site; 
                    }

                    if (idx == 25) // Batch trips to the database
                    {
                        selectedDb.SaveChanges();
                        idx = 0;
                    }
                    idx++;
                }

                selectedDb.SaveChanges();
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "ProcessSection: " + company.CompanyName);
            }
        }

        private void ProcessPlacements(PlacementInfo[] placements)
        {
            try
            {
                dataSizmekAdvertiser[] advertisers = (from m in db.dataSizmekAdvertisers where m.CompanyId != null select m).ToArray();

                foreach (dataSizmekAdvertiser advertiser in advertisers)
                {
                    PlacementInfo[] placmnts = placements.Where(x => x.PlacementExtendedInfo.AdvertiserName == advertiser.AdvertiserName).ToArray();
                     
                    Company com = (from m in db.Companies where m.Id == advertiser.CompanyId && m.IsActive == true select m).FirstOrDefault();

                    if (com == null)
                        continue;
 
                    Data processor = new Data(com.DatabaseName, com.Login, com.Password ,com.Server);
                    UpdatePlacementList(placmnts, processor, advertiser); 
                }
            }
            catch (Exception ex)
            { 
                    SolutionHelper.Error.LogError(ex, "PlacementDataServiceHandler.Run"); 
            }
        }

        private PlacementInfo[] GetPlacementInfo(AuthenticationUtil authenticationUtil)
        { 
            bool specify = false;

            int count;

            ListPaging Paging = new ListPaging();
            Paging.PageSize = 1000;
            Paging.PageIndex = 0;
             
            PlacementServiceClient placement = new PlacementServiceClient();

            List<PlacementInfo> Placements = new List<PlacementInfo>();

            int idx = 0;

            try
            {
                do
                {  
                    List<PlacementInfo> _Placements = placement.GetPlacements(authenticationUtil.UserSecurityToken, Paging, null, true, out count).ToList();

                    Placements.AddRange(_Placements);
                    idx = _Placements.Count();

                    Paging.PageIndex++;

                    Console.WriteLine("GetPlacementInfo.GetPlacements: " + idx + " of " + Placements.Count().ToString());
                } while (idx >= 1000);

                return Placements.ToArray(); // Testing to see which placements are valid
            }
            catch (Exception ex)
            {
                string ignore = "Known error due to permission.      " + ex.Message;
            }

            return null;
        }

        public static PlacementInfo CreatePlacement(PlacementInfo Placement)
        {
            using (AuthenticationUtil authenticationUtil = new AuthenticationUtil())
            {
                try
                { 
                    ListPaging Paging = new ListPaging();
                    Paging.PageSize = 10000;
                    Paging.PageIndex = 0;

                    PlacementServiceFilter filter = new PlacementServiceFilter();

                    PlacementServiceClient placementServiceClient = new PlacementServiceClient();
                    placementServiceClient.CreatePlacement(authenticationUtil.UserSecurityToken, ref Placement);

                    return Placement;
                }
                catch (Exception ex)
                {
                    SolutionHelper.Error.LogError(ex, "CreatePlacement");
                }
            }

            return null;
        }
        public static PlacementInfo[] CreatePlacements(PlacementInfo[] Placements)
        {
            using (AuthenticationUtil authenticationUtil = new AuthenticationUtil())
            {
                try
                { 
                    int count;

                    ListPaging Paging = new ListPaging();
                    Paging.PageSize = 10000;
                    Paging.PageIndex = 0;

                    PlacementServiceFilter filter = new PlacementServiceFilter();

                    PlacementServiceClient placementServiceClient = new PlacementServiceClient();
                    placementServiceClient.CreatePlacements(authenticationUtil.UserSecurityToken, ref Placements, out count);

                    return Placements;
                }
                catch (Exception ex)
                {
                    SolutionHelper.Error.LogError(ex, "CreatePlacements");
                }
            }

            return null;
        }

        public async Task<PlacementInfo[]>  CreatePlacementsAsync(PlacementInfo[] Placements)
        {
            using (AuthenticationUtil authenticationUtil = new AuthenticationUtil())
            {
                try
                {  
                    ListPaging Paging = new ListPaging();
                    Paging.PageSize = 10000;
                    Paging.PageIndex = 0;

                    PlacementServiceFilter filter = new PlacementServiceFilter();

                    PlacementServiceClient placementServiceClient = new PlacementServiceClient();
                    CreatePlacementsRequest cr = new CreatePlacementsRequest(authenticationUtil.UserSecurityToken, Placements);

                    var result =  await placementServiceClient.CreatePlacementsAsync(cr);
                     
                    return result.Placements;
                }
                catch (Exception ex)
                {
                    SolutionHelper.Error.LogError(ex, "CreatePlacements");
                }
            }

            return null;
        } 

        private void UpdatePlacementList(PlacementInfo[] placements, Data processor, dataSizmekAdvertiser advertiser)
        {
            try
            {
                int _idx = 0;
                int idx = 0;

                var selectedDb = new mediaLavidgeEntities();
                selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

                foreach (PlacementInfo placement in placements)
                {
                    int id = placement.ID;
                    dataSizmekPlacement place = (from m in selectedDb.dataSizmekPlacements where m.PlacementId == id select m).FirstOrDefault();

                    if (place == null)
                    {
                        dataSizmekPlacement placemen = new dataSizmekPlacement();
                        placemen.PlacementId = placement.ID;
                        placemen.PlacementName = placement.PlacementName;
                        placemen.PackageName = placement.PlacementExtendedInfo.PlacementPackageName;
                        placemen.SiteId = placement.SiteID;

                        selectedDb.dataSizmekPlacements.Add(placemen);
                    }
                    else
                    {
                        place.PlacementId = placement.ID;
                        place.PlacementName = placement.PlacementName;
                        place.PackageName = placement.PlacementExtendedInfo.PlacementPackageName;
                        place.SiteId = placement.SiteID;
                    }

                    if (idx == 100) // Batch trips to the database
                    {
                        selectedDb.SaveChanges();
                        idx = 0;

                        Console.WriteLine(String.Format("Processing Advertisers for {2}: {0} of {1}", _idx, placements.Count().ToString(), advertiser.AdvertiserName));
                    }

                    idx++;
                    _idx++;
                }

                selectedDb.SaveChanges(); 
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "UpdatePlacementList: " + processor.DatabaseName);
            }
        }


        public static BannerSizeInfo GetBannerId(int _width, int _height)
        {
            PlacementServiceClient placementServiceClient = new PlacementServiceClient();

            using (AuthenticationUtil authenticationUtil = new AuthenticationUtil())
            {
                try
                { 
                    // For example, Banner Size property, we want to set banner size of 10 X 10 on our placement.
                    // we'll get this banner size and set his ID on BannerSize property
                    BannerSizeInfo bannerSizeInfo = placementServiceClient.GetBannerSizeByWidthAndHeight(authenticationUtil.UserSecurityToken, _width, _height);

                    if (bannerSizeInfo != null && bannerSizeInfo.ID > 0) // if we have this banner size on our system - set it
                    {
                        return bannerSizeInfo;
                    }
                    else // we can create new one and then set him.
                    {
                        bannerSizeInfo = new BannerSizeInfo();
                        bannerSizeInfo.Width = _width;
                        bannerSizeInfo.Height = _height;
                        placementServiceClient.CreateBannerSize(authenticationUtil.UserSecurityToken, ref bannerSizeInfo); 
                    }

                    return bannerSizeInfo;
                }
                catch (Exception ex)
                {
                    SolutionHelper.Error.LogError(ex, "GetBannerId");
                }
            }

            return null;
        }
    }
}
