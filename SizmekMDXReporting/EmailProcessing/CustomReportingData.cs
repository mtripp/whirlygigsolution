﻿using OpenPop.Mime;
using OpenPop.Pop3;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using SolutionHelper;
using System.Data.SqlClient;
using System.Configuration;
using WhirlygigSolutionHelper;
using Microsoft.VisualBasic.FileIO;
using System.Data.OleDb;
using System.Text.RegularExpressions;
using System.Data.Linq.SqlClient;
using System.Text;

namespace SizmekMDXReporting
{
    public class CustomReportingData
    {
        mediaLavidgeEntities db = new mediaLavidgeEntities();
        public Data Processor = new Data();
        public Company company;
        private EmailMessage emailMessage;
        public bool deleteEmail;

        string server;
        string accountname;
        string accountpassword;
        string csvExportCompleted;
        string csvExportFailed;
        string csvExportWaiting;
        string csvExport;
        bool debug;
        StringBuilder sbErrorMsg = new StringBuilder();

        string tableName = "dataSizmekDataFeedReportingData";

        public CustomReportingData()
        {
            server = ConfigurationManager.AppSettings["EmailAccountServer"];
            accountname = ConfigurationManager.AppSettings["EmailAccountName"];
            accountpassword = ConfigurationManager.AppSettings["EmailAccountPassword"];
            csvExportCompleted = ConfigurationManager.AppSettings["csvExportCompleted"];
            csvExportFailed = ConfigurationManager.AppSettings["csvExportFailed"];
            csvExport = ConfigurationManager.AppSettings["csvExport"];
            csvExportWaiting = ConfigurationManager.AppSettings["csvExportWaiting"];
            debug = bool.Parse(ConfigurationManager.AppSettings["debug"]);
        }

        public void Run()
        {
            ProcessReports();
        }

        /// <summary>
        /// Example showing:
        ///  - how to fetch all messages from a POP3 server
        /// </summary>
        /// <param name="hostname">Hostname of the server. For example: pop3.live.com</param>
        /// <param name="port">Host port to connect to. Normally: 110 for plain POP3, 995 for SSL POP3</param>
        /// <param name="useSsl">Whether or not to use SSL to connect to server</param>
        /// <param name="username">Username of the user on the server</param>
        /// <param name="password">Password of the user on the server</param>
        /// http://hpop.sourceforge.net/
        /// <returns>All Messages on the POP3 server</returns>
        private void ProcessReports()
        {
            DateTime startTime = System.DateTime.Now;

            restart: // This goto was implemented due to a Pop3Client timeout. Connection to the mail server needs to be reauthenticated between each message

            try
            {
                using (Pop3Client client = new Pop3Client())
                {
                    client.Connect(server, 110, false);
                    client.Authenticate(accountname, accountpassword);

                    int messageCount = client.GetMessageCount();

                    List<Message> allMessages = new List<Message>(messageCount);

                    if (messageCount > 0)
                    {
                        for (int i = messageCount; i > 0; i--)
                        {
                            // Reset all previous 
                            Reset();

                            bool result = false;
                            Message message = client.GetMessage(i);

                            Console.WriteLine("Processing " + i + " of " + messageCount + "  -  " + message.Headers.Subject);
                            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();

                            if (message.Headers.Subject != null)
                            {
                                bool isClustered = false;

                                if (message.Headers.Subject.ContainsAny(ConfigurationManager.AppSettings["EmailSubjectCustom"].Split(','))) // Data Feed from Sizmek
                                {
                                    emailMessage = InitMessage(message);

                                    // Big files often cause timeouts when deleting the email. This allows the next attempt to be deleted faster.
                                    if (!emailMessage.IsProcessed)
                                    { 
                                        //if (!String.IsNullOrEmpty(emailMessage.ErrorMessage))
                                        //    continue;

                                        emailMessage.IsProcessed = ProcessDataFeed(message); 
                                        emailMessage.EndDate = System.DateTime.Now;
                                    }
                                     
                                    if (emailMessage.IsProcessed || deleteEmail)
                                    {
                                        emailMessage.IsProcessed = true;
                                        client.DeleteMessage(i);
                                        goto restart;
                                    }
                                    else
                                    {
                                        emailMessage.IsProcessed = false;
                                        emailMessage.ErrorMessage = sbErrorMsg.ToString();
                                    }

                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (emailMessage != null)
                {
                    emailMessage.EndDate = System.DateTime.Now;
                    emailMessage.IsProcessed = false;
                    emailMessage.ErrorMessage = ex.Message;
                    db.SaveChanges();
                }

                WriteError("SizmekMDXReporting.OpenPopHandler: " + ex.Message.ToString());
            }
        }

        private void WriteError(Exception ex, string error = "")
        {
            if (error != "")
                error += " - ";

            sbErrorMsg.Append(System.DateTime.Now.ToString() + " - " + error + ex.Message + System.Environment.NewLine + System.Environment.NewLine + "--------------------------------------------------------------");

            SolutionHelper.Error.LogError("SizmekMDXReporting.OpenPopHandler: " + ex.Message.ToString());
        }

        private void WriteError(string error)
        {
            if (error != "")
                error += " - ";

            sbErrorMsg.Append(System.DateTime.Now.ToString() + " - " + error + System.Environment.NewLine + System.Environment.NewLine + "--------------------------------------------------------------");


            SolutionHelper.Error.LogError(error);
        }

        public static bool ExportFile(Message message, string location)
        {
            try
            {
                List<MessagePart> attachments = message.FindAllAttachments();

                if (location == "")
                    location = ConfigurationManager.AppSettings["csvExport"];

                int counter = 0;

                foreach (MessagePart attachment in message.FindAllAttachments())
                {
                    counter++;
                    string path = "", ext = "", company = "";
                    bool bolStream = true;
                    Streem stream = new Streem();
                    Streem[] streams = null;
                    string date = System.DateTime.Now.ToString().ReplaceAny("", "/", ":", "AM", "PM").Trim();

                    stream.Stream = new MemoryStream(attachment.Body);

                    if (attachment.FileName.Contains(".zip"))
                        streams = stream.ToStreamFromZip(bolStream);

                    foreach (Streem strStream in streams)
                    {
                        if (strStream.Name.Contains(".tsv"))
                            ext = ".tsv";
                        else if (strStream.Name.Contains(".csv"))
                            ext = ".csv";

                        company = strStream.Name.ToString().Split('_')[1].ToString();

                        path = String.Format(@"{0}\Sizmek Custom Report - {1} {2}{3}{4}", location, company, date, counter, ext);

                        strStream.Stream.ToFileFromStream(path);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "SizmekMDXReporting.ReportingData.ExportFile:");
            }

            return false;
        }



        private void Reset()
        {
            company = null;
            Processor = null;
            deleteEmail = false;
            sbErrorMsg = new StringBuilder();
            emailMessage = null;
        }

        private static bool GetIsClustered(Message message)
        {
            try
            {
                List<MessagePart> attachments = message.FindAllAttachments();

                if (attachments.Count <= 1)
                    return false;

                foreach (MessagePart attachment in attachments)
                {
                    if (attachment.FileName.Contains(".png"))
                        continue;

                    Message _message = new Message(attachment.Body, null);

                    if (_message != null)
                        return true;
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.GetIsClustered: " + ex.Message.ToString());
            }

            return false;
        }

        /// <summary>
        /// This resets the processor,company,deleteEmail bools
        /// </summary>
        private void reset()
        {
            Processor = null;
            company = null;
            deleteEmail = false;
        }

        private bool ProcessDataFeed(Message message)
        {
            try
            {
                Streem stream = new Streem();
                Streem[] streams = null;
                bool result = false;
                bool bolStream = true;

                List<MessagePart> attachments = message.FindAllAttachments();


                stream.Stream = new MemoryStream(attachments[0].Body);

                if (attachments[0].FileName.Contains(".zip"))
                    streams = stream.ToStreamFromZip(bolStream);

                foreach (Streem strStream in streams)
                {
                    DataTable dt = null;

                    reset();

                    Console.WriteLine(strStream.Name);

                    company = GetCompany(strStream.Name);

                    if (company == null)
                    {
                        if (strStream.Name.Contains("McDonald"))
                        {
                            deleteEmail = true;
                            return false;
                        }
                        else
                        continue;
                    }

                    if (company.Id == 4)
                    {
                        deleteEmail = true;
                        return false;
                    }

                    //if (debug) // TESTING
                    //    if (company.CompanyName == "Deslta Dental")
                    //        continue; // bolStream = bolStream;  //        

                    Console.WriteLine("Begin: Convert stream to datatable - " + System.DateTime.Now.ToString());

                    dt = strStream.ToDataTable();

                    Console.WriteLine("Finished: Convert stream to datatable - " + System.DateTime.Now.ToString());

                    if (dt.Rows.Count == 0)
                        continue;

                    if (attachments[0].FileName.Contains(".zip"))
                    {
                        try
                        {
                            string file = "";
                            string ext = "";

                            if (strStream.Name.Contains(".tsv"))
                            {
                                ext = ".tsv";
                                file = dt.ToDataSetExportTsv();
                            }
                            else if (strStream.Name.Contains(".csv"))
                            {
                                ext = ".csv";
                                file = dt.ToDataSetExportCsv();
                            }

                            emailMessage.FileName = @"\Sizmek Custom Report -" + strStream.Name;

                            File.WriteAllText(csvExport + @"\" + emailMessage.MessageId + ext, file.ToString());

                            return true;
                        }
                        catch (Exception ex)
                        {
                            WriteError("SizmekMDXReporting.ProcessDataFeed.ToDataTableCsv Big File" + ex.Message);
                            return false;
                        }
                    }

                    continue;
                }

            }
            catch (Exception ex)
            {
                WriteError("SizmekMDXReporting.ProcessDataFeed.ToDataTableCsv 2" + ex.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Save message to the database.
        /// </summary>
        /// <param name="msg"></param>
        private EmailMessage InitMessage(Message msg)
        {
            EmailMessage message = (from m in db.EmailMessages where m.MessageId == msg.Headers.MessageId select m).FirstOrDefault();

            if (message == null)
            {
                EmailMessage _message = new EmailMessage();

                _message.Date = msg.Headers.Date;
                _message.DateSent = msg.Headers.DateSent;
                _message.MessageId = msg.Headers.MessageId;
                _message.TableName = tableName;
                _message.Subject = msg.Headers.Subject;
                _message.StartDate = System.DateTime.Now;

                db.EmailMessages.Add(_message);

                message = _message;
            }

            db.SaveChanges();

            return message;
        }

        /// <summary>
        /// Save message to the database.
        /// </summary>
        /// <param name="msg"></param>
        private void DELETEMESaveMessage(Message msg)
        {
            EmailMessage message = (from m in db.EmailMessages where m.MessageId == msg.Headers.MessageId select m).FirstOrDefault();

            if (message == null)
            {
                EmailMessage _message = new EmailMessage();

                _message.CompanyId = company.Id;
                _message.Date = msg.Headers.Date;
                _message.DateSent = msg.Headers.DateSent;
                _message.MessageId = msg.Headers.MessageId;
                _message.TableName = tableName;
                _message.Subject = msg.Headers.Subject;
                _message.StartDate = System.DateTime.Now;

                db.EmailMessages.Add(_message);
            }
            else
            {
                message.Company.Id = company.Id;
                message.Date = msg.Headers.Date;
                message.DateSent = msg.Headers.DateSent;
                message.MessageId = msg.Headers.MessageId;
                message.TableName = tableName;
                message.Subject = msg.Headers.Subject;
                message.StartDate = System.DateTime.Now;
            }

            db.SaveChanges();
        }

        public void ProcessDataFeed(FileInfo file)
        {
            DataTable dt = GetDataTabletFromCSVTSVFile(file.FullName);

            Console.ForegroundColor = ConsoleColor.White;

            foreach(DataColumn dc in dt.Columns)
            {
                int counter = 0;

                foreach (DataRow dr in dt.Rows)
                {
                    if (dr[dc].ToString().Length > counter)
                        counter = dr[dc].ToString().Length;
                }

                string query = String.Format("ALTER TABLE [dbo].[InfusionsoftDatabaseExportV2] ADD [{0}] varchar ({1})", dc.ColumnName,counter + 5);
                Processor.RunQuery(query);

                Console.WriteLine(dc.ColumnName + " - " + counter.ToString());
            }

             
            bool result = ProcessMessageDataFeed(dt);

            if (result)
            {
                file.MoveTo(csvExportCompleted + @"\" + file.Name.Replace(file.Extension, " ") + System.DateTime.Now.ToString().Replace(":", ".").Replace("/", "") + file.Extension);
            }
            else if (deleteEmail)
            {
                string path = csvExportCompleted + @"\" + file.Name.Replace(file.Extension, " Duplicate ") + System.DateTime.Now.ToString().Replace(":", ".").Replace("/", "") + file.Extension;
                file.MoveTo(path);
            }
            else
            {
                file.MoveTo(csvExportFailed + @"\" + file.Name);
            }
        }

        /// <summary>
        /// Determine if there is a base file that needs to be processed first.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private bool bolBaseClassExist(string name)
        {
            try
            {
                DirectoryInfo d = new DirectoryInfo(csvExport);

                FileInfo[] infos = d.GetFiles();

                foreach (FileInfo f in infos.Where(m => m.FullName.ContainsAny("Sizmek Daily Search -")).OrderBy(n => n.CreationTime))
                    if (f.FullName.Contains("Base - "))
                        return false;

            }
            catch (Exception ex)
            {
                WriteError("SizmekMDXReporting.DailySearchProcessing.bolBaseClassExist:" + name + " - " + ex.Message);
                return false;
            }

            return true;
        }

        public static DataTable GetDataTabletFromCSVTSVFile(string csv_file_path)
        {
            if (csv_file_path.Contains(".csv"))
                return GetDataTabletFromFile(csv_file_path, "\t");
            if (csv_file_path.Contains(".tsv"))
                return GetDataTabletFromFile(csv_file_path, "\t");
            if (csv_file_path.Contains(".xls"))
                return GetDataTabletXls(csv_file_path);
            else
                return null;
        }

        public static DataTable GetDataTabletFromFile(string csv_file_path, string delimeter)
        {
            DataTable csvData = new DataTable();
            try
            {
                using (TextFieldParser csvReader = new TextFieldParser(csv_file_path))
                {
                    csvReader.SetDelimiters(new string[] { delimeter });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    string[] colFields = csvReader.ReadFields();

                    if (colFields.Count() == 1)
                    {
                        if (colFields[0].ToString().Contains(","))
                        {
                            csvReader.SetDelimiters(new string[] { "," });
                            colFields = colFields[0].Split(',');
                        }
                        else if (colFields[0].ToString().Contains("\t"))
                        {
                            csvReader.SetDelimiters(new string[] { "\t" });
                            colFields = colFields[0].Split('\t');
                        }
                    }

                    foreach (string column in colFields)
                    {
                        DataColumn datecolumn = new DataColumn(column);
                        datecolumn.AllowDBNull = true;
                        csvData.Columns.Add(datecolumn);
                    }
                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();
                        //Making empty value as null
                        for (int i = 0; i < fieldData.Length; i++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = null;
                            }
                        }
                        csvData.Rows.Add(fieldData);
                    }
                }
            }
            catch (Exception)
            {
            }
            return csvData;
        }

        public bool ProcessMessageDataFeed(DataTable dt)
        {
            ColumnChecker(dt);

            CleanDataFeed(dt);

            bool processed = ProcessAnalyticsReport(dt);

            return processed;
        }

        private static DataTable GetDataTabletXls(string csv_file_path)
        {
            try
            {
                string sheetName = "Sheet1";

                if (csv_file_path.Contains("Search_Overview"))
                    sheetName = "Submissions";

                DataTable table = new DataTable();
                string strConn = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1;TypeGuessRows=0;ImportMixedTypes=Text\"", csv_file_path);

                using (OleDbConnection dbConnection = new OleDbConnection(strConn))
                {
                    using (OleDbDataAdapter dbAdapter = new OleDbDataAdapter(String.Format("SELECT * FROM [{0}$] WHERE [F4] <> ''", sheetName), dbConnection)) //rename sheet if required!
                        dbAdapter.Fill(table);
                    int rows = table.Rows.Count;
                }

                try
                {
                    DataTable dt_Parsed = new DataTable();
                    String str_Connection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + csv_file_path + ";Extended Properties=\"Excel 12.0;HDR=YES;IMEX=1;\"";

                    // CREATED DB CONNECTION OBJECT TO READ FILE
                    using (System.Data.OleDb.OleDbConnection oledb_Connection = new System.Data.OleDb.OleDbConnection(str_Connection))
                    {
                        // OPEN DB CONNECTION
                        oledb_Connection.Open();

                        // READS FILE DB SCHEMA
                        System.Data.DataTable dt_Schema = oledb_Connection.GetSchema("COLUMNS");

                        // CHECKS THAT DB SCHEMA IS NOT NULL
                        if (dt_Schema != null)
                        {
                            // CREATES DB COMMAND TO READ FILE
                            System.Data.OleDb.OleDbCommand odbc_Command = new System.Data.OleDb.OleDbCommand(String.Format("SELECT * FROM [{0}] WHERE [F4] <> ''",
                                dt_Schema.Rows[15]["TABLE_NAME"].ToString()), oledb_Connection);

                            // SETS DB COMMAND TYPE
                            odbc_Command.CommandType = CommandType.Text;

                            // PARSES FILE ACCORDINGLY
                            new System.Data.OleDb.OleDbDataAdapter(odbc_Command).Fill(dt_Parsed);
                        }

                        // CLOSE DB CONNECTION
                        oledb_Connection.Close();
                    }
                }
                catch (Exception)
                {
                }



                //var connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0};Extended Properties='Excel 8.0;IMEX=1;HDR=NO;TypeGuessRows=0;ImportMixedTypes=Text'", csv_file_path);

                //OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + csv_file_path + @";Extended Properties=""Excel 8.0;IMEX=1;HDR=NO;TypeGuessRows=0;ImportMixedTypes=Text""");

                var connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0; data source={0};Extended Properties='Excel 12.0;IMEX=1;HDR=YES;TypeGuessRows=0;ImportMixedTypes=Text'", csv_file_path);

                //  OleDbConnection connection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + csv_file_path + @";Extended Properties=""Excel 8.0;IMEX=0;HDR=YES;TypeGuessRows=0;ImportMixedTypes=Text""");

                OleDbConnection connection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + csv_file_path + @";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;'");

                OleDbCommand cmd = new OleDbCommand(String.Format("SELECT * FROM [{0}$] WHERE [F4] <> 'Date'", sheetName), connection);
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);


                DataSet ds = new DataSet();
                ds.Tables.Add("xlsImport", "Excel");
                da.Fill(ds, "xlsImport");

                DataTable dt = ds.Tables[0];

                return dt;
            }
            catch (Exception ex)
            {

            }

            return null;
        }

        private Company GetCompany(string _companyString)
        {
            Company[] companys = (from m in db.Companies where m.IsActive == true select m).ToArray();

            foreach (Company company in companys)
            {
                if (company.Sizemek_Keyword != null && _companyString.ToLower().ContainsAny(company.Sizemek_Keyword.ToLower().Split(',')))
                {
                    Data processor = new Data();
                    processor.DatabaseName = company.DatabaseName;
                    processor.UserName = company.Login;
                    processor.Password = company.Password;
                    processor.Server = company.Server;

                    Processor = processor;

                    emailMessage.Company = company;

                    return company;
                }
            }

            return null;
        }

        /// <summary>
        /// Check if all the columns are created in the database for the reporting table. If the column doesnt exist it will be added.
        /// </summary>
        /// <param name="dt"></param>
        private void ColumnChecker(DataTable dt)
        {
            Console.WriteLine("Begin: Column Checker - " + System.DateTime.Now.ToString());

            try
            {
                DateTime today = System.DateTime.Now;

                string[] columnCol = GetColumnsFromDatabase();

                foreach (DataColumn dc in dt.Columns)
                {
                    if (dc.ColumnName.Contains("* "))
                    {
                        dc.ColumnName = dc.ColumnName.Replace("* ", "");
                    }
                    if (dc.ColumnName.Contains("_"))
                    {
                        dc.ColumnName = dc.ColumnName.Replace("_", " ");
                    }

                    string col = dc.ColumnName.ToLower().Trim();

                    if (col.ToLower() == "date" || col.ToLower() == "asofdate")
                    {
                        dc.ColumnName = "AsOfDate";
                        continue;
                    }
                    if (dc.ColumnName.ToLower() == "modifieddate")
                    {
                        dc.ColumnName = "ModifiedDate";
                        continue;
                    }

                    dc.ColumnName = dc.ColumnName.ToProperCase();

                    if (!columnCol.Contains(col))
                    {
                        if (dc.ColumnName.Length > 150)
                            throw new Exception("Column too large");

                        AddColumnDataFeed(tableName, dc.ColumnName);
                    }
                }

                if (!dt.Columns.Contains("ModifiedDate"))
                {
                    System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                    newColumn.DefaultValue = today.ToString();
                    dt.Columns.Add(newColumn);
                }

                bool bolChecker = DebugHelper.CheckColumnsAgainstDatabase(Processor, dt, tableName);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                throw ex;
            }

            Console.WriteLine("Finished: Column Checker - " + System.DateTime.Now.ToString());
        }

        private string[] GetColumnsFromDatabase()
        {
            string query = "";

            query += "IF Object_id('tempdb..#list') IS NOT NULL   DROP TABLE #list ";
            query += "DECLARE @columns varchar(max) ";
            query += String.Format("SELECT COLUMN_NAME into #list FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{0}' order by COLUMN_NAME ", tableName);
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(max), COLUMN_NAME) FROM (select COLUMN_NAME from #list) as tab select @columns ";
            query += "IF Object_id('tempdb..#list') IS NOT NULL   DROP TABLE #list ";

            DataSet ds = Processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString();

            string[] columnCol = columns.ToLower().Split(',').Select(x => x.Trim()).ToArray();

            return columnCol;
        }

        /// <summary>
        /// Clean the data based on the column type define in the AddColumn method 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private DataTable CleanDataFeed(DataTable dt)
        {
            Console.WriteLine("Begin: Cleaning data - " + System.DateTime.Now.ToString());

            DateTime starttime = System.DateTime.Now;
            string column = "";

            int idx = 0;

            try
            {
                foreach (DataRow dr in dt.Rows)
                {
                    try
                    {
                        idx++;

                        foreach (DataColumn col in dt.Columns)
                        {
                            column = col.ColumnName + ": " + dr[col.ColumnName].ToString();

                            WhirlygigSolutionHelper.ConsoleWriter.WriteCurrentLine(String.Format("Cleaning Row {0} of {1}", idx, dt.Rows.Count));

                            try
                            {
                                if (col.ColumnName.ContainsAny("ModifiedDate", "AsOfDate", "Advertiser Name", "Campaign Name", "Campaign Id", "Site Name", "Site Id", "Package Name", "Placement Name", "Placement Id", "Ad Name", "Ad Id", "State", "City", "Dma", "Zip", "Device Type Name", "Entity Type"))
                                    continue;


                                if (col.ColumnName.ToUpper().ContainsAny("CLICKS",
                                   "POST-IMPRESSION CONVERSIONS",
                                   "POST IMP",
                                   "IMPRESSIONS",
                                   "UNIQUE USERS",
                                   "POST-CLICK CONVERSIONS",
                                   "POST CLICK CON",
                                   "TOTAL CONVERSIONS",
                                   "TOTALCON",
                                   "VIDEO STARTED",
                                   "VIEWABLE IMPRESSIONS",
                                   "VIDEO FULL PLAY",
                                   "VIDEO PLAYED 25",
                                   "VIDEO PLAYED 50",
                                   "Section Totalco".ToUpper(),
                                   "Section Post Im".ToUpper(),
                                   "Section Post Cl".ToUpper(),
                                   "VIDEO PLAYED 75"))
                                {
                                    string apr = dr[col.ColumnName].ToString();

                                    try
                                    {

                                        if (String.IsNullOrEmpty(dr[col.ColumnName].ToString()) || dr[col.ColumnName].ToString().ToUpper() == "NULL")
                                            dr[col.ColumnName] = 0;

                                        if (dr[col.ColumnName].ToString().Contains(","))
                                            dr[col.ColumnName] = dr[col.ColumnName].ToString().Replace(",", "");

                                        dr[col.ColumnName] = decimal.ToInt32(decimal.Parse(apr));
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine("Error Message: " + ex.Message);
                                        throw;
                                    }
                                }

                                if (col.ColumnName.ToUpper().ContainsAny("VIEWABILITY RATE", "VIDEO FULLY PLAYED RATE"))
                                {
                                    if (String.IsNullOrEmpty(dr[col.ColumnName].ToString()) || dr[col.ColumnName].ToString().ToUpper() == "NULL")
                                        dr[col.ColumnName] = 0;
                                    else if (dr[col.ColumnName].ToString().ContainsAny(",", "%"))
                                        dr[col.ColumnName] = dr[col.ColumnName].ToString().Replace(",", "").Replace("%", "");
                                }

                                column = "";
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(column);
                                string msg = ex.Message;
                                throw;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        throw;
                    }
                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }

            Console.WriteLine("Finished: Cleaning data - " + System.DateTime.Now.ToString());

            return dt;
        }

        /// <summary>
        /// Due to the columns needing to be dynamic this method is designed to add additional column to the table
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        private void AddColumnDataFeed(string table, string column)
        {
            bool processColumn = false;

            column = column.ToProperCase();

            string query = "ALTER TABLE {0} ADD [{1}] {2}";

            if (column.ToUpper().ContainsAny("CLICKS",
                "POST-IMPRESSION CONVERSIONS",
                "POST IMP",
                "POST-CLICK CONVERSIONS",
                "POST CLICK CON",
                "TOTAL CONVERSIONS",
                "TOTALCON",
                "VIDEO STARTED",
                "VIDEO FULL PLAY"))
            {
                query = string.Format(query, table, column, "[int] NULL");
                processColumn = true;
            }
            else if (column.ToUpper().ContainsAny("VIEWABILITY RATE", "VIDEO FULLY PLAYED RATE"))
            {
                query = string.Format(query, table, column, "decimal(21, 8) NULL");
                processColumn = true;
            }
            else if (column.ToUpper().ContainsAny("VIDEO PLAYED 25", "VIDEO PLAYED 50", "VIDEO PLAYED 75"))
            {
                query = string.Format(query, table, column, "[int] NULL");
                processColumn = true;
            }
            else
                query = string.Format(query, table, column, "[nvarchar](250)");

            Processor.RunQuery(query);

            if (processColumn && company.Id == 4)
                AddColumnToView(table, column);
        }

        private void AddColumnToView(string table, string column)
        {
            string addQuery = string.Format(" SUM([{0}]) AS [{0}_TEMP],", column);
            string query = "select object_definition(object_id) Name from sys.objects where type='v' and NAME = 'Tools_100_ServerCustom' ";

            DataSet results = Processor.RunQuery(query);

            string _result = results.Tables[0].Rows[0][0].ToString().Replace("CREATE VIEW ", "ALTER VIEW ");

            var regex = new Regex(Regex.Escape("ModifiedDate"));
            var newText = regex.Replace(_result, addQuery, 1);


            Processor.RunQuery(_result);

        }

        private bool ProcessAnalyticsReport(DataTable dt)
        {
            bool processed = false;
            DateTime today = System.DateTime.Now.Date;
            if (!dt.Columns.Contains("AsOfDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("AsOfDate", typeof(System.DateTime));
                newColumn.DefaultValue = today.ToString();
                dt.Columns.Add(newColumn);
            }
            DateTime[] reports = GetReportDates();

             



            DataSet ds = dt.ToDataSetDateSplit();

            foreach (DataTable datatab in ds.Tables)
            {
                DateTime tablename = DateTime.Parse(datatab.TableName.ToString());
                if (reports.Count() > 0)
                {
                    if (reports.Contains(tablename))
                    {
                        bool comparedata = CompareReportDataAgainstDatabaseData(datatab);

                        if (comparedata)
                            deleteEmail = true;
                        else
                        {
                            deleteEmail = false;
                            return false;
                        }

                        continue;
                    }
                    else if (tablename == today)
                    {
                        continue;
                    }
                    else
                    {
                        deleteEmail = false;
                    }
                }

                processed = ProcessSizmekDatatable(datatab);

                if (company.CompanyName == "Arizona State University")
                    if (tableName.Contains("DataFeed"))
                        processed = ProcessGeoSummaryData(datatab);

                if (!processed)         // If at least one insert was improper than all need to be reviewed
                    return processed;
            }

            return processed;
        }

        /// <summary>
        /// Start the process of aggregatting the data. This kicks off a sql job that runs every hour
        /// </summary>
        /// <param name="datatab"></param>
        /// <returns></returns>
        private bool ProcessGeoSummaryData(DataTable datatab)
        {
            string query = String.Format("[dbo].[spUpdateSizmekServerGeo] @Date = '{0}',  @Table = '{1}' ", datatab.TableName, tableName);

            try
            {

                Processor.RunQuery(query);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }

            return true;
        }

        /// <summary>
        /// This is to compare the total records and the sum of the impressions against the database. 
        /// </summary>
        /// <param name="datatab"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private bool CompareReportDataAgainstDatabaseData(DataTable datatab)
        {
            try
            {
                string query = String.Format(@"SELECT count(*) Total, sum([Impressions]) SumImp FROM [dbo].[{0}] where AsOfDate =  '{1}'", tableName, datatab.TableName);

                if (Processor.DatabaseName == "mediaArizonaPublicService")
                {
                    List<string> ids = new List<string>();

                    foreach (DataRow dr in datatab.Rows)
                    {

                    }
                }

                DataSet ds = Processor.RunQuery(query);

                if (ds != null)
                {
                    if (ds.Tables.Count != 0)
                    {
                        int _sumImp = 0;
                        int total = int.Parse(ds.Tables[0].Rows[0]["Total"].ToString());
                        int sumImp = int.Parse(ds.Tables[0].Rows[0]["SumImp"].ToString());

                        foreach (DataRow dr in datatab.Rows)
                        {
                            _sumImp += int.Parse(dr["Impressions"].ToString());
                        }

                        int _total = datatab.Rows.Count;

                        if (total == _total && sumImp == _sumImp)
                            return true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }

        private DateTime[] GetReportDates()
        {
            List<DateTime> dates = new List<DateTime>();

            string query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[{0}] where [AsOfDate] is not null", tableName);

            DataSet ds = Processor.RunQuery(query);

            if (ds != null) // Add date to list of date
                if (ds.Tables.Count != 0) // Add date to list of date
                    foreach (DataRow dr in ds.Tables[0].Rows)
                        dates.Add(DateTime.Parse(dr[0].ToString())); // Date is a non nullable date type

            return dates.ToArray();
        }

        private bool ProcessSizmekDatatable(DataTable dt)
        {
            try
            {
                bool bolChecker = DebugHelper.CheckColumnsAgainstDatabase(Processor, dt, tableName);

                Console.WriteLine("CustomReportingData: Started " + Processor.DatabaseName + dt.TableName + System.DateTime.Now.ToString());
                SqlConnection conn = new SqlConnection(Processor.ConnectionString);

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);

                    bulkCopy.BatchSize = 200000;
                    bulkCopy.BulkCopyTimeout = 5000;

                    foreach (DataColumn dc in dt.Columns)
                    {
                        SqlBulkCopyColumnMapping mapping = new SqlBulkCopyColumnMapping(dc.ColumnName, dc.ColumnName);



                        if (dc.ColumnName != "Idx")
                        {
                            bulkCopy.ColumnMappings.Add(mapping);
                            Console.WriteLine(dc.ColumnName + ": " + dc.DataType.ToString());
                        }
                        else
                            bolChecker = bolChecker;
                    }
                     
                    bulkCopy.DestinationTableName = tableName;

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 200000;
                    conn.Open();

                    bulkCopy.WriteToServer(dt);

                    Console.WriteLine("CustomReportingData: Finished " + Processor.DatabaseName + dt.TableName + System.DateTime.Now.ToString());

                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("CustomReportingData: ERROR " + Processor.DatabaseName + dt.TableName + System.DateTime.Now.ToString());
                WriteError(ex, "CustomReportingData.ProcessSizmekDatatable:" + Processor.DatabaseName);
            }

            return false;
        }

        private void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(String.Format("{0} Rows have been copied.", e.RowsCopied.ToString()));
        }
    }
}

