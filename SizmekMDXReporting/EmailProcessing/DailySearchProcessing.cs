﻿using OpenPop.Mime;
using OpenPop.Pop3;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using SolutionHelper;
using System.Data.SqlClient;
using System.Configuration;
using WhirlygigSolutionHelper;
using System.Data.OleDb;
using System.Text.RegularExpressions;
using System.Net;
using System.Text;

namespace SizmekMDXReporting.EmailProcessing
{
    public class DailySearchProcessing
    {
        mediaLavidgeEntities db = new mediaLavidgeEntities();

        public Company company;
        private EmailMessage emailMessage;
        public Data Processor = new Data();
        public bool deleteEmail;

        string server;
        string accountname;
        string accountpassword;
        string tablename = "dataSizmekSearchReportingData";
        string tablenameStage = "dataSizmekSearchReportingData_Staging";

        string csvExportCompleted;
        string csvExportFailed;
        string csvExportWaiting;
        string csvExport;

        bool duplicate;
        bool _override = false;

        public DailySearchProcessing()
        {
            server = ConfigurationManager.AppSettings["EmailAccountServer"];
            accountname = ConfigurationManager.AppSettings["EmailAccountName"];
            accountpassword = ConfigurationManager.AppSettings["EmailAccountPassword"];
            csvExportCompleted = ConfigurationManager.AppSettings["csvExportCompleted"];
            csvExportFailed = ConfigurationManager.AppSettings["csvExportFailed"];
            csvExport = ConfigurationManager.AppSettings["csvExport"];
            csvExportWaiting = ConfigurationManager.AppSettings["csvExportWaiting"];
        }

        #region Public Extract Methods  

        public void Run()
        {
            ProcessSearchReports();
        }
        private void ProcessSearchReports()
        {
            string status = "";

            try
            {
                restart: // This goto was implemented due to a Pop3Client timeout. Connection to the mail server needs to be reauthenticated between each message

                do // LOOP AND REFRESH POP3 CLIENT
                {
                    DateTime startTime = System.DateTime.Now;

                    status = "Keep running";
                    Console.WriteLine(status);

                    using (Pop3Client client = new Pop3Client())
                    {
                        client.Connect(server, 110, false);
                        client.Authenticate(accountname, accountpassword);

                        int messageCount = client.GetMessageCount();

                        List<Message> allMessages = new List<Message>(messageCount);

                        if (messageCount == 0)
                            return;

                        for (int i = messageCount; i > 0; i--)
                        {
                            if (i == 1)
                                status = "Stop"; // Once the last email passes status should remain "Stop"
                            else
                                status = "Keep running";
                            Console.WriteLine("Processing " + i + " of " + messageCount);
                            bool result = false;

                            Message message = client.GetMessage(i);

                            if (message.Headers.Subject == null)
                                continue;

                            if (message.Headers.Subject.ContainsAny(ConfigurationManager.AppSettings["EmailSubjectSearch"].Split(',')))
                            {
                                Reset();

                                emailMessage = InitMessage(message);

                                result = ExportSearch(message);

                                if (result || deleteEmail)
                                {
                                    client.DeleteMessage(i);
                                    goto restart;
                                }
                            }

                            TimeSpan span = System.DateTime.Now - startTime;

                            if (span.TotalMinutes > 3)
                                break;
                        }
                    }
                } while (status == "Keep running"); // DONT STOP UNLESS ERROR
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.OpenPopHandler: " + ex.Message.ToString());
            }
        }

        private bool ExportSearch(Message message)
        {
            bool result = false;

            Console.WriteLine("Processing: " + message.Headers.Subject.ToString());

            try
            {
                result = ExportFile(message);

                Console.WriteLine("Finished: " + message.Headers.Subject.ToString());

                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in processing: " + message.Headers.Subject.ToString() + ex.Message.ToString());
                SolutionHelper.Error.LogError(ex, "SizmekMDXReporting.ProcessAnalytics:");
            }

            return result;
        }

        /// <summary>
        /// Save message to the database.
        /// </summary>
        /// <param name="msg"></param>
        private EmailMessage InitMessage(Message msg)
        {
            EmailMessage message = (from m in db.EmailMessages where m.MessageId == msg.Headers.MessageId select m).FirstOrDefault();

            if (message == null)
            {
                EmailMessage _message = new EmailMessage();

                _message.Date = msg.Headers.Date;
                _message.DateSent = msg.Headers.DateSent;
                _message.MessageId = msg.Headers.MessageId;
                _message.TableName = tablename;
                _message.Subject = msg.Headers.Subject;
                _message.StartDate = System.DateTime.Now;

                db.EmailMessages.Add(_message);

                message = _message;
            }

            db.SaveChanges();

            return message;
        }

        private bool ExportFile(Message message)
        {
            List<MessagePart> attachments = message.FindAllAttachments();

            if (attachments.Count == 0)
                return ExportMesaageLink(message);
            else
                return ExportMessageAttachment(message);

        }

        private bool ExportMesaageLink(Message message)
        {
            try
            {
                string url = GetUrlFromDownload(message);

                if (!String.IsNullOrEmpty(url))
                {
                    return ExtractReport(url, message.Headers.Subject.ToString());
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "SizmekMDXReporting.ProcessMessage:");
            }

            return false;
        }

        /// <summary>
        /// TODO: Soon to be obsolete
        /// </summary>
        /// <param name="url"></param>
        /// <param name="subject"></param>
        /// <returns></returns>
        public bool ExtractReport(string url, string subject)
        {
            string ext = "";
            Streem streem = new Streem();

            try
            {
                WebRequest wrGETURL;
                wrGETURL = WebRequest.Create(url);


                if (subject.ContainsAny("SearchConnect1Search", "SearchConnect1 Report"))
                    ext = "Base - ";
                else if (subject.ContainsAny("SearchConnect2Search", "SearchConnect2 Report"))
                    ext = "Addendum_1 - ";
                else if (subject.ContainsAny("SearchConnect3Search", "SearchConnect3 Report"))
                    ext = "Addendum_2 - ";
                else if (subject.ContainsAny("SearchConnect4Search", "SearchConnect4 Report"))
                    ext = "Addendum_3 - ";
                else if (subject.ContainsAny("SearchConnect5Search", "SearchConnect5 Report"))
                    ext = "Addendum_4 - ";
                else
                    ext = "";


                Stream objStream;
                var objNew = wrGETURL.GetResponse();
                objStream = wrGETURL.GetResponse().GetResponseStream();
                StreamReader objReader = new StreamReader(objStream, Encoding.UTF8);
                WebResponse wr = wrGETURL.GetResponse();
                streem.Stream = wr.GetResponseStream();

                string fileLocation = csvExport + @"\Sizmek Daily Search - " + ext;

                streem.ToStreamFromZip(fileLocation);

                //    File.Copy(fileLocation+streem.Name, fileLocation.Replace("Base - ",emailMessage.MessageId));

                return true;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "ExtractReport DailySearchProcessing");
            }

            return false;
        }


        private bool ExportMessageAttachment(Message message)
        {
            try
            {
                List<MessagePart> attachments = message.FindAllAttachments();
                Stream stream = new MemoryStream(attachments[0].Body);

                SaveFileStream(ConfigurationManager.AppSettings["csvExport"] + @"\Sizmek Search - " + attachments[0].FileName, stream);

                return true;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "SizmekMDXReporting.ProcessMessage:");
            }

            return false;
        }

        private void SaveFileStream(String path, Stream stream)
        {
            var fileStream = new FileStream(path, FileMode.Create, FileAccess.Write);
            stream.CopyTo(fileStream);
            fileStream.Dispose();
        }

        #endregion

        #region Public Processing Methods  
        public void ProcessDatatable(FileInfo file)
        {
            //      Reset();

            if (!bolProcessFile(file))
                return;

            DataTable dt = ExtractDatatable(file);

            dt.TableName = file.Name.Replace(file.Extension, "");

            bool processed = ProcessSearchReport(dt);

            if (processed)
            {
                file.MoveTo(csvExportCompleted + @"\" + file.Name.Replace(file.Extension, " ") + System.DateTime.Now.ToString().Replace(":", ".").Replace("/", "") + file.Extension);
            }
            else if (duplicate || deleteEmail)
            {
                string path = csvExportCompleted + @"\" + file.Name.Replace(file.Extension, " Duplicate ") + System.DateTime.Now.ToString().Replace(":", ".").Replace("/", "") + file.Extension;
                file.MoveTo(path);
            }
            else
            {
                file.MoveTo(csvExportFailed + @"\" + file.Name);
            }
        }
        private bool ProcessSearchReport(DataTable dt)
        {
            ExtractReport(dt);
            ColumnChecker(dt);
            CleanDataFeed(dt);

            bool processed = false;

            if (company.Id == 4)
                processed = ProcessMultipleSearchFilesToDatabase(dt);
            else
                processed = ProcessSearchToDatabase(dt);

            return processed;
        }

        private bool ProcessMultipleSearchFilesToDatabase(DataTable dt)
        {
            bool processed = false;

            string tablenameV2 = ""; // This table was created to fix a duplication issue that is well known.  https://lavidge.atlassian.net/browse/ASU-504
            tablename = "";


            //TODO: dataSizmekSearchReportingData_Addendum* - should be deprecated.


            if (dt.TableName.Contains("Sizmek Daily Search - Base"))
            {
                tablename = "dataSizmekSearchReportingData_Base_1";
                tablenameV2 = "dataSizmekSearchReportingData_File_0";
            }
            else if (dt.TableName.ContainsAny("Sizmek Daily Search - Addendum_1", "Sizmek Daily Search - Addendum1"))
            {
                tablename = "dataSizmekSearchReportingData_Addendum_1";
                tablenameV2 = "dataSizmekSearchReportingData_File_1"; 
            }
            else if (dt.TableName.ContainsAny("Sizmek Daily Search - Addendum_2", "Sizmek Daily Search - Addendum2"))
            {
                tablename = "dataSizmekSearchReportingData_Addendum_2";
                tablenameV2 = "dataSizmekSearchReportingData_File_2";
            }
            else if (dt.TableName.ContainsAny("Sizmek Daily Search - Addendum_3", "Sizmek Daily Search - Addendum3"))
            {
                tablename = "dataSizmekSearchReportingData_Addendum_3";
                tablenameV2 = "dataSizmekSearchReportingData_File_3";
            }
            else if (dt.TableName.ContainsAny("Sizmek Daily Search - Addendum_4", "Sizmek Daily Search - Addendum4"))
            {
                tablename = "dataSizmekSearchReportingData_Addendum_4";
                tablenameV2 = "dataSizmekSearchReportingData_File_4";
            }

            if (tablename == "")   // Verify tablename is renamed
                return false;

            TruncateTable(tablename); 

            ColumnChecker(dt);
             
            ProcessDatatableToDb(dt, tablename); // TODO: This should be depricated

            TruncateTable(tablenameV2);
            processed = ProcessDatatableToDb(dt, tablenameV2);

            if (processed)
                ProcessLastFileCheck();

            return processed;
        }

        /// <summary>
        /// Check if all the files have been uploaded and the max asofdate are all the same
        /// </summary>
        /// <returns></returns>
        private bool ProcessLastFileCheck()
        {
            int idx = 0;

            string query = "EXEC [dbo].[spSizmekSearchLastFileCheck]"; 

            DataSet ds = Processor.RunQuery(query);

            string[] asOfDates = new string[5];

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                string value = dr[0].ToString();

                if (value == "")
                    return false;

                asOfDates[idx] = value;

                idx++;
            }

            if (
                asOfDates[0] == asOfDates[1] &&
                asOfDates[0] == asOfDates[2] &&
                asOfDates[0] == asOfDates[3] &&
                asOfDates[0] == asOfDates[4]
                )
                return ProcessLastFile();
            else
                return false;
        }

        private bool ProcessLastFile()
        {
            string query = "Exec spSizmekSearchCombinedReporting";

            DataSet ds = Processor.RunQuery(query);
            return true;
        }
        private void TruncateTable(string tablename)
        {
            string query = "";
            query += String.Format("Truncate Table [dbo].[{0}] ", tablename);
         
            DataSet ds = Processor.RunQuery(query);
        }

        private bool ProcessSearchToDatabase(DataTable dt)
        {
            bool processed = false;

            DateTime today = System.DateTime.Now.Date;
            DateTime[] reports = GetReportDates();

            DataSet ds = dt.ToDataSetDateSplit();

            foreach (DataTable datatab in ds.Tables)
            {
                Console.WriteLine("Processing: " + datatab.TableName);
                Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();

                DateTime _date = DateTime.Parse(datatab.TableName.ToString());

                if (company.CompanyName == "Arizona State University")
                {

                    // Check that all emails have been accounted for
                    if (reports.Contains(_date))
                        processed = ProcessSizmekStagingDatatable(datatab, tablenameStage);
                    else
                        return false;
                }
                else
                {
                    if (reports.Count() > 0)
                    {
                        if (reports.Contains(_date))
                        {
                            DeleteOldData(datatab);
                        }
                    }

                    processed = ProcessDatatableToDb(datatab, tablename);
                }

                if (!processed)         // If at least one insert was improper than all need to be reviewed
                    return processed;
            }

            return processed;
        }

        private bool ProcessDatatableToDb(DataTable dt, string tableName)
        {
            try
            {
                string[] columnCol = GetColumnsFromDatabase(false);
                SqlConnection conn = new SqlConnection(Processor.ConnectionString);

                bool bolChecker = DebugHelper.CheckColumnsAgainstDatabase(Processor, dt, tablename);

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);

                    bulkCopy.BatchSize = 100000;
                    bulkCopy.BulkCopyTimeout = 500;

                    #region ColumnMapper

                    foreach (DataColumn dc in dt.Columns)
                    {
                        string dbCol = "";

                        if (dc.ColumnName == "Idx")
                            continue;

                        foreach (string c in columnCol)
                        {
                            if (dc.ColumnName.ToLower() == c.ToLower())
                            {
                                dbCol = c;
                                break;
                            }
                        }

                        if (dbCol == "")
                        {
                            throw new Exception("Column not matching: " + dc.ColumnName.ToString());
                        }

                        SqlBulkCopyColumnMapping mapping = new SqlBulkCopyColumnMapping(dc.ColumnName, dbCol);

                        bulkCopy.ColumnMappings.Add(mapping);
                    }

                    #endregion

                    bulkCopy.DestinationTableName = tableName;

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 10000;
                    conn.Open();

                    bulkCopy.WriteToServer(dt);

                    return true;
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "CustomReportingData.ProcessSearchToDatabase:" + Processor.DatabaseName);
            }

            return false;
        }

        private bool ProcessSizmekStagingDatatable(DataTable datatab, string tableNameStage)
        {
            bool processed = false;

            try
            {
                TuncateStagingTable();

                processed = ProcessDatatableToDb(datatab, tablenameStage); // Loading data into Staging table

                if (processed)
                {
                    AppendSizmekData(datatab);
                    PrepTableForInsertSizmekData(datatab);
                    InsertSizmekData(datatab);
                }
            }
            catch (Exception ex)
            {
                processed = false;
                SolutionHelper.Error.LogError(ex, "SizmekMDXReporting.ProcessSizmekStagingDatatable:" + Processor.DatabaseName);
            }
            return processed;
        }

        /// <summary>
        /// This process preps the data which does not exist in the initial table 'datasizmekreportingdata'
        /// </summary>
        /// <param name="datatab"></param>
        private void PrepTableForInsertSizmekData(DataTable datatab)
        {
            // This part of this procedure inserts data that was not part of the update. 
            Processor.RunQuery(String.Format("exec [dbo].[spProcessSizmekSearchReportingStaging] '{0}'", datatab.TableName.ToString()));
        }

        /// <summary>
        /// This process was designed to update the reporting table with the data from a supporting file. The second part of this procedure inserts data that was not part of the update. 
        /// 
        /// Composite key needs to be updated if columns are added or else this will cause a one to many relationship which will cause the data to appear to be duplicated 'REF: MIT - 9262018'
        /// 
        /// </summary>
        /// <param name="dt"></param>
        private void AppendSizmekData(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();
            // We are using the AsOfDate, Campaign Name, Search Engine, Keyword, and Placement Id as a composite key
            sb.Append("UPDATE " + tablename + " ");
            sb.Append("SET {0} ");
            sb.Append("FROM   " + tablenameStage + " ");
            sb.Append("INNER JOIN " + tablename + " ");
            sb.Append("ON " + tablenameStage + ".AsOfDate = ");
            sb.Append(tablename + ".asofdate ");
            sb.Append("AND " + tablenameStage + ".[Campaign Name] = ");
            sb.Append(tablename + ".[Campaign Name] ");
            sb.Append("AND " + tablenameStage + ".[Search Engine] = ");
            sb.Append(tablename + ".[Search Engine] ");
            sb.Append("AND " + tablenameStage + ".[Keyword] = ");
            sb.Append(tablename + ".[Keyword] ");

            string setQuery = "";
            // Build 'SET' replacement token  
            foreach (DataColumn col in dt.Columns)
            {
                if (col.ColumnName.ToString() == "AsOfDate" ||
                    col.ColumnName.ToString() == "Brand" ||
                    col.ColumnName.ToString() == "Campaign Name" ||
                    col.ColumnName.ToString() == "Campaign Id" ||
                    col.ColumnName.ToString() == "Site Name" ||
                    col.ColumnName.ToString() == "Site Id" ||
                    col.ColumnName.ToString() == "Package Name" ||
                    col.ColumnName.ToString() == "Placement Id" ||
                    col.ColumnName.ToString() == "Placement Name" ||
                    col.ColumnName.ToString() == "Idx" ||
                    col.ColumnName.ToString() == "Ad Id" ||
                    col.ColumnName.ToString() == "Ad Name")
                    continue;

                setQuery += tablename + ".[" + col.ColumnName.ToString() + "] = " + tablenameStage + ".[" + col.ColumnName.ToString() + "], ";
            }

            setQuery = setQuery.ToString().Remove(setQuery.Length - 2, 2).ToString();

            string query = String.Format(sb.ToString(), setQuery);

            Processor.RunQuery(query);
        }

        /// <summary>
        /// This process inserts data which does not exist in the initial table 'datasizmekreportingdata'
        /// </summary>
        /// <param name="datatab"></param>
        private void InsertSizmekData(DataTable datatab)
        {
            string query = "select * from [mediaArizonaStateUniversity].[dbo].[dataSizmekSearchReportingData_temp];";

            DataSet ds = Processor.RunQuery(query);
            DataTable dt = ds.Tables[0];

            if (!dt.Columns.Contains("ModifiedDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                newColumn.DefaultValue = System.DateTime.Now.ToString();
                dt.Columns.Add(newColumn);
            }

            // This part of this procedure inserts data that was not part of the update. 
            if (dt.Rows.Count != 0)
                ProcessDatatableToDb(dt, tablename);
        }
        #endregion

        #region Private Processing Methods  

        /// <summary>
        /// Determine if there is a base file that needs to be processed first.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private bool bolProcessFile(FileInfo file)
        {
            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor(); // Random......I know!

            string fileName = file.Name;

            if (fileName.Contains("Base - "))
                return true;
            else if (fileName.Contains("Addendum - "))
                return bolBaseClassExist(file.Name);
            else
                return true;
        }

        public DataTable ExtractDatatable(FileInfo file)
        {
            DataTable dt = null;
            DataTable table = new DataTable();

            string errorMsg = "";
            string sheetNames = "Search_Overview_FlatExcel,Search_Display_Overview_FlatExc"; // This is for multiple Sheet names


            foreach (string sheetname in sheetNames.Split(','))
            {
                string strConn = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1;TypeGuessRows=0;ImportMixedTypes=Text\"", file.FullName);

                try
                {
                    dt = new DataTable();

                    using (OleDbConnection dbConnection = new OleDbConnection(strConn))
                    {
                        using (OleDbDataAdapter dbAdapter = new OleDbDataAdapter(String.Format("SELECT * FROM [{0}$]", sheetname), dbConnection)) //rename sheet if required!
                            dbAdapter.Fill(table);

                        int rows = table.Rows.Count;

                        if (rows > 0)
                            return table;
                    }

                }
                catch (Exception ex)
                {
                    errorMsg += ex.Message + System.Environment.NewLine + System.Environment.NewLine;
                }

            }

            try
            { 
                dt = CsvToDatabase.GetDataTabletFromCSVTSVFile(file.FullName.ToString());
            }
            catch (Exception ex)
            {
  
            } 


            SolutionHelper.Error.LogError("SizmekMDXReporting.ProcessSearchReports.ExtractDatatable:" + file.Name + " - " + errorMsg);
            return dt;
        }
        private static DataTable ExtractReport(DataTable dt)
        {
            DateTime today = System.DateTime.Now;
            int intRow = 14;
            int idx = 0;

            // Removing first 14 rows which serve no purpose to the data
            for (int counter = 0; intRow > 0; intRow--)
            {


                dt.Rows[counter].Delete();
                counter++;
            }

            dt.AcceptChanges();

            //Rename column headers based on first row
            foreach (DataColumn col in dt.Columns)
            {
                string colName = dt.Rows[0][col.ColumnName].ToString();

                if (colName == "")
                    colName = "Delete";

                dt.Columns[idx].ColumnName = colName;

                idx++;
            }

            dt.Columns.Remove("Delete");
            dt.AcceptChanges();

            dt.Rows[0].Delete();
            dt.AcceptChanges();

            // Add modified date to the datatable if needed
            if (!dt.Columns.Contains("ModifiedDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                newColumn.DefaultValue = today.ToString();
                dt.Columns.Add(newColumn);
            }

            return dt;
        }

        private void ColumnChecker(DataTable dt)
        {
            DateTime today = System.DateTime.Now;

            string[] columnCol = GetColumnsFromDatabase(false);

            foreach (DataColumn dc in dt.Columns)
            {
                dc.ColumnName = dc.ColumnName.ToString().Trim();

                if (dc.ColumnName.Contains("_"))
                    dc.ColumnName = dc.ColumnName.Replace("_", " ");
                if (dc.ColumnName.Contains("* "))
                    dc.ColumnName = dc.ColumnName.Replace("* ", "");

                dc.ColumnName = dc.ColumnName.Trim().ToProperCase();

                if (dc.ColumnName.ToLower() == "date" || dc.ColumnName.ToLower() == "asofdate")
                {
                    dc.ColumnName = "AsOfDate";
                    continue;
                }

                if (dc.ColumnName.ToLower() == "modifieddate")
                {
                    dc.ColumnName = "ModifiedDate";
                    continue;
                }

                if (!columnCol.Contains(dc.ColumnName))
                {
                    bool isLowerCaseMatch = false;
                    string renameColumn = "";

                    Array.ForEach<string>(columnCol, delegate (string col)
                    {
                        if (col.ToLower() == dc.ColumnName.ToLower())
                        {
                            renameColumn = col;
                            isLowerCaseMatch = true;
                        }
                    });

                    if (isLowerCaseMatch)
                        RenameColumn(renameColumn);
                    else
                        AddColumn(dc.ColumnName.ToString());
                }
            }

            if (!dt.Columns.Contains("ModifiedDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                newColumn.DefaultValue = today.ToString();
                dt.Columns.Add(newColumn);
            }

            bool bolChecker = DebugHelper.CheckColumnsAgainstDatabase(Processor, dt, tablename);
        }

        /// <summary>
        /// Clean the data based on the column type define in the AddColumn method 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private DataTable CleanDataFeed(DataTable dt)
        {
            DataTable dtCloned = dt.Clone();
            try
            {
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName.ContainsAny("Total Clicks", "Total Conversions", "Served Impressions"))
                        dtCloned.Columns[col.ColumnName].DataType = typeof(Int32);
                    else if (col.ColumnName.ContainsAny("Search Engine", "Campaign Name", "Ad Group", "Keyword"))
                        dtCloned.Columns[col.ColumnName].DataType = typeof(String);
                    else if (col.ColumnName.ContainsAny("AsOfDate", "ModifiedDate"))
                        dtCloned.Columns[col.ColumnName].DataType = typeof(DateTime);
                    else
                        dtCloned.Columns[col.ColumnName].DataType = typeof(Decimal); ;
                }

            }
            catch (Exception)
            {
                Console.Clear();
            }


            int idx = 0;
            DeleleExtraRows(dt);

            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName.ToUpper().ContainsAny("FREQUENCY", "RATE", "COST", "CPM", "PROFIT", "REVENUE", "CTR", "AVERAGE", "ROAS", "ROI"))
                    {
                        if (String.IsNullOrEmpty(dr[col.ColumnName].ToString()) || dr[col.ColumnName].ToString().ToUpper() == "NULL")
                            dr[col.ColumnName] = 0;
                        if (dr[col.ColumnName].ToString().Contains(","))
                            dr[col.ColumnName] = dr[col.ColumnName].ToString().Replace(",", "");
                        if (dr[col.ColumnName].ToString().Contains("%"))
                            dr[col.ColumnName] = dr[col.ColumnName].ToString().Replace("%", "");
                    }
                    else if (col.ColumnName.ToUpper().ContainsAny("CLICK", "IMPRESSIONS", "CONVERSION", "UNIQUE", "AD ID"))
                    {
                        if (String.IsNullOrEmpty(dr[col.ColumnName].ToString()) || dr[col.ColumnName].ToString().ToUpper() == "NULL")
                            dr[col.ColumnName] = 0;

                        string apr = dr[col.ColumnName].ToString();

                        if (dr[col.ColumnName].ToString().Contains(","))
                            dr[col.ColumnName] = dr[col.ColumnName].ToString().Replace(",", "");
                        try
                        {
                            dr[col.ColumnName] = decimal.ToInt32(decimal.Parse(apr));
                        }
                        catch (Exception ex)
                        {
                            string s = apr;
                            throw ex;
                        }
                    }
                    else if (col.ColumnName.ToUpper().ContainsAny("ASOFDATE"))
                    {
                        string date = dr[col.ColumnName].ToString();

                        try
                        {
                            DateTime dateTime;
                            if (!DateTime.TryParse(date, out dateTime))
                            {
                                double d = double.Parse(dr[col.ColumnName].ToString());
                                DateTime conv = DateTime.FromOADate(d);

                                dr[col.ColumnName] = conv;
                            }
                        }
                        catch (Exception ex)
                        {
                            string s = date;
                            throw ex;
                        }
                    }
                }
                idx++;

                DataRow workRow = dtCloned.NewRow();

                string value = "";


                try
                {
                    foreach (DataColumn dc1 in dtCloned.Columns)
                    {
                        value = dc1.ColumnName + ": " + dr[dc1.ColumnName].ToString();

                        if (dc1.ColumnName.ContainsAny("Total Clicks", "Total Conversions", "Served Impressions"))
                            workRow[dc1.ColumnName] = int.Parse(dr[dc1.ColumnName].ToString());
                        else if (dc1.ColumnName.ContainsAny("Search Engine", "Campaign Name", "Ad Group", "Keyword"))
                            workRow[dc1.ColumnName] = dr[dc1.ColumnName].ToString();
                        else if (dc1.ColumnName.ContainsAny("AsOfDate", "ModifiedDate"))
                            workRow[dc1.ColumnName] = DateTime.Parse(dr[dc1.ColumnName].ToString());
                        else
                            workRow[dc1.ColumnName] = Decimal.Parse(dr[dc1.ColumnName].ToString());
                    }
                }
                catch (Exception ex)
                {


                    SolutionHelper.Error.LogError("SizmekMDXReporting.DailySearchProcessing.CleanDataFeed - " + ex.Message + " - Value: " + value);
                    throw ex;
                }

                dtCloned.Rows.Add(workRow);
            }


            return dtCloned;
        }

        private DateTime[] GetReportDates()
        {
            List<DateTime> dates = new List<DateTime>();

            string query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[{0}] where [AsOfDate] is not null", tablename);

            DataSet ds = Processor.RunQuery(query);

            if (ds != null) // Add date to list of date
            {
                if (ds.Tables.Count != 0) // Add date to list of date
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        dates.Add(DateTime.Parse(dr[0].ToString())); // Date is a non nullable date type
                    }
                }
            }

            return dates.ToArray();
        }

        private void DeleteOldData(DataTable dt)
        {
            try
            {
                string query = String.Format(@"delete FROM [dbo].[{0}] where AsOfDate = '{1}'", tablename, dt.TableName);

                Processor.RunQuery(query);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        /// <summary>
        /// Determine if there is a base file that needs to be processed first.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private bool bolBaseClassExist(string name)
        {
            try
            {
                DirectoryInfo d = new DirectoryInfo(csvExport);

                FileInfo[] infos = d.GetFiles();

                foreach (FileInfo f in infos.Where(m => m.FullName.ContainsAny("Sizmek Daily Search -")).OrderBy(n => n.CreationTime))
                    if (f.FullName.Contains("Base - "))
                        return false;

            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.DailySearchProcessing.bolBaseClassExist:" + name + " - " + ex.Message);
                return false;
            }

            return true;
        }


        /// <summary>
        /// This is to compare the total records and the sum of the impressions against the database. 
        /// </summary>
        /// <param name="datatab"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private bool CompareReportDataAgainstDatabaseData(DataTable datatab)
        {
            try
            {
                string query = String.Format(@"SELECT count(*) Total, sum([Served Impressions]) SumImp FROM [dbo].[{0}] where AsOfDate =  '{1}'", tablename, datatab.TableName);

                DataSet ds = Processor.RunQuery(query);

                if (ds != null)
                {
                    if (ds.Tables.Count != 0)
                    {
                        int _sumImp = 0;
                        int total = int.Parse(ds.Tables[0].Rows[0]["Total"].ToString());
                        int sumImp = int.Parse(ds.Tables[0].Rows[0]["SumImp"].ToString());

                        foreach (DataRow dr in datatab.Rows)
                        {
                            _sumImp += int.Parse(dr["Served Impressions"].ToString());
                        }

                        int _total = datatab.Rows.Count;

                        //   reimportData = true;      // Ommitted due to error
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }

            return false;
        }
        private void TuncateStagingTable()
        {
            string query = "exec [dbo].[spCreateSizmekSearchReporting_Staging]";
            Processor.RunQuery(query);
        }

        private string[] GetColumnsFromDatabase(bool isToLower = true)
        {
            string query = "";
            query += "DECLARE @columns varchar(MAX) ";
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(MAX), COLUMN_NAME) ";
            query += String.Format("FROM (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{0}') as tab ", tablename);
            query += "select @columns ";

            DataSet ds = Processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString().Trim();

            string[] columnCol;

            if (isToLower)
                columnCol = columns.ToLower().Split(',').Select(x => x.Trim()).ToArray();
            else
                columnCol = columns.Split(',').Select(x => x.Trim()).ToArray();

            return columnCol;
        }

        #region Internal Methods
        private void Reset()
        {
            duplicate = false;
            company = null;
            Processor = null;
            deleteEmail = false;
        }

        private string GetUrlFromDownload(Message message)
        {
            try
            {
                string result = "";
                string pattern = @"\b(?:https?://|www\.)\S+\b.zip";

                #region RegexNotes
                /*
                 *   \b       -matches a word boundary (spaces, periods..etc)
                 *   (?:      -define the beginning of a group, the ?: specifies not to capture the data within this group.
                 *   https?://  - Match http or https (the '?' after the "s" makes it optional)
                 *   |        -OR
                 *   www\.    -literal string, match www. (the \. means a literal ".")
                 *   )        -end group
                 *   \S+      -match a series of non-whitespace characters.
                 *   \b       -match the closing word boundary.
                 * 
                 */
                #endregion
                Regex r = new Regex(pattern, RegexOptions.IgnoreCase);
                try
                {
                    if (message.MessagePart.Body != null)
                        result = System.Text.Encoding.UTF8.GetString(message.MessagePart.Body);
                    if (String.IsNullOrEmpty(result))
                        if (message.MessagePart.MessageParts[0].Body != null)
                            result = System.Text.Encoding.UTF8.GetString(message.MessagePart.MessageParts[0].Body);
                    if (String.IsNullOrEmpty(result))
                        if (message.MessagePart.MessageParts[1].Body != null)
                            result = System.Text.Encoding.UTF8.GetString(message.MessagePart.MessageParts[1].Body);
                }
                catch (Exception)
                {

                }

                // Match the regular expression pattern against a text string.
                Match m = r.Match(result);

                Console.WriteLine("Url: {0}", m.Value.Replace("=3D", "="));

                // 3D are the hex digits corresponding to ='s ASCII value (61).
                string url = m.Value.Replace("=3D", "=").Replace("\" > click", "");

                return url;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "GetUrlFromDownload Daily Search Processing");
                return "";
            }
        }

        /// <summary>
        /// This will throw an error if a column does not match the database column. Bulk Sql Insert is case sensitive
        /// </summary>
        /// <param name="columnCol"></param>
        /// <param name="dc"></param>
        private static void ColumnCheck(string[] columnCol, DataColumn dc)
        {
            string dbCol = "";

            foreach (string c in columnCol)
            {
                if (dc.ColumnName == c)
                {
                    dbCol = c;
                    break;
                }

            }

            if (dbCol == "")
            {
                throw new Exception("Column not matching: " + dc.ColumnName.ToString());
            }
        }

        private void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(String.Format("{0} Rows have been copied.", e.RowsCopied.ToString()));
        }

        private static void DeleleExtraRows(DataTable dt)
        {
            List<int> deleteInts = new List<int>();

            // Delete unwanted blank rows
            for (int i = 0; i < dt.Rows.Count; i++)
                if (dt.Rows[i]["AsOfDate"].ToString() == "" && dt.Rows[i]["Served Impressions"].ToString() == "" && dt.Rows[i]["Total Conversions"].ToString() == "")
                    dt.Rows[i].Delete();

            dt.AcceptChanges();
        }

        /// <summary>
        /// Due to the columns needing to be dynamic this method is designed to add additional column to the table
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        private void AddColumn(string column)
        {
            column = column.ToProperCase();

            string query = "ALTER TABLE {0} ADD [{1}] {2}";
            string orgQuery = query;

            if (column.ToUpper().ContainsAny("FREQUENCY", "RATE"))
                query = string.Format(query, tablename, column, "decimal(24,12) Null");
            else if (column.ToUpper().ContainsAny("CLICK", "IMPRESSIONS", "CONVERSION", "UNIQUE", "AD ID"))
                query = string.Format(query, tablename, column, "[int] NULL");
            else
                query = string.Format(query, tablename, column, "[nvarchar](250)");

            Processor.RunQuery(query);
        }

        /// <summary>
        /// Due to the columns needing to be dynamic this method is designed to rename columns within the table
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        private void RenameColumn(string column)
        {
            string query = "EXEC sp_rename '[{0}].[dbo].[{1}].[{2}]', '{3}', 'COLUMN';";

            query = string.Format(query, Processor.DatabaseName, tablename, column.Replace("'", "''"), column.Replace("'", "''").Trim().ToProperCase());

            Processor.RunQuery(query);
        }

        #endregion 
    }
}
