﻿using SolutionHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhirlygigSolutionHelper;

namespace SizmekMDXReporting.EmailProcessing
{
    public class FileProcessing
    {
        mediaLavidgeEntities db = new mediaLavidgeEntities();

        Company company;

        Data Processor = new Data();

        string csvExportCompleted;
        string csvExportFailed;
        string csvExportWaiting;
        string csvExport;
        bool debug = false;
        bool colSizmekSearch = false;
        bool isBase = false;
        bool isAddendum1 = false;
        bool isAddendum2 = false;
        bool isAddendum3 = false;
        bool isAddendum4 = false; 
        int idxSizmekSearch = 0; 
        string[] unzippedFiles = null;
         
        public FileProcessing()
        {
            csvExportCompleted = ConfigurationManager.AppSettings["csvExportCompleted"];
            csvExportFailed = ConfigurationManager.AppSettings["csvExportFailed"];
            csvExportWaiting = ConfigurationManager.AppSettings["csvExportWaiting"];
            csvExport = ConfigurationManager.AppSettings["csvExport"];
            debug = bool.Parse(ConfigurationManager.AppSettings["debug"].ToString());
        }

        private void RenameSearchFiles()
        {  

            try
            {
                DirectoryInfo d = new DirectoryInfo(csvExport);
                FileInfo[] infos = d.GetFiles();
                DailySearchProcessing ds = new DailySearchProcessing(); 
                 
                foreach (FileInfo f in infos.Where(m => m.FullName.ContainsAny( "Sizmek Daily Search -")).OrderBy(n => n.CreationTime))
                {

                    DataTable dt = ds.ExtractDatatable(f);
                    string date = dt.Rows[7][1].ToString().Replace("Custom (", "_").Replace(").", "").Replace("/", ".");

                    string fullName = f.FullName.ToString().Replace(f.Extension, date + f.Extension);

                    f.MoveTo(fullName);

                } 
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.FileProcessing.RenameSearchFiles:" + ex.Message);
            }
        }

        public void RunProcessingFiles()
        {
            //if (debug) 
            //        RenameSearchFiles();

            try
            {
                //if (debug)
                //    csvExport = @"I:\Client Files\ASU\MEDIA\OLD_Enterprise Marketing Hub\OLD_Ad Hoc\All ASU\Conversion Analysis\Full Path to Conversion Re Group_Nov18\Historic Visits Reports";

                DirectoryInfo d = new DirectoryInfo(csvExport);


                FileInfo[] infos = d.GetFiles();

                int idx = 0;
                bool bypass = false;    // For testing purposes
                Reset();
                 
                foreach (FileInfo f in infos.Where(m => m.FullName.ContainsAny("Sizmek Custom Report -", "Sizmek Daily Search -"
                        , "Sizmek Uniques - ", "Sizmek Path To Conversion"
                        , "Unique_Metrics_Summary", "Sizmek Analytics Report"
                        , "eyedcny.local")).OrderBy(n => n.CreationTime))
                {
                    Result result = new Result(); 
                    idx++;

                     
                    if (f.FullName.ContainsAny(".zip"))
                        continue;

                    try
                    { 
                        company = GetCompany(f.Name);

                        if (company == null)
                        {
                            MoveFileForLaterProcessing(idx, f);
                        }
                        else
                        { 
                            if (f.FullName.ContainsAny("Sizmek Custom Report"))
                            {
                                if (bypass)
                                    continue;

                                CustomReportingData custom = new CustomReportingData();
                                custom.company = company;
                                custom.Processor = Processor;
                                custom.ProcessDataFeed(f);
                            } 
                            if (f.FullName.ContainsAny("Sizmek Analytics Report"))
                            {
                                ReportingData custom = new ReportingData();
                                custom.company = company;
                                custom.Processor = Processor;
                                custom.ProcessDatatable(f);
                            }
                            if (f.FullName.ContainsAny("Sizmek Path To Conversion"))
                            { 
                                PathToConversionProcessing custom = new PathToConversionProcessing();
                                custom.company = company;
                                custom.Processor = Processor;
                                custom.ProcessDatatable(f);
                            }
                            if (f.FullName.ContainsAny("Sizmek Daily Search"))
                            { 
                                //if (company.Id == 4)
                                //    if(!SizmekSearchCheck(infos))
                                //        continue;   // testing purposes
                                // MIT 11/13/18: The order in which the files are delivered do not affect how the process works.

                                DailySearchProcessing search = new DailySearchProcessing();
                                search.company = company;
                                search.Processor = Processor;
                                search.ProcessDatatable(f);

                                if (colSizmekSearch)
                                    idxSizmekSearch++;
                            }
                            else if (f.FullName.ContainsAny("Sizmek Uniques", "Unique_Metrics_Summary"))
                            {
                                UniquesProcessing uniques = new UniquesProcessing();
                                uniques.company = company;
                                uniques.Processor = Processor;
                                uniques.ProcessDatatable(f);
                            }
                        } 
                    }
                    catch (Exception ex)
                    {
                        SolutionHelper.Error.LogError("SizmekMDXReporting.FileProcessing.RunProcessingFiles:" + f.Name + " - " + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.FileProcessing.RunProcessingFiles - " + ex.Message);
            }
        }

        private void Reset()
        { 
            colSizmekSearch = false;
            isBase = false;
            isAddendum1 = false;
            isAddendum2 = false;
            isAddendum3 = false;
            isAddendum4 = false;
            idxSizmekSearch = 0;
        }

        // Determine if ASU's files are all present to process
        private bool SizmekSearchCheck(FileInfo[] infos)
        {

            // This process should only check once
            if (!colSizmekSearch)
            { 
                // Determine if all files are ready

                foreach (FileInfo f in infos.Where(m => m.FullName.ContainsAny("Sizmek Custom Report -", "Sizmek Daily Search -", "Sizmek Uniques - ", "Sizmek Path To Conversion", "Unique_Metrics_Summary", "Sizmek Analytics Report", "eyedcny.local")).OrderBy(n => n.CreationTime))
                {
                    Company _company = GetCompany(f.Name);

                    if (_company.Id == 4)
                    { 
                        if (f.FullName.Contains("- Base -"))
                            isBase = true;
                        else if (f.FullName.Contains("- Addendum_1 -"))
                            isAddendum1 = true;
                        else if (f.FullName.Contains("- Addendum_2 -"))
                            isAddendum2 = true;
                        else if (f.FullName.Contains("- Addendum_3 -"))
                            isAddendum3 = true;
                        else if (f.FullName.Contains("- Addendum_4 -"))
                            isAddendum4 = true;
                    } 
                }

                // Check all files are present
                if(isBase && isAddendum1 && isAddendum2 && isAddendum3 && isAddendum4)
                {
                    ResetSizmekTables();
                    colSizmekSearch = true;
                } 
            }
             
            return colSizmekSearch;
        }

        /// <summary>
        /// Resetting loading tables to ensure a file check before replacing production data
        /// </summary>
        private void ResetSizmekTables()
        {
            string query = "EXEC [dbo].[spSizmekSearchPrepTable]"; 
     
            DataSet ds = Processor.RunQuery(query);
        }

        public void UnzipFiles()
        { 
            try
            {
                Unzip();
                DeleteUnzippedFiles();
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.FileProcessing.RunProcessingFiles: " + ex.Message);
            }
        }

        private void DeleteUnzippedFiles()
        {
            foreach(string str in unzippedFiles)
            {
                FileInfo file = new FileInfo(str);
                file.Delete(); 
            }
        }

        public void Unzip()
        {
            unzippedFiles = null;

            try
            {
                DirectoryInfo d = new DirectoryInfo(csvExport);
                FileInfo[] infos = d.GetFiles();
                List<string> _unzippedFiles = new List<string>();
                 
                foreach (FileInfo f in infos.Where(m => m.Extension.Contains(".zip")))
                {
                    string prefix = "";

                    Company company = GetCompany(f.Name);

                    if (company == null)
                        continue;

                    if (f.FullName.Contains("Sizmek Path To Conversion"))
                        prefix = String.Format(@"Sizmek Path To Conversion - {0} {1}", company.CompanyName, System.DateTime.Now.ToString().Replace(":", ".").Replace(@"/", "-"));
                     
                    Stream fs = File.OpenRead(f.FullName);
                    Streem str = new Streem();
                    str.Stream = fs;
                     
                    str.ToStreamFromZip(csvExport, prefix);

                    fs.Close();
                    _unzippedFiles.Add(f.FullName);
                }

                unzippedFiles = _unzippedFiles.ToArray();
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.FileProcessing.RunProcessingFiles: " + ex.Message);
            }
        }

        private DataTable FormatDataTable(DataTable dt)
        {

            int intRow = 15;

            for (int counter = 0; intRow > 0; intRow--)
            {
                dt.Rows[counter].Delete();
                counter++;
            }

            dt.AcceptChanges();

            foreach (DataColumn col in dt.Columns)
            {
                string colName = dt.Rows[0][col].ToString();

                if (colName == "")
                {
                    dt.Columns.Remove(col);
                    dt.AcceptChanges();
                    break;
                }
                else
                    dt.Columns[col.ColumnName.ToString()].ColumnName = colName.Trim();
            }

            dt.Rows[0].Delete();

            dt.Rows[dt.Rows.Count - 1].Delete();
            dt.Rows[dt.Rows.Count - 2].Delete();
            dt.AcceptChanges();

            return dt;
        }

        private void MoveFileForLaterProcessing(int idx, FileInfo f)
        {
            FileInfo fInfo = new FileInfo(csvExportWaiting + "/" + f.Name);

            if (!fInfo.Exists)
                f.MoveTo(csvExportWaiting + "/" + f.Name.Replace(f.Extension, "").ToString() + f.Extension);
            else
                f.MoveTo(csvExportWaiting + "/" + f.Name.Replace(f.Extension, " Duplicated " + idx.ToString() + f.Extension));
        }

        private Result ProcessLargeFiles(CsvToDatabase csvToDatabase, FileInfo f)
        {
            Result result = new Result();
            result.result = csvToDatabase.Run();
            result.duplicate = csvToDatabase._duplicate;

            return result;
        }

        private void Reset(out bool result, out bool deleteReport)
        {
            company = null;
            Processor = null;

            result = false;
            deleteReport = false;
        }

        private Company GetCompany(string fullName)
        {
            db = new mediaLavidgeEntities();

            Company[] companys = (from m in db.Companies where m.IsActive == true && m.Sizemek_Keyword != null select m).ToArray();

            foreach (Company company in companys)
            {
                if (fullName.ToLower().ContainsAny(company.Sizemek_Keyword.ToLower().Split(',')))
                {
                    Data processor = new Data(company.DatabaseName, company.Login, company.Password, company.Server);

                    Processor = processor;
                    return company;
                }
            }

            return null;
        }

        /// <summary>
        /// Move back to processing folder if company name is established
        /// </summary>
        internal void RunWaitingFiles()
        {
            Console.WriteLine("Checking waiting files");
            try
            {
                DirectoryInfo d = new DirectoryInfo(csvExportWaiting);
                FileInfo[] infos = d.GetFiles();


                foreach (
                    FileInfo f in infos.Where(m => m.FullName.ContainsAny(
                    "Sizmek Custom Report -",
                    "Sizmek Daily Search -",
                    "Sizmek Uniques - ")))
                {
                    try
                    {
                        company = GetCompany(f.Name);

                        if (company == null)
                            continue;
                        else
                            f.MoveTo(csvExport + "/" + f.Name);
                    }
                    catch (Exception ex)
                    {
                        SolutionHelper.Error.LogError("SizmekMDXReporting.FileProcessing.RunProcessingFiles:" + f.Name + " - " + ex.Message);
                    }
                }

            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.FileProcessing.RunWaitingFiles:" + ex.Message);
            }

            Console.WriteLine("Checking waiting files Finished");
        }

        class Result
        {
            public bool result { get; set; }
            public bool duplicate { get; set; }
        }
    }
}
