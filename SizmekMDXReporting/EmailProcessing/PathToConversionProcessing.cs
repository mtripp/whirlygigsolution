﻿using Microsoft.VisualBasic.FileIO;
using OpenPop.Mime;
using OpenPop.Pop3;
using SolutionHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using WhirlygigSolutionHelper;

namespace SizmekMDXReporting.EmailProcessing
{
    public class PathToConversionProcessing
    {
        mediaLavidgeEntities db = new mediaLavidgeEntities();

        public Company company;

        public Data Processor = new Data();

        string server;
        string accountname;
        string accountpassword;
        string csvExportCompleted;
        string csvExportFailed;
        string csvExportWaiting;
        string csvExport;
        bool debug;
        public bool deleteEmail;
        bool reimportData;
        string tableName = "dataSizmekPathToConversion";

        public PathToConversionProcessing(string companyName = "")
        {
            debug = bool.Parse(ConfigurationManager.AppSettings["debug"].ToString());
            server = ConfigurationManager.AppSettings["EmailAccountServer"];
            accountname = ConfigurationManager.AppSettings["EmailAccountName"];
            accountpassword = ConfigurationManager.AppSettings["EmailAccountPassword"];
            csvExportCompleted = ConfigurationManager.AppSettings["csvExportCompleted"];
            csvExportFailed = ConfigurationManager.AppSettings["csvExportFailed"];
            csvExport = ConfigurationManager.AppSettings["csvExport"];
            csvExportWaiting = ConfigurationManager.AppSettings["csvExportWaiting"];

            if (companyName != "")
                company = GetCompany(companyName);
        }

        public void Run()
        {
            ProcessReports();
        }

        /// <summary>
        /// Example showing:
        ///  - how to fetch all messages from a POP3 server
        /// </summary>
        /// <param name="hostname">Hostname of the server. For example: pop3.live.com</param>
        /// <param name="port">Host port to connect to. Normally: 110 for plain POP3, 995 for SSL POP3</param>
        /// <param name="useSsl">Whether or not to use SSL to connect to server</param>
        /// <param name="username">Username of the user on the server</param>
        /// <param name="password">Password of the user on the server</param>
        ///  http://hpop.sourceforge.net/
        /// <returns>All Messages on the POP3 server</returns>
        private void ProcessReports()
        {
            try
            {
                restart: // This goto was implemented due to a Pop3Client timeout. Connection to the mail server needs to be reauthenticated between each message

                DateTime startTime = System.DateTime.Now;

                using (Pop3Client client = new Pop3Client())
                {
                    client.Connect(server, 110, false);
                    client.Authenticate(accountname, accountpassword);

                    int messageCount = client.GetMessageCount();

                    List<Message> allMessages = new List<Message>(messageCount);

                    if (messageCount != 0)
                    {
                        for (int i = 1; i <= messageCount; i++) // Loop through messages
                        {
                            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();

                            bool result = false;

                            Message message = client.GetMessage(i);
                            Console.WriteLine("Processing " + i + " of " + messageCount + ": " + message.Headers.Subject + " " + message.Headers.Date.ToString());


                            if (message.Headers.Subject != null)
                            {
                                if (message.Headers.Subject.ContainsAny(ConfigurationManager.AppSettings["EmailPathSubject"].Split(',')))
                                {
                                    //  if (message.Headers.Subject.ContainsAny("Custom Report", "Custom Report Report"))
                                    result = ProcessReport(message);

                                    if (result)
                                    {
                                        client.DeleteMessage(i);
                                        goto restart;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.OpenPopHandler: " + ex.Message);
            }
        }

        public bool CleanseFile(FileInfo file, string csvExportProcessing)
        {
            try
            {
                DataTable dt = ExtractDatatable(file);

                if (dt == null)
                    return false;

                dt.TableName = file.Name.Replace(file.Extension, ".tsv");

                if(company == null)
                    company = GetCompany(dt.TableName);

                ColumnChecker(dt);
                CleanDataFeed(dt);

                dt.ToDataSetExportTsv(csvExportProcessing);
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.PathToConversionProcessing.CleanseFile: " + ex.Message);
                return false;
            }

            return true;
        }

        private bool ProcessReport(Message message)
        {
            company = null;

            FileInfo result = ExportReport(message);

            if (result != null)
                return true;
            else
                return false;
        }

        public static bool ExportReport(Message message, string location)
        {
            string fileName = "";
            string path = "", ext = "";
            bool bolStream = true;
            Streem stream = new Streem();
            Streem[] streams = null;

            string url = GetUrlFromDownload(message);
            string companyName = GetCompanyName(message);
            int idx = companyName.IndexOf('(');

            companyName = companyName.Substring(0, idx);

            if (url != "")
            {
                string date = System.DateTime.Now.ToString().ReplaceAny("", "/", ":", "AM", "PM").Trim();
                fileName = String.Format(@"{0}\Sizmek Path To Conversion - {1} {2}.zip", location, companyName, date);

                //Create a stream for the file
                Stream _stream = null;

                //This controls how many bytes to read at a time and send to the client
                int bytesToRead = 10000;

                // Buffer to read bytes in chunk size specified above
                byte[] buffer = new Byte[bytesToRead];

                // The number of bytes read
                try
                {

                    //Create a WebRequest to get the file
                    HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(url);

                    //Create a response for this request
                    HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

                    if (fileReq.ContentLength > 0)
                        fileResp.ContentLength = fileReq.ContentLength;

                    //Get the Stream returned from the response
                    _stream = fileResp.GetResponseStream();

                    StreamReader sr = new StreamReader(_stream);

                    using (Stream s = File.Create(fileName))
                        _stream.CopyTo(s);

                    Stream fs = File.OpenRead(fileName);
                    Streem str = new Streem();
                    str.Stream = fs;
                    str.Name = fileName.Replace(location, "");

                    str.ToStreamFromZip(String.Format(@"{0}\Sizmek Path To Conversion - {1} ", location, companyName));

                    fs.Close();

                    FileInfo file = new FileInfo(fileName);
                    file.Delete();

                    return true;
                }
                catch (Exception ex)
                {
                    SolutionHelper.Error.LogError("SizmekMDXReporting.PathToConversionProcessing.ExportReport: " + ex.Message);
                }
            }

            return false;
        }

        /// <summary>
        /// TODO: This class is marked for deletion.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private FileInfo ExportReport(Message message)
        {
            string fileName = "";

            string companyName = GetCompanyName(message);
            string url = GetUrlFromDownload(message);

            if (url != "")
            {
                fileName = String.Format(@"{0}\Sizmek Path To Conversion - {1} {2}.zip", csvExport, companyName, System.DateTime.Now.ToString().Replace(":", ".").Replace(@"/", "-"));

                //Create a stream for the file
                Stream stream = null;

                //This controls how many bytes to read at a time and send to the client
                int bytesToRead = 10000;

                // Buffer to read bytes in chunk size specified above
                byte[] buffer = new Byte[bytesToRead];

                // The number of bytes read
                try
                {
                    //Create a WebRequest to get the file
                    HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(url);

                    //Create a response for this request
                    HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

                    if (fileReq.ContentLength > 0)
                        fileResp.ContentLength = fileReq.ContentLength;

                    //Get the Stream returned from the response
                    stream = fileResp.GetResponseStream();

                    StreamReader sr = new StreamReader(stream);

                    using (Stream s = File.Create(fileName))
                    {
                        stream.CopyTo(s);
                    }

                    FileInfo file = new FileInfo(fileName);
                    return file;
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }
            }

            return null;
        }

        private static string GetCompanyName(Message message)
        {
            try
            {
                List<MessagePart> attachments = message.FindAllAttachments();

                foreach (MessagePart attachment in attachments)
                {
                    if (attachment.FileName.Contains("MetaData.txt"))
                    {
                        Stream stream = new MemoryStream(attachment.Body);
                        DataTable dt = stream.ToDataTableTsv(0);

                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dr[0].ToString().Contains("Advertiser:"))
                            {
                                string company = dr[0].ToString().Replace("Advertiser: ", "");

                                if (company != "")
                                    return company;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "PathToConversionProcessing.GetCompanyName:");
            }

            return "";
        }

        private static string GetUrlFromDownload(Message message)
        {
            try
            {
                string pattern = @"\b(?:https?://|www\.)platform.mediamind.com\S+\b";
                // string pattern = @"\b(?:https?://|www\.)\S+\b";
                // http://platform.mediamind.com/OuterAccessFiles/ARB/8844fb28-64b7-4286-a19a-3573c091099e.zip

                /*
                 *   \b       -matches a word boundary (spaces, periods..etc)
                 *   (?:      -define the beginning of a group, the ?: specifies not to capture the data within this group.
                 *   https?://  - Match http or https (the '?' after the "s" makes it optional)
                 *   |        -OR
                 *   www\.    -literal string, match www. (the \. means a literal ".")
                 *   )        -end group
                 *   \S+      -match a series of non-whitespace characters.
                 *   \b       -match the closing word boundary.
                 * 
                 */

                Regex r = new Regex(pattern, RegexOptions.IgnoreCase);

                string result = System.Text.Encoding.UTF8.GetString(message.FindFirstHtmlVersion().Body);
                // Match the regular expression pattern against a text string.
                Match m = r.Match(result);

                Console.WriteLine("Url: {0}", m.Value.Replace("=3D", "="));

                // 3D are the hex digits corresponding to ='s ASCII value (61).
                string url = m.Value.Replace("=3D", "=");
                url = url.Replace(">Click", "");
                url = url.Replace("\"", "");

                return url;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "GetUrlFromDownload Conversant");
                return "";
            }
        }

        private DataTable ExtractReport(string url)
        {
            try
            {
                WebRequest wrGETURL;
                wrGETURL = WebRequest.Create(url);

                Stream objStream;
                var objNew = wrGETURL.GetResponse();
                objStream = wrGETURL.GetResponse().GetResponseStream();
                StreamReader objReader = new StreamReader(objStream, Encoding.UTF8);
                WebResponse wr = wrGETURL.GetResponse();
                Stream receiveStream = wr.GetResponseStream();


                Streem newStreem = new Streem { Name = "", Stream = receiveStream };
                Streem[] streams = null;
                streams = newStreem.ToStreamFromZip(true);

                DataTable srfds = receiveStream.ToDataTableFromZip();

                StreamReader reader = new StreamReader(receiveStream, Encoding.UTF8);
                string content = reader.ReadToEnd();

                File.WriteAllText(csvExport + @"\Sizmek Path To Conversion - " + "fdafdafd.zip", content.ToString());

                DataTable dt = new DataTable();

                string[] tableData = content.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                var col = from cl in tableData[0].Split(",".ToCharArray())
                          select new DataColumn(cl);
                dt.Columns.AddRange(col.ToArray());

                (from st in tableData.Skip(1)
                 select dt.Rows.Add(st.Split(",".ToCharArray()))).ToList();

                return dt;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "ExtractReport PathToConversion");
            }

            return null;
        }

        public void ProcessDatatable(FileInfo f)
        { 
            Console.WriteLine("RunFileProcessingData : *******************************************" + System.DateTime.Now);
            DateTime startTime = System.DateTime.Now;

            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();

            bool results = false;

            ProcessDatatable(f, results);

            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("Total Time : " + endTime.Subtract(startTime).TotalMinutes);
        }

        public bool ProcessDatatable(FileInfo file, string movefile)
        {
            DataTable dt = GetDataTabletFromCSVTSVFile(file.FullName);

           return ProcessCleansedPathToConversionFile(dt); 
        }

        private void ProcessDatatable(FileInfo file, bool results)
        {
            DataTable dt = GetDataTabletFromCSVTSVFile(file.FullName);

            bool result = ProcessPathToConversion(dt);

            if (result)
            {
                file.MoveTo(csvExportCompleted + @"\" + file.Name.Replace(file.Extension, " ") + System.DateTime.Now.ToString().Replace(":", ".").Replace("/", "") + file.Extension);
            }
            else if (deleteEmail)
            {
                string path = csvExportCompleted + @"\" + file.Name.Replace(file.Extension, " Duplicate ") + System.DateTime.Now.ToString().Replace(":", ".").Replace("/", "") + file.Extension;
                file.MoveTo(path);
            }
            else
            {
                file.MoveTo(csvExportFailed + @"\" + file.Name);
            }
        }

        public bool ProcessPathToConversion(DataTable dt)
        {
            if (dt.Rows.Count == 0)
                return false;

            TableChecker(dt);
            ColumnChecker(dt);  
            CleanDataFeed(dt);

            return ProcessPathToConversionReport(dt);
        }

        private void TableChecker(DataTable dt)
        { 
            int idxVisit = 0;
            int idxNonVisit = 0;

            foreach (DataRow dr in dt.Rows)
            {
                if (dr["ConversionTagName"].ToString().ToLower().Contains("visit"))
                    idxVisit++;
                else
                    idxNonVisit++;
            }

            if (idxVisit > idxNonVisit )
                tableName = "dataSizmekPathToConversion_Visits";
        }

        /// <summary>
        /// This method is the same as its counter part except for the file is cleansed.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool ProcessCleansedPathToConversionFile(DataTable dt)
        {
            if (dt.Rows.Count == 0)
                return false;
             bool result =  ProcessPathToConversionReport(dt);

            return result;
        }

        public bool ProcessPathToConversionReport(DataTable dt)
        {
            bool processed = false;
            DateTime today = System.DateTime.Now.Date;
            DateTime[] reports = GetReportDates();

            DataSet ds = dt.ToDataSetDateSplit();

            foreach (DataTable datatab in ds.Tables)
            {
                Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();
                DateTime tablename = DateTime.Parse(datatab.TableName.ToString());
                if (reports.Count() > 0)
                {
                    if (reports.Contains(tablename))
                    {
                        bool comparedata = CompareReportDataAgainstDatabaseData(datatab);

                        if (comparedata)
                        { 
                            deleteEmail = true;
                            continue;
                        }
                        else if (reimportData)
                        {
                            DeleteOldData(datatab);
                        }
                        else
                        {
                            deleteEmail = false;
                            return false;
                        }

                    }
                    else if (tablename == today)
                    {
                        continue;
                    }
                    else
                    {
                        deleteEmail = false;
                    }
                }

                processed = ProcessPathToConversionTable(datatab);

                if (!processed)         // If at least one insert was improper than all need to be reviewed
                    return processed;
            }

            return processed;
        }
        private void DeleteOldData(DataTable dt)
        {
            try
            {
                string query = String.Format(@"delete FROM [dbo].[{0}] where AsOfDate = '{1}'", tableName, dt.TableName);

                Processor.RunQuery(query);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private bool ProcessPathToConversionTable(DataTable dt)
        {
            try
            {
                if (dt.Columns.Contains("Idx"))
                {
                    dt.Columns.Remove("Idx");
                    dt.AcceptChanges();
                }

                bool bolChecker = DebugHelper.CheckColumnsAgainstDatabase(Processor, dt, tableName);
                string[] columnCol = GetColumnsFromDatabase();

                Console.WriteLine("ProcessPathToConversion: Started " + Processor.DatabaseName + dt.TableName + System.DateTime.Now.ToString());
                SqlConnection conn = new SqlConnection(Processor.ConnectionString);

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);
                     
                    bulkCopy.BatchSize = 100000;
                    bulkCopy.BulkCopyTimeout = 5000;

                    foreach (DataColumn dc in dt.Columns)
                    {
                        if (dc.ColumnName == "Idx")
                            continue;

                        string dbCol = "";

                        foreach (string c in columnCol)
                        {
                            if (dc.ColumnName.ToLower() == c.ToLower())
                            {
                                dbCol = c;
                                break;
                            }
                        }

                        if (dbCol == "")
                            throw new Exception("Column not matching: " + dc.ColumnName.ToString());

                        SqlBulkCopyColumnMapping mapping = new SqlBulkCopyColumnMapping(dc.ColumnName, dc.ColumnName);
                        bulkCopy.ColumnMappings.Add(mapping);
                    }

                    bulkCopy.DestinationTableName = tableName;

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 200000;
                    conn.Open();
                     
                    bulkCopy.WriteToServer(dt);

                    ProcessPathToConversionSummary(dt, dt.Rows[0]["ModifiedDate"].ToString());      // TODO: Fix

                    Console.WriteLine("ProcessPathToConversionTable: Finished " + Processor.DatabaseName + dt.TableName + System.DateTime.Now.ToString());

                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("CustomReportingData: ERROR " + Processor.DatabaseName + dt.TableName + System.DateTime.Now.ToString());
                SolutionHelper.Error.LogError(ex, "ProcessPathToConversionTable:" + Processor.DatabaseName);
            }

            return false;
        }

        private void ProcessPathToConversionSummary(DataTable dt, string modifieddate)
        {

            if (debug)
                if (tableName == "dataSizmekPathToConversion_Visits")
                    return; // This is strictly for testing.


            try
            {
                string query = string.Format("EXEC [dbo].[spProcessPathToConversionDetailsReport] '{0}'", modifieddate);

                Processor.RunQuery(query);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ProcessPathToConversionSummary: ERROR " + Processor.DatabaseName + dt.TableName + System.DateTime.Now.ToString());
                SolutionHelper.Error.LogError(ex, "ProcessPathToConversionSummary:" + Processor.DatabaseName);
                throw ex;
            }
        }

        /// <summary>
        /// This is to compare the total records and the sum of the impressions against the database. 
        /// </summary>
        /// <param name="datatab"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private bool CompareReportDataAgainstDatabaseData(DataTable datatab)
        {
            //if (_override)
            //{
            //    reimportData = true;
            //    return false;
            //}

            try
            {
                string imps = "";

                string query = String.Format(@"SELECT count(*) Total FROM [dbo].[{0}] where AsOfDate =  '{1}'", tableName, datatab.TableName);

                DataSet ds = Processor.RunQuery(query);


                if (ds != null)
                {
                    if (ds.Tables.Count != 0)
                    {
                        int _sumImp = 0;
                        int total = int.Parse(ds.Tables[0].Rows[0]["Total"].ToString()); 
                         
                        int _Newtotal = datatab.Rows.Count;

                        if (total == _Newtotal || total == 0 )
                        {
                            return true;
                        }
                        else
                            reimportData = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }

        private DateTime[] GetReportDates()
        {
            List<DateTime> dates = new List<DateTime>();

            string query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[{0}] where [AsOfDate] is not null", tableName);

            DataSet ds = Processor.RunQuery(query);

            if (ds != null) // Add date to list of date
                if (ds.Tables.Count != 0) // Add date to list of date
                    foreach (DataRow dr in ds.Tables[0].Rows)
                        dates.Add(DateTime.Parse(dr[0].ToString())); // Date is a non nullable date type

            return dates.ToArray();
        }

        /// <summary>
        /// Clean the data based on the column type define in the AddColumn method 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private DataTable CleanDataFeed(DataTable dt)
        {
            Console.WriteLine("Begin: Cleaning data - " + System.DateTime.Now.ToString());

            DateTime starttime = System.DateTime.Now;
            string column = "";

            int idx = 0;

            try
            {
                foreach (DataRow dr in dt.Rows)
                {
                    try
                    {
                        idx++;

                        if (idx == 9999)
                            column = column;

                        foreach (DataColumn col in dt.Columns)
                        {
                            try
                            {
                                if (col.ColumnName.ContainsAny("ModifiedDate", "AsOfDate", "Advertiser Name", "Campaign Name", "Campaign Id", "Site Name", "Site Id", "Package Name", "Placement Name", "Placement Id", "Ad Name", "Ad Id", "State", "City", "Dma", "Zip", "Device Type Name", "Entity Type"))
                                    continue;

                                column = col.ColumnName + ": " + dr[col.ColumnName].ToString();
                                //  Console.WriteLine(column);

                                if (col.ColumnName.ToUpper().ContainsAny("CLICKS",
                                   "POST-IMPRESSION CONVERSIONS",
                                   "POST IMP",
                                   "IMPRESSIONS",
                                   "UNIQUE USERS",
                                   "POST-CLICK CONVERSIONS",
                                   "POST CLICK CON",
                                   "TOTAL CONVERSIONS",
                                   "TOTALCON",
                                   "VIDEO STARTED",
                                   "VIEWABLE IMPRESSIONS",
                                   "VIDEO FULL PLAY",
                                   "VIDEO PLAYED 25",
                                   "VIDEO PLAYED 50",
                                   "Section Totalco".ToUpper(),
                                   "Section Post Im".ToUpper(),
                                   "Section Post Cl".ToUpper(),
                                   "VIDEO PLAYED 75"))
                                {
                                    string apr = dr[col.ColumnName].ToString();

                                    try
                                    {

                                        if (String.IsNullOrEmpty(dr[col.ColumnName].ToString()) || dr[col.ColumnName].ToString().ToUpper() == "NULL")
                                            dr[col.ColumnName] = 0;

                                        if (dr[col.ColumnName].ToString().Contains(","))
                                            dr[col.ColumnName] = dr[col.ColumnName].ToString().Replace(",", "");

                                        dr[col.ColumnName] = decimal.ToInt32(decimal.Parse(apr));
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine("Error Message: " + ex.Message);
                                        throw;
                                    }
                                }

                                if (col.ColumnName.ToUpper().ContainsAny("VIEWABILITY RATE", "VIDEO FULLY PLAYED RATE"))
                                {
                                    if (String.IsNullOrEmpty(dr[col.ColumnName].ToString()) || dr[col.ColumnName].ToString().ToUpper() == "NULL")
                                        dr[col.ColumnName] = 0;
                                    else if (dr[col.ColumnName].ToString().ContainsAny(",", "%"))
                                        dr[col.ColumnName] = dr[col.ColumnName].ToString().Replace(",", "").Replace("%", "");
                                }

                                column = "";
                            }
                            catch (Exception ex)
                            { 
                                Console.WriteLine(column);
                                string msg = ex.Message;
                                throw;
                            }
                        }

                        if (idx % 10000 == 0 && idx != 0)
                        {
                            try
                            {
                                DateTime currenttime = System.DateTime.Now;
                                string duration = currenttime.Subtract(starttime).TotalMilliseconds.ToString();

                                starttime = System.DateTime.Now;
                                string fd = ((int.Parse(duration) / 10000) * dt.Rows.Count - idx).ToString();
                                string EstimatedCompletion = starttime.AddMilliseconds(double.Parse(fd)).ToString();
                                Console.WriteLine("Processing: " + idx.ToString() + " of " + dt.Rows.Count.ToString() + " Duration " + (int.Parse(duration) / 1000).ToString() + " Estimated: " + EstimatedCompletion);
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        throw;
                    }
                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }

            Console.WriteLine("Finished: Cleaning data - " + System.DateTime.Now.ToString());

            return dt;
        }

        /// <summary>
        /// Check if all the columns are created in the database for the reporting table. If the column doesnt exist it will be added.
        /// </summary>
        /// <param name="dt"></param>
        private void ColumnChecker(DataTable dt)
        {
            Console.WriteLine("Begin: Column Checker - " + System.DateTime.Now.ToString());
             
            try
            {

                foreach (string s in removeColumns)
                {
                    if (dt.Columns.Contains(s.ToProperCase()))
                    {
                        dt.Columns.Remove(s);
                        dt.AcceptChanges();
                    }
                } 

                DateTime today = System.DateTime.Now;

                string[] columnCol = GetColumnsFromDatabase();

                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    string col1 = "Param" + i.ToString() + "name";
                    string col2 = "Param" + i.ToString() + "value";

                    if (dt.Columns.Contains(col1))
                        dt.Columns.Remove(col1);

                    if (dt.Columns.Contains(col2))
                        dt.Columns.Remove(col2);
                }

                foreach (DataColumn dc in dt.Columns)
                {
                    if (dc.ColumnName.Contains("* "))
                    {
                        dc.ColumnName = dc.ColumnName.Replace("* ", "");
                    }
                    if (dc.ColumnName.Contains("_"))
                    {
                        dc.ColumnName = dc.ColumnName.Replace("_", " ");
                    }

                    string col = dc.ColumnName.ToLower().Trim();

                    if (col.ToLower() == "date" || col.ToLower() == "asofdate")
                    {
                        dc.ColumnName = "AsOfDate";
                        continue;
                    }
                    if (dc.ColumnName.ToLower() == "modifieddate")
                    {
                        dc.ColumnName = "ModifiedDate";
                        continue;
                    }

                    dc.ColumnName = dc.ColumnName.ToProperCase();

                    if (!columnCol.Contains(col))
                    {
                        if (dc.ColumnName.Length > 150)
                            throw new Exception("Column too large");

                        AddColumn(tableName, dc.ColumnName);
                    }
                }

                if (!dt.Columns.Contains("ModifiedDate"))
                {
                    System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                    newColumn.DefaultValue = today.ToString();
                    dt.Columns.Add(newColumn);
                }

                if (!dt.Columns.Contains("AsOfDate"))
                {
                    System.Data.DataColumn newColumn = new System.Data.DataColumn("AsOfDate", typeof(System.DateTime));
                    dt.Columns.Add(newColumn);

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dt.Columns.Contains("Conversiondate"))
                            dr["AsOfDate"] = DateTime.Parse(dr["Conversiondate"].ToString()).ToShortDateString();
                        else if (dt.Columns.Contains("Conversion Time"))
                            dr["AsOfDate"] = DateTime.Parse(dr["Conversion Time"].ToString()).ToShortDateString();
                    }

                }

                bool bolChecker = DebugHelper.CheckColumnsAgainstDatabase(Processor, dt, tableName);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                throw ex;
            }

            Console.WriteLine("Finished: Column Checker - " + System.DateTime.Now.ToString());
        }
        private string[] GetColumnsFromDatabase()
        {
            string query = "";

            query += "IF Object_id('tempdb..#list') IS NOT NULL   DROP TABLE #list ";
            query += "DECLARE @columns varchar(max) ";
            query += String.Format("SELECT COLUMN_NAME into #list FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{0}' order by COLUMN_NAME ", tableName);
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(max), COLUMN_NAME) FROM (select COLUMN_NAME from #list) as tab select @columns ";
            query += "IF Object_id('tempdb..#list') IS NOT NULL   DROP TABLE #list ";

            DataSet ds = Processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString();

            string[] columnCol = columns.ToLower().Split(',').Select(x => x.Trim()).ToArray();

            return columnCol;
        }

        public static DataTable GetDataTabletFromCSVTSVFile(string csv_file_path)
        {
            if (csv_file_path.Contains(".csv"))
                return GetDataTabletFromFile(csv_file_path, "\t");
            if (csv_file_path.Contains(".tsv") || csv_file_path.Contains(".txt"))
                return GetDataTabletFromFile(csv_file_path, "\t");
            if (csv_file_path.Contains(".xls"))
                return GetDataTabletXls(csv_file_path);
            else
                return null;
        }

        private static DataTable GetDataTabletXls(string csv_file_path)
        {
            string error = "";

            try
            {
                DataTable table = new DataTable();
                PathToConversionProcessing p = new PathToConversionProcessing();

                List<string> sheetNames = p.ListSheetInExcel(csv_file_path);

                string sheetName = sheetNames[0].Replace("'", "");
                string strConn = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1;TypeGuessRows=0;ImportMixedTypes=Text\"", csv_file_path);

                try
                {
                    using (OleDbConnection dbConnection = new OleDbConnection(strConn))
                    {
                        if (!sheetName.Contains("$"))
                            sheetName = sheetName + "$";

                        using (OleDbDataAdapter dbAdapter = new OleDbDataAdapter(String.Format("SELECT * FROM [{0}]", sheetName), dbConnection)) //rename sheet if required!
                            dbAdapter.Fill(table);

                        return table;
                    }
                }
                catch (Exception ex)
                {
                    error = ex.Message + Environment.NewLine;
                }

                try
                {
                    var connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0; data source={0};Extended Properties='Excel 12.0;IMEX=1;HDR=YES;TypeGuessRows=0;ImportMixedTypes=Text'", csv_file_path);

                    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + csv_file_path + @";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;'");

                    OleDbCommand cmd = new OleDbCommand(String.Format("SELECT * FROM [{0}$] WHERE [F4] <> 'Date'", sheetName), connection);
                    OleDbDataAdapter da = new OleDbDataAdapter(cmd);

                    DataSet ds = new DataSet();
                    ds.Tables.Add("xlsImport", "Excel");
                    da.Fill(ds, "xlsImport");

                    DataTable dt = ds.Tables[0];

                    return dt;
                }
                catch (Exception ex)
                {
                    error += ex.Message;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ProcessPathToConversion: GetDataTabletXls Error: " + csv_file_path);
                SolutionHelper.Error.LogError(ex, "ProcessPathToConversion.GetDataTabletXls: " + csv_file_path);
            }

            return null;
        }

        public List<string> ListSheetInExcel(string filePath)
        {
            OleDbConnectionStringBuilder sbConnection = new OleDbConnectionStringBuilder();
            String strExtendedProperties = String.Empty;
            sbConnection.DataSource = filePath;
            if (Path.GetExtension(filePath).Equals(".xls"))             //for 97-03 Excel file
            {
                sbConnection.Provider = "Microsoft.Jet.OLEDB.4.0";
                strExtendedProperties = "Excel 8.0;HDR=Yes;IMEX=1";     //HDR=ColumnHeader,IMEX=InterMixed
            }
            else if (Path.GetExtension(filePath).Equals(".xlsx"))       //for 2007 Excel file
            {
                sbConnection.Provider = "Microsoft.ACE.OLEDB.12.0";
                strExtendedProperties = "Excel 12.0;HDR=Yes;IMEX=1";
            }
            sbConnection.Add("Extended Properties", strExtendedProperties);
            List<string> listSheet = new List<string>();
            using (OleDbConnection conn = new OleDbConnection(sbConnection.ToString()))
            {
                conn.Open();
                DataTable dtSheet = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                foreach (DataRow drSheet in dtSheet.Rows)
                {
                    if (drSheet["TABLE_NAME"].ToString().Contains("$")) //checks whether row contains '_xlnm#_FilterDatabase' or sheet name(i.e. sheet name always ends with $ sign)
                    {
                        listSheet.Add(drSheet["TABLE_NAME"].ToString());
                    }
                }
            }
            return listSheet;
        }

        public static DataTable GetDataTabletFromFile(string csv_file_path, string delimeter)
        {
            DataTable csvData = new DataTable();
            try
            {
                using (TextFieldParser csvReader = new TextFieldParser(csv_file_path))
                {
                    csvReader.SetDelimiters(new string[] { delimeter });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    string[] colFields = csvReader.ReadFields();

                    if (colFields.Count() == 1)
                    {
                        if (colFields[0].ToString().Contains(","))
                        {
                            csvReader.SetDelimiters(new string[] { "," });
                            colFields = colFields[0].Split(',');
                        }
                        else if (colFields[0].ToString().Contains("\t"))
                        {
                            csvReader.SetDelimiters(new string[] { "\t" });
                            colFields = colFields[0].Split('\t');
                        }
                    }

                    foreach (string column in colFields)
                    {
                        DataColumn datecolumn = new DataColumn(column);
                        datecolumn.AllowDBNull = true;
                        csvData.Columns.Add(datecolumn);
                    }
                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();
                        //Making empty value as null
                        for (int i = 0; i < fieldData.Length; i++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = null;
                            }
                        }
                        csvData.Rows.Add(fieldData);
                    }
                }
            }
            catch (Exception ex)
            {
                string dido = "";
            }
            return csvData;
        }
 


        private void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(String.Format("{0} Rows have been copied.", e.RowsCopied.ToString()));
        }

        /// <summary>
        /// Check if all the columns are created in the database for the reporting table. If the column doesnt exist it will be added.
        /// </summary>
        /// <param name="dt"></param>
        private void ColumnChecker(DataTable dt, string tableName)
        {
            Console.WriteLine("Begin: Column Checker - " + System.DateTime.Now.ToString());

            try
            {
                DateTime today = System.DateTime.Now;

                string[] columnCol = GetColumnsFromDatabase(tableName);

                foreach (DataColumn dc in dt.Columns)
                {
                    if (dc.ColumnName.Contains("* "))
                    {
                        dc.ColumnName = dc.ColumnName.Replace("* ", "");
                    }
                    if (dc.ColumnName.Contains("_"))
                    {
                        dc.ColumnName = dc.ColumnName.Replace("_", " ");
                    }

                    string col = dc.ColumnName.ToLower().Trim();

                    if (col.ToLower() == "date" || col.ToLower() == "asofdate")
                    {
                        dc.ColumnName = "AsOfDate";
                        continue;
                    }
                    if (dc.ColumnName.ToLower() == "modifieddate")
                    {
                        dc.ColumnName = "ModifiedDate";
                        continue;
                    }

                    dc.ColumnName = dc.ColumnName.ToProperCase();

                    if (!columnCol.Contains(col))
                    {
                        if (dc.ColumnName.Length > 150)
                            throw new Exception("Column too large");

                        AddColumn(tableName, dc.ColumnName);
                    }
                }

                if (!dt.Columns.Contains("ModifiedDate"))
                {
                    System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                    newColumn.DefaultValue = today.ToString();
                    dt.Columns.Add(newColumn);
                }

                bool bolChecker = DebugHelper.CheckColumnsAgainstDatabase(Processor, dt, tableName);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                throw ex;
            }

            Console.WriteLine("Finished: Column Checker - " + System.DateTime.Now.ToString());
        }

        /// <summary>
        /// Due to the columns needing to be dynamic this method is designed to add additional column to the table
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        private void AddColumn(string table, string column)
        {
            bool processColumn = false;

            column = column.ToProperCase();

            string query = "ALTER TABLE {0} ADD [{1}] {2}";

            if (column.ToUpper().ContainsAny("Date".ToUpper()))
                query = string.Format(query, table, column, "[DateTime] NULL");
            if (column.ToUpper().ContainsAny("VersionClassificationID".ToUpper()))
                query = string.Format(query, table, column, "[int] NULL");
            if (column.ToUpper().ContainsAny("HasPaid".ToUpper(), "HasSite".ToUpper(), "WasViewable".ToUpper(), "DidAutoExpand".ToUpper(), "DidUserExpand".ToUpper(), "DidUserInteract".ToUpper(), "WasVideoCompleted".ToUpper(), "WasVideoStarted".ToUpper(), "WasDwelled".ToUpper()))
                query = string.Format(query, table, column, "[varchar](2) NULL");
            if (column.ToUpper().ContainsAny("QueryString".ToUpper(), "WinningClickthroughURL".ToUpper()))
                query = string.Format(query, table, column, "[varchar](MAX) NULL");
            if (column.ToUpper().ContainsAny("WinningPlacementName/WinningSearchKeywordName".ToUpper(), "ClickthroughURL".ToUpper(), "PackageName".ToUpper(), "WinningClickthroughURL".ToUpper(), "DisplayAdName/SearchAdName".ToUpper(), "PlacementName/SearchKeywordName".ToUpper()))
                query = string.Format(query, table, column, "[varchar](250) NULL");
            else
                query = string.Format(query, table, column, "[varchar](75) NULL");

            Processor.RunQuery(query);
        }

        private string[] GetColumnsFromDatabase(string tableName)
        {
            string query = "";

            query += "IF Object_id('tempdb..#list') IS NOT NULL   DROP TABLE #list ";
            query += "DECLARE @columns varchar(max) ";
            query += String.Format("SELECT COLUMN_NAME into #list FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{0}' order by COLUMN_NAME ", tableName);
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(max), COLUMN_NAME) FROM (select COLUMN_NAME from #list) as tab select @columns ";
            query += "IF Object_id('tempdb..#list') IS NOT NULL   DROP TABLE #list ";

            DataSet ds = Processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString();

            string[] columnCol = columns.ToLower().Split(',').Select(x => x.Trim()).ToArray();

            return columnCol;
        }

        private DataTable ExtractDatatable(FileInfo file)
        {
            DataTable dt = new DataTable(); 

            dt =  GetDataTabletFromCSVTSVFile(file.FullName.ToString());

            return dt;
        }

        private Company GetCompany(string companyName)
        {
            Company[] companys = (from m in db.Companies where m.IsActive == true && m.Sizemek_Keyword != null select m).ToArray();

            foreach (Company company in companys)
            {
                if (companyName.ToLower().ContainsAny(company.Sizemek_Keyword.ToLower().Split(',')))
                {
                    Data processor = new Data(company.DatabaseName, company.Login, company.Password, company.Server);

                    Processor = processor;
                    return company;
                }
            }

            return null;
        }

        public DataTable GetBaseTable(List<string> rows)
        {
            DataTable dt = new DataTable();
            int idx = 0;
            int counter = 0;
            int colIdx = 75;

            foreach (string field in rows[0].ToString().Split(',', '\t'))
            {
                if (idx < colIdx)
                {
                    DataColumn dc = new DataColumn { ColumnName = field };
                    dt.Columns.Add(dc);
                }

                idx++;
            }

            idx = 0; // reset counter

            foreach (string row in rows)
            {
                string[] _fields = row.Split(',', '\t').Take(colIdx).Select(i => i.ToString()).ToArray();
                if (idx != 0)
                {
                    DataRow dr = dt.NewRow();
                    dr.ItemArray = _fields; // (string[])row.Split(',').Take(colIdx);
                    dt.Rows.Add(dr);
                    dt.AcceptChanges();
                }

                if (counter == 10000)
                {
                    Console.WriteLine("Processing Base Table : " + idx + " of " + rows.Count + "  " + System.DateTime.Now);
                    counter = 0;
                }

                idx++;
                counter++;
            }

            return dt;
        }
        public DataTable GetDerivedTable(List<string> rows)
        {
            DataTable dt = new DataTable();
            int idx = 0;
            int counter = 0;
            int idxBegin = 75;
            int idxEnd = 119;

            foreach (string field in rows[0].ToString().Split(',', '\t'))
            {
                if (idxBegin <= idx && idx < idxEnd || idx == 0 || idx == 1 || idx == 2)
                {
                    DataColumn dc = new DataColumn { ColumnName = field.Replace("1", "") };
                    dt.Columns.Add(dc);
                }
                idx++;
            }

            idx = 0;

            for (int i = 0; i < 100; i++)
            {

                Console.WriteLine("Processing Event :" + i + "     " + System.DateTime.Now);
                foreach (string row in rows)
                {
                    var offset = row.IndexOf('\t');
                    offset = row.IndexOf('\t', offset + 1);
                    var result = row.IndexOf('\t', offset + 1);

                    string remainder = row.Substring(50).Replace("\t", "");

                    if (remainder == "")
                        continue;

                    if (idx != 0) //      if (idx != 10000000000000000) // 
                    {

                        List<string> _newRecord = new List<string>();
                        int colIdx = 0;

                        string[] _fields = row.Split(',', '\t');

                        foreach (string _field in _fields)
                        {
                            if (idxBegin <= colIdx && colIdx < idxEnd || colIdx == 0 || colIdx == 1 || colIdx == 2)
                            {
                                _newRecord.Add(_field);
                            }

                            colIdx++;
                        }

                        DataRow dr = dt.NewRow();
                        dr.ItemArray = _newRecord.ToArray();
                        dt.Rows.Add(dr);
                        dt.AcceptChanges();
                    }

                    if (counter == 10000)
                    {
                        Console.WriteLine("Processing Event " + i + " Table : " + idx + " of " + rows.Count + "  " + " - " + System.DateTime.Now);
                        counter = 0;
                    }

                    counter++;
                    idx++;
                }

                idxBegin += 44;
                idxEnd += 44;
            }

            return dt;
        }

        #region removeColums
        string[] removeColumns = new string[] {
            "Last Impression FCap",
            "Order Id",
            "# Non-display Clicks",
            "# Frequency Cycles and Ad Groups Where FCap is Reached",
            "# Frequency Cycles and Ad Groups w/ Activity",
            "# Frequency Cycles and Ad Groups Where FCap is Reached",
            "Path to Conversion",
            "Monetary Value",
            "Monetary Value Currency",
            "Last Impression Categories",
            "Last Impression Country",
            "Last Impression Metro",
            "First Display Click Time",
            "First Display Click Campaign Id",
            "First Display Click Campaign Name",
            "First Display Click Ad Group Id",
            "First Display Click Ad Group Name",
            "First Display Click Creative Id",
            "First Display Click Creative Name",
            "First Display Click Format",
            "Last Display Click Time",
            "Last Display Click Campaign Id",
            "Last Display Click Campaign Name",
            "Last Display Click Ad Group Id",
            "Last Display Click Ad Group Name",
            "Last Display Click Creative Id",
            "Last Display Click Creative Name",
            "Last Display Click Format",
            "First Non-display Click Time",
            "First Non-display Click Channel",
            "First Non-display Click Distribution Network",
            "First Non-display Click Distribution Match Type",
            "First Non-display Click Campaign Id",
            "First Non-display Click Campaign Name",
            "First Non-display Click Keyword Id",
            "First Non-display Click Keyword",
            "Last Non-display Click Time",
            "Last Non-display Click Channel",
            "Last Non-display Click Distribution Network",
            "Last Non-display Click Distribution Match Type",
            "Last Non-display Click Campaign Id",
            "Last Non-display Click Campaign Name",
            "Last Non-display Click Keyword Id",
            "Last Non-display Click Keyword",
            "Partner Id",
            "TD 1",
            "TD 2",
            "TD 3",
            "TD 4",
            "TD 5",
            "TD 6",
            "TD 7",
            "TD 8",
            "TD 9",
            "TD 10",
            "Full Path to Conversion",
            "First Display Click Device Type",
            "First Display Click Ad Environment",
            "Last Display Click Device Type",
            "Last Display Click Ad Environment",
            "First Non-display Click Device Type",
            "First Non-display Click Ad Environment",
            "Last Non-display Click Device Type",
            "Xdid",
            "Last Non-display Click Ad Environment"};

        #endregion
    }
}
