﻿using OpenPop.Mime;
using OpenPop.Pop3;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using SolutionHelper;
using System.Data.SqlClient;
using System.Configuration;
using WhirlygigSolutionHelper;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace SizmekMDXReporting
{
    public class ReportingData
    {
        mediaLavidgeEntities db = new mediaLavidgeEntities();

        public Company company;

        public Data Processor = new Data();

        string server;
        string accountname;
        string accountpassword;
        string csvExportCompleted;
        string csvExportFailed;
        string csvExportWaiting;
        string csvExport;
        bool debug;
        bool addendum;
        bool duplicate;
        bool reimportData;
        bool isHistorical = false;
        string tableName = "dataSizmekReportingData";
        public bool completeFile;
        SizmekEmails[] colEmailCollections = null;
        private string fullName;

        #region Public Methods  

        public ReportingData(string companyName = "")
        {
            debug = bool.Parse(ConfigurationManager.AppSettings["debug"].ToString());
            server = ConfigurationManager.AppSettings["EmailAccountServer"];
            accountname = ConfigurationManager.AppSettings["EmailAccountName"];
            accountpassword = ConfigurationManager.AppSettings["EmailAccountPassword"];
            csvExportCompleted = ConfigurationManager.AppSettings["csvExportCompleted"];
            csvExportFailed = ConfigurationManager.AppSettings["csvExportFailed"];
            csvExport = ConfigurationManager.AppSettings["csvExport"];
            csvExportWaiting = ConfigurationManager.AppSettings["csvExportWaiting"];

            if (companyName != "")
                company = GetCompany(companyName);
        }

        public void Run()
        {
            ProcessReports();
        }

        public bool CleanseFile(FileInfo file, string csvExportProcessing)
        {
            try
            {
                DataTable dt = ExtractDatatable(file);
                dt.TableName = file.Name.Replace(file.Extension, ".tsv");

                ColumnChecker(dt, tableName);
                CleanData(dt);

                dt.ToDataSetExportTsv(csvExportProcessing);
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.ReportingData.CleanseFile: " + ex.Message);
                return false;
            }

            return true;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Example showing:
        ///  - how to fetch all messages from a POP3 server
        /// </summary>
        /// <param name="hostname">Hostname of the server. For example: pop3.live.com</param>
        /// <param name="port">Host port to connect to. Normally: 110 for plain POP3, 995 for SSL POP3</param>
        /// <param name="useSsl">Whether or not to use SSL to connect to server</param>
        /// <param name="username">Username of the user on the server</param>
        /// <param name="password">Password of the user on the server</param>
        ///  http://hpop.sourceforge.net/
        /// <returns>All Messages on the POP3 server</returns>
        private void ProcessReports()
        {
            try
            {
                restart: // This goto was implemented due to a Pop3Client timeout. Connection to the mail server needs to be reauthenticated between each message

                DateTime startTime = System.DateTime.Now;

                using (Pop3Client client = new Pop3Client())
                {
                    client.Connect(server, 110, false);
                    client.Authenticate(accountname, accountpassword);

                    int messageCount = client.GetMessageCount();

                    if (messageCount != 0)
                    {
                        GenerateMessageQueue(client, messageCount);

                        for (int i = 1; i <= messageCount; i++) // Loop through messages
                        {
                            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();

                            bool result = false;

                            Reset();

                            Message message = client.GetMessage(i);

                            Console.WriteLine("Processing " + i + " of " + messageCount + ": " + message.Headers.Subject + " " + message.Headers.Date.ToString());

                            if (message.Headers.Subject != null)
                            {
                                if (message.Headers.Subject.ContainsAny(ConfigurationManager.AppSettings["EmailSubject"].Split(',')))
                                {
                                    if (message.Headers.Subject.ContainsAny("Custom Report", "Custom Report Report"))
                                    {
                                        if (message.Headers.Subject.ToUpper().ContainsAny("MONTHLY", "WEEKLYHISTORY"))
                                           isHistorical = true;

                                        if (message.Headers.Subject.ToUpper().ContainsAny("HISTORY REPORT"))
                                            if (!message.Headers.Subject.ToUpper().ContainsAny("WEEKLYHISTORY"))
                                                continue; // Files have become too big to process.

                                        result = ProcessAnalytics(message);

                                        if (result || completeFile)
                                        {
                                            client.DeleteMessage(i);
                                            goto restart;
                                        }
                                        else result = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.OpenPopHandler: " + ex.Message);
            }
        }

        private bool ProcessAnalytics(Message message)
        {
            bool result = false;

            Console.WriteLine("Processing: " + message.Headers.Subject.ToString());

            try
            {
                company = GetCompany(message.Headers.Subject.ToString());

                if (company == null)  //    This should be a manual process
                    return false;

                if (company.Id == 4)
                    if (message.Headers.Subject.Contains("MonthlyCustom"))  //    This should be a manual process
                        return false;
                 
                bool _override = ProcessExceptions(message, ref result);

                if (_override)
                    result = ProcessMessage(message);

                Console.WriteLine("Finished: " + message.Headers.Subject.ToString());

                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in processing: " + message.Headers.Subject.ToString() + ex.Message.ToString());
                SolutionHelper.Error.LogError(ex, "SizmekMDXReporting.ProcessAnalytics:");
            }

            return result;
        }
          
        private bool ProcessExceptions(Message message, ref bool result)
        {
            if (company != null)
            {
                if (company.Abbreviation == "ASU")
                {
                    if (colEmailCollections != null)
                    {
                        if (message.Headers.Subject.ContainsAny(ConfigurationManager.AppSettings["ReportVariations"].Split(',')))
                        {
                            SizmekEmails _email = colEmailCollections.Where(v => v.CompanyName == company.CompanyName && !v.MessageSubject.Contains("Monthly") && v.MessageSubject.Contains("Report1")).FirstOrDefault();

                            if (_email == null)
                            {
                                addendum = true;
                                return true;
                            }
                            else
                                return false;

                        }
                        else if (message.Headers.Subject.Contains("Report1"))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        SizmekEmails _email = colEmailCollections.Where(v => v.CompanyName == "ERROR").FirstOrDefault();

                        if (_email != null)
                        {
                            SolutionHelper.Error.LogError("SizmekMDXReporting.ProcessAnalytics.Error Getting Emails");
                        }
                    }
                }
                else
                {
                    return true;
                }
            }

            return false;
        }

        private bool ProcessMessage(Message message)
        {
            try
            {
                List<MessagePart> attachments = message.FindAllAttachments();

                Stream stream = new MemoryStream(attachments[0].Body);

                DataTable dt = new DataTable();

                if (attachments[0].FileName.Contains(".zip"))
                    stream = stream.ToStreamFromZip();
                if (attachments[0].FileName.Contains(".tsv"))
                    dt = stream.ToDataTableTsv(16);
                else if (attachments[0].FileName.Contains(".csv"))
                    dt = stream.ToDataTableCsv(16);

                if (dt.Columns[0].ColumnName.ToString().ToLower().Contains("data was updated last") || dt.Rows.Count == 0)
                {
                    completeFile = true;
                    return false;
                }

                bool processed = ProcessAnalyticsReport(dt);

                return processed;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "SizmekMDXReporting.ProcessMessage:");
            }

            return false;
        }

        private bool ProcessAnalyticsReport(DataTable dt)
        {
            bool processed = false;
             
            ColumnChecker(dt, tableName);
             
            try
            {
                DateTime[] reports = GetReportDates(tableName, dt);
                DataSet ds = dt.ToDataSetDateSplit();

                Console.WriteLine("SizmekMDXReporting - ProcessAnalyticsReport: " + System.DateTime.Now);
                Console.WriteLine(" ");

                DateTime lasttablename = DateTime.Parse(ds.Tables[ds.Tables.Count - 1].TableName.ToString());

                foreach (DataTable datatab in ds.Tables)
                {
                    DateTime tablename = DateTime.Parse(datatab.TableName.ToString());

                    WhirlygigSolutionHelper.ConsoleWriter.WriteCurrentLine("ProcessAnalyticsReport: Processing - " + tablename.ToShortDateString() + " of " + lasttablename.ToShortDateString());

                    Console.WriteLine("ProcessAnalyticsReport: Processing - " + tablename.ToShortDateString() + " of " + lasttablename.ToShortDateString());

                    if (company.CompanyName == "Arizona State University" && addendum)
                    {
                        if (reports.Contains(tablename))
                            processed = ProcessSizmekStagingDatatable(datatab, "dataSizmekReportingData_Staging");
                        else
                            return false;
                    }
                    else
                    {
                        reimportData = false;

                        if (reports.Contains(tablename))
                        {
                            bool comparedata = CompareReportDataAgainstDatabaseData(datatab, tableName, true);  //MIT 7.24.2018: Overriding all data compares

                            if (comparedata)
                            {
                                completeFile = true;
                                continue;
                            }
                            else if (reimportData)
                            {
                                DeleteOldData(datatab);
                            }
                            else
                            {
                                completeFile = false;
                                return false;
                            }
                        }
                        else
                        {
                            completeFile = false;
                        }

                        processed = ProcessSizmekDatatable(datatab, tableName);
                    }

                    if (!processed)
                        return processed;
                }
            }
            catch (Exception ex)
            {
                throw ex;   // This error is dw
            }

            return processed;
        }
        private bool ProcessSizmekDatatable(DataTable dt, string tableName)
        {
            try
            {
                string[] columnCol = GetColumnsFromDatabase(tableName, false);

                CleanData(dt);

                SqlConnection conn = new SqlConnection(Processor.ConnectionString);

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);

                    bulkCopy.BatchSize = 500000;
                    bulkCopy.BulkCopyTimeout = 1500;

                    #region ColumnChecker  
                    foreach (DataColumn dc in dt.Columns)
                    {

                        string dbCol = "";

                        foreach (string c in columnCol)
                        {
                            if (dc.ColumnName.ToLower() == c.ToLower())
                            {
                                dbCol = c;
                                break;
                            }
                        }

                        if (dbCol == "")
                        {
                            throw new Exception("Column not matching: " + dc.ColumnName.ToString());
                        }

                        SqlBulkCopyColumnMapping mapping = new SqlBulkCopyColumnMapping(dc.ColumnName, dbCol);

                        bulkCopy.ColumnMappings.Add(mapping);
                    }
                    #endregion

                    bulkCopy.DestinationTableName = tableName;

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 2000;
                    conn.Open();

                    bulkCopy.WriteToServer(dt);

                    return true;
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "SizmekMDXReporting.ProcessSizmekDatatable:" + Processor.DatabaseName);
            }

            return false;
        }

        #region Addendum Methods
        private bool ProcessSizmekStagingDatatable(DataTable datatab, string tableNameStage)
        {
            bool processed = false;

            try
            {
                TuncateStagingTable();

                processed = ProcessSizmekDatatable(datatab, tableNameStage); // Loading data into Staging table

                if (processed)
                {
                    AppendSizmekData(datatab);
                    PrepTableForInsertSizmekData(datatab);
                    InsertSizmekData(datatab);
                }
            }
            catch (Exception ex)
            {
                processed = false;
                SolutionHelper.Error.LogError(ex, "SizmekMDXReporting.ProcessSizmekStagingDatatable:" + Processor.DatabaseName);
            }
            return processed;
        }

        /// <summary>
        /// This process preps the data which does not exist in the initial table 'datasizmekreportingdata'
        /// </summary>
        /// <param name="datatab"></param>
        private void PrepTableForInsertSizmekData(DataTable datatab)
        {
            // This part of this procedure inserts data that was not part of the update. 
            Processor.RunQuery(String.Format("exec [dbo].[spProcessSizmekReportingStaging] '{0}'", datatab.TableName.ToString()));
        }

        /// <summary>
        /// This process inserts data which does not exist in the initial table 'datasizmekreportingdata'
        /// </summary>
        /// <param name="datatab"></param>
        private void InsertSizmekData(DataTable datatab)
        {
            string query = "select * from [mediaArizonaStateUniversity].[dbo].[datasizmekreportingdata_temp];";

            DataSet ds = Processor.RunQuery(query);
            DataTable dt = ds.Tables[0];

            if (!dt.Columns.Contains("ModifiedDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                newColumn.DefaultValue = System.DateTime.Now.ToString();
                dt.Columns.Add(newColumn);
            }

            // This part of this procedure inserts data that was not part of the update. 
            if (dt.Rows.Count != 0)
                ProcessSizmekDatatable(dt, tableName);
        }

        /// <summary>
        /// This process was designed to update the reporting table with the data from a supporting file. The second part of this procedure inserts data that was not part of the update. 
        /// </summary>
        /// <param name="dt"></param>
        private void AppendSizmekData(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();
            // We are using the AsOfDate, Ad Id, and Placement Id as a composite key
            sb.Append("UPDATE " + tableName + " ");
            sb.Append("SET {0} ");
            sb.Append("FROM   datasizmekreportingdata_staging ");
            sb.Append("INNER JOIN " + tableName + " ");
            sb.Append("ON datasizmekreportingdata_staging.AsOfDate = ");
            sb.Append(tableName + ".asofdate ");
            sb.Append("AND datasizmekreportingdata_staging.[ad id] = ");
            sb.Append(tableName + ".[ad id] ");
            sb.Append("AND datasizmekreportingdata_staging.[Placement Id] = ");
            sb.Append(tableName + ".[Placement Id] ");

            string setQuery = "";

            foreach (DataColumn col in dt.Columns)
            {
                if (col.ColumnName.ToString() == "AsOfDate" ||
                    col.ColumnName.ToString() == "Brand" ||
                    col.ColumnName.ToString() == "Campaign Name" ||
                    col.ColumnName.ToString() == "Campaign Id" ||
                    col.ColumnName.ToString() == "Site Name" ||
                    col.ColumnName.ToString() == "Site Id" ||
                    col.ColumnName.ToString() == "Package Name" ||
                    col.ColumnName.ToString() == "Placement Id" ||
                    col.ColumnName.ToString() == "Placement Name" ||
                    col.ColumnName.ToString() == "Ad Id" ||
                    col.ColumnName.ToString() == "Ad Name")
                    continue;

                setQuery += tableName + ".[" + col.ColumnName.ToString() + "] = dataSizmekReportingData_Staging.[" + col.ColumnName.ToString() + "], ";
            }

            setQuery = setQuery.ToString().Remove(setQuery.Length - 2, 2).ToString();

            string query = String.Format(sb.ToString(), setQuery);

            Processor.RunQuery(query);
        }

        private void TuncateStagingTable()
        {
            string query = "exec [dbo].[spProcessSizmekReportingStaging_Step_2]";
            Processor.RunQuery(query);
        }

        #endregion
        #endregion

        #region Internal Methods
        private DataTable ExtractDatatable(FileInfo file)
        {
            DataTable dt = new DataTable();

            int deleteCol = -1;

            dt = CsvToDatabase.GetDataTabletFromCSVTSVFile(file.FullName.ToString());

            if (dt != null)
            {
                if (dt.Columns.Contains("Column1") || dt.Columns.Contains("Column2") || dt.Columns.Contains("Column3"))
                {
                    foreach (DataRow dr in dt.Rows)
                        if (dr[1].ToString() == "" && dr[2].ToString() == "" && dr[3].ToString() == "")
                            deleteCol++;

                    if (deleteCol > 0)
                        for (int i = 0; i <= deleteCol - 1; i++)
                            dt.Rows.Remove(dt.Rows[0]); //Remove empty rows 

                    dt.AcceptChanges();

                    foreach (DataColumn dc in dt.Columns)
                        dc.ColumnName = dt.Rows[0][dc.ColumnName].ToString();

                    dt.AcceptChanges();

                    dt.Rows.Remove(dt.Rows[0]); //Remove empty rows  

                    dt.AcceptChanges();

                    string lastRow = dt.Rows[dt.Rows.Count - 1][0].ToString();

                    if (lastRow.Contains("Data was updated last on"))
                    {
                        dt.Rows.Remove(dt.Rows[dt.Rows.Count - 1]);
                        dt.AcceptChanges();
                    }
                }
            }

            return dt;
        }

        public bool ExportReport(Message message, string location = "")
        {
            bool result = false;

            Console.WriteLine("Processing: " + message.Headers.Subject.ToString());

            try
            {
                result = ExportFile(message, location);

                Console.WriteLine("Finished: " + message.Headers.Subject.ToString());

                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in processing: " + message.Headers.Subject.ToString() + ex.Message.ToString());
                SolutionHelper.Error.LogError(ex, "SizmekMDXReporting.ProcessAnalytics:");
            }

            return result;
        }

        public static bool ExportFile(Message message, string location = "")
        {
            try
            {
                List<MessagePart> attachments = message.FindAllAttachments();

                if (location == "")
                    location = ConfigurationManager.AppSettings["csvExport"];

                string[] reportVariations = ConfigurationManager.AppSettings["ReportVariations"].ToString().Split(',');

                int counter = 0;

                foreach (MessagePart attachment in message.FindAllAttachments())
                {
                    string path = "", ext = "", company = "";
                    string prefix = "";
                    string date = System.DateTime.Now.ToString().ReplaceAny("", "/", ":", "AM", "PM").Trim();
                    Stream stream = new MemoryStream(attachment.Body);

                    int idx = attachment.FileName.IndexOf("Custom_Report");

                    company = attachment.FileName.Substring(0, idx).Trim('_');

                    if (attachment.FileName.Contains(".tsv.zip"))
                        ext = ".tsv.zip";
                    else if (attachment.FileName.Contains(".tsv"))
                        ext = ".tsv";
                    else if (attachment.FileName.Contains(".csv"))
                        ext = ".csv";


                    if (message.Headers.Subject.ContainsAny("Report1", "Report2", "Report3", "Report4", "Report5", "Report6", "Report7", "Report8", "Report9", "Report10"))
                    {
                        for (int i = 1; i < 15; i++)
                        {
                            prefix = String.Format("Report{0}", i);


                            if (message.Headers.Subject.Contains("History"))
                                prefix += " History";

                            path = String.Format(@"{0}\Sizmek Analytics Report - {1} {5} {2}{3}{4}", location, company, date, counter, ext, prefix);

                            bool exist = File.Exists(path);

                            if (!exist)
                                break;
                        }
                    }
                    else
                        path = String.Format(@"{0}\Sizmek Analytics Report - {1} {2}{3}{4}", location, company, date, counter, ext);

                    var fileStream = new FileStream(path, FileMode.Create, FileAccess.Write);
                    stream.CopyTo(fileStream);
                    fileStream.Dispose();
                }

                return true;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "SizmekMDXReporting.ReportingData.ExportFile:");
            }

            return false;
        }

        private Company GetCompany(string _companyString)
        {
            Company[] companys = (from m in db.Companies where m.Sizemek_Keyword != null && m.IsActive == true select m).ToArray();

            foreach (Company company in companys)
            {
                if (_companyString.ToLower().ContainsAny(company.Sizemek_Keyword.ToLower().Split(',')))
                {
                    Data processor = new Data();
                    processor.DatabaseName = company.DatabaseName;
                    processor.UserName = company.Login;
                    processor.Password = company.Password;
                    processor.Server = company.Server;

                    Processor = processor;

                    return company;
                }
            }

            return null;
        }

        private bool ProcessDailySearchMessage(Message message)
        {
            try
            {
                string url = GetUrlFromDownload(message);

                if (url == "")
                    return false;

                return ExtractReport(url);
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "SizmekMDXReporting.ProcessDailySearchMessage:");
            }

            return false;
        }

        private bool ExtractReport(string url)
        {
            try
            {
                DataTable dt = new DataTable();

                WebRequest wrGETURL;
                wrGETURL = WebRequest.Create(url);

                Stream objStream;
                var objNew = wrGETURL.GetResponse();
                objStream = wrGETURL.GetResponse().GetResponseStream();
                StreamReader objReader = new StreamReader(objStream, Encoding.UTF8);
                WebResponse wr = wrGETURL.GetResponse();

                byte[] b = null;
                using (Stream stream = wr.GetResponseStream())
                using (MemoryStream ms = new MemoryStream())
                {
                    int count = 0;
                    do
                    {
                        byte[] buf = new byte[1024];
                        count = stream.Read(buf, 0, 1024);
                        ms.Write(buf, 0, count);
                    } while (stream.CanRead && count > 0);
                    b = ms.ToArray();
                }

                Stream stream2 = new MemoryStream(b);
                Streem streem = new Streem();
                streem.Stream = stream2;

                streem.Stream = streem.ToStreamFromZip();

                SaveFileStream(ConfigurationManager.AppSettings["csvExport"] + @"\SizmekDailySearch - " + streem.Name, streem.Stream);

                return true;
            }
            catch (Exception ex)
            {
                //    SolutionHelper.Error.LogError(ex, "ExtractReport Conversant");
            }

            return false;
        }

        /// <summary>
        /// Copies the contents of input to output. Doesn't close either stream.
        /// </summary>
        //public void CopyStream(Stream input, Stream output)
        //{
        //    byte[] buffer = new byte[8 * 1024];
        //    int len;
        //    while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
        //    {
        //        output.Write(buffer, 0, len);
        //    }
        //}

        private void SaveFileStream(String path, Stream stream)
        {
            var fileStream = new FileStream(path, FileMode.Create, FileAccess.Write);
            stream.CopyTo(fileStream);
            fileStream.Dispose();
        }

        private string GetUrlFromDownload(Message message)
        {
            try
            {
                string pattern = @"\b(?:https?://|www\.)\S+\b";
                // string pattern = @"\b(?:https?://|www\.)\S+\b";

                /*
                 *   \b       -matches a word boundary (spaces, periods..etc)
                 *   (?:      -define the beginning of a group, the ?: specifies not to capture the data within this group.
                 *   https?://  - Match http or https (the '?' after the "s" makes it optional)
                 *   |        -OR
                 *   www\.    -literal string, match www. (the \. means a literal ".")
                 *   )        -end group
                 *   \S+      -match a series of non-whitespace characters.
                 *   \b       -match the closing word boundary. 
                 */

                Regex r = new Regex(pattern, RegexOptions.IgnoreCase);

                string result = System.Text.Encoding.UTF8.GetString(message.FindFirstPlainTextVersion().Body);
                // Match the regular expression pattern against a text string.
                Match m = r.Match(result);

                Console.WriteLine("Url: {0}", m.Value.Replace("=3D", "="));

                // 3D are the hex digits corresponding to ='s ASCII value (61).
                string url = m.Value.Replace("=3D", "=");

                return url;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "GetUrlFromDownload Conversant");
                return "";
            }
        }

        private void UpdateReferenceTable()
        {
            try
            {
                string query = "EXEC spUpdatedatasizmekplacementreference";

                DataSet ds = Processor.RunQuery(query);
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "UpdateReferenceTable: " + Processor.DatabaseName);
            }
        }

        /// <summary>
        /// Pull sites from Lavidge database current database based on the site ids
        /// </summary>
        private void ProcessSites()
        {
            try
            {
                string query = "";
                query += "SELECT distinct [SITE ID] ";
                query += "FROM [dbo].[dataSizmekReportingData] ";
                query += "where [SITE ID] not in (select [SiteId] from [dataSizmekSites])";

                DataSet ds = Processor.RunQuery(query);

                List<int> ids = new List<int>();

                foreach (DataRow dr in ds.Tables[0].Rows)
                    ids.Add(int.Parse(dr[0].ToString()));

                dataSizmekSite[] sites = (from m in db.dataSizmekSites select m).Where(o => ids.Contains(o.SiteId ?? 0)).ToArray();

                ProcessSitesToDatabase(sites);
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "ProcessSites: " + Processor.DatabaseName);
            }
        }

        private void ProcessSitesToDatabase(dataSizmekSite[] sites)
        {
            foreach (dataSizmekSite site in sites)
            {
                string query = "INSERT " + Environment.NewLine;
                query += "INTO   dataSizmekSites(  SiteId, SiteName) ";
                query += String.Format("VALUES        ({0},'{1}') ", site.SiteId, site.SiteName) + Environment.NewLine + Environment.NewLine;

                Processor.RunQuery(query);
                LoadSections(site);
            }
        }

        private void LoadSections(dataSizmekSite site)
        {
            try
            {
                PlacementDataServiceHandler.LoadSections(site, company);
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "CustomReport.LoadSections");
            }
        }

        /// <summary>
        /// Check if all the columns are created in the database for the reporting table. If the column doesnt exist it will be added.
        /// </summary>
        /// <param name="dt"></param>
        private void ColumnChecker(DataTable dt, string tableName)
        {
            string currentCol = "";
            try
            {
                Console.WriteLine("SizmekMDXReporting - ColumnChecker: " + Processor.DatabaseName);

                string[] columnCol = GetColumnsFromDatabase(tableName);

                foreach (DataColumn dc in dt.Columns)
                {
                    currentCol = dc.ColumnName;

                    if (dc.ColumnName.Contains("* "))
                        dc.ColumnName = dc.ColumnName.Replace("* ", "");

                    if (dc.ColumnName != "AsOfDate" || dc.ColumnName != "ModifiedDate")
                        dc.ColumnName = dc.ColumnName.ToProperCase();

                    if (dc.ColumnName.ToLower() == "date")
                    {
                        dc.ColumnName = "AsOfDate";
                        continue;
                    }

                    if (!columnCol.Contains(dc.ColumnName.ToLower()))
                    {
                        if (dc.ColumnName.Length > 150)
                            throw new Exception("Column too large");

                        AddColumn(tableName, dc.ColumnName);
                    }
                }

                DateTime today = System.DateTime.Now;

                if (!dt.Columns.Contains("ModifiedDate"))
                {
                    System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                    newColumn.DefaultValue = today.ToString();
                    dt.Columns.Add(newColumn);
                }

                //  bool bolChecker = DebugHelper.CheckColumnsAgainstDatabase(Processor, dt, tableName);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private string[] GetColumnsFromDatabase(string tableName, bool isToLower = true)
        {
            string query = "";
            query += "DECLARE @columns varchar(MAX) ";
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(MAX), COLUMN_NAME) ";
            query += String.Format("FROM (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{0}') as tab ", tableName);
            query += "select @columns ";

            DataSet ds = Processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString();

            string[] columnCol;

            if (isToLower)
                columnCol = columns.ToLower().Split(',').Select(x => x.Trim()).ToArray();
            else
                columnCol = columns.Split(',').Select(x => x.Trim()).ToArray();

            return columnCol;
        }

        /// <summary>
        /// Clean the data based on the column type define in the AddColumn method 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private DataTable CleanData(DataTable dt)
        {
            Console.WriteLine("SizmekMDXReporting - CleanData: " + Processor.DatabaseName);
            Console.WriteLine(" ");

            int idx = 0;
            foreach (DataRow dr in dt.Rows)
            {
                WhirlygigSolutionHelper.ConsoleWriter.WriteCurrentLine(String.Format("Cleaning Row {0} of {1}", idx, dt.Rows.Count));


                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName.ToUpper().ContainsAny("CLICKS", "POST-IMPRESSION CONVERSIONS", "IMPRESSIONS", "POST-CLICK CONVERSIONS", "TOTAL CONVERSIONS", "VIDEO STARTED", "VIDEO FULLY PLAYED"))
                    {
                        if (String.IsNullOrEmpty(dr[col.ColumnName].ToString()) || dr[col.ColumnName].ToString().ToUpper() == "NULL")
                            dr[col.ColumnName] = 0;
                        if (dr[col.ColumnName].ToString().Contains(","))
                            dr[col.ColumnName] = dr[col.ColumnName].ToString().Replace(",", "");
                    }

                    if (col.ColumnName.ToUpper().ContainsAny("VIDEO FULLY PLAYED RATE"))
                    {
                        if (String.IsNullOrEmpty(dr[col.ColumnName].ToString()) || dr[col.ColumnName].ToString().ToUpper() == "NULL")
                            dr[col.ColumnName] = 0;
                        if (dr[col.ColumnName].ToString().Contains(","))
                            dr[col.ColumnName] = dr[col.ColumnName].ToString().Replace(",", "");
                        if (dr[col.ColumnName].ToString().Contains("%"))
                            dr[col.ColumnName] = dr[col.ColumnName].ToString().Replace("%", "");
                    }
                }
                idx++;
            }

            idx = 0;

            retry:

            string last = dt.Rows[dt.Rows.Count - 1][0].ToString();

            if (last.ContainsAny("Data was updated last", "The following columns were not included in the report"))
            {
                dt.Rows[dt.Rows.Count - 1].Delete();
                dt.AcceptChanges();
                idx++;

                if (idx > 5)
                    throw new Exception("CleanData: Overflow Error");
                else
                    goto retry;
            }

            return dt;

        }

        /// <summary>
        /// Due to the columns needing to be dynamic this method is designed to alter the view in order to reindex new columns to the table
        /// </summary>
        private void RecreateView()
        {
            string query = "ALTER VIEW [dbo].[101_Server] AS " + Environment.NewLine;
            query += "SELECT  d.* ";
            query += String.Format("FROM [{0}].[dbo].[dataSizmekReportingData] d ", Processor.DatabaseName) + Environment.NewLine + Environment.NewLine;

            Processor.RunQuery(query);

            query = "exec sp_refreshview[dbo.101_Server]";

            Processor.RunQuery(query);
        }

        /// <summary>
        /// Due to the columns needing to be dynamic this method is designed to add additional column to the table
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        private void AddColumn(string table, string column)
        {
            column = column.ToProperCase();

            string query = "ALTER TABLE {0} ADD [{1}] {2}";

            if (column.ToUpper().ContainsAny("CLICKS", "POST-IMPRESSION CONVERSIONS", "POST-CLICK CONVERSIONS", "TOTAL CONVERSIONS", "VIDEO STARTED") && !column.ToUpper().ContainsAny("Clicks Total Revenue".ToUpper()))
                query = string.Format(query, table, column, "int NULL");
            else if (column.ToUpper().ContainsAny("VIDEO FULLY PLAYED RATE", "TOTAL REVENUE"))
                query = string.Format(query, table, column, "[decimal](18, 2) NULL");
            else
                query = string.Format(query, table, column, "[nvarchar](250)");

            Processor.RunQuery(query);

            try
            {
                if (!debug)
                    Processor.RunQuery(query.Replace(table, table + "_ManualAdds"));
            }
            catch (Exception)
            {
                // TODO: Determine whether or not we need to record this error.    
                // This does not need to stop the import process.
            }

            try
            {
                if (company.Abbreviation.ToUpper() == "ASU")
                    Processor.RunQuery(query.Replace(table, table + "_Staging"));
            }
            catch (Exception)
            {
                // TODO: Determine whether or not we need to record this error.    
                // This does not need to stop the import process.
            }
        }
         
        private void DeleteOldData(DataTable dt)
        {
            try
            {
                string query = String.Format(@"delete FROM [dbo].[{0}] where AsOfDate = '{1}'", tableName, dt.TableName);

                Processor.RunQuery(query);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This is to compare the total records and the sum of the impressions against the database. 
        /// </summary>
        /// <param name="datatab"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private bool CompareReportDataAgainstDatabaseData(DataTable datatab, string tableName, bool _override = false)
        {

            if (_override)
            {
                reimportData = true;
                return false;
            }

            string imps = "";

            try
            {

                string query = String.Format(@"SELECT count(*) Total, sum([Served Impressions]) SumImp FROM [dbo].[{0}] where AsOfDate =  '{1}'", tableName, datatab.TableName);

                if (Processor.DatabaseName == "mediaArizonaPublicService")
                {
                    List<int> ids = new List<int>();

                    foreach (DataRow dr in datatab.Rows)
                    {
                        int id = int.Parse(dr["Advertiser Id"].ToString());

                        if (!ids.Contains(id))
                            ids.Add(id);
                    }

                    if (ids.Count == 1)
                    {
                        query = String.Format(@"SELECT count(*) Total, isnull(sum([Served Impressions]),0) SumImp FROM [dbo].[{0}] where AsOfDate =  '{1}' and [Advertiser Id] = {2}", tableName, datatab.TableName, ids[0]);
                    }
                    if (ids.Count > 1)
                    {
                        var result = string.Join(",", ids);

                        query = String.Format(@"SELECT count(*) Total, isnull(sum([Served Impressions]),0) SumImp FROM [dbo].[{0}] where AsOfDate =  '{1}' and [Advertiser Id] in ({2})", tableName, datatab.TableName, result);
                    }
                }

                if (Processor.DatabaseName == "mediaAAAInsurance")
                {
                    List<string> names = new List<string>();

                    foreach (DataRow dr in datatab.Rows)
                    {
                        string name = dr["Campaign Name"].ToString();

                        if (!names.Contains("'" + name + "'"))
                            names.Add("'" + name + "'");
                    }

                    if (names.Count == 1)
                    {
                        query = String.Format(@"SELECT count(*) Total, isnull(sum([Served Impressions]),0) SumImp FROM [dbo].[{0}] where AsOfDate =  '{1}' and [Campaign Name] = {2}", tableName, datatab.TableName, names[0]);
                    }

                    if (names.Count > 1)
                    {
                        var result = string.Join(",", names);

                        query = String.Format(@"SELECT count(*) Total, isnull(sum([Served Impressions]),0) SumImp FROM [dbo].[{0}] where AsOfDate =  '{1}' and [Campaign Name] in ({2})", tableName, datatab.TableName, result);
                    }
                }

                DataSet ds = Processor.RunQuery(query);

                if (ds != null)
                {
                    if (ds.Tables.Count != 0)
                    {
                        int _sumImp = 0;
                        int total = int.Parse(ds.Tables[0].Rows[0]["Total"].ToString());
                        int sumImp = int.Parse(ds.Tables[0].Rows[0]["SumImp"].ToString());

                        foreach (DataRow dr in datatab.Rows)
                        {
                            imps = dr["Served Impressions"].ToString();
                            _sumImp += int.Parse(dr["Served Impressions"].ToString().Replace(",", "").Replace("\"", ""));
                        }

                        int _Newtotal = datatab.Rows.Count;

                        if (total == _Newtotal && sumImp == _sumImp || total == 0 && sumImp == 0)
                        {
                            return true;
                        }
                        else
                            reimportData = true;
                    }
                }

            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }

        private DateTime[] GetReportDates(string tableName, DataTable dt)
        {
            List<DateTime> dates = new List<DateTime>();

            string query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[{0}] where [AsOfDate] is not null", tableName);


            if (Processor.DatabaseName == "mediaArizonaPublicService")
            {
                List<int> ids = new List<int>();

                foreach (DataRow dr in dt.Rows)
                {
                    int id = int.Parse(dr["Site Id"].ToString());

                    if (!ids.Contains(id))
                        ids.Add(id);
                }

                if (ids.Count == 1)
                {
                    query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[{0}] where [Site Id] = {1}", tableName, ids[0]);
                }
                if (ids.Count > 1)
                {
                    var result = string.Join(",", ids);

                    query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[{0}] where [Site Id] in ({1})", tableName, result);
                }
            }

            DataSet ds = Processor.RunQuery(query);

            if (ds != null) // Add date to list of date
                if (ds.Tables.Count != 0) // Add date to list of date
                    foreach (DataRow dr in ds.Tables[0].Rows)
                        if (dr[0].ToString() != "")
                            dates.Add(DateTime.Parse(dr[0].ToString())); // Date is a non nullable date type

            return dates.ToArray();
        }

        public static bool ProcessSitesReport(DataTable dt)
        {
            try
            {
                ReportingData cr = new ReportingData();
                Data proc = new Data();

                var selectedDb = new mediaLavidgeEntities();
                selectedDb.Database.Connection.ConnectionString = ConfigurationManager.AppSettings["LavidgeDatabase"].ToString();

                Company company = (from m in selectedDb.Companies where m.Id == 1 select m).FirstOrDefault();

                Data processor = new Data();
                proc.DatabaseName = company.DatabaseName;
                proc.UserName = company.Login;
                proc.Password = company.Password;
                proc.Server = company.Server;

                TruncateSiteLoadTable(proc);

                SqlConnection conn = new SqlConnection(proc.ConnectionString);

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);

                    bulkCopy.BatchSize = 100000;
                    bulkCopy.BulkCopyTimeout = 30;

                    foreach (DataColumn dc in dt.Columns)
                    {
                        SqlBulkCopyColumnMapping mapping = new SqlBulkCopyColumnMapping(dc.ColumnName, dc.ColumnName);

                        bulkCopy.ColumnMappings.Add(mapping);
                    }

                    bulkCopy.DestinationTableName = "dataSizmekSitesLoad";

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(cr.bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 20000;
                    conn.Open();

                    bulkCopy.WriteToServer(dt);

                    ProcessSitesTable(proc);

                    return true;
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "CustomReport.ProcessSitesReport:");
            }

            return false;
        }

        private static void TruncateSiteLoadTable(Data proc)
        {
            string query = "Truncate Table [dbo].[dataSizmekSitesLoad];";

            proc.RunQuery(query);
        }

        private static void ProcessSitesTable(Data proc)
        {
            string query = "INSERT INTO dataSizmekSites ";
            query += "(SiteId, SiteName) ";
            query += "SELECT SiteId, SiteName ";
            query += "FROM dataSizmekSitesLoad ";
            query += "WHERE Siteid NOT IN (Select SiteId from dataSizmekSites)"; // TODO:

            proc.RunQuery(query);
        }

        private void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(String.Format("{0} Rows have been copied.", e.RowsCopied.ToString()));
        }

        private void Reset()
        {
            completeFile = false;
            company = null;
            Processor = null;
            duplicate = false;
            reimportData = false;
            addendum = false;
            isHistorical = false;

            tableName = "dataSizmekReportingData";
        }
     
        /// <summary> 
        /// Notes:
        /// This method was created as an exception for ASU. We needed to verify there are no "REPORT1" emails in the inbox before we start processing "REPORT2" and "REPORT3". This change will eliminate the manual work that I normally do.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="messageCount"></param>
        private void GenerateMessageQueue(Pop3Client client, int messageCount)
        {
            List<SizmekEmails> _colEmailCollections = new List<SizmekEmails>();
            colEmailCollections = null;

            try
            {

                for (int i = 1; i <= messageCount; i++) // Loop through messages
                {
                    Message message = client.GetMessage(i);
                    if (message.Headers.Subject.ContainsAny(ConfigurationManager.AppSettings["EmailSubject"].Split(',')))
                    {
                        Company tempCompany = GetCompany(message.Headers.Subject.ToString());

                        if (message.Headers.Subject != null)
                        {
                            SizmekEmails email = new SizmekEmails { MessageId = message.Headers.MessageId, CompanyName = tempCompany.CompanyName, MessageSubject = message.Headers.Subject };
                            _colEmailCollections.Add(email);
                        }
                    }
                }

                colEmailCollections = _colEmailCollections.ToArray();
            }
            catch (Exception ex)
            {
                SizmekEmails email = new SizmekEmails { MessageId = "ERROR", CompanyName = "ERROR", MessageSubject = "ERROR" };
                _colEmailCollections.Add(email);
                colEmailCollections = _colEmailCollections.ToArray();

                SolutionHelper.Error.LogError("SizmekMDXReporting.GenerateMessageQueue: " + ex.Message);
            }
        }

        internal void ProcessDatatable(FileInfo file)
        {
            bool processed = false;

            if (company.Abbreviation == "ASU")
            {
                if (file.Name.ContainsAny(ConfigurationManager.AppSettings["ReportVariations"].Split(',')))
                {
                    addendum = true;

                    if (!bolBaseClassExist(file))
                        return;
                }
                else
                    addendum = false;
            }

            DataTable dt = ExtractDatatable(file);

            if (dt != null)
                if (dt.Rows.Count != 0)
                    processed = ProcessAnalyticsReport(dt);

            if (processed)
            {
                file.MoveTo(csvExportCompleted + @"\" + file.Name.Replace(file.Extension, " ") + System.DateTime.Now.ToString().Replace(":", ".").Replace("/", "") + file.Extension);
            }
            else if (duplicate || completeFile)
            {
                string path = csvExportCompleted + @"\" + file.Name.Replace(file.Extension, " Duplicate ") + System.DateTime.Now.ToString().Replace(":", ".").Replace("/", "") + file.Extension;
                file.MoveTo(path);
            }
            else
            {
                file.MoveTo(csvExportFailed + @"\" + file.Name);
            }
        }

        /// <summary>
        /// Determine if there is a base file that needs to be processed first.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private bool bolBaseClassExist(FileInfo file)
        {
            try
            {
                DirectoryInfo d = new DirectoryInfo(csvExport);

                FileInfo[] infos = d.GetFiles();

                foreach (FileInfo f in infos.Where(m => m.FullName.ContainsAny("Sizmek Analytics Report")).OrderBy(n => n.CreationTime))
                    if (f.FullName.Contains("Report1"))
                        return false;

            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.DailySearchProcessing.bolBaseClassExist:" + file.FullName + " - " + ex.Message);
                return false;
            }

            return true;
        }

        #endregion

    }
    class SizmekEmails
    {
        public string MessageSubject { get; set; }
        public string CompanyName { get; set; }
        public string MessageId { get; set; }
    }
    class ResultsData
    {
        private bool result { get; set; }
        private bool error { get; set; }
    }
}
