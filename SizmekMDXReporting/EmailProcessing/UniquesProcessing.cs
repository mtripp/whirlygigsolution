﻿using OpenPop.Mime;
using OpenPop.Pop3;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using SolutionHelper;
using System.Data.SqlClient;
using System.Configuration;
using WhirlygigSolutionHelper;
using System.Data.OleDb;

namespace SizmekMDXReporting.EmailProcessing
{
    public class UniquesProcessing
    {
        mediaLavidgeEntities db = new mediaLavidgeEntities();

        public Company company; 
        public Data Processor = new Data();

        public bool deleteEmail;
        string server;
        string accountname;
        string accountpassword;
        string tablename = "dataSizmekUniquesReportingData";
        string csvExportCompleted;
        string csvExportFailed;
        string csvExportWaiting;
        string csvExport;
        bool duplicate;
        bool reimportData;
         
        public UniquesProcessing()
        {
            csvExportCompleted = ConfigurationManager.AppSettings["csvExportCompleted"];
            csvExportWaiting = ConfigurationManager.AppSettings["csvExportWaiting"];
            accountpassword = ConfigurationManager.AppSettings["EmailAccountPassword"];
            csvExportFailed = ConfigurationManager.AppSettings["csvExportFailed"];
            accountname = ConfigurationManager.AppSettings["EmailAccountName"];
            csvExport = ConfigurationManager.AppSettings["csvExport"];
            server = ConfigurationManager.AppSettings["EmailAccountServer"];
        }
        public void Run()
        {
            ProcessUniqueReports();
        }

        #region ExportReport
        private void ProcessUniqueReports()
        {
            try
            {
                string status = "";

                do // LOOP AND REFRESH POP3 CLIENT
                {
                    DateTime startTime = System.DateTime.Now;

                    status = "Keep running";
                    Console.WriteLine(status);

                    using (Pop3Client client = new Pop3Client())
                    {
                        client.Connect(server, 110, false);
                        client.Authenticate(accountname, accountpassword);

                        int messageCount = client.GetMessageCount();

                        List<Message> allMessages = new List<Message>(messageCount);

                        if (messageCount == 0)
                            return;

                        for (int i = messageCount; i > 0; i--)
                        {
                            if (i == 1)
                                status = "Stop"; // Once the last email passes status should remain "Stop"
                            else
                                status = "Keep running";

                            Console.WriteLine("Processing " + i + " of " + messageCount);
                            bool result = false;

                            deleteEmail = false;
                            company = null;
                            Processor = null;

                            Message message = client.GetMessage(i);

                            if (message.Headers.Subject == null)
                                continue;

                            if (message.Headers.Subject.ContainsAny(ConfigurationManager.AppSettings["EmailSubjectUniques"].Split(',')))
                            {
                                result = ExportUniques(message);

                                if (result || deleteEmail)
                                {
                                    client.DeleteMessage(i);
                                }
                            }

                            TimeSpan span = System.DateTime.Now - startTime;

                            if (span.TotalMinutes > 3)
                                break;
                        }
                    }

                    status = "Stop";

                } while (status == "Keep running"); // DONT STOP UNLESS ERROR 
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.OpenPopHandler: " + ex.Message);
            }
        }

        private bool ExportUniques(Message message)
        {
            bool result = false;

            Console.WriteLine("Processing: " + message.Headers.Subject.ToString());

            try
            {
                result = ExportFile(message);

                Console.WriteLine("Finished: " + message.Headers.Subject.ToString());

                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in processing: " + message.Headers.Subject.ToString() + ex.Message.ToString());
                SolutionHelper.Error.LogError(ex, "SizmekMDXReporting.ProcessAnalytics:");
            }

            return result;
        }

        private bool ExportFile(Message message)
        {
            try
            {
                List<MessagePart> attachments = message.FindAllAttachments();

                Stream stream = new MemoryStream(attachments[0].Body);

                SaveFileStream(ConfigurationManager.AppSettings["csvExport"] + @"\Sizmek Uniques - " + attachments[0].FileName, stream);

                return true;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "SizmekMDXReporting.ProcessMessage:");
            }

            return false;
        }

        private void SaveFileStream(String path, Stream stream)
        {
            var fileStream = new FileStream(path, FileMode.Create, FileAccess.Write);
            stream.CopyTo(fileStream);
            fileStream.Dispose();
        }
        #endregion
        #region ProcessReport
        public void ProcessDatatable(FileInfo file)
        {
            duplicate = false;
            deleteEmail = false;

            DataTable dt = ExtractDatatable(file);

            bool processed = ProcessUniquesReport(dt);

            if (processed)
            {
                file.MoveTo(csvExportCompleted + @"\" + file.Name.Replace(file.Extension, " ") + System.DateTime.Now.ToString().Replace(":", ".").Replace("/", "") + file.Extension);
            }
            else if (duplicate || deleteEmail)
            {
                string path = csvExportCompleted + @"\" + file.Name.Replace(file.Extension, " Duplicate ") + System.DateTime.Now.ToString().Replace(":", ".").Replace("/", "") + file.Extension;
                file.MoveTo(path);
            }
            else
            {
                file.MoveTo(csvExportFailed + @"\" + file.Name);
            }
        }

        private DataTable ExtractDatatable(FileInfo file)
        {
            DataTable dt = null;
            DataTable table = new DataTable();

            string sheetName = "Sheet1";
            string strConn = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1;TypeGuessRows=0;ImportMixedTypes=Text\"", file.FullName);

            try
            {
                dt = new DataTable();

                using (OleDbConnection dbConnection = new OleDbConnection(strConn))
                {
                    using (OleDbDataAdapter dbAdapter = new OleDbDataAdapter(String.Format("SELECT * FROM [{0}$]", sheetName), dbConnection)) //rename sheet if required!
                        dbAdapter.Fill(table);

                    int rows = table.Rows.Count;

                    if (rows > 0)
                        return table;
                }

            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.ProcessUniqueReports.ExtractDatatable:" + file.Name + " - " + ex.Message); //TODO: MAY NEED TO UNZIP FIRST
            }

            return dt;
        }

        private bool ProcessUniquesReport(DataTable dt)
        {
            DataTable dt2 = ExtractReport(dt);

            ColumnChecker(dt2);
            CleanDataFeed(dt2);

            bool processed = ProcessUniquesToDatabase(dt2);

            return processed;
        }
        private bool ProcessUniquesToDatabase(DataTable dt)
        {
            bool processed = false;

            DateTime today = System.DateTime.Now.Date;
            DateTime[] reports = GetReportDates(dt);

            DataSet ds = dt.ToDataSetDateSplit();

            foreach (DataTable datatab in ds.Tables)
            {
                reimportData = false;
                DateTime tablename = DateTime.Parse(datatab.TableName.ToString());

                if (reports.Count() > 0)
                {
                    if (reports.Contains(tablename))
                    {
                        bool comparedata = CompareReportDataAgainstDatabaseData(datatab);

                        if (comparedata)
                        {
                            deleteEmail = true;
                            continue;
                        }
                        else if (reimportData)
                        {
                            DeleteOldData(datatab);
                        }
                        else
                        {
                            deleteEmail = false;
                            return false;
                        }
                    } 
                    else
                    {
                        deleteEmail = false;
                    }
                }

                processed = ProcessDatatableToDb(datatab);

                if (!processed)         // If at least one insert was improper than all need to be reviewed
                    return processed;
            }

            return processed;
        }
        private bool ProcessDatatableToDb(DataTable dt)
        {
            try
            {
                Console.WriteLine(String.Format("Processing Uniques To Database: {0} - {1}", Processor.DatabaseName,dt.TableName.ToString()));

                SqlConnection conn = new SqlConnection(Processor.ConnectionString);

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);

                    bulkCopy.BatchSize = 10000;
                    bulkCopy.BulkCopyTimeout = 500;

                    foreach (DataColumn dc in dt.Columns)
                    {
                        SqlBulkCopyColumnMapping mapping = new SqlBulkCopyColumnMapping(dc.ColumnName, dc.ColumnName);

                        bulkCopy.ColumnMappings.Add(mapping);
                    }

                    bulkCopy.DestinationTableName = tablename;

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 10000;
                    conn.Open();

                    bulkCopy.WriteToServer(dt);

                    return true;
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "CustomReportingData.ProcessUniquesToDatabase:" + Processor.DatabaseName);
            }

            return false;
        }
        #endregion      
        #region FunctionMethods
        private static DataTable ExtractReport(DataTable dt)
        {
            string campaignName = "";
            int intRow = 20;

            // Need to determine which the actual headers are in. This changes from time to time
            for (int counter = 0; intRow >= counter; counter++)
            {
                if (dt.Rows[counter][1].ToString().ToUpper() == "CAMPAIGN NAME:")
                    campaignName = dt.Rows[counter][10].ToString();


                if (dt.Rows[counter][0].ToString().ToUpper() == "DATE")
                    intRow = counter;
            }
            ///

            string asofdate = dt.Rows[intRow + 1][0].ToString();
            DateTime today = System.DateTime.Now;

            // Build a new table to insert the data into. Changing original table currupts data for some reason
            DataTable _dt = new DataTable();
            for (int counter2 = 0; dt.Columns.Count > counter2; counter2++)
            {
                string columnName = dt.Rows[intRow][counter2].ToString();

                if (columnName == "")
                    columnName = "Unknown" + counter2.ToString();

                _dt.Columns.Add(columnName);
            }
            ///
            string previousDate = "";

            // Add only relevant data after the intRow
            int currentRow = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (currentRow > intRow)
                {
                    string date = dr[0].ToString();

                    if (dr[0].ToString() != "")
                      previousDate   = dr[0].ToString();
                    else
                        dr[0] = previousDate;


                    _dt.Rows.Add(dr.ItemArray);
                }

                currentRow++;
            }

            if (!_dt.Columns.Contains("ModifiedDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                newColumn.DefaultValue = today.ToString();
                _dt.Columns.Add(newColumn);
            }

            if (_dt.Columns.Contains("Date"))
            {
                _dt.Columns["Date"].ColumnName = "AsOfDate";
            }

            if (!_dt.Columns.Contains("AsOfDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("AsOfDate", typeof(System.String));
                newColumn.DefaultValue = asofdate.ToString();
                _dt.Columns.Add(newColumn);
            }
            if (!_dt.Columns.Contains("Campaign Name"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("Campaign Name", typeof(System.String));
                newColumn.DefaultValue = campaignName;
                _dt.Columns.Add(newColumn);
            }

            _dt.Rows[0].Delete();
            _dt.AcceptChanges();

            int headerIndex = 0;
            int siteIndex = 0;
            int prevIndex = 0;

            foreach (DataRow dr in _dt.Rows)
            {

                string placement = _dt.Rows[headerIndex]["Placement ID"].ToString();
                string adid = _dt.Rows[headerIndex]["Ad ID"].ToString();
                string site = _dt.Rows[headerIndex]["Site Name"].ToString();

                if (site == "")
                {
                    if (placement == "")
                    {
                        if (adid != "")
                        {
                            _dt.Rows[headerIndex]["Placement ID"] = _dt.Rows[prevIndex]["Placement ID"];
                            _dt.Rows[headerIndex]["Section Name"] = _dt.Rows[prevIndex]["Section Name"];
                            _dt.Rows[headerIndex]["Placement Name"] = _dt.Rows[prevIndex]["Placement Name"];
                            _dt.Rows[headerIndex]["Unit Size"] = _dt.Rows[prevIndex]["Unit Size"];
                            _dt.Rows[headerIndex]["Site Name"] = _dt.Rows[siteIndex]["Site Name"];
                        }
                    }
                    else
                        prevIndex = headerIndex;
                }
                else
                    siteIndex = headerIndex;

                headerIndex++;
            }


            int headerIndex2 = 0;

            foreach (DataRow dr2 in _dt.Rows)
            {
                string adid = _dt.Rows[headerIndex2]["Ad ID"].ToString();

                if (adid == "")
                {
                    _dt.Rows[headerIndex2].Delete();
                }

                headerIndex2++;
            }

            _dt.AcceptChanges();

            for (int coldx = 0; coldx < 50; coldx++)
            {
                if (_dt.Columns.Contains("Unknown" + coldx.ToString()))
                {
                    _dt.Columns.Remove("Unknown" + coldx.ToString());

                    _dt.AcceptChanges();
                }

            }


            dt = _dt;
            return dt;
        }

        private void DeleteOldData(DataTable dt)
        {
            try
            {
                string query = String.Format(@"delete FROM [dbo].[{0}] where AsOfDate = '{1}'", tablename, dt.TableName);

                List<string> names = new List<string>();

                foreach (DataRow dr in dt.Rows)
                {
                    string _name = dr["Campaign Name"].ToString();

                    if (!names.Contains("'" + _name + "'"))
                        names.Add("'" + _name + "'");
                }

                if (names.Count == 1)
                {
                    query = String.Format(@"delete FROM [dbo].[{0}] where AsOfDate = '{1}' and [Campaign Name] = {2}", tablename, dt.TableName, names[0]);
                }
                if (names.Count > 1)
                {
                    var result = string.Join(",", names);

                    query = String.Format(@"delete FROM [dbo].[{0}] where AsOfDate = '{1}' and [Campaign Name] in ({2})", tablename, dt.TableName, result);
                }
                 
                Processor.RunQuery(query);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This is to compare the total records and the sum of the impressions against the database. 
        /// </summary>
        /// <param name="datatab"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private bool CompareReportDataAgainstDatabaseData(DataTable datatab)
        {
            try
            { 
                string query = String.Format(@"SELECT count(*) Total, sum([Served Impressions]) SumImp FROM [dbo].[{0}] where AsOfDate =  '{1}'", tablename, datatab.TableName);
                 
                List<string> names = new List<string>();

                foreach (DataRow dr in datatab.Rows)
                {
                    string _name = dr["Campaign Name"].ToString();

                    if (!names.Contains("'" + _name + "'"))
                        names.Add("'" + _name + "'");
                }

                if (names.Count == 1)
                {
                    query = String.Format(@"SELECT count(*) Total, isnull(sum([Served Impressions]),0) SumImp FROM [dbo].[{0}] where AsOfDate =  '{1}' and [Campaign Name] = {2}", tablename, datatab.TableName, names[0]);
                }
                if (names.Count > 1)
                {
                    var result = string.Join(",", names);

                    query = String.Format(@"SELECT count(*) Total, isnull(sum([Served Impressions]),0) SumImp FROM [dbo].[{0}] where AsOfDate =  '{1}' and [Campaign Name] in ({2})", tablename, datatab.TableName, result);
                }

                DataSet ds = Processor.RunQuery(query);

                if (ds != null)
                {
                    if (ds.Tables.Count != 0)
                    {
                        int _sumImp = 0;
                        int total = int.Parse(ds.Tables[0].Rows[0]["Total"].ToString());
                        int sumImp = int.Parse(ds.Tables[0].Rows[0]["SumImp"].ToString());

                        foreach (DataRow dr in datatab.Rows)
                        {
                            _sumImp += int.Parse(dr["Served Impressions"].ToString());
                        }

                        int _total = datatab.Rows.Count;

                        if (total == _total && sumImp == _sumImp)
                            return true;
                        else
                            reimportData = true;
                    }
                }

            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }
        private Company GetCompany(string _companyString)
        {
            Company[] companys = (from m in db.Companies select m).ToArray();

            foreach (Company company in companys)
            {
                if (_companyString.ToLower().ContainsAny(company.Sizemek_Keyword.ToLower().Split(',')))
                {
                    Data processor = new Data();
                    processor.DatabaseName = company.DatabaseName;
                    processor.UserName = company.Login;
                    processor.Password = company.Password;

                    Processor = processor;
                    return company;
                }
            }

            return null;
        }

        private DateTime[] GetReportDates()
        {
            List<DateTime> dates = new List<DateTime>();

            string query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[{0}] where [AsOfDate] is not null", tablename);

            DataSet ds = Processor.RunQuery(query);

            if (ds != null) // Add date to list of date
            {
                if (ds.Tables.Count != 0) // Add date to list of date
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        dates.Add(DateTime.Parse(dr[0].ToString())); // Date is a non nullable date type
                    }
                }
            }

            return dates.ToArray();
        }
        private DateTime[] GetReportDates(DataTable dt)
        {  
            List<DateTime> dates = new List<DateTime>(); 
            List<string> names = new List<string>();

            string query = "";

            foreach (DataRow dr in dt.Rows)
            {
                string _name = dr["Campaign Name"].ToString();

                if (!names.Contains("'" + _name + "'"))
                    names.Add("'" + _name + "'");
            }

            if (names.Count == 1)
            {
                query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[{0}] where [Campaign Name] = {1}", tablename, names[0]);
            }
            if (names.Count > 1)
            {
                var result = string.Join(",", names);

                query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[{0}] where [Campaign Name] in ({2})", tablename, result);
            }

            DataSet ds = Processor.RunQuery(query);
             
            if (ds != null) // Add date to list of date
            {
                if (ds.Tables.Count != 0) // Add date to list of date
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        dates.Add(DateTime.Parse(dr[0].ToString())); // Date is a non nullable date type
                    }
                }
            }

            return dates.ToArray();
        }

        private void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(String.Format("{0} Rows have been copied.", e.RowsCopied.ToString()));
        }

        private void ColumnChecker(DataTable dt)
        {
            DateTime today = System.DateTime.Now;

            string[] columnCol = GetColumnsFromDatabase();

            foreach (DataColumn dc in dt.Columns)
            {
                if (dc.ColumnName.Contains("_"))
                    dc.ColumnName = dc.ColumnName.Replace("_", " ");
                if (dc.ColumnName.Contains("* "))
                    dc.ColumnName = dc.ColumnName.Replace("* ", "");

                dc.ColumnName = dc.ColumnName.ToProperCase();

                if (dc.ColumnName.ToLower() == "date" || dc.ColumnName.ToLower() == "asofdate")
                {
                    dc.ColumnName = "AsOfDate";
                    continue;
                }

                if (dc.ColumnName.ToLower() == "modifieddate")
                {
                    dc.ColumnName = "ModifiedDate";
                    continue;
                }

                if (!columnCol.Contains(dc.ColumnName.ToLower().Trim()))
                    AddColumn(dc.ColumnName.ToString());
            }

            if (!dt.Columns.Contains("ModifiedDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                newColumn.DefaultValue = today.ToString();
                dt.Columns.Add(newColumn);
            }
        }

        /// <summary>
        /// Clean the data based on the column type define in the AddColumn method 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private DataTable CleanDataFeed(DataTable dt)
        {
            int idx = 0;
            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName.ToUpper().ContainsAny("CLICK", "IMPRESSIONS", "UNIQUE", "AD ID"))
                    {
                        if (String.IsNullOrEmpty(dr[col.ColumnName].ToString()) || dr[col.ColumnName].ToString().ToUpper() == "NULL")
                            dr[col.ColumnName] = 0;

                        string apr = dr[col.ColumnName].ToString();

                        if (dr[col.ColumnName].ToString().Contains(","))
                            dr[col.ColumnName] = dr[col.ColumnName].ToString().Replace(",", "");

                        dr[col.ColumnName] = decimal.ToInt32(decimal.Parse(apr));
                    }
                    else if (col.ColumnName.ToUpper().ContainsAny("FREQUENCY"))
                    {
                        if (String.IsNullOrEmpty(dr[col.ColumnName].ToString()) || dr[col.ColumnName].ToString().ToUpper() == "NULL")
                            dr[col.ColumnName] = 0;
                        if (dr[col.ColumnName].ToString().Contains(","))
                            dr[col.ColumnName] = dr[col.ColumnName].ToString().Replace(",", "");
                        if (dr[col.ColumnName].ToString().Contains("%"))
                            dr[col.ColumnName] = dr[col.ColumnName].ToString().Replace("%", "");
                    }
                }
                idx++;
            }
            return dt;
        }

        private string[] GetColumnsFromDatabase()
        {
            string query = "";
            query += "DECLARE @columns varchar(8000) ";
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(8000), COLUMN_NAME) ";
            query += String.Format("FROM (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{0}') as tab ", tablename);
            query += "select @columns ";

            DataSet ds = Processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString();

            string[] columnCol = columns.ToLower().Split(',');

            return columnCol;
        }

        /// <summary>
        /// Due to the columns needing to be dynamic this method is designed to add additional column to the table
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        private void AddColumn(string column)
        {
            column = column.ToProperCase();

            string query = "ALTER TABLE {0} ADD [{1}] {2}";

            if (column.ToUpper().ContainsAny("CLICK", "IMPRESSIONS", "UNIQUE", "AD ID"))
                query = string.Format(query, tablename, column, "[int] NULL");
            else if (column.ToUpper().ContainsAny("FREQUENCY"))
                query = string.Format(query, tablename, column, "decimal(18, 4) NULL");
            else
                query = string.Format(query, tablename, column, "[nvarchar](250)");

            Processor.RunQuery(query);
        }

        #endregion
    }
}
