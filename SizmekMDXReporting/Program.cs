﻿using SizmekMDXReporting.EmailProcessing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Script.Serialization;

/*
 
    Please find the Sizmek MDX API reporting credentials for mtripp@lavidge.com below:

    Email Address: mtripp@lavidge.com
    Username: mtripp.sizmek
    Password: fG8143123
    API Key: 5bd8a44e-5971-4475-bfd2-0c542840d8da
 
    As a reminder – here is a link to our API documentation - http://platform.mediamind.com/Eyeblaster.MediaMind.API.Doc/

    Test advertiser 152256

    Sizmek contact: Chinasa.Olie@sizmek.com

*/

namespace SizmekMDXReporting
{
    public class Program
    {
        public static void Main(string[] args)
        {
            RunReportingData();
            RunCustomReportingData();
            RunUniquesProcessingData();
            RunSearchProcessingData();
            RunPathToConversion();
            RunFileProcessingData();
            RunAdvertisers_Brands_ConversionTag();
            RunCampaigns();
            RunPlacementData();
            // RunAnalyticsData();
            RunSitesData();
        }
        
        private static void RunPathToConversion()
        {
            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();

            Console.WriteLine("RunPathToConversion : *******************************************" + System.DateTime.Now);
            DateTime startTime = System.DateTime.Now;

            PathToConversionProcessing service = new PathToConversionProcessing();
            service.Run();
            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("Total Time : " + endTime.Subtract(startTime).TotalMinutes);
        }

        private static void RunSitesData()
        {
            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();

            Console.WriteLine("RunSitesData : *******************************************" + System.DateTime.Now);
            DateTime startTime = System.DateTime.Now;

            AdvertiserServiceHandler service = new AdvertiserServiceHandler();
            service.RunProcessSites();
            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("Total Time : " + endTime.Subtract(startTime).TotalMinutes);
        }

        private static void RunAdvertisers_Brands_ConversionTag()
        {
            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();
            Console.WriteLine("RunAdvertisers_Brands_ConversionTag : *******************************************" + System.DateTime.Now);
            DateTime startTime = System.DateTime.Now;

            AdvertiserServiceHandler service = new AdvertiserServiceHandler();
            service.Run();
            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("Total Time : " + endTime.Subtract(startTime).TotalMinutes);
        }

        private static void RunCampaigns()
        {
            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();

            Console.WriteLine("RunCampaigns : *******************************************" + System.DateTime.Now);

            DateTime startTime = System.DateTime.Now;

            CampaignServiceHandler service = new CampaignServiceHandler();
            service.Run();
            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("Total Time : " + endTime.Subtract(startTime).TotalMinutes);

        }

        private static void RunAnalyticsData()
        {
            Console.WriteLine("RunAnalyticsData : *******************************************" + System.DateTime.Now);
            DateTime startTime = System.DateTime.Now;

            //if (startTime == startTime)
            //    return; //  TODO: This data import is not needed anymore.

            AnalyticsDataServiceHandler service = new AnalyticsDataServiceHandler();
            service.Run();
            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("Total Time : " + endTime.Subtract(startTime).TotalMinutes);
        }

        private static void RunPlacementData()
        {
            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();

            Console.WriteLine("RunPlacementData : *******************************************" + System.DateTime.Now);
            DateTime startTime = System.DateTime.Now;

            PlacementDataServiceHandler service = new PlacementDataServiceHandler();
            service.Run();
            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("Total Time : " + endTime.Subtract(startTime).TotalMinutes);
        }

        private static void RunAdData()
        {
            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();

            Console.WriteLine("RunAdData : *******************************************" + System.DateTime.Now);
            DateTime startTime = System.DateTime.Now;

            AdServiceHandler service = new AdServiceHandler();
            service.Run();
            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("Total Time : " + endTime.Subtract(startTime).TotalMinutes); 
        }

        private static void RunReportingData()
        {
            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();

            Console.WriteLine("RunReportingData : *******************************************" + System.DateTime.Now);
            DateTime startTime = System.DateTime.Now;

            ReportingData service = new ReportingData();
            service.Run();
            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("Total Time : " + endTime.Subtract(startTime).TotalMinutes);
        }
        private static void RunCustomReportingData()
        {
            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();

            Console.WriteLine("RunCustomReportingData : *******************************************" + System.DateTime.Now);
            DateTime startTime = System.DateTime.Now;

            CustomReportingData service = new CustomReportingData();
            service.Run();
            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("Total Time : " + endTime.Subtract(startTime).TotalMinutes);
        }
        private static void RunFileProcessingData()
        {
            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();

            Console.WriteLine("RunFileProcessingData : *******************************************" + System.DateTime.Now);
            DateTime startTime = System.DateTime.Now;

            FileProcessing service = new FileProcessing();

            service.RunWaitingFiles();
            service.UnzipFiles();
            service.RunProcessingFiles();

            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("Total Time : " + endTime.Subtract(startTime).TotalMinutes);
        }
        private static void RunUniquesProcessingData()
        {
            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();

            Console.WriteLine("RunUniquesProcessingData : *******************************************" + System.DateTime.Now);
            DateTime startTime = System.DateTime.Now;

            UniquesProcessing service = new UniquesProcessing();
            service.Run();
            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("Total Time : " + endTime.Subtract(startTime).TotalMinutes);
        }
        private static void RunSearchProcessingData()
        {
            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();

            Console.WriteLine("RunSearchProcessingData : *******************************************" + System.DateTime.Now);
            DateTime startTime = System.DateTime.Now;

            DailySearchProcessing service = new DailySearchProcessing();
            service.Run();
            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("Total Time : " + endTime.Subtract(startTime).TotalMinutes);
        }
    }
}

