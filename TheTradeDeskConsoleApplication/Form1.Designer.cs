﻿namespace TheTradeDeskConsoleApplication
{
    partial class frmConsole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConsole));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblDashboard = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.tbDashboard = new System.Windows.Forms.TabControl();
            this.tbAdvertisers = new System.Windows.Forms.TabPage();
            this.dgAdvertisers = new System.Windows.Forms.DataGridView();
            this.tbCampaigns = new System.Windows.Forms.TabPage();
            this.dgCampaigns = new System.Windows.Forms.DataGridView();
            this.dataTradeDeskCampaignsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbAdGroups = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.advertiserNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgAdvertisers_Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.advertiserIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.domainAddressDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgAdvertisers_CompanyId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataTradeDeskAdvertisersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mediaLavidgeDataSet = new TheTradeDeskConsoleApplication.mediaLavidgeDataSet();
            this.mediaLavidgeDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mediaLavidgeDataSetBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dataTradeDeskAdvertisersTableAdapter = new TheTradeDeskConsoleApplication.mediaLavidgeDataSetTableAdapters.dataTradeDeskAdvertisersTableAdapter();
            this.dataTradeDeskCampaignsTableAdapter = new TheTradeDeskConsoleApplication.mediaLavidgeDataSetTableAdapters.dataTradeDeskCampaignsTableAdapter();
            this.dataTradeDeskAdGroupBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataTradeDeskAdGroupTableAdapter = new TheTradeDeskConsoleApplication.mediaLavidgeDataSetTableAdapters.dataTradeDeskAdGroupTableAdapter();
            this.campaignNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.campaignIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modifiedDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.advertiserIdDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.campaignObjectDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adGroupNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adGroupIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adGroupObjectDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modifiedDateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.campaignIdDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.tbDashboard.SuspendLayout();
            this.tbAdvertisers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAdvertisers)).BeginInit();
            this.tbCampaigns.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCampaigns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTradeDeskCampaignsBindingSource)).BeginInit();
            this.tbAdGroups.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTradeDeskAdvertisersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mediaLavidgeDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mediaLavidgeDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mediaLavidgeDataSetBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTradeDeskAdGroupBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel1.Controls.Add(this.pictureBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(88)))), ((int)(((byte)(122)))));
            this.splitContainer1.Panel2.Controls.Add(this.lblDashboard);
            this.splitContainer1.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel2_Paint);
            this.splitContainer1.Size = new System.Drawing.Size(1030, 177);
            this.splitContainer1.SplitterDistance = 49;
            this.splitContainer1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(214, 30);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lblDashboard
            // 
            this.lblDashboard.AutoSize = true;
            this.lblDashboard.Font = new System.Drawing.Font("Lucida Sans Typewriter", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDashboard.ForeColor = System.Drawing.Color.White;
            this.lblDashboard.Location = new System.Drawing.Point(21, 15);
            this.lblDashboard.Name = "lblDashboard";
            this.lblDashboard.Size = new System.Drawing.Size(168, 33);
            this.lblDashboard.TabIndex = 0;
            this.lblDashboard.Text = "Dashboard";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.splitContainer2);
            this.panel1.Location = new System.Drawing.Point(12, 139);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1006, 497);
            this.panel1.TabIndex = 1;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.flowLayoutPanel1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.tbDashboard);
            this.splitContainer2.Size = new System.Drawing.Size(1006, 497);
            this.splitContainer2.SplitterDistance = 197;
            this.splitContainer2.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(197, 497);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(165)))));
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(171, 31);
            this.button1.TabIndex = 0;
            this.button1.Text = "Advertisers";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbDashboard
            // 
            this.tbDashboard.Controls.Add(this.tbAdvertisers);
            this.tbDashboard.Controls.Add(this.tbCampaigns);
            this.tbDashboard.Controls.Add(this.tbAdGroups);
            this.tbDashboard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbDashboard.Location = new System.Drawing.Point(0, 0);
            this.tbDashboard.Name = "tbDashboard";
            this.tbDashboard.SelectedIndex = 0;
            this.tbDashboard.Size = new System.Drawing.Size(805, 497);
            this.tbDashboard.TabIndex = 0;
            // 
            // tbAdvertisers
            // 
            this.tbAdvertisers.Controls.Add(this.dgAdvertisers);
            this.tbAdvertisers.Location = new System.Drawing.Point(4, 22);
            this.tbAdvertisers.Name = "tbAdvertisers";
            this.tbAdvertisers.Padding = new System.Windows.Forms.Padding(3);
            this.tbAdvertisers.Size = new System.Drawing.Size(797, 471);
            this.tbAdvertisers.TabIndex = 0;
            this.tbAdvertisers.Text = "Advertisers";
            this.tbAdvertisers.UseVisualStyleBackColor = true;
            // 
            // dgAdvertisers
            // 
            this.dgAdvertisers.AllowUserToAddRows = false;
            this.dgAdvertisers.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.dgAdvertisers.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgAdvertisers.AutoGenerateColumns = false;
            this.dgAdvertisers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAdvertisers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.advertiserNameDataGridViewTextBoxColumn,
            this.dgAdvertisers_Id,
            this.advertiserIdDataGridViewTextBoxColumn,
            this.domainAddressDataGridViewTextBoxColumn,
            this.dgAdvertisers_CompanyId});
            this.dgAdvertisers.DataSource = this.dataTradeDeskAdvertisersBindingSource;
            this.dgAdvertisers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgAdvertisers.Location = new System.Drawing.Point(3, 3);
            this.dgAdvertisers.Name = "dgAdvertisers";
            this.dgAdvertisers.ReadOnly = true;
            this.dgAdvertisers.Size = new System.Drawing.Size(791, 465);
            this.dgAdvertisers.TabIndex = 0;
            this.dgAdvertisers.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // tbCampaigns
            // 
            this.tbCampaigns.Controls.Add(this.dgCampaigns);
            this.tbCampaigns.Location = new System.Drawing.Point(4, 22);
            this.tbCampaigns.Name = "tbCampaigns";
            this.tbCampaigns.Padding = new System.Windows.Forms.Padding(3);
            this.tbCampaigns.Size = new System.Drawing.Size(797, 471);
            this.tbCampaigns.TabIndex = 1;
            this.tbCampaigns.Text = "Campaign";
            this.tbCampaigns.UseVisualStyleBackColor = true;
            // 
            // dgCampaigns
            // 
            this.dgCampaigns.AllowUserToAddRows = false;
            this.dgCampaigns.AllowUserToDeleteRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.dgCampaigns.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgCampaigns.AutoGenerateColumns = false;
            this.dgCampaigns.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCampaigns.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.campaignNameDataGridViewTextBoxColumn,
            this.campaignIdDataGridViewTextBoxColumn,
            this.modifiedDateDataGridViewTextBoxColumn,
            this.advertiserIdDataGridViewTextBoxColumn1,
            this.campaignObjectDataGridViewTextBoxColumn,
            this.idDataGridViewTextBoxColumn});
            this.dgCampaigns.DataSource = this.dataTradeDeskCampaignsBindingSource;
            this.dgCampaigns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgCampaigns.Location = new System.Drawing.Point(3, 3);
            this.dgCampaigns.Name = "dgCampaigns";
            this.dgCampaigns.ReadOnly = true;
            this.dgCampaigns.Size = new System.Drawing.Size(791, 465);
            this.dgCampaigns.TabIndex = 0;
            this.dgCampaigns.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgCampaigns_CellDoubleClick);
            // 
            // dataTradeDeskCampaignsBindingSource
            // 
            this.dataTradeDeskCampaignsBindingSource.DataMember = "dataTradeDeskCampaigns";
            this.dataTradeDeskCampaignsBindingSource.DataSource = this.mediaLavidgeDataSetBindingSource;
            // 
            // tbAdGroups
            // 
            this.tbAdGroups.Controls.Add(this.dataGridView1);
            this.tbAdGroups.Location = new System.Drawing.Point(4, 22);
            this.tbAdGroups.Name = "tbAdGroups";
            this.tbAdGroups.Size = new System.Drawing.Size(797, 471);
            this.tbAdGroups.TabIndex = 2;
            this.tbAdGroups.Text = "Ad Group";
            this.tbAdGroups.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.adGroupNameDataGridViewTextBoxColumn,
            this.adGroupIdDataGridViewTextBoxColumn,
            this.adGroupObjectDataGridViewTextBoxColumn,
            this.modifiedDateDataGridViewTextBoxColumn1,
            this.campaignIdDataGridViewTextBoxColumn1,
            this.idDataGridViewTextBoxColumn1});
            this.dataGridView1.DataSource = this.dataTradeDeskAdGroupBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(797, 471);
            this.dataGridView1.TabIndex = 0;
            // 
            // advertiserNameDataGridViewTextBoxColumn
            // 
            this.advertiserNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.advertiserNameDataGridViewTextBoxColumn.DataPropertyName = "AdvertiserName";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(165)))));
            this.advertiserNameDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.advertiserNameDataGridViewTextBoxColumn.HeaderText = "AdvertiserName";
            this.advertiserNameDataGridViewTextBoxColumn.Name = "advertiserNameDataGridViewTextBoxColumn";
            this.advertiserNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dgAdvertisers_Id
            // 
            this.dgAdvertisers_Id.DataPropertyName = "Id";
            this.dgAdvertisers_Id.HeaderText = "Id";
            this.dgAdvertisers_Id.Name = "dgAdvertisers_Id";
            this.dgAdvertisers_Id.ReadOnly = true;
            this.dgAdvertisers_Id.Visible = false;
            // 
            // advertiserIdDataGridViewTextBoxColumn
            // 
            this.advertiserIdDataGridViewTextBoxColumn.DataPropertyName = "AdvertiserId";
            this.advertiserIdDataGridViewTextBoxColumn.HeaderText = "AdvertiserId";
            this.advertiserIdDataGridViewTextBoxColumn.Name = "advertiserIdDataGridViewTextBoxColumn";
            this.advertiserIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // domainAddressDataGridViewTextBoxColumn
            // 
            this.domainAddressDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.domainAddressDataGridViewTextBoxColumn.DataPropertyName = "DomainAddress";
            this.domainAddressDataGridViewTextBoxColumn.HeaderText = "DomainAddress";
            this.domainAddressDataGridViewTextBoxColumn.Name = "domainAddressDataGridViewTextBoxColumn";
            this.domainAddressDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dgAdvertisers_CompanyId
            // 
            this.dgAdvertisers_CompanyId.DataPropertyName = "CompanyId";
            this.dgAdvertisers_CompanyId.HeaderText = "CompanyId";
            this.dgAdvertisers_CompanyId.Name = "dgAdvertisers_CompanyId";
            this.dgAdvertisers_CompanyId.ReadOnly = true;
            // 
            // dataTradeDeskAdvertisersBindingSource
            // 
            this.dataTradeDeskAdvertisersBindingSource.DataMember = "dataTradeDeskAdvertisers";
            this.dataTradeDeskAdvertisersBindingSource.DataSource = this.mediaLavidgeDataSet;
            // 
            // mediaLavidgeDataSet
            // 
            this.mediaLavidgeDataSet.DataSetName = "mediaLavidgeDataSet";
            this.mediaLavidgeDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // mediaLavidgeDataSetBindingSource
            // 
            this.mediaLavidgeDataSetBindingSource.DataSource = this.mediaLavidgeDataSet;
            this.mediaLavidgeDataSetBindingSource.Position = 0;
            // 
            // mediaLavidgeDataSetBindingSource1
            // 
            this.mediaLavidgeDataSetBindingSource1.DataSource = this.mediaLavidgeDataSet;
            this.mediaLavidgeDataSetBindingSource1.Position = 0;
            // 
            // dataTradeDeskAdvertisersTableAdapter
            // 
            this.dataTradeDeskAdvertisersTableAdapter.ClearBeforeFill = true;
            // 
            // dataTradeDeskCampaignsTableAdapter
            // 
            this.dataTradeDeskCampaignsTableAdapter.ClearBeforeFill = true;
            // 
            // dataTradeDeskAdGroupBindingSource
            // 
            this.dataTradeDeskAdGroupBindingSource.DataMember = "dataTradeDeskAdGroup";
            this.dataTradeDeskAdGroupBindingSource.DataSource = this.mediaLavidgeDataSetBindingSource;
            // 
            // dataTradeDeskAdGroupTableAdapter
            // 
            this.dataTradeDeskAdGroupTableAdapter.ClearBeforeFill = true;
            // 
            // campaignNameDataGridViewTextBoxColumn
            // 
            this.campaignNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.campaignNameDataGridViewTextBoxColumn.DataPropertyName = "Campaign Name";
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(165)))));
            this.campaignNameDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.campaignNameDataGridViewTextBoxColumn.HeaderText = "Campaign Name";
            this.campaignNameDataGridViewTextBoxColumn.Name = "campaignNameDataGridViewTextBoxColumn";
            this.campaignNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // campaignIdDataGridViewTextBoxColumn
            // 
            this.campaignIdDataGridViewTextBoxColumn.DataPropertyName = "Campaign Id";
            this.campaignIdDataGridViewTextBoxColumn.HeaderText = "Campaign Id";
            this.campaignIdDataGridViewTextBoxColumn.Name = "campaignIdDataGridViewTextBoxColumn";
            this.campaignIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // modifiedDateDataGridViewTextBoxColumn
            // 
            this.modifiedDateDataGridViewTextBoxColumn.DataPropertyName = "ModifiedDate";
            this.modifiedDateDataGridViewTextBoxColumn.HeaderText = "ModifiedDate";
            this.modifiedDateDataGridViewTextBoxColumn.Name = "modifiedDateDataGridViewTextBoxColumn";
            this.modifiedDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.modifiedDateDataGridViewTextBoxColumn.Visible = false;
            // 
            // advertiserIdDataGridViewTextBoxColumn1
            // 
            this.advertiserIdDataGridViewTextBoxColumn1.DataPropertyName = "Advertiser Id";
            this.advertiserIdDataGridViewTextBoxColumn1.HeaderText = "Advertiser Id";
            this.advertiserIdDataGridViewTextBoxColumn1.Name = "advertiserIdDataGridViewTextBoxColumn1";
            this.advertiserIdDataGridViewTextBoxColumn1.ReadOnly = true;
            this.advertiserIdDataGridViewTextBoxColumn1.Visible = false;
            // 
            // campaignObjectDataGridViewTextBoxColumn
            // 
            this.campaignObjectDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.campaignObjectDataGridViewTextBoxColumn.DataPropertyName = "Campaign Object";
            this.campaignObjectDataGridViewTextBoxColumn.HeaderText = "Campaign Object";
            this.campaignObjectDataGridViewTextBoxColumn.Name = "campaignObjectDataGridViewTextBoxColumn";
            this.campaignObjectDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // adGroupNameDataGridViewTextBoxColumn
            // 
            this.adGroupNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.adGroupNameDataGridViewTextBoxColumn.DataPropertyName = "Ad Group Name";
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(165)))));
            this.adGroupNameDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.adGroupNameDataGridViewTextBoxColumn.HeaderText = "Ad Group Name";
            this.adGroupNameDataGridViewTextBoxColumn.Name = "adGroupNameDataGridViewTextBoxColumn";
            this.adGroupNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // adGroupIdDataGridViewTextBoxColumn
            // 
            this.adGroupIdDataGridViewTextBoxColumn.DataPropertyName = "Ad Group Id";
            this.adGroupIdDataGridViewTextBoxColumn.HeaderText = "Ad Group Id";
            this.adGroupIdDataGridViewTextBoxColumn.Name = "adGroupIdDataGridViewTextBoxColumn";
            this.adGroupIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // adGroupObjectDataGridViewTextBoxColumn
            // 
            this.adGroupObjectDataGridViewTextBoxColumn.DataPropertyName = "Ad Group Object";
            this.adGroupObjectDataGridViewTextBoxColumn.HeaderText = "Ad Group Object";
            this.adGroupObjectDataGridViewTextBoxColumn.Name = "adGroupObjectDataGridViewTextBoxColumn";
            this.adGroupObjectDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // modifiedDateDataGridViewTextBoxColumn1
            // 
            this.modifiedDateDataGridViewTextBoxColumn1.DataPropertyName = "ModifiedDate";
            this.modifiedDateDataGridViewTextBoxColumn1.HeaderText = "ModifiedDate";
            this.modifiedDateDataGridViewTextBoxColumn1.Name = "modifiedDateDataGridViewTextBoxColumn1";
            this.modifiedDateDataGridViewTextBoxColumn1.ReadOnly = true;
            this.modifiedDateDataGridViewTextBoxColumn1.Visible = false;
            // 
            // campaignIdDataGridViewTextBoxColumn1
            // 
            this.campaignIdDataGridViewTextBoxColumn1.DataPropertyName = "Campaign Id";
            this.campaignIdDataGridViewTextBoxColumn1.HeaderText = "Campaign Id";
            this.campaignIdDataGridViewTextBoxColumn1.Name = "campaignIdDataGridViewTextBoxColumn1";
            this.campaignIdDataGridViewTextBoxColumn1.ReadOnly = true;
            this.campaignIdDataGridViewTextBoxColumn1.Visible = false;
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.ReadOnly = true;
            this.idDataGridViewTextBoxColumn1.Visible = false;
            // 
            // frmConsole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.ClientSize = new System.Drawing.Size(1030, 648);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmConsole";
            this.Text = "The Tradedesk Console";
            this.Load += new System.EventHandler(this.frmConsole_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tbDashboard.ResumeLayout(false);
            this.tbAdvertisers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgAdvertisers)).EndInit();
            this.tbCampaigns.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgCampaigns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTradeDeskCampaignsBindingSource)).EndInit();
            this.tbAdGroups.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTradeDeskAdvertisersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mediaLavidgeDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mediaLavidgeDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mediaLavidgeDataSetBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTradeDeskAdGroupBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblDashboard;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabControl tbDashboard;
        private System.Windows.Forms.TabPage tbAdvertisers;
        private System.Windows.Forms.DataGridView dgAdvertisers;
        private System.Windows.Forms.TabPage tbCampaigns;
        private mediaLavidgeDataSet mediaLavidgeDataSet;
        private System.Windows.Forms.BindingSource dataTradeDeskAdvertisersBindingSource;
        private mediaLavidgeDataSetTableAdapters.dataTradeDeskAdvertisersTableAdapter dataTradeDeskAdvertisersTableAdapter;
        private System.Windows.Forms.TabPage tbAdGroups;
        private System.Windows.Forms.DataGridView dgCampaigns;
        private System.Windows.Forms.BindingSource mediaLavidgeDataSetBindingSource;
        private System.Windows.Forms.BindingSource dataTradeDeskCampaignsBindingSource;
        private mediaLavidgeDataSetTableAdapters.dataTradeDeskCampaignsTableAdapter dataTradeDeskCampaignsTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn advertiserNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgAdvertisers_Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn advertiserIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn domainAddressDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgAdvertisers_CompanyId;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource mediaLavidgeDataSetBindingSource1;
        private System.Windows.Forms.BindingSource dataTradeDeskAdGroupBindingSource;
        private mediaLavidgeDataSetTableAdapters.dataTradeDeskAdGroupTableAdapter dataTradeDeskAdGroupTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn campaignNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn campaignIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modifiedDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn advertiserIdDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn campaignObjectDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn adGroupNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn adGroupIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn adGroupObjectDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modifiedDateDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn campaignIdDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
    }
}

