﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WhirlygigSolutionHelper;
using static TheTradeDeskConsoleApplication.mediaLavidgeDataSet;

namespace TheTradeDeskConsoleApplication
{
    public partial class frmConsole : Form
    {
        TradedeskConsoleEntities db = new TradedeskConsoleEntities();
        Company company = new Company();
        Data processor = new Data();

        public frmConsole()
        {
            InitializeComponent();
        }

        private void toolStripContainer1_TopToolStripPanel_Click(object sender, EventArgs e)
        {

        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void frmConsole_Load(object sender, EventArgs e)
        { 

        }
        private void button1_Click(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mediaLavidgeDataSet.dataTradeDeskAdvertisers' table. You can move, or remove it, as needed.
            this.dataTradeDeskAdvertisersTableAdapter.Fill(this.mediaLavidgeDataSet.dataTradeDeskAdvertisers);
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                FillCampaignDatatable(e);
            }
        }

        private void FillCampaignDatatable(DataGridViewCellEventArgs e)
        {
            var selectedDb = new TradedeskConsoleEntities();

            try
            { 
                long comId = long.Parse(dgAdvertisers["dgAdvertisers_CompanyId", e.RowIndex].Value.ToString());

                company = (from m in db.Companies where m.Id == comId && m.IsActive == true select m).FirstOrDefault();
                processor = new Data(company.DatabaseName, company.Login, company.Password, company.Server);

                dataTradeDeskCampaignsTableAdapter.Connection.ConnectionString = processor.ConnectionString;

                this.dataTradeDeskCampaignsTableAdapter.Fill(this.mediaLavidgeDataSet.dataTradeDeskCampaigns);

                tbDashboard.SelectedTab = tbCampaigns;
            }
            catch (Exception ex)
            {
                string ec = ex.Message;
            }
        }

        private void dgCampaigns_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                FillAdGroupDatatable(e);
            }
        }

        private void FillAdGroupDatatable(DataGridViewCellEventArgs e)
        {
            try
            { 
                var selectedDb = new TradedeskConsoleEntities(); 

                dataTradeDeskAdGroupTableAdapter.Connection.ConnectionString = processor.ConnectionString;

                this.dataTradeDeskAdGroupTableAdapter.Fill(this.mediaLavidgeDataSet.dataTradeDeskAdGroup);

                tbDashboard.SelectedTab = tbAdGroups;
            }
            catch (Exception ex)
            {
                string ec = ex.Message;
            }
        }
    }
}
