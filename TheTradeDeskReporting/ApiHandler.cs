﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Configuration;

namespace TheTradeDeskReporting
{
    public class ApiHandler
    {
        #region PrivateMethods

        private void GetAdvertisers()
        {
            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();
            DateTime startTime = System.DateTime.Now;

            Console.WriteLine("GetAdvertisers : Start Time - " + startTime);

            AdvertiserServiceHandler service = new AdvertiserServiceHandler();
            service.Run();

            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("GetAdvertisers : " + endTime.Subtract(startTime).TotalMinutes);
        }

        private void GetCampaigns()
        {
            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();
            DateTime startTime = System.DateTime.Now;

            Console.WriteLine("GetCampaigns : Start Time - " + startTime);

            CampaignServiceHandler service = new CampaignServiceHandler(); 
            service.Run();

            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("GetCampaigns : " + endTime.Subtract(startTime).TotalMinutes);
        }

        private void GetAdGroups()
        {
            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();
            DateTime startTime = System.DateTime.Now;

            Console.WriteLine("GetAdGroups : Start Time - " + startTime);

            AdGroupServiceHandler service = new AdGroupServiceHandler();
            service.Run();
            service.RunAdgroups();

            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("GetAdGroups : " + endTime.Subtract(startTime).TotalMinutes);
        }

        private void GetCreative()
        {
            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor(); 

            DateTime startTime = System.DateTime.Now;

            Console.WriteLine("GetCreative : Start Time - " + startTime);

            CreativeServiceHandler service = new CreativeServiceHandler();
            service.Run();

            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("GetCreative : " + endTime.Subtract(startTime).TotalMinutes);
        }

        private void GetHDReports()
        {
            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();
            DateTime startTime = System.DateTime.Now;

            Console.WriteLine("GetHDReports : Start Time - " + startTime);

            HDReportsServiceHandler service = new HDReportsServiceHandler();
            service.Run();

            DateTime endTime = System.DateTime.Now;
            Console.WriteLine("GetHDReports : " + endTime.Subtract(startTime).TotalMinutes);
        }

        private void GetMyReports()
        {
            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();
            DateTime startTime = System.DateTime.Now;

            Console.WriteLine("GetMyReports : Start Time - " + startTime);

            MyReportsServiceHandler service = new MyReportsServiceHandler();
            service.RunGetFile();
            service.RunProcessFile();

            DateTime endTime = System.DateTime.Now;
            Console.WriteLine("GetMyReports : " + endTime.Subtract(startTime).TotalMinutes);
        }

        private void ProcessEmails()
        {
            Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();
            DateTime startTime = System.DateTime.Now;

            Console.WriteLine("ProcessEmails : Start Time - " + startTime);
             
            ProcessingEmail service = new ProcessingEmail();
            service.Run(); 

            DateTime endTime = System.DateTime.Now;
            Console.WriteLine("ProcessEmails : " + endTime.Subtract(startTime).TotalMinutes);
        }

        #endregion

        public void Process()
        {
            DateTime startTime = System.DateTime.Now; 

            ProcessEmails();
            GetAdvertisers();
            GetCampaigns(); 
            GetAdGroups();
            GetHDReports();
            GetMyReports();
            GetCreative();

            DateTime endTime = System.DateTime.Now;

            Console.WriteLine("Total Time : " + endTime.Subtract(startTime).TotalMinutes); 
        } 
    }
}
