﻿using SolutionHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Script.Serialization;
using TheTradeDeskReporting.Models;
using WhirlygigSolutionHelper;

namespace TheTradeDeskReporting
{
    public class AdGroupServiceHandler
    {
        mediaTradeDeskEntities1 db = new mediaTradeDeskEntities1();
        private bool debug = bool.Parse(ConfigurationManager.AppSettings["debug"].ToString());
        bool sandbox = bool.Parse(ConfigurationManager.AppSettings["sandbox"].ToString() ?? "false");
        private string url = ConfigurationManager.AppSettings["APIURL"] + "adgroup/query/advertiser/";
        private string urlForAdgroup = ConfigurationManager.AppSettings["APIURL"] + "adgroup/";

        public Company company;
        public Data processor = new Data();

        #region Public Methods

        public AdGroupServiceHandler()
        {
            bool sandbox = bool.Parse(ConfigurationManager.AppSettings["sandbox"].ToString() ?? "false");
              
            if (sandbox)
            {
                url = @"https://apisb.thetradedesk.com/v3/adgroup/query/advertiser/";
                urlForAdgroup = @"https://apisb.thetradedesk.com/v3/adgroup/";
                db.Database.Connection.ConnectionString = new Data("mediaTestDatabase", "mediaTestDatabaseUser", "mediaTestDatabase@!@", "172.16.8.201").ConnectionString; ;
            }
        }

        public void Run()
        {
            try
            {
                List<dataTradeDeskAdvertiser> advertisers = (from m in db.dataTradeDeskAdvertisers where m.CompanyId != null select m).ToList();

                foreach (dataTradeDeskAdvertiser advertiser in advertisers)
                {
                    Console.WriteLine(advertiser.AdvertiserName + ": Starting adgroup advertiser");

                    AdgroupProcessor(advertiser);

                    Console.WriteLine(advertiser.AdvertiserName + ": Finished adgroup advertiser");
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "AdGroupServiceHandler.Run:");
            }
        }

        public void RunAdgroups()
        {
            try
            {
                Company[] companies = (from m in db.Companies where m.IsActive == true && m.Id != 1 select m).ToArray();

                foreach (Company com in companies)
                {
                    Console.WriteLine(com.CompanyName + ": Starting adgroup");

                    UpdateAdgroup();

                    Console.WriteLine(com.CompanyName + ": Finished adgroup");
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "AdGroupServiceHandler.RunAdgroups:");
            }
        }

        public void CreateAdgroup(dataTradeDeskAdGroup adgroup)
        {
            try
            {
                if (debug && sandbox)
                {
                    using (AuthenticationServiceHandler authenticationUtil = new AuthenticationServiceHandler())
                    {
                        CreateAdgroup(adgroup, authenticationUtil);
                    }
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "AdGroupServiceHandler.CreateAdgroup:");
            }
        }

        #endregion
        #region Private Methods

        private void AdgroupProcessor(dataTradeDeskAdvertiser advertiser)
        { 
            GetCompany(advertiser.CompanyId);

            AdGroupResponse response = GetAdgroups(advertiser);

            if (response != null)
                UpdateAdgroupList(response);

            RecheckAdGroupsBasedOnReport(advertiser);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="com"></param>
        private void AdGroupFlights()
        {
            int count = 0;
            int idx = 0;
            var selectedDb = new mediaTradeDeskEntities1();

            try
            {
                selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

                dataTradeDeskAdGroup[] adGroups = (from n in selectedDb.dataTradeDeskAdGroups select n).ToArray();

                count = adGroups.Count();

                foreach (dataTradeDeskAdGroup _adGroup in adGroups)
                {
                    idx++;

                    if (debug)
                    {
                        if (company.DatabaseName != "mediaAAAInsurance")
                            continue;

                        if (_adGroup.Campaign_Id != "8ixx8wb")
                            continue;
                    }

                    string _adGroupId = _adGroup.Ad_Group_Id;

                    AdGroupResponse response = GetAdgroup(_adGroup);

                    if (response != null)
                    {
                        AdGroupFlightCollection collection = new AdGroupFlightCollection(response.AdgroupDynamicObject.RTBAttributes.BudgetSettings.AdGroupFlights);

                        _adGroup.Ad_Group_Object = response.AdgroupObject;
                        _adGroup.Campaign_Id = response.AdgroupDynamicObject.CampaignId ?? "";
                        _adGroup.AdGroup_Budget = response.AdgroupDynamicObject.RTBAttributes.BudgetSettings.Budget.Amount ?? "";
                        _adGroup.AdGroup_Budget_Current = collection.AdGroupFlights.OrderByDescending(x => x.CampaignFlightId).First().BudgetInAdvertiserCurrency;

                        _adGroup.ModifiedDate = System.DateTime.Now;
                    }

                    selectedDb.SaveChanges();
                    Console.WriteLine(String.Format("{0} Adgroup Processed: {1} of {2} - {3}", company.CompanyName, idx, count, _adGroup.Ad_Group_Name));
                }
            }
            catch (Exception ex)
            {
                Error.LogError(ex, "RecheckAdGroupsBasedOnReport:" + company.CompanyName ?? "");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="com"></param>
        private void UpdateAdgroup()
        {
            int count = 0;
            int idx = 0;
            var selectedDb = new mediaTradeDeskEntities1();

            try
            {
                selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

                dataTradeDeskAdGroup[] adGroups = (from n in selectedDb.dataTradeDeskAdGroups select n).ToArray();

                count = adGroups.Count();

                foreach (dataTradeDeskAdGroup _adGroup in adGroups)
                {
                    if (debug)
                    {
                        if (company.DatabaseName != "mediaAAAInsurance")
                            continue;

                        if (_adGroup.Campaign_Id != "8ixx8wb")
                            continue;
                    }

                    idx++;
                    string _adGroupId = _adGroup.Ad_Group_Id;

                    AdGroupResponse response = GetAdgroup(_adGroup);

                    if (response != null)
                    {
                        AdGroupFlightCollection _flights = new AdGroupFlightCollection(response.AdgroupDynamicObject.RTBAttributes.BudgetSettings.AdGroupFlights);
                        ProcessAdGroupFlights(_flights);

                        _adGroup.Ad_Group_Object = response.AdgroupObject;
                        _adGroup.Campaign_Id = response.AdgroupDynamicObject.CampaignId ?? "";
                        _adGroup.AdGroup_Budget = response.AdgroupDynamicObject.RTBAttributes.BudgetSettings.Budget.Amount ?? "";
                        _adGroup.AdGroup_Budget_Current = _flights.AdGroupFlights.OrderByDescending(x => x.CampaignFlightId).First().BudgetInAdvertiserCurrency;
                        _adGroup.ModifiedDate = System.DateTime.Now;
                    }

                    selectedDb.SaveChanges();
                    Console.WriteLine(String.Format("{0} Adgroup Processed: {1} of {2} - {3}", company.CompanyName, idx, count, _adGroup.Ad_Group_Name));
                }
            }
            catch (Exception ex)
            {
                Error.LogError(ex, "AdGroupServiceHandler.UpdateAdgroup:" + company.CompanyName ?? "");
            }
        }

        private void ProcessAdGroupFlights(AdGroupFlightCollection _flights)
        {
            int count = 0;
            int idx = 0;
            var selectedDb = new mediaTradeDeskEntities1();

            try
            {
                selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

                foreach (AdGroupFlight _flight in _flights.AdGroupFlights)
                {

                    dataTradeDeskAdGroupFlight _newFlight = (from n in selectedDb.dataTradeDeskAdGroupFlights where n.AdGroupId == _flight.AdGroupId && n.CampaignFlightId == _flight.CampaignFlightId select n).FirstOrDefault();

                    if (_newFlight == null)
                    {
                        dataTradeDeskAdGroupFlight __newFlight = new dataTradeDeskAdGroupFlight();
                        __newFlight.AdGroupId = _flight.AdGroupId;
                        __newFlight.CampaignFlightId = _flight.CampaignFlightId;
                        __newFlight.BudgetInAdvertiserCurrency = _flight.BudgetInAdvertiserCurrency;
                        __newFlight.DailyTargetInAdvertiserCurrency = _flight.DailyTargetInAdvertiserCurrency;
                        __newFlight.ModifiedDate = System.DateTime.Now;

                        if (_flight.BudgetInImpressions != null)
                            __newFlight.BudgetInImpressions = int.Parse(_flight.BudgetInImpressions);
                        if (_flight.DailyTargetInImpressions != null)
                            __newFlight.DailyTargetInImpressions = int.Parse(_flight.DailyTargetInImpressions);

                        selectedDb.dataTradeDeskAdGroupFlights.Add(__newFlight);
                    }
                    else
                    {
                        _newFlight.AdGroupId = _flight.AdGroupId;
                        _newFlight.CampaignFlightId = _flight.CampaignFlightId;
                        _newFlight.BudgetInAdvertiserCurrency = _flight.BudgetInAdvertiserCurrency;
                        _newFlight.DailyTargetInAdvertiserCurrency = _flight.DailyTargetInAdvertiserCurrency;

                        if (_flight.BudgetInImpressions != null)
                            _newFlight.BudgetInImpressions = int.Parse(_flight.BudgetInImpressions);
                        if (_flight.DailyTargetInImpressions != null)
                            _newFlight.DailyTargetInImpressions = int.Parse(_flight.DailyTargetInImpressions);

                        _newFlight.ModifiedDate = System.DateTime.Now;

                    }

                    if (idx == 25) // Batch trips to the database
                    {
                        selectedDb.SaveChanges();
                        idx = 0;
                    }

                    idx++;

                    Console.WriteLine(String.Format("{0} Adgroup Flights Processed", company.CompanyName));
                }

                selectedDb.SaveChanges();
            }
            catch (Exception ex)
            {
                Error.LogError(ex, "ProcessAdGroupFlights:" + company.CompanyName ?? "");
            }
        }

        private void RecheckAdGroupsBasedOnReport(dataTradeDeskAdvertiser advertiser)
        {
            int idx = 0;
            var selectedDb = new mediaTradeDeskEntities1();

            try
            {
                if (company != null)
                {
                    selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

                    string[] adGroupIds = (from n in selectedDb.dataTradeDeskAdGroups select n.Ad_Group_Id).ToArray(); // Get list of creative ids 
                    string[] adGroupIdsReporting = (from m in selectedDb.dataTradeDeskReportingDatas where m.Advertiser_Id == advertiser.AdvertiserId select m.Ad_Group_Id).Distinct().ToArray(); // Get list of distinct creative Id's from reporting data
                    string[] results = (from _str in adGroupIdsReporting where !adGroupIds.Contains(_str) select _str).ToArray();

                    foreach (string _adGroupId in results)
                    {
                        if (_adGroupId == "[Unknown]")
                            continue;

                        AdGroupResponse response = GetAdgroup(_adGroupId);

                        if (response != null)
                        {
                            AdGroupResponseData cr = new AdGroupResponseData();
                            cr.AdGroupId = response.AdGroupId;
                            cr.AdGroupName = response.AdGroupName;

                            AdGroupResponseData[] Result = new AdGroupResponseData[1];
                            Result[0] = cr;
                            response.Result = Result;

                            UpdateAdGroupList(response, advertiser);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Error.LogError(ex, "RecheckAdGroupsBasedOnReport:" + advertiser.AdvertiserName ?? "");
            }
        }

        private AdGroupResponse GetAdgroup(string _adGroupId)
        {

            return null;
        }

        private void UpdateAdGroupList(AdGroupResponse response, dataTradeDeskAdvertiser advertiser)
        {
            try
            {
                var selectedDb = new mediaTradeDeskEntities1();

                Company com = (from m in db.Companies where m.Id == advertiser.CompanyId && m.IsActive == true select m).FirstOrDefault();

                if (com == null)
                    return;

                int idx = 0;

                Data processor = new Data(com.DatabaseName, com.Login, com.Password, com.Server);
                selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

                for (int i = 0; i <= response.Result.Count() - 1; i++)
                {
                    AdGroupResponseData row = response.Result[i];

                    dataTradeDeskAdGroup AdGroup = (from m in selectedDb.dataTradeDeskAdGroups where m.Ad_Group_Id == row.AdGroupId select m).FirstOrDefault();

                    if (AdGroup == null)
                    {
                        dataTradeDeskAdGroup adGroup = new dataTradeDeskAdGroup();
                        adGroup.Ad_Group_Id = row.AdGroupId;
                        adGroup.Ad_Group_Name = row.AdGroupName;

                        selectedDb.dataTradeDeskAdGroups.Add(adGroup);
                    }
                    else
                    {
                        AdGroup.Ad_Group_Id = row.AdGroupId;
                        AdGroup.Ad_Group_Name = row.AdGroupName;
                    }

                    if (idx == 25) // Batch trips to the database
                    {
                        selectedDb.SaveChanges();
                        idx = 0;
                    }

                    Console.WriteLine(row.AdGroupName);

                    idx++;
                }

                selectedDb.SaveChanges();
            }
            catch (Exception ex)
            {
                Error.LogError(ex, "UpdateCreativeList:" + advertiser.AdvertiserName ?? "");
            }
        }

        private AdGroupResponse GetAdgroup(dataTradeDeskAdGroup _adGroup)
        {
            AdGroupResponse _response = null;

            using (AuthenticationServiceHandler authenticationUtil = new AuthenticationServiceHandler())
            {
                try
                {
                    string _url = urlForAdgroup + _adGroup.Ad_Group_Id;

                    var request = (HttpWebRequest)WebRequest.Create(_url);
                    request.Headers.Add("TTD-Auth", authenticationUtil.UserSecurityToken);

                    request.ContentType = "application/json";
                    request.Method = "GET";

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                    using (Stream responseStream = response.GetResponseStream())
                    {
                        _response = new AdGroupResponse();

                        StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);


                        string json = reader.ReadToEnd() ?? "";
                        dynamic data = Json.Decode(json);

                        _response.AdgroupDynamicObject = data;
                        _response.AdgroupObject = json;
                    }
                }
                catch (Exception ex)
                {
                    // //Invalid CreativeId will can return 400 error code.
                    //  SolutionHelper.Error.LogError(ex, "GetCreatives 2:" + creativeid);
                    return null;
                }
            }

            return _response;
        }
        private AdGroupResponse GetCampaignFlight(dataTradeDeskAdGroup _adGroup)
        {
            AdGroupResponse _response = null;

            using (AuthenticationServiceHandler authenticationUtil = new AuthenticationServiceHandler())
            {
                try
                {
                    string _url = urlForAdgroup + _adGroup.Ad_Group_Id;

                    var request = (HttpWebRequest)WebRequest.Create(_url);
                    request.Headers.Add("TTD-Auth", authenticationUtil.UserSecurityToken);

                    request.ContentType = "application/json";
                    request.Method = "GET";

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                    using (Stream responseStream = response.GetResponseStream())
                    {
                        _response = new AdGroupResponse();

                        StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);


                        string json = reader.ReadToEnd() ?? "";
                        dynamic data = Json.Decode(json);

                        _response.AdgroupDynamicObject = data;
                        _response.AdgroupObject = json;
                    }
                }
                catch (Exception ex)
                {
                    // //Invalid CreativeId will can return 400 error code.
                    //  SolutionHelper.Error.LogError(ex, "GetCreatives 2:" + creativeid);
                    return null;
                }
            }

            return _response;
        }

        private void GetCompany(int? companyId)
        {
            Reset();

            if (companyId == 0)
                return;
            if (sandbox)
                company = (from v in db.Companies where v.DatabaseName == "mediaTestDatabase" select v).FirstOrDefault();
            else
                company = (from v in db.Companies where v.Id == companyId && v.IsActive == true select v).FirstOrDefault();

            processor = new Data(company.DatabaseName, company.Login, company.Password, company.Server);
        }
        private void Reset()
        {
            company = null;
            processor = null;
        }
        private void UpdateAdgroupList(AdGroupResponse response)
        {
            try
            {
                var selectedDb = new mediaTradeDeskEntities1();

                if (company != null)
                {
                    int idx = 0;

                    selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

                    for (int i = 0; i <= response.Result.Count() - 1; i++)
                    {
                        AdGroupResponseData row = response.Result[i];

                        dataTradeDeskAdGroup adgroup = (from m in selectedDb.dataTradeDeskAdGroups where m.Ad_Group_Id == row.AdGroupId select m).FirstOrDefault();

                        if (adgroup == null)
                        {
                            dataTradeDeskAdGroup ad = new dataTradeDeskAdGroup();
                            ad.Ad_Group_Id = row.AdGroupId;
                            ad.Ad_Group_Name = row.AdGroupName;

                            selectedDb.dataTradeDeskAdGroups.Add(ad);
                        }
                        else
                        {
                            adgroup.Ad_Group_Id = row.AdGroupId;
                            adgroup.Ad_Group_Name = row.AdGroupName;
                        }

                        if (idx == 25) // Batch trips to the database
                        {
                            selectedDb.SaveChanges();
                            idx = 0;
                        }

                        idx++;
                    }
                    selectedDb.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Error.LogError(ex, "UpdateAdgroupList: " + response.AdGroupId);
            }
        }

        private AdGroupResponse GetAdgroups(dataTradeDeskAdvertiser advertiser)
        {
            using (AuthenticationServiceHandler authenticationUtil = new AuthenticationServiceHandler())
            {
                try
                {
                    var request = (HttpWebRequest)WebRequest.Create(url);
                    request.Headers.Add("TTD-Auth", authenticationUtil.UserSecurityToken);

                    request.ContentType = "application/json; charset=utf-8";
                    request.Method = "POST";

                    using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                    {
                        string json = new JavaScriptSerializer().Serialize(new
                        {
                            AdvertiserId = advertiser.AdvertiserId,
                            PageStartIndex = 1,
                            PageSize = 100000
                        });

                        streamWriter.Write(json);
                    }

                    var response = (HttpWebResponse)request.GetResponse();
                    DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(AdGroupResponse));
                    object objResponse = jsonSerializer.ReadObject(response.GetResponseStream());

                    AdGroupResponse jsonResponse = objResponse as AdGroupResponse;

                    return jsonResponse;
                }
                catch (Exception ex)
                {
                    SolutionHelper.Error.LogError(ex, "GetAdgroups:" + advertiser.AdvertiserId);
                    return null;
                }
            }
        }

        private void CreateAdgroup(dataTradeDeskAdGroup adgroup, AuthenticationServiceHandler authenticationUtil)
        {
            try
            {
                AdGroupResponse _response = null;
                string _method = "POST";

                if (adgroup != null)
                    _method = "PUT";

            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "AdGroupServiceHandler.CreateAdgroup w/ auth:");
            }
        }
        #endregion


    }
}
