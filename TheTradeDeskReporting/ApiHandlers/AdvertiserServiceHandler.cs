﻿using SolutionHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Script.Serialization;
using WhirlygigSolutionHelper;

namespace TheTradeDeskReporting
{
    public class AdvertiserServiceHandler
    {

        mediaTradeDeskEntities1 db = new mediaTradeDeskEntities1();
        Company company;
        Data processor = new Data();

        private string url = ConfigurationManager.AppSettings["APIURL"] + "advertiser/query/partner/";
        private string urlCreate = ConfigurationManager.AppSettings["APIURL"] + "advertiser/";
        bool debug = bool.Parse(ConfigurationManager.AppSettings["debug"].ToString());
        bool sandbox = bool.Parse(ConfigurationManager.AppSettings["sandbox"].ToString() ?? "false");

        #region PublicMethods

        public AdvertiserServiceHandler()
        {
            if (sandbox)
            {
                url = @"https://apisb.thetradedesk.com/v3/advertiser/query/partner/";
                urlCreate = @"https://apisb.thetradedesk.com/v3/advertiser/";
            }
        }

        public void Run()
        {
            using (AuthenticationServiceHandler authenticationUtil = new AuthenticationServiceHandler())
            {
                try
                {
                    AdvertiserResponse response = GetAdvertisers(authenticationUtil);

                    if (response != null)
                    {
                        ProcessAdvertisers(response);
                        ProcessAdvertisersIndividually(response);

                        if (debug && sandbox)
                        {
                            CreateUpdateAdvertiser();
                            CreateUpdateAdvertiser(new dataTradeDeskAdvertiser());
                        }
                    }
                }
                catch (Exception ex)
                {
                    SolutionHelper.Error.LogError(ex, "AdvertiserServiceHandler.Run:");
                }
            }
        }

        public void CreateUpdateAdvertiser(dataTradeDeskAdvertiser advertiser = null)
        {
            using (AuthenticationServiceHandler authenticationUtil = new AuthenticationServiceHandler())
            {
                try
                {
                    if (advertiser == null)
                        advertiser = new dataTradeDeskAdvertiser { AdvertiserName = "Tester Advertiser 321", AdvertiserId = "" };

                    CreateAdvertiser(authenticationUtil, advertiser);
                }
                catch (Exception ex)
                {
                    SolutionHelper.Error.LogError(ex, "CreateUpdateAdvertiser:");
                }
            }
        }

        #endregion

        #region PrivateMethods

        private void ProcessAdvertisers(AdvertiserResponse response)
        {
            int idx = 0;
            var selectedDb = new mediaTradeDeskEntities1();

            if (sandbox)
                selectedDb.Database.Connection.ConnectionString = new Data("mediaTestDatabase", "mediaTestDatabaseUser", "mediaTestDatabase@!@", "172.16.8.201").ConnectionString;     /*     db.Database.Connection.ConnectionString.Replace("mssql.tlciweb.com,2433", "172.16.8.201").Replace("catalog=mediaLavidge;", "catalog=mediaTestDatabase;");*/
            else
                selectedDb.Database.Connection.ConnectionString = db.Database.Connection.ConnectionString;

            for (int i = 0; i <= response.Result.Count() - 1; i++)
            {
                try
                {
                    AdvertiserResponseData row = response.Result[i];

                    dataTradeDeskAdvertiser advertiser = (from m in selectedDb.dataTradeDeskAdvertisers where m.AdvertiserId == row.AdvertiserId select m).FirstOrDefault();

                    if (advertiser == null)
                    {
                        dataTradeDeskAdvertiser adv = new dataTradeDeskAdvertiser();
                        adv.AdvertiserId = row.AdvertiserId;
                        adv.AdvertiserName = row.AdvertiserName;
                        adv.DomainAddress = row.DomainAddress;
                        adv.Active = false;
                        adv.bolDataElements = false;

                        selectedDb.dataTradeDeskAdvertisers.Add(adv);

                        Console.WriteLine(row.AdvertiserName);
                    }
                    else
                    {
                        advertiser.AdvertiserId = row.AdvertiserId;
                        advertiser.AdvertiserName = row.AdvertiserName;
                        advertiser.DomainAddress = row.DomainAddress;

                        Console.WriteLine(advertiser.AdvertiserName);
                    }

                    selectedDb.SaveChanges();

                    idx++;
                }
                catch (Exception ex)
                {
                    Error.LogError(ex, "ProcessAdvertisers: " + response.Result[i].AdvertiserName);
                }
            }
        }

        private void ProcessAdvertisersIndividually(AdvertiserResponse response)
        {
            int idx = 0;

            for (int i = 0; i <= response.Result.Count() - 1; i++)
            {
                try
                {
                    AdvertiserResponseData row = response.Result[i];

                    int companyId = (from m in db.dataTradeDeskAdvertisers where m.AdvertiserId == row.AdvertiserId && m.CompanyId != null select m.CompanyId).FirstOrDefault() ?? 0;

                    if (companyId == 0)
                        continue;

                    GetCompany(companyId);

                    if (company == null)
                        continue;

                    var selectedDb = new mediaTradeDeskEntities1();

                    selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

                    dataTradeDeskAdvertiser advertiser = (from m in selectedDb.dataTradeDeskAdvertisers where m.AdvertiserId == row.AdvertiserId select m).FirstOrDefault();

                    if (advertiser == null)
                    {
                        dataTradeDeskAdvertiser adv = new dataTradeDeskAdvertiser();
                        adv.AdvertiserId = row.AdvertiserId;
                        adv.AdvertiserName = row.AdvertiserName;
                        adv.DomainAddress = row.DomainAddress;
                        adv.CompanyId = row.CompanyId;
                        adv.Active = row.Active;
                        adv.bolDataElements = row.bolDataElements;
                        adv.bolMyReports = row.bolMyReports;
                        adv.bolConversionReport = row.bolConversionReport;
                        adv.Advertiser_Object = row.Advertiser_Object;
                        adv.Conversion_Success_tags = row.Conversion_Success_tags;
                        adv.Action_Tags = row.Action_Tags;

                        selectedDb.dataTradeDeskAdvertisers.Add(adv);
                    }
                    else
                    {
                        advertiser.AdvertiserId = row.AdvertiserId;
                        advertiser.AdvertiserName = row.AdvertiserName;
                        advertiser.DomainAddress = row.DomainAddress;
                        advertiser.CompanyId = row.CompanyId;
                        advertiser.Active = row.Active;
                        advertiser.bolDataElements = row.bolDataElements;
                        advertiser.bolMyReports = row.bolMyReports;
                        advertiser.bolConversionReport = row.bolConversionReport;
                        advertiser.Advertiser_Object = row.Advertiser_Object;
                        advertiser.Conversion_Success_tags = row.Conversion_Success_tags;
                        advertiser.Action_Tags = row.Action_Tags;
                    }

                    selectedDb.SaveChanges();

                    idx++;
                }
                catch (Exception ex)
                {
              //      Error.LogError(ex, "ProcessAdvertisersIndividually: " + response.Result[i].AdvertiserName);
                }
            }
        }

        private void GetCompany(int companyId)
        {
            Reset();

            if (sandbox)
                company = (from v in db.Companies where v.DatabaseName == "mediaTestDatabase" select v).FirstOrDefault();
            else
                company = (from v in db.Companies where v.Id == companyId && v.IsActive == true select v).FirstOrDefault();

            processor = new Data(company.DatabaseName, company.Login, company.Password, company.Server);
        }

        private void Reset()
        {
            company = null;
            processor = null;
        }

        private AdvertiserResponse GetAdvertisers(AuthenticationServiceHandler authenticationUtil)
        {
            AdvertiserResponse _response = null;
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Headers.Add("TTD-Auth", authenticationUtil.UserSecurityToken);

                request.ContentType = "application/json; charset=utf-8";
                request.Method = "POST";

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(new
                    {
                        PartnerId = authenticationUtil.PartnerId,
                        PageStartIndex = 0,
                        PageSize = 10000
                    });

                    streamWriter.Write(json);
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                using (Stream responseStream = response.GetResponseStream())
                using (var stream = new MemoryStream())
                {
                    responseStream.CopyTo(stream);

                    try
                    {
                        DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(AdvertiserResponse));
                        Stream resp = response.GetResponseStream();

                        stream.Position = 0;

                        object objResponse = jsonSerializer.ReadObject(stream);
                         
                        AdvertiserResponse jsonResponse = objResponse as AdvertiserResponse;
                         
                        return jsonResponse;

                        _response = new AdvertiserResponse();

                        StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);

                        string json = reader.ReadToEnd() ?? "";

                        dynamic data = Json.Decode(json);

                        _response.AdvertiserDynamicObject = data;
                        _response.AdvertiserObject = json;
                        _response.Result = data.Result;
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                    }
                }

                return _response;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "GetAdvertisers:");
                return null;
            }
        }

        private bool CreateAdvertiser(AuthenticationServiceHandler authenticationUtil, dataTradeDeskAdvertiser advertiser)
        {
            AdvertiserResponse _response = null;
            string _method = "POST";

            if (advertiser != null)
                _method = "PUT";

            try
            {
                var request = (HttpWebRequest)WebRequest.Create(urlCreate);

                request.Headers.Add("TTD-Auth", authenticationUtil.UserSecurityToken);

                request.ContentType = "application/json; charset=utf-8";
                request.Method = _method;

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(new
                    {
                        PartnerId = authenticationUtil.PartnerId,
                        AdvertiserName = advertiser.AdvertiserName,
                        AdvertiserId = advertiser.AdvertiserId,
                        AttributionClickLookbackWindowInSeconds = 2592000,
                        AttributionImpressionLookbackWindowInSeconds = 2592000,
                        ClickDedupWindowInSeconds = 7,
                        ConversionDedupWindowInSeconds = 60,
                        DefaultRightMediaOfferTypeId = 8,
                        IndustryCategoryId = 58
                    });

                    streamWriter.Write(json);
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                using (Stream responseStream = response.GetResponseStream())
                {
                    _response = new AdvertiserResponse();

                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);

                    string json = reader.ReadToEnd() ?? "";
                    dynamic data = Json.Decode(json);

                    _response.AdvertiserDynamicObject = data;
                    _response.AdvertiserObject = json;
                }

                return true;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "CreateAdvertiser:");
            }

            return false;
        }

        #endregion

    }
}
