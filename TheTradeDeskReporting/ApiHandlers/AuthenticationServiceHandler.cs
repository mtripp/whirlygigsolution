﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace TheTradeDeskReporting
{
    /// <summary>
    /// This class is a utility class to handle authentication for tests
    /// </summary>
    public class AuthenticationServiceHandler : IDisposable
    {
        public string UserSecurityToken { get; set; }
        public string PartnerId { get; set; }

        public AuthenticationServiceHandler()
            : this(ConfigurationManager.AppSettings["APILogin"],
                   ConfigurationManager.AppSettings["APIPassword"],
                   ConfigurationManager.AppSettings["APIURL"] + "authentication")
        {
        }

        /// <summary>
        /// Authenticates the user's credentials on the Trade Desk API and creates a session which its 
        /// identifier is kept in the UserSecurityToken property.
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <param name="url"></param>
        public AuthenticationServiceHandler(string login, string password, string url)
        { 
            bool sandbox = bool.Parse(ConfigurationManager.AppSettings["sandbox"].ToString() ?? "false");

            PartnerId = ConfigurationManager.AppSettings["APIPartnerID"];

            if (sandbox)
            {
                PartnerId = "j7jk0s3";
                login = "ttd_test_api@lavidge.com";
                password = @"*>VhA?E7gYv(}5";
                url = @"https://apisb.thetradedesk.com/v3/authentication";
            }

            string Message = "";

            for (int i = 0; i < 4; i++) // 3 attempts
            { 
                try
                {
                    if (i == 3)
                        url += url + "/";

                    var request = (HttpWebRequest)WebRequest.Create(url);
                    request.ContentType = "application/json";
                    request.Method = "POST";

                    using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                    {
                        string json = new JavaScriptSerializer().Serialize(new
                        {
                            Login = login,
                            Password = password
                        });

                        streamWriter.Write(json);
                    }

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    var response = (HttpWebResponse)request.GetResponse();
                    DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(Response));
                    object objResponse = jsonSerializer.ReadObject(response.GetResponseStream());
                    Response jsonResponse = objResponse as Response;

                    UserSecurityToken = jsonResponse.Token;
                    return;
                }
                catch (Exception ex)
                { 
                    Message = ex.Message;
                }
            }
        }

        /// <summary>
        /// Clears the current session on the MediaMind API.
        /// </summary>
        public void Dispose()
        {
            UserSecurityToken = "";
        }

        [DataContract]
        public class Response
        {
            [DataMember(Name = "Token")]
            public string Token { get; set; }
        }
    }
}
