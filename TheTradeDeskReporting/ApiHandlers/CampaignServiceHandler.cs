﻿using SolutionHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Script.Serialization;
using TheTradeDeskReporting.Models;
using WhirlygigSolutionHelper;

namespace TheTradeDeskReporting
{
    public class CampaignServiceHandler
    {
        Company company;
        Data processor = new Data();
        mediaTradeDeskEntities1 db = new mediaTradeDeskEntities1();

        private string url;
        private string urlForCampaign;
        private string urlForCampaignFlight;
        private bool debug = bool.Parse(ConfigurationManager.AppSettings["debug"].ToString());
        private bool sandbox = bool.Parse(ConfigurationManager.AppSettings["sandbox"].ToString() ?? "false");

        #region Private 
        private void RecheckCampaignBasedOnReport(dataTradeDeskAdvertiser advertiser)
        {
            try
            {
                var selectedDb = new mediaTradeDeskEntities1();

                GetCompany(advertiser.CompanyId);

                if (company != null)
                {
                    selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

                    string[] campaignIds = (from n in selectedDb.dataTradeDeskCampaigns select n.Campaign_Id).ToArray();
                    string[] campaignIdsReporting = (from m in selectedDb.dataTradeDeskReportingDatas where m.Advertiser_Id == advertiser.AdvertiserId select m.Campaign_Id).Distinct().ToArray(); // Get list of distinct Campaign Id's from reporting data
                    string[] results = (from _str in campaignIdsReporting where !campaignIds.Contains(_str) select _str).ToArray();

                    foreach (string _campaignId in results)
                    {
                        if (_campaignId == "[Unknown]")
                            continue;

                        CampaignResponseData response = GetCampaigns(_campaignId);

                        if (response != null)
                        {
                            CampaignResponseData cr = new CampaignResponseData();
                            cr.CampaignId = response.CampaignId;
                            cr.CampaignName = response.CampaignName;

                            UpdateCampaignList(cr, advertiser);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Error.LogError(ex, "RecheckCampaignBasedOnReport:" + advertiser.AdvertiserName ?? "", false);
            }
        }
        private CampaignResponseData GetCampaigns(string _campaignId)
        {
            using (AuthenticationServiceHandler authenticationUtil = new AuthenticationServiceHandler())
            {
                try
                {
                    string _url = urlForCampaign + _campaignId;

                    var request = (HttpWebRequest)WebRequest.Create(_url);
                    request.Headers.Add("TTD-Auth", authenticationUtil.UserSecurityToken);

                    request.ContentType = "application/json";
                    request.Method = "GET";

                    var response = (HttpWebResponse)request.GetResponse();
                    DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(CampaignResponseData));
                    object objResponse = jsonSerializer.ReadObject(response.GetResponseStream());

                    CampaignResponseData jsonResponse = objResponse as CampaignResponseData;

                    return jsonResponse;
                }
                catch (Exception ex)
                {
                    SolutionHelper.Error.LogError(ex, "GetCampaignId: " + _campaignId);
                    return null;
                }
            }
        }
        private CampaignResponse GetCampaigns(dataTradeDeskAdvertiser advertiser)
        {
            CampaignResponse _response = null;
            List<CampaignResponseData> list = new List<CampaignResponseData>();

            using (AuthenticationServiceHandler authenticationUtil = new AuthenticationServiceHandler())
            {
                try
                {
                    var request = (HttpWebRequest)WebRequest.Create(url);
                    request.Headers.Add("TTD-Auth", authenticationUtil.UserSecurityToken);

                    request.ContentType = "application/json; charset=utf-8";
                    request.Method = "POST";

                    using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                    {
                        string json = new JavaScriptSerializer().Serialize(new
                        {
                            AdvertiserId = advertiser.AdvertiserId,
                            PageStartIndex = 1,
                            PageSize = 100000
                        });

                        streamWriter.Write(json);
                    }

                    var response = (HttpWebResponse)request.GetResponse();

                    using (Stream responseStream = response.GetResponseStream())
                    {
                        _response = new CampaignResponse();

                        StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);


                        string json = reader.ReadToEnd() ?? "";
                        dynamic data = Json.Decode(json);

                        _response.ResultCount = data.ResultCount.ToString();

                        foreach (dynamic obj in data.Result)
                        {
                            var _json = Json.Encode(obj);

                            CampaignResponseData item = new CampaignResponseData();
                            item.CampaignDynamicObject = obj;
                            item.CampaignObject = _json;
                            item.CampaignName = obj.CampaignName;
                            item.CampaignId = obj.CampaignId;
                            item.AdvertiserId = obj.AdvertiserId;

                            list.Add(item);
                        }

                        _response.Result = list.ToArray();
                    }

                    return _response;
                }
                catch (Exception ex)
                {
                    SolutionHelper.Error.LogError(ex, "GetCampaigns:" + advertiser.AdvertiserId);
                    return null;
                }
            }
        }
        private void ProcessCampaignFlights(CampaignResponseData campaign)
        {
            int idx = 0;
            var selectedDb = new mediaTradeDeskEntities1();

            try
            {
                selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

                CampaignFlightCollection _flights = new CampaignFlightCollection(campaign.CampaignDynamicObject.CampaignFlights);

                if (_flights.CampaignFlights.Count() == 0)
                    return;

                foreach (CampaignFlight _flight in _flights.CampaignFlights)
                {

                    dataTradeDeskCampaignFlight _newFlight = (from n in selectedDb.dataTradeDeskCampaignFlights where n.CampaignId == _flight.CampaignId && n.CampaignFlightId == _flight.CampaignFlightId select n).FirstOrDefault();

                    if (_newFlight == null)
                    {
                        dataTradeDeskCampaignFlight __newFlight = new dataTradeDeskCampaignFlight();
                        __newFlight.CampaignId = _flight.CampaignId;
                        __newFlight.CampaignFlightId = _flight.CampaignFlightId;
                        __newFlight.BudgetInAdvertiserCurrency = _flight.BudgetInAdvertiserCurrency;
                        __newFlight.DailyTargetInAdvertiserCurrency = _flight.DailyTargetInAdvertiserCurrency;
                        __newFlight.EndDateExclusiveUTC = _flight.EndDateExclusiveUTC;
                        __newFlight.StartDateInclusiveUTC = _flight.StartDateInclusiveUTC;

                        if (_flight.BudgetInImpressions != null)
                            __newFlight.BudgetInImpressions = int.Parse(_flight.BudgetInImpressions);
                        if (_flight.DailyTargetInImpressions != null)
                            __newFlight.DailyTargetInImpressions = int.Parse(_flight.DailyTargetInImpressions);

                        __newFlight.ModifiedDate = System.DateTime.Now;

                        selectedDb.dataTradeDeskCampaignFlights.Add(__newFlight);
                    }
                    else
                    {
                        _newFlight.CampaignId = _flight.CampaignId;
                        _newFlight.CampaignFlightId = _flight.CampaignFlightId;
                        _newFlight.BudgetInAdvertiserCurrency = _flight.BudgetInAdvertiserCurrency;
                        _newFlight.DailyTargetInAdvertiserCurrency = _flight.DailyTargetInAdvertiserCurrency;
                        _newFlight.EndDateExclusiveUTC = _flight.EndDateExclusiveUTC;
                        _newFlight.StartDateInclusiveUTC = _flight.StartDateInclusiveUTC;


                        if (_flight.BudgetInImpressions != null)
                            _newFlight.BudgetInImpressions = int.Parse(_flight.BudgetInImpressions);
                        if (_flight.DailyTargetInImpressions != null)
                            _newFlight.DailyTargetInImpressions = int.Parse(_flight.DailyTargetInImpressions);

                        _newFlight.ModifiedDate = System.DateTime.Now;
                    }

                    if (idx == 25) // Batch trips to the database
                    {
                        selectedDb.SaveChanges();
                        idx = 0;
                    }

                    idx++;

                    Console.WriteLine(String.Format("{0} Campaign Flights Processed", company.CompanyName));
                }

                selectedDb.SaveChanges();
            }
            catch (Exception ex)
            {
                Error.LogError(ex, "ProcessAdGroupFlights:" + company.CompanyName ?? "");
            }
        }
        private void UpdateCampaignList(CampaignResponseData response, dataTradeDeskAdvertiser advertiser)
        {
            try
            {
                var selectedDb = new mediaTradeDeskEntities1();

                GetCompany(advertiser.CompanyId);

                if (company != null)
                {
                    selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

                    dataTradeDeskCampaign campaign = (from m in selectedDb.dataTradeDeskCampaigns where m.Campaign_Id == response.CampaignId select m).FirstOrDefault();

                    if (campaign == null)
                    {
                        dataTradeDeskCampaign cam = new dataTradeDeskCampaign();
                        cam.Campaign_Id = response.CampaignId;
                        cam.Campaign_Name = response.CampaignName;

                        selectedDb.dataTradeDeskCampaigns.Add(cam);
                    }
                    else
                    {
                        campaign.Campaign_Id = response.CampaignId;
                        campaign.Campaign_Name = response.CampaignName;
                    }

                    selectedDb.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Error.LogError(ex, "UpdateCampaignList2:" + advertiser.AdvertiserName);
            }
        }
        private void GetCompany(int? companyId)
        {
            Reset();

            if (companyId == 0)
                return;
            if (sandbox)
                company = (from v in db.Companies where v.DatabaseName == "mediaTestDatabase" select v).FirstOrDefault();
            else
                company = (from v in db.Companies where v.Id == companyId && v.IsActive == true select v).FirstOrDefault();

            processor = new Data(company.DatabaseName, company.Login, company.Password, company.Server);
        }
        private void Reset()
        {
            company = null;
            processor = null;
        }
        private void UpdateCampaignList(CampaignResponse response, dataTradeDeskAdvertiser advertiser)
        {
            try
            {
                var selectedDb = new mediaTradeDeskEntities1();

                GetCompany(advertiser.CompanyId);

                if (company != null)
                {
                    selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

                    int idx = 0;

                    for (int i = 0; i <= response.Result.Count() - 1; i++)
                    {
                        CampaignResponseData row = response.Result[i];

                        dataTradeDeskCampaign campaign = (from m in selectedDb.dataTradeDeskCampaigns where m.Campaign_Id == row.CampaignId select m).FirstOrDefault();

                        if (debug)
                        {
                            if (advertiser.CompanyId != 6)
                                continue;

                            ProcessCampaignFlights(row);
                        }

                        if (campaign == null)
                        {
                            dataTradeDeskCampaign cam = new dataTradeDeskCampaign();
                            cam.Campaign_Id = row.CampaignId;
                            cam.Campaign_Name = row.CampaignName;
                            cam.Campaign_Object = row.CampaignObject;
                            cam.Advertiser_Id = row.AdvertiserId;
                            cam.Campaign_Budget = row.CampaignDynamicObject.Budget.Amount ?? "";

                            selectedDb.dataTradeDeskCampaigns.Add(cam);
                        }
                        else
                        {
                            campaign.Campaign_Id = row.CampaignId;
                            campaign.Campaign_Name = row.CampaignName;
                            campaign.Campaign_Object = row.CampaignObject;
                            campaign.Advertiser_Id = row.AdvertiserId;
                            campaign.ModifiedDate = System.DateTime.Now;
                            campaign.Campaign_Budget = row.CampaignDynamicObject.Budget.Amount ?? "";
                        }

                        if (idx == 25) // Batch trips to the database
                        {
                            selectedDb.SaveChanges();
                            idx = 0;
                        }

                        idx++;

                        Console.WriteLine(String.Format("Processing Campaign: {0} - {1}: {2} of {3}", advertiser.AdvertiserName, row.CampaignName, idx, response.Result.Count()));
                    }

                    selectedDb.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Error.LogError(ex, "UpdateCampaignList:" + advertiser.AdvertiserName);
            }
        }
        #endregion

        #region Public
        public CampaignServiceHandler()
        {
            bool sandbox = bool.Parse(ConfigurationManager.AppSettings["sandbox"].ToString() ?? "false");

            url = ConfigurationManager.AppSettings["APIURL"] + "campaign/query/advertiser/";
            urlForCampaign = ConfigurationManager.AppSettings["APIURL"] + "campaign/";
            urlForCampaignFlight = ConfigurationManager.AppSettings["APIURL"] + "campaignflight/";

            if (sandbox)
            {
                url = url.Replace("https://api.thetradedesk.com/v3/", "https://apisb.thetradedesk.com/v3/");
                urlForCampaign = urlForCampaign.Replace("https://api.thetradedesk.com/v3/", "https://apisb.thetradedesk.com/v3/");
                urlForCampaignFlight = urlForCampaignFlight.Replace("https://api.thetradedesk.com/v3/", "https://apisb.thetradedesk.com/v3/");

                db.Database.Connection.ConnectionString = new Data("mediaTestDatabase", "mediaTestDatabaseUser", "mediaTestDatabase@!@", "172.16.8.201").ConnectionString; ;
            }
        }
        public void Run()
        {
            try
            {
                List<dataTradeDeskAdvertiser> advertisers = (from m in db.dataTradeDeskAdvertisers where m.CompanyId != null && m.Active == true select m).ToList();

                foreach (dataTradeDeskAdvertiser advertiser in advertisers)
                {
                    CampaignResponse response = GetCampaigns(advertiser);

                    if (response != null && response.ResultCount != "0")
                    {
                        UpdateCampaignList(response, advertiser);
                    }


                    if (debug)
                        if (sandbox)
                            CreateUpdateCampaign(advertiser);

                    RecheckCampaignBasedOnReport(advertiser);
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "CampaignServiceHandler.Run:");
            }
        }
        public void RunAdvertisers()
        {
            try
            {
                List<dataTradeDeskAdvertiser> advertisers = (from m in db.dataTradeDeskAdvertisers where m.CompanyId != null && m.Active == true select m).ToList();

                foreach (dataTradeDeskAdvertiser advertiser in advertisers)
                {
                    CampaignResponse response = GetCampaigns(advertiser);

                    if (response != null && response.ResultCount != "0")
                    {
                        UpdateCampaignList(response, advertiser);
                    }

                    RecheckCampaignBasedOnReport(advertiser);
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "CampaignServiceHandler.RunAdvertisers.Run:");
            }
        }
        public void CreateUpdateCampaign(dataTradeDeskAdvertiser advertiser, dataTradeDeskCampaign campaign = null)
        {
            using (AuthenticationServiceHandler authenticationUtil = new AuthenticationServiceHandler())
            {
                try
                {
                    if (campaign == null)
                        campaign = new dataTradeDeskCampaign { Campaign_Name = "TestCampaign", Campaign_Id = "" };

                    CreateCampaign(authenticationUtil, advertiser, campaign);
                }
                catch (Exception ex)
                {
                    SolutionHelper.Error.LogError(ex, "CreateUpdateCampaign:");
                }
            }
        }

        private bool CreateCampaign(AuthenticationServiceHandler authenticationUtil, dataTradeDeskAdvertiser advertiser, dataTradeDeskCampaign campaign)
        {
            CampaignResponse _response = null;

            string _method = "GET";

            if (campaign.Campaign_Id == "")
                _method = "POST";

            try
            {
                var request = (HttpWebRequest)WebRequest.Create(urlForCampaign);

                request.Headers.Add("TTD-Auth", authenticationUtil.UserSecurityToken);

                request.ContentType = "application/json; charset=utf-8";
                request.Method = _method;

                _CampaignConversionReportingColumns cols = new _CampaignConversionReportingColumns { TrackingTagId = "rgzprgcz", TrackingTagName = "Payment Success - Static", ReportingColumnId = 1 };

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(new
                    {
                        AdvertiserId = advertiser.AdvertiserId,
                        CampaignId = campaign.Campaign_Id,
                        CampaignName = campaign.Campaign_Name,
                        CampaignConversionReportingColumns = cols
                    });

                    streamWriter.Write(json);
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                using (Stream responseStream = response.GetResponseStream())
                {
                    _response = new CampaignResponse();

                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);

                    string json = reader.ReadToEnd() ?? "";
                    dynamic data = Json.Decode(json);

                    //_response.AdvertiserDynamicObject = data;
                    //_response.AdvertiserObject = json;
                }

                return true;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "CreateCampaign:");
            }

            return false;
        }
        public class _CampaignConversionReportingColumns
        {
            public string TrackingTagId { get; set; }
            public string TrackingTagName { get; set; }
            public int ReportingColumnId { get; set; }
            public int CrossDeviceAttributionModelId { get; set; }
        }
        #endregion
    }
}
