﻿using SolutionHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using WhirlygigSolutionHelper;

namespace TheTradeDeskReporting 
{
    public class CreativeServiceHandler
    {
        mediaTradeDeskEntities1 db = new mediaTradeDeskEntities1();
        private bool debug = bool.Parse(ConfigurationManager.AppSettings["debug"].ToString());
        private string url = ConfigurationManager.AppSettings["APIURL"] + "creative/query/advertiser/";
        private string urlForCreative = ConfigurationManager.AppSettings["APIURL"] + "creative/";

        public CreativeServiceHandler()
        {
            bool sandbox = bool.Parse(ConfigurationManager.AppSettings["sandbox"].ToString() ?? "false");

            if (sandbox)
            {
                url = @"https://apisb.thetradedesk.com/v3/creative/query/advertiser/";
                urlForCreative = @"https://apisb.thetradedesk.com/v3/creative/";
            }
        }

        public void Run()
        {
            try
            {
                List<dataTradeDeskAdvertiser> advertisers = (from m in db.dataTradeDeskAdvertisers where m.CompanyId != null && m.Active == true select m).ToList().OrderByDescending(n => n.AdvertiserName).ToList();

                foreach (dataTradeDeskAdvertiser advertiser in advertisers)
                {
                    if (debug)
                    {
                        string dothis;

                        //if (advertiser.CompanyId == 14)
                        //    dothis = "";
                        //else
                        //{
                        //    if (debug)
                        //        continue; 
                        //}
                    }

                    CreativeResponse response = GetCreatives(advertiser);


                    if (response != null)
                    {
                        UpdateCreativeList(response,advertiser);
                    }

                    RecheckCreativesBasedOnReport(advertiser);
                } 
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "CreativeServiceHandler.Run:"); 
            }
        }

        /// <summary>
        /// Occassionally there will be some creative Ids that appear in the custom report but will excluded from the api GetCreatives
        /// </summary>
        /// <param name="advertiser"></param>
        private void RecheckCreativesBasedOnReport(dataTradeDeskAdvertiser advertiser)
        {
            try
            {
                var selectedDb = new mediaTradeDeskEntities1();

                Company com = (from m in db.Companies where m.Id == advertiser.CompanyId && m.IsActive == true select m).FirstOrDefault();

                if (com == null)
                    return;

                int idx = 0;

                Data processor = new Data(com.DatabaseName, com.Login, com.Password, com.Server);
                selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

                string[] creativeIds = (from n in selectedDb.dataTradeDeskCreatives select n.Creative_Id).ToArray(); // Get list of creative ids 
                string[] creativeIdsReporting = (from m in selectedDb.dataTradeDeskReportingDatas where m.Advertiser_Id == advertiser.AdvertiserId select m.Creative_Id).Distinct().ToArray(); // Get list of distinct creative Id's from reporting data
                string[] results = (from _str in creativeIdsReporting where !creativeIds.Contains(_str) select _str).ToArray();

                foreach(string _creativeId in results)
                {
                    if (_creativeId == "[Unknown]")
                        continue;

                    CreativeResponse response = GetCreatives(_creativeId);

                    if (response != null)
                    {
                        CreativeResponseData cr = new CreativeResponseData();
                        cr.CreativeId = response.CreativeId;
                        cr.CreativeName = response.CreativeName;

                        CreativeResponseData[] Result = new CreativeResponseData[1];
                        Result[0] = cr;
                        response.Result = Result;

                        UpdateCreativeList(response, advertiser);

                        Console.WriteLine(response.CreativeName);
                    }
                } 
            }
            catch (Exception ex)
            {
                Error.LogError(ex, "RecheckCreativesBasedOnReport:" + advertiser.AdvertiserName ?? "");
            }
        }

        private CreativeResponse GetCreatives(dataTradeDeskAdvertiser advertiser)
        {
            using (AuthenticationServiceHandler authenticationUtil = new AuthenticationServiceHandler())
            {
                try
                {
                    var request = (HttpWebRequest)WebRequest.Create(url);
                    request.Headers.Add("TTD-Auth", authenticationUtil.UserSecurityToken);

                    request.ContentType = "application/json; charset=utf-8";
                    request.Method = "POST";

                    using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                    {
                        string json = new JavaScriptSerializer().Serialize(new
                        {
                            AdvertiserId = advertiser.AdvertiserId,
                            PageStartIndex = 1,
                            PageSize = 100000
                        });

                        streamWriter.Write(json);
                    }

                    var response = (HttpWebResponse)request.GetResponse();
                    DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(CreativeResponse));
                    object objResponse = jsonSerializer.ReadObject(response.GetResponseStream());

                    CreativeResponse jsonResponse = objResponse as CreativeResponse;

                    return jsonResponse;
                }
                catch (Exception ex)
                {
                    SolutionHelper.Error.LogError(ex, "GetCreatives:" + advertiser.AdvertiserId);
                    return null;
                }
            }
        }

        private CreativeResponse GetCreatives(string creativeid)
        {
            using (AuthenticationServiceHandler authenticationUtil = new AuthenticationServiceHandler())
            {
                try
                {
                    string _url = urlForCreative + creativeid;

                    var request = (HttpWebRequest)WebRequest.Create(_url);
                    request.Headers.Add("TTD-Auth", authenticationUtil.UserSecurityToken);

                    request.ContentType = "application/json";
                    request.Method = "GET";

                    var response = (HttpWebResponse)request.GetResponse();
                    DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(CreativeResponse));
                    object objResponse = jsonSerializer.ReadObject(response.GetResponseStream());

                    CreativeResponse jsonResponse = objResponse as CreativeResponse;

                    return jsonResponse;
                }
                catch (Exception ex)
                {
                  // //Invalid CreativeId will can return 400 error code.
                  //  SolutionHelper.Error.LogError(ex, "GetCreatives 2:" + creativeid);
                    return null;
                }
            }
        }

        private void UpdateCreativeList(CreativeResponse response, dataTradeDeskAdvertiser advertiser)
        {
            try
            {  
                var selectedDb = new mediaTradeDeskEntities1();

                Company com = (from m in db.Companies where m.Id == advertiser.CompanyId && m.IsActive == true select m).FirstOrDefault();

                if (com == null)
                    return;

                int idx = 0;

                Data processor = new Data(com.DatabaseName, com.Login, com.Password,com.Server);
                selectedDb.Database.Connection.ConnectionString = processor.ConnectionString;

                for (int i = 0; i <= response.Result.Count() - 1; i++)
                {
                    CreativeResponseData row = response.Result[i];
                     
                    dataTradeDeskCreative creative = (from m in selectedDb.dataTradeDeskCreatives where m.Creative_Id == row.CreativeId && m.Creative_Name == row.CreativeName select m).FirstOrDefault();

                    if (creative == null)
                    {
                        dataTradeDeskCreative create = new dataTradeDeskCreative();
                        create.Creative_Id = row.CreativeId;
                        create.Creative_Name = row.CreativeName;

                        selectedDb.dataTradeDeskCreatives.Add(create);
                    } 

                    if (idx == 25) // Batch trips to the database
                    {
                        selectedDb.SaveChanges();
                        idx = 0;
                    }

                    Console.WriteLine(row.CreativeName);

                    idx++;
                }

                selectedDb.SaveChanges(); 
            }
            catch (Exception ex)
            {
                Error.LogError(ex, "UpdateCreativeList:" + advertiser.AdvertiserName ?? "");
            }
        }
    }
}
