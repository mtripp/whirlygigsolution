﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml;
using SolutionHelper;
using System.Data.SqlClient;
using WhirlygigSolutionHelper;

namespace TheTradeDeskReporting
{
    public class HDReportsServiceHandler
    {
        mediaTradeDeskEntities1 db = new mediaTradeDeskEntities1();

        private string url = ConfigurationManager.AppSettings["APIURL"] + "hdreports/";
        private string csvExportCompleted = ConfigurationManager.AppSettings["csvExportCompleted"];
        private string csvExportFailed = ConfigurationManager.AppSettings["csvExportFailed"];
        private string csvExportWaiting = ConfigurationManager.AppSettings["csvExportWaiting"];
        private string csvExport = ConfigurationManager.AppSettings["csvExport"];
        private bool debug = bool.Parse(ConfigurationManager.AppSettings["debug"].ToString());

        Data Processor = new Data();
        Data LavidgeProcessor = new Data();

        Company company;
        dataTradeDeskAdvertiser currentAdvertiser = new dataTradeDeskAdvertiser();
        dataTradeDeskAdvertiser[] advertisers;

        public HDReportsServiceHandler()
        {
            Company _com = (from m in db.Companies where m.Id == 1 select m).FirstOrDefault();
            LavidgeProcessor = new Data(_com.DatabaseName, _com.Login, _com.Password, _com.Server);
        }

        public void Run()
        {
            int idx = 0;

            advertisers = (from m in db.dataTradeDeskAdvertisers where m.CompanyId != null && m.Active == true select m).Where(x => x.bolConversionReport == true && x.Conversion_Success_tags != null && x.Action_Tags != null).OrderByDescending(m => m.AdvertiserName).ToArray();

            foreach (dataTradeDeskAdvertiser advertiser in advertisers)
            {
                Reset();

                currentAdvertiser = advertiser;

                if (advertiser.AdvertiserName.ToLower().Contains("deleted"))
                    continue;

                company = GetCompany(advertiser);

                if (company != null)
                {
                    if (debug)
                    {
                        if (company.Id != 10021)    //  if (company.Id != 6 && company.Id != 4)                  //  testing only
                            continue;
                    }

                    Console.WriteLine("Current Advertiser: " + advertiser.AdvertiserName + " - " + advertiser.AdvertiserId + " - " + idx.ToString() + " - " + advertisers.Count().ToString());
                    GetHDReport(advertiser);
                }
                idx++;
            }
        }

        private void Reset()
        {
            currentAdvertiser = null;
            company = null;
            Processor = null;
        }

        /// <summary>
        /// This was design to make one api call and bring back the reports for multiple tables
        /// </summary>
        /// <param name="advertiser"></param>
        private void GetHDReport(dataTradeDeskAdvertiser advertiser)
        {
            DateTime now = System.DateTime.Now.AddDays(-1);
            DateTime date = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);

            DateTime[] reports = null;
            DateTime[] reportsElement = null;
            DateTime[] reportsConversions = null;

            if (advertiser != null)
                reports = GetReportDates(advertiser, "dataTradeDeskReportingData");
            if (advertiser.bolDataElements ?? false)
                reportsElement = GetReportDates(advertiser, "dataTradeDeskDataElementReportingData");
            if (advertiser.bolConversionReport ?? false)
                reportsConversions = GetReportDates(advertiser, "dataTradeDeskConversionReportData");

            DateTime StartDate = date.AddDays(-120); //DateTime.Parse("09/01/2017"); //
            DateTime EndDate = date; // DateTime.Parse("9/28/2016");   
            DateTime CutOffDate = DateTime.Parse("03/01/2019");


            if (debug)
                if (StartDate != StartDate) // This is meant to be a manual process for debugging
                    StartDate = DateTime.Parse("8/29/2018");

            using (AuthenticationServiceHandler authenticationUtil = new AuthenticationServiceHandler())
            {

                if (authenticationUtil.UserSecurityToken == null)
                    return;

                for (date = StartDate; date.Date <= EndDate.Date; date = date.AddDays(1))  /*    LOOP THROUGH LOGIC */
                {
                    Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();

                    if (debug)
                        if (date > CutOffDate)
                            EndDate = EndDate; //  testing only     continue;   //   

                    try
                    {
                        if (advertiser.bolDataElements == true && advertiser.bolConversionReport == true)  // If advertiser is supposed to have all three data sources check and make sure there is data for the date in all three tables
                        {
                            if (reports.Contains(date) && reportsElement.Contains(date) && reportsConversions.Contains(date))
                                continue;
                        }
                        else if (advertiser.bolDataElements == true)  // If advertiser is supposed to have both data sources check and make sure there is data for the date in both tables
                        {
                            if (reports.Contains(date) && reportsElement.Contains(date))
                                continue;
                        }
                        else if (advertiser.bolConversionReport == true)  // If advertiser is supposed to have both data sources check and make sure there is data for the date in both tables
                        {
                            if (reports.Contains(date) && reportsConversions.Contains(date))
                                continue;
                        }
                        else  // Check if advertiser has data for the date
                        {
                            if (reports.Contains(date))
                                continue;
                        }

                        HDReportResponse jsonResponse = GetResponseFromApi(advertiser, date, authenticationUtil);

                        if (jsonResponse.Result.Count() != 0)
                            ProcessHdReport(advertiser, jsonResponse, date, reports, reportsElement, reportsConversions, authenticationUtil);

                        Console.WriteLine(jsonResponse.Result.Count());
                    }
                    catch (Exception ex)
                    {
                        SolutionHelper.Error.LogError(ex, "GetHDReport:" + advertiser.AdvertiserId + " - " + date.ToString());
                    } 
                }   /* LOOP THROUGH LOGIC  */
            }
        }

        /// <summary>
        /// Get response from the api using the advertiser and date
        /// </summary>
        /// <param name="advertiser"></param>
        /// <param name="date"></param>
        /// <param name="authenticationUtil"></param>
        /// <returns></returns>
        private HDReportResponse GetResponseFromApi(dataTradeDeskAdvertiser advertiser, DateTime date, AuthenticationServiceHandler authenticationUtil)
        {
            Console.WriteLine(String.Format("{1} Processing {0}", date, advertiser.AdvertiserName));

            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Headers.Add("TTD-Auth", authenticationUtil.UserSecurityToken);

            request.ContentType = "application/json; charset=utf-8";
            request.Method = "POST";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(new
                {
                    AdvertiserId = advertiser.AdvertiserId,
                    ReportDateUTC = date.ToString()
                });

                streamWriter.Write(json);
            }

            var response = (HttpWebResponse)request.GetResponse();
            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(HDReportResponse));
            object objResponse = jsonSerializer.ReadObject(response.GetResponseStream());

            HDReportResponse jsonResponse = objResponse as HDReportResponse;
            return jsonResponse;
        }

        private DateTime[] GetReportDates(dataTradeDeskAdvertiser advertiser, string tableName)
        {
            List<DateTime> dates = new List<DateTime>();

            string query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[{0}] where [Advertiser Id] = '{1}'", tableName, advertiser.AdvertiserId.ToString());

            DataSet ds = Processor.RunQuery(query);

            if (ds != null) // Add date to list of date
                if (ds.Tables.Count != 0) // Add date to list of date
                    foreach (DataRow dr in ds.Tables[0].Rows)
                        dates.Add(DateTime.Parse(dr[0].ToString())); // Date is a non nullable date type

            return dates.ToArray();
        }

        private void ProcessHdReport(dataTradeDeskAdvertiser advertiser, HDReportResponse jsonResponse, DateTime date, DateTime[] reports, DateTime[] reportsElement, DateTime[] reportsConversions, AuthenticationServiceHandler auth)
        {
            bool dataPerformanceReport = true; // These are meant to not try and reprocess the same import multiple times
            bool dataElementReport = true;
            bool dataConversionReport = true;
            bool troubleshoot = false;

            foreach (HDReportResponseData HDReport in jsonResponse.Result)
            {
                if (debug)
                {
                    if (troubleshoot)
                    {
                        Console.WriteLine(HDReport.Type + " - " + HDReport.Duration);

                        if (HDReport.Type == "ConversionReport" && HDReport.Duration == "SevenDays" && dataConversionReport) //            if (HDReport.Type == "TimeOfDayReport" && HDReport.Duration == "OneDay" )// && HDReport.Scope == "Partner") //
                            ExportHdReportDownload(advertiser, auth, date, HDReport);

                        continue;
                    }
                }

                if (HDReport.Type == "AdGroupPerformanceReport" && HDReport.Duration == "OneDay" && HDReport.Scope == "Partner" && dataPerformanceReport)
                {
                    if (!reports.Contains(date))
                        if (!String.IsNullOrEmpty(HDReport.DownloadUrl))
                        {
                            ProcessHdReportDownload(date, "dataTradeDeskReportingData", HDReport.DownloadUrl);
                            dataPerformanceReport = false;
                        }
                }

                if (advertiser.bolDataElements ?? false)
                {
                    if (HDReport.Type == "DataElementReport" && HDReport.Duration == "OneDay" && HDReport.Scope == "Partner" && dataElementReport)
                    {
                        if (!reportsElement.Contains(date))
                            if (!String.IsNullOrEmpty(HDReport.DownloadUrl))
                            {
                                ProcessHdReportDownload(date, "dataTradeDeskDataElementReportingData", HDReport.DownloadUrl);
                                dataElementReport = false;
                            }
                    }
                }

                if (advertiser.bolConversionReport ?? false)
                {
                    if (HDReport.Type == "ConversionReport")
                    {
                        if (HDReport.Duration == "OneDay")
                        {
                            if (HDReport.Scope == "Advertiser" && dataConversionReport)
                            {
                                if (!reportsConversions.Contains(date))
                                { 
                                    if (!String.IsNullOrEmpty(advertiser.Action_Tags) && !String.IsNullOrEmpty(advertiser.Conversion_Success_tags))
                                    {
                                        if (!String.IsNullOrEmpty(HDReport.DownloadUrl))
                                        {
                                            ProcessHdReportDownload(date, "dataTradeDeskConversionReportData", HDReport.DownloadUrl, true);
                                            dataConversionReport = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Console.WriteLine("ProcessHdReport: Completed - " + advertiser.AdvertiserName);
        }
        private void ProcessHdReportDownload(DateTime date, string tablename, string downloadurl, bool dynamic = false)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(downloadurl);
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

            Stream stream = resp.GetResponseStream();
            DataTable dt = stream.ToDataTableTsv();

            dt.TableName = date.ToShortDateString();

            if (!dynamic)
                DataTableToDatabase(tablename, dt);
            else
                DataTableToDatabaseDynamically(tablename, dt);
        }
        private void ExportHdReportDownload(dataTradeDeskAdvertiser advertiser, AuthenticationServiceHandler auth, DateTime date, HDReportResponseData report)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(report.DownloadUrl);
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

            Stream stream = resp.GetResponseStream();
            DataTable dt = stream.ToDataTableTsv();

            dt.TableName = date.ToShortDateString();

            string file = dt.ToDataSetExportTsv();

            File.WriteAllText(csvExport + @"\Tradedesk Trouble Shoot Report - " + report.Type + "_" + report.Scope + "_" + report.Duration + ".tsv", file.ToString());// + strStream.Name, file.ToString()); 
        }

        private void DataTableToDatabaseDynamically(string tablename, DataTable dt)
        {
            DynamicColumnChecker(tablename, dt);
            DynamicCleanData(dt);
            ProcessToDatabase(tablename, dt);
        }

        private void ProcessToDatabase(string tablename, DataTable dt)
        {
            try
            {
                SqlConnection conn = new SqlConnection(Processor.ConnectionString);

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);

                    bulkCopy.BatchSize = 10000;
                    bulkCopy.BulkCopyTimeout = 500;

                    foreach (DataColumn dc in dt.Columns)
                    {
                        SqlBulkCopyColumnMapping mapping = new SqlBulkCopyColumnMapping(dc.ColumnName, dc.ColumnName);

                        bulkCopy.ColumnMappings.Add(mapping);
                    }

                    bulkCopy.DestinationTableName = tablename;

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 10000;
                    conn.Open();

                    bulkCopy.WriteToServer(dt);

                    ProcessDetails(dt, tablename);
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "HDReportsServiceHandler.ProcessToDatabase:" + Processor.DatabaseName);
            }
        }

        private void ProcessDetails(DataTable dt, string tablename)
        {

            DateTime modifiedDate = DateTime.Parse(dt.Rows[0]["ModifiedDate"].ToString());

            try
            {
                string query = String.Format("EXEC spProcessConversionDetailsReport '{0}','{1}'", modifiedDate, currentAdvertiser.AdvertiserId);
                DataSet ds = Processor.RunQuery(query);

                if (ds.Tables[0].Columns.Contains("AsOfDate"))
                    query = "Success";
                else if (ds.Tables[0].Columns.Contains("SQL ERROR"))
                    throw new Exception("Error in processing Conversion Details.");
            }
            catch (Exception ex)
            {
                string query = String.Format("Delete From datatradedeskconversionreportdata where ModifiedDate = '{0}' and [advertiser id] = '{1}'", modifiedDate, currentAdvertiser.AdvertiserId);
                DataSet ds = Processor.RunQuery(query);

                throw ex;
            }
        }

        private void DynamicColumnChecker(string tablename, DataTable dt)
        {
            DateTime today = System.DateTime.Now;

            string[] columnCol = GetColumnsFromDatabase(tablename);

            foreach (DataColumn dc in dt.Columns)
                dc.ColumnName = dc.ColumnName.ToProperCase();


            foreach (string s in removeColumns)
            {
                if (dt.Columns.Contains(s.ToProperCase()))
                {
                    dt.Columns.Remove(s);
                    dt.AcceptChanges();
                }
            }

            foreach (DataColumn dc in dt.Columns)
            {
                if (dc.ColumnName.Contains("_"))
                    dc.ColumnName = dc.ColumnName.Replace("_", " ");
                if (dc.ColumnName.Contains("* "))
                    dc.ColumnName = dc.ColumnName.Replace("* ", "");


                if (dc.ColumnName.ToLower() == "date" || dc.ColumnName.ToLower() == "asofdate")
                {
                    dc.ColumnName = "AsOfDate";
                    continue;
                }

                if (dc.ColumnName.ToLower() == "modifieddate")
                {
                    dc.ColumnName = "ModifiedDate";
                    continue;
                }

                if (!columnCol.Contains(dc.ColumnName.ToLower().Trim()))
                    DynamicAddColumn(dc.ColumnName.ToString(), tablename);
            }

            if (!dt.Columns.Contains("ModifiedDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                newColumn.DefaultValue = today.ToString();
                dt.Columns.Add(newColumn);
            }

            if (!dt.Columns.Contains("AsOfDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("AsOfDate", typeof(System.DateTime));
                newColumn.DefaultValue = dt.TableName.ToString();
                dt.Columns.Add(newColumn);
            }
        }

        /// <summary>
        /// Due to the columns needing to be dynamic this method is designed to add additional column to the table
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        private void DynamicAddColumn(string column, string tablename)
        {
            column = column.ToProperCase();

            string query = "ALTER TABLE {0} ADD [{1}] {2}";

            if (column.ToUpper().ContainsAny("# Impressions".ToUpper(),
                "# Display Clicks".ToUpper(),
                "# Non-display Clicks".ToUpper(),
                "# Ad Groups w/ Activity".ToUpper(),
                "# Frequency Cycles and Ad Groups w/ Activity".ToUpper(),
                "# Frequency Cycles and Ad Groups Where FCap is Reached".ToUpper(),
                "Monetary Value".ToUpper(),
                "Last Impression Frequency".ToUpper()))
                query = string.Format(query, tablename, column, "[int] NULL");
            else if (column.ToUpper().ContainsAny("TIME"))
                query = string.Format(query, tablename, column, "datetime NULL");
            else if (column.ToUpper().ContainsAny("Referrer Url".ToUpper(), "Xdids".ToUpper()))
                query = string.Format(query, tablename, column, "[varchar](2000)");
            else if (column.ToUpper().ContainsAny("Conversion Time".ToUpper(),
                "Conversion Type".ToUpper(),
                "Tracking Tag Name".ToUpper(),
                "Monetary Value Currency".ToUpper(),
                "First Impression Campaign Id".ToUpper(),
                "First Impression Ad Group Id".ToUpper(),
                "First Impression Creative Id".ToUpper(),
                "First Impression Format".ToUpper(),
                "Last Impression Campaign Id".ToUpper(),
                "Last Impression Ad Group Id".ToUpper(),
                "Last Impression Creative Id".ToUpper(),
                "Last Impression Format".ToUpper(),
                "Last Impression Country".ToUpper(),
                "Last Impression Region".ToUpper(),
                "Last Impression Metro".ToUpper(),
                "First Display Click Campaign Id".ToUpper(),
                "First Display Click Ad Group Id".ToUpper(),
                "First Display Click Creative Id".ToUpper(),
                "First Display Click Format".ToUpper(),
                "Last Display Click Campaign Id".ToUpper(),
                "Last Display Click Ad Group Id".ToUpper(),
                "Last Display Click Creative Id".ToUpper(),
                "Last Display Click Format".ToUpper(),
                "Partner Id".ToUpper(),
                "Advertiser Id".ToUpper(),
                "Attribution Model".ToUpper(),
                "Conversion Device Type".ToUpper(),
                "First Impression Device Type".ToUpper(),
                "First Impression Ad Environment".ToUpper(),
                "Last Impression Device Type".ToUpper(),
                "Last Impression Ad Environment".ToUpper(),
                "First Display Click Device Type".ToUpper(),
                "First Display Click Ad Environment".ToUpper(),
                "Last Display Click Device Type".ToUpper(),
                "Last Display Click Ad Environment".ToUpper(),
                "First Non-display Click Device Type".ToUpper(),
                "First Non-display Click Ad Environment".ToUpper(),
                "Last Non-display Click Device Type".ToUpper(),
                "Last Non-display Click Ad Environment".ToUpper()
                 ))
                query = string.Format(query, tablename, column, "[varchar](50)");
            else
                query = string.Format(query, tablename, column, "[varchar](250)");

            Processor.RunQuery(query);
        }

        private string[] GetColumnsFromDatabase(string tablename)
        {
            string query = "";
            query += "DECLARE @columns varchar(8000) ";
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(8000), COLUMN_NAME) ";
            query += String.Format("FROM (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{0}') as tab ", tablename);
            query += "select @columns ";

            DataSet ds = Processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString();

            string[] columnCol = columns.ToLower().Split(',').Select(x => x.Trim()).ToArray();

            return columnCol;
        }

        private void DataTableToDatabase(string DataSource, DataTable dt)
        {
            string modifiedDate = System.DateTime.Now.ToString();
            try
            {
                SqlConnection conn = new SqlConnection(Processor.ConnectionString);

                dt = CleanData(dt);

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);

                    bulkCopy.BatchSize = 10000;
                    bulkCopy.BulkCopyTimeout = 60;

                    if (dt.Columns.Contains("Date"))
                        dt.Columns["Date"].ColumnName = "AsOfDate";

                    if (!dt.Columns.Contains("ModifiedDate"))
                    {
                        System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                        newColumn.DefaultValue = System.DateTime.Now.ToString();
                        dt.Columns.Add(newColumn);
                    }

                    #region Mappings Region
                    SqlBulkCopyColumnMapping mapping1 = new SqlBulkCopyColumnMapping("AsOfDate", "AsOfDate");
                    SqlBulkCopyColumnMapping mapping2 = new SqlBulkCopyColumnMapping("Partner Id", "Partner Id");
                    SqlBulkCopyColumnMapping mapping3 = new SqlBulkCopyColumnMapping("Advertiser Id", "Advertiser Id");
                    SqlBulkCopyColumnMapping mapping4 = new SqlBulkCopyColumnMapping("Campaign Id", "Campaign Id");
                    SqlBulkCopyColumnMapping mapping5 = new SqlBulkCopyColumnMapping("Ad Group Id", "Ad Group Id");
                    SqlBulkCopyColumnMapping mapping6 = new SqlBulkCopyColumnMapping("Ad Format", "Ad Format");
                    SqlBulkCopyColumnMapping mapping7 = new SqlBulkCopyColumnMapping("Creative Id", "Creative Id");
                    SqlBulkCopyColumnMapping mapping8 = new SqlBulkCopyColumnMapping("Frequency", "Frequency");
                    SqlBulkCopyColumnMapping mapping9 = new SqlBulkCopyColumnMapping("Ad Group Budget", "Ad Group Budget");
                    SqlBulkCopyColumnMapping mapping10 = new SqlBulkCopyColumnMapping("Ad Group Imps Budget", "Ad Group Imps Budget");
                    SqlBulkCopyColumnMapping mapping11 = new SqlBulkCopyColumnMapping("Ad Group Daily Target", "Ad Group Daily Target");
                    SqlBulkCopyColumnMapping mapping12 = new SqlBulkCopyColumnMapping("Ad Group Daily Imps Target", "Ad Group Daily Imps Target");
                    SqlBulkCopyColumnMapping mapping13 = new SqlBulkCopyColumnMapping("Ad Group Daily Cap", "Ad Group Daily Cap");
                    SqlBulkCopyColumnMapping mapping14 = new SqlBulkCopyColumnMapping("Ad Group Daily Imps Cap", "Ad Group Daily Imps Cap");
                    SqlBulkCopyColumnMapping mapping15 = new SqlBulkCopyColumnMapping("Ad Group Base Bid CPM", "Ad Group Base Bid CPM");
                    SqlBulkCopyColumnMapping mapping16 = new SqlBulkCopyColumnMapping("Bids", "Bids");
                    SqlBulkCopyColumnMapping mapping17 = new SqlBulkCopyColumnMapping("Bid Amount", "Bid Amount");
                    SqlBulkCopyColumnMapping mapping18 = new SqlBulkCopyColumnMapping("Imps", "Imps");
                    SqlBulkCopyColumnMapping mapping19 = new SqlBulkCopyColumnMapping("Clicks", "Clicks");
                    SqlBulkCopyColumnMapping mapping20 = new SqlBulkCopyColumnMapping("PC 1", "PC 1");
                    SqlBulkCopyColumnMapping mapping21 = new SqlBulkCopyColumnMapping("PC 2", "PC 2");
                    SqlBulkCopyColumnMapping mapping22 = new SqlBulkCopyColumnMapping("PC 3", "PC 3");
                    SqlBulkCopyColumnMapping mapping23 = new SqlBulkCopyColumnMapping("PC 4", "PC 4");
                    SqlBulkCopyColumnMapping mapping24 = new SqlBulkCopyColumnMapping("PC 5", "PC 5");
                    SqlBulkCopyColumnMapping mapping25 = new SqlBulkCopyColumnMapping("PC 6", "PC 6");
                    SqlBulkCopyColumnMapping mapping26 = new SqlBulkCopyColumnMapping("PI 1", "PI 1");
                    SqlBulkCopyColumnMapping mapping27 = new SqlBulkCopyColumnMapping("PI 2", "PI 2");
                    SqlBulkCopyColumnMapping mapping28 = new SqlBulkCopyColumnMapping("PI 3", "PI 3");
                    SqlBulkCopyColumnMapping mapping29 = new SqlBulkCopyColumnMapping("PI 4", "PI 4");
                    SqlBulkCopyColumnMapping mapping30 = new SqlBulkCopyColumnMapping("PI 5", "PI 5");
                    SqlBulkCopyColumnMapping mapping31 = new SqlBulkCopyColumnMapping("PI 6", "PI 6");
                    SqlBulkCopyColumnMapping mapping32 = new SqlBulkCopyColumnMapping("PC 1 Rev", "PC 1 Rev");
                    SqlBulkCopyColumnMapping mapping33 = new SqlBulkCopyColumnMapping("PC 2 Rev", "PC 2 Rev");
                    SqlBulkCopyColumnMapping mapping34 = new SqlBulkCopyColumnMapping("PC 3 Rev", "PC 3 Rev");
                    SqlBulkCopyColumnMapping mapping35 = new SqlBulkCopyColumnMapping("PC 4 Rev", "PC 4 Rev");
                    SqlBulkCopyColumnMapping mapping36 = new SqlBulkCopyColumnMapping("PC 5 Rev", "PC 5 Rev");
                    SqlBulkCopyColumnMapping mapping37 = new SqlBulkCopyColumnMapping("PC 6 Rev", "PC 6 Rev");
                    SqlBulkCopyColumnMapping mapping38 = new SqlBulkCopyColumnMapping("PI 1 Rev", "PI 1 Rev");
                    SqlBulkCopyColumnMapping mapping39 = new SqlBulkCopyColumnMapping("PI 2 Rev", "PI 2 Rev");
                    SqlBulkCopyColumnMapping mapping40 = new SqlBulkCopyColumnMapping("PI 3 Rev", "PI 3 Rev");
                    SqlBulkCopyColumnMapping mapping41 = new SqlBulkCopyColumnMapping("PI 4 Rev", "PI 4 Rev");
                    SqlBulkCopyColumnMapping mapping42 = new SqlBulkCopyColumnMapping("PI 5 Rev", "PI 5 Rev");
                    SqlBulkCopyColumnMapping mapping43 = new SqlBulkCopyColumnMapping("PI 6 Rev", "PI 6 Rev");
                    SqlBulkCopyColumnMapping mapping44 = new SqlBulkCopyColumnMapping("PC 1 Cur", "PC 1 Cur");
                    SqlBulkCopyColumnMapping mapping45 = new SqlBulkCopyColumnMapping("PC 2 Cur", "PC 2 Cur");
                    SqlBulkCopyColumnMapping mapping46 = new SqlBulkCopyColumnMapping("PC 3 Cur", "PC 3 Cur");
                    SqlBulkCopyColumnMapping mapping47 = new SqlBulkCopyColumnMapping("PC 4 Cur", "PC 4 Cur");
                    SqlBulkCopyColumnMapping mapping48 = new SqlBulkCopyColumnMapping("PC 5 Cur", "PC 5 Cur");
                    SqlBulkCopyColumnMapping mapping49 = new SqlBulkCopyColumnMapping("PC 6 Cur", "PC 6 Cur");
                    SqlBulkCopyColumnMapping mapping50 = new SqlBulkCopyColumnMapping("PI 1 Cur", "PI 1 Cur");
                    SqlBulkCopyColumnMapping mapping51 = new SqlBulkCopyColumnMapping("PI 2 Cur", "PI 2 Cur");
                    SqlBulkCopyColumnMapping mapping52 = new SqlBulkCopyColumnMapping("PI 3 Cur", "PI 3 Cur");
                    SqlBulkCopyColumnMapping mapping53 = new SqlBulkCopyColumnMapping("PI 4 Cur", "PI 4 Cur");
                    SqlBulkCopyColumnMapping mapping54 = new SqlBulkCopyColumnMapping("PI 5 Cur", "PI 5 Cur");
                    SqlBulkCopyColumnMapping mapping55 = new SqlBulkCopyColumnMapping("PI 6 Cur", "PI 6 Cur");
                    SqlBulkCopyColumnMapping mapping56 = new SqlBulkCopyColumnMapping("TTD Total Cost", "TTD Total Cost");
                    SqlBulkCopyColumnMapping mapping57 = new SqlBulkCopyColumnMapping("Partner Total Cost", "Partner Total Cost");
                    SqlBulkCopyColumnMapping mapping58 = new SqlBulkCopyColumnMapping("Advertiser Total Cost", "Advertiser Total Cost");
                    SqlBulkCopyColumnMapping mapping59 = new SqlBulkCopyColumnMapping("VideoEventCreativeView", "VideoEventCreativeView");
                    SqlBulkCopyColumnMapping mapping60 = new SqlBulkCopyColumnMapping("VideoEventStart", "VideoEventStart");
                    SqlBulkCopyColumnMapping mapping61 = new SqlBulkCopyColumnMapping("VideoEventFirstQuarter", "VideoEventFirstQuarter");
                    SqlBulkCopyColumnMapping mapping62 = new SqlBulkCopyColumnMapping("VideoEventMidpoint", "VideoEventMidpoint");
                    SqlBulkCopyColumnMapping mapping63 = new SqlBulkCopyColumnMapping("VideoEventThirdQuarter", "VideoEventThirdQuarter");
                    SqlBulkCopyColumnMapping mapping64 = new SqlBulkCopyColumnMapping("VideoEventComplete", "VideoEventComplete");
                    SqlBulkCopyColumnMapping mapping65 = new SqlBulkCopyColumnMapping("VideoEventMute", "VideoEventMute");
                    SqlBulkCopyColumnMapping mapping66 = new SqlBulkCopyColumnMapping("VideoEventUnmute", "VideoEventUnmute");
                    SqlBulkCopyColumnMapping mapping67 = new SqlBulkCopyColumnMapping("VideoEventPause", "VideoEventPause");
                    SqlBulkCopyColumnMapping mapping68 = new SqlBulkCopyColumnMapping("VideoEventResume", "VideoEventResume");
                    SqlBulkCopyColumnMapping mapping69 = new SqlBulkCopyColumnMapping("VideoEventFullscreen", "VideoEventFullscreen");
                    SqlBulkCopyColumnMapping mapping70 = new SqlBulkCopyColumnMapping("VideoEventError", "VideoEventError");
                    SqlBulkCopyColumnMapping mapping71 = new SqlBulkCopyColumnMapping("VideoEventSkip", "VideoEventSkip");
                    SqlBulkCopyColumnMapping mapping72 = new SqlBulkCopyColumnMapping("VideoEventEngagedView", "VideoEventEngagedView");
                    SqlBulkCopyColumnMapping mapping73 = new SqlBulkCopyColumnMapping("VideoEventRewind", "VideoEventRewind");
                    SqlBulkCopyColumnMapping mapping74 = new SqlBulkCopyColumnMapping("VideoEventExpand", "VideoEventExpand");
                    SqlBulkCopyColumnMapping mapping75 = new SqlBulkCopyColumnMapping("VideoEventCollapse", "VideoEventCollapse");
                    SqlBulkCopyColumnMapping mapping76 = new SqlBulkCopyColumnMapping("TWD 5 Rev", "TWD 5 Rev");
                    SqlBulkCopyColumnMapping mapping77 = new SqlBulkCopyColumnMapping("TWD 6 Rev", "TWD 6 Rev");
                    SqlBulkCopyColumnMapping mapping78 = new SqlBulkCopyColumnMapping("Deal Code", "Deal Code");
                    SqlBulkCopyColumnMapping mapping79 = new SqlBulkCopyColumnMapping("AdServer Name", "AdServer Name");
                    SqlBulkCopyColumnMapping mapping80 = new SqlBulkCopyColumnMapping("AdServer Creative Placement Id", "AdServer Creative Placement Id");
                    SqlBulkCopyColumnMapping mapping81 = new SqlBulkCopyColumnMapping("CreativeIsTrackable", "CreativeIsTrackable");
                    SqlBulkCopyColumnMapping mapping82 = new SqlBulkCopyColumnMapping("CreativeWasViewable", "CreativeWasViewable");
                    SqlBulkCopyColumnMapping mapping83 = new SqlBulkCopyColumnMapping("ModifiedDate", "Modified Date");

                    bulkCopy.ColumnMappings.Add(mapping1);
                    bulkCopy.ColumnMappings.Add(mapping2);
                    bulkCopy.ColumnMappings.Add(mapping3);
                    bulkCopy.ColumnMappings.Add(mapping4);
                    bulkCopy.ColumnMappings.Add(mapping5);
                    bulkCopy.ColumnMappings.Add(mapping9);
                    bulkCopy.ColumnMappings.Add(mapping10);
                    bulkCopy.ColumnMappings.Add(mapping11);
                    bulkCopy.ColumnMappings.Add(mapping12);
                    bulkCopy.ColumnMappings.Add(mapping13);
                    bulkCopy.ColumnMappings.Add(mapping14);
                    bulkCopy.ColumnMappings.Add(mapping15);
                    bulkCopy.ColumnMappings.Add(mapping16);
                    bulkCopy.ColumnMappings.Add(mapping17);
                    bulkCopy.ColumnMappings.Add(mapping18);
                    bulkCopy.ColumnMappings.Add(mapping19);
                    bulkCopy.ColumnMappings.Add(mapping20);
                    bulkCopy.ColumnMappings.Add(mapping21);
                    bulkCopy.ColumnMappings.Add(mapping22);
                    bulkCopy.ColumnMappings.Add(mapping23);
                    bulkCopy.ColumnMappings.Add(mapping24);
                    bulkCopy.ColumnMappings.Add(mapping25);
                    bulkCopy.ColumnMappings.Add(mapping26);
                    bulkCopy.ColumnMappings.Add(mapping27);
                    bulkCopy.ColumnMappings.Add(mapping28);
                    bulkCopy.ColumnMappings.Add(mapping29);
                    bulkCopy.ColumnMappings.Add(mapping30);
                    bulkCopy.ColumnMappings.Add(mapping31);
                    bulkCopy.ColumnMappings.Add(mapping32);
                    bulkCopy.ColumnMappings.Add(mapping33);
                    bulkCopy.ColumnMappings.Add(mapping34);
                    bulkCopy.ColumnMappings.Add(mapping35);
                    bulkCopy.ColumnMappings.Add(mapping36);
                    bulkCopy.ColumnMappings.Add(mapping37);
                    bulkCopy.ColumnMappings.Add(mapping38);
                    bulkCopy.ColumnMappings.Add(mapping39);
                    bulkCopy.ColumnMappings.Add(mapping40);
                    bulkCopy.ColumnMappings.Add(mapping41);
                    bulkCopy.ColumnMappings.Add(mapping42);
                    bulkCopy.ColumnMappings.Add(mapping43);
                    bulkCopy.ColumnMappings.Add(mapping44);
                    bulkCopy.ColumnMappings.Add(mapping45);
                    bulkCopy.ColumnMappings.Add(mapping46);
                    bulkCopy.ColumnMappings.Add(mapping47);
                    bulkCopy.ColumnMappings.Add(mapping48);
                    bulkCopy.ColumnMappings.Add(mapping49);
                    bulkCopy.ColumnMappings.Add(mapping50);
                    bulkCopy.ColumnMappings.Add(mapping51);
                    bulkCopy.ColumnMappings.Add(mapping52);
                    bulkCopy.ColumnMappings.Add(mapping53);
                    bulkCopy.ColumnMappings.Add(mapping54);
                    bulkCopy.ColumnMappings.Add(mapping55);
                    bulkCopy.ColumnMappings.Add(mapping56);
                    bulkCopy.ColumnMappings.Add(mapping57);
                    bulkCopy.ColumnMappings.Add(mapping58);
                    bulkCopy.ColumnMappings.Add(mapping59);
                    bulkCopy.ColumnMappings.Add(mapping60);
                    bulkCopy.ColumnMappings.Add(mapping61);
                    bulkCopy.ColumnMappings.Add(mapping62);
                    bulkCopy.ColumnMappings.Add(mapping63);
                    bulkCopy.ColumnMappings.Add(mapping64);
                    bulkCopy.ColumnMappings.Add(mapping65);
                    bulkCopy.ColumnMappings.Add(mapping66);
                    bulkCopy.ColumnMappings.Add(mapping67);
                    bulkCopy.ColumnMappings.Add(mapping68);
                    bulkCopy.ColumnMappings.Add(mapping69);
                    bulkCopy.ColumnMappings.Add(mapping70);
                    bulkCopy.ColumnMappings.Add(mapping71);
                    bulkCopy.ColumnMappings.Add(mapping72);
                    bulkCopy.ColumnMappings.Add(mapping73);
                    bulkCopy.ColumnMappings.Add(mapping74);
                    bulkCopy.ColumnMappings.Add(mapping75);
                    bulkCopy.ColumnMappings.Add(mapping76);
                    bulkCopy.ColumnMappings.Add(mapping77);
                    bulkCopy.ColumnMappings.Add(mapping81);
                    bulkCopy.ColumnMappings.Add(mapping82);
                    bulkCopy.ColumnMappings.Add(mapping83);

                    if (DataSource == "dataTradeDeskDataElementReportingData")
                    {
                        SqlBulkCopyColumnMapping mapping84 = new SqlBulkCopyColumnMapping("THIRD PARTY DATA BRAND NAME", "Third Party Data Brand Name");
                        SqlBulkCopyColumnMapping mapping85 = new SqlBulkCopyColumnMapping("DATA ELEMENT ID", "Data Element Id");
                        SqlBulkCopyColumnMapping mapping86 = new SqlBulkCopyColumnMapping("DATA NAME", "Data Name");
                        bulkCopy.ColumnMappings.Add(mapping84);
                        bulkCopy.ColumnMappings.Add(mapping85);
                        bulkCopy.ColumnMappings.Add(mapping86);
                    }
                    else
                    {
                        bulkCopy.ColumnMappings.Add(mapping6);
                        bulkCopy.ColumnMappings.Add(mapping7);
                        bulkCopy.ColumnMappings.Add(mapping8);
                        bulkCopy.ColumnMappings.Add(mapping78);
                        bulkCopy.ColumnMappings.Add(mapping79);
                        bulkCopy.ColumnMappings.Add(mapping80);
                    }
                    #endregion

                    bulkCopy.DestinationTableName = DataSource;

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 5000;

                    conn.Open();

                    bulkCopy.WriteToServer(dt);
                }
            }
            catch (Exception ex)
            {
                //TODO: Delete record inserted if there was an error.
                SolutionHelper.Error.LogError(ex, "HDReportsServiceHandler.DataTableToDatabase:" + Processor.DatabaseName + " Modified Date: " + modifiedDate);
                throw ex;
            }
        }

        /// <summary>
        /// Check if all the columns are created in the database for the reporting table. If the column doesnt exist it will be added.
        /// </summary>
        /// <param name="dt"></param>
        private void ColumnChecker(string datasource, DataTable dt)
        {
            string query = "";
            query += "DECLARE @columns varchar(8000) ";
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(8000), COLUMN_NAME) ";
            query += "FROM (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" + datasource + "') as tab ";
            query += "select @columns ";

            DataSet ds = Processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString();

            string[] columnCol = columns.ToLower().Split(',');

            if (dt.Columns.Contains("Date"))
                dt.Columns["Date"].ColumnName = "AsOfDate";

            foreach (DataColumn dc in dt.Columns)
            {
                //  dc.ColumnName = dc.ColumnName.ToPascalCase();

                if (!columnCol.Contains(dc.ColumnName.ToLower()))
                    AddColumn(datasource, dc.ColumnName);
            }

            DateTime today = System.DateTime.Now;

            if (!dt.Columns.Contains("ModifiedDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                newColumn.DefaultValue = today.ToString("d");
                dt.Columns.Add(newColumn);
            }
        }

        /// <summary>
        /// Due to the columns needing to be dynamic this method is designed to add additional column to the table
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        private void AddColumn(string table, string column)
        {
            string query = "ALTER TABLE {0} ADD [{1}] {2}";

            if (column.ToUpper().ContainsAny("* CLICKS", "POST-IMPRESSION CONVERSIONS", "POST-CLICK CONVERSIONS", "TOTAL CONVERSIONS"))
                query = string.Format(query, table, column, "[nvarchar](250)"); // query = string.Format(query, table, column, "int NULL");
            else
                query = string.Format(query, table, column, "[nvarchar](250)");

            Processor.RunQuery(query);
        }

        private DataTable CleanData(DataTable dt)
        {
            #region CleanData
            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName.ToLower().ContainsAny("ad group imps budget",
                        "ad group daily target",
                        "ad group daily cap",
                        "ad group daily imps target",
                        "ad group daily imps cap",
                        "pc 1 cur",
                        "pc 2 cur",
                        "pc 3 cur",
                        "pc 4 cur",
                        "pc 5 cur",
                        "pc 6 cur",
                        "pi 1 cur",
                        "pi 2 cur",
                        "pi 3 cur",
                        "pi 4 cur",
                        "pi 5 cur",
                        "pi 6 cur",
                        "partner total cost",
                        "ttd total cost",
                        "advertiser total cost"))
                    {
                        if (String.IsNullOrEmpty(dr[col.ColumnName].ToString()) || dr[col.ColumnName].ToString().ToUpper() == "NULL")
                            dr[col.ColumnName] = 0;
                        if (dr[col.ColumnName].ToString().Contains(","))
                            dr[col.ColumnName] = dr[col.ColumnName].ToString().Replace(",", "");
                    }
                }
            }
            #endregion

            return dt;
        }

        private DataTable DynamicCleanData(DataTable dt)
        {
            DataTable _debugDt = new DataTable();

            #region CleanData
            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName.ToUpper().ContainsAny("TIME"))
                        if (String.IsNullOrEmpty(dr[col.ColumnName].ToString()) || dr[col.ColumnName].ToString().ToUpper() == "NULL")
                            dr[col.ColumnName] = "2999-01-01 00:00:00.000";

                    //Check character limits


                    string adjustcolumn = ""; // TODO: Auto adjust columns in database

                    if (col.ColumnName.ToUpper().ContainsAny("Referrer Url".ToUpper(), "Xdids".ToUpper()))
                    {
                        if (dr[col.ColumnName].ToString().Length > 2000)
                            adjustcolumn = col.ColumnName;
                    }
                    else if (col.ColumnName.ToUpper().ContainsAny("Conversion Time".ToUpper(),
                    "Conversion Type".ToUpper(),
                    "Tracking Tag Name".ToUpper(),
                    "Monetary Value Currency".ToUpper(),
                    "First Impression Campaign Id".ToUpper(),
                    "First Impression Ad Group Id".ToUpper(),
                    "First Impression Creative Id".ToUpper(),
                    "First Impression Format".ToUpper(),
                    "Last Impression Campaign Id".ToUpper(),
                    "Last Impression Ad Group Id".ToUpper(),
                    "Last Impression Creative Id".ToUpper(),
                    "Last Impression Format".ToUpper(),
                    "Last Impression Country".ToUpper(),
                    "Last Impression Region".ToUpper(),
                    "Last Impression Metro".ToUpper(),
                    "First Display Click Campaign Id".ToUpper(),
                    "First Display Click Ad Group Id".ToUpper(),
                    "First Display Click Creative Id".ToUpper(),
                    "First Display Click Format".ToUpper(),
                    "Last Display Click Campaign Id".ToUpper(),
                    "Last Display Click Ad Group Id".ToUpper(),
                    "Last Display Click Creative Id".ToUpper(),
                    "Last Display Click Format".ToUpper(),
                    "Partner Id".ToUpper(),
                    "Advertiser Id".ToUpper(),
                    "Attribution Model".ToUpper(),
                    "Conversion Device Type".ToUpper(),
                    "First Impression Device Type".ToUpper(),
                    "First Impression Ad Environment".ToUpper(),
                    "Last Impression Device Type".ToUpper(),
                    "Last Impression Ad Environment".ToUpper(),
                    "First Display Click Device Type".ToUpper(),
                    "First Display Click Ad Environment".ToUpper(),
                    "Last Display Click Device Type".ToUpper(),
                    "Last Display Click Ad Environment".ToUpper(),
                    "First Non-display Click Device Type".ToUpper(),
                    "First Non-display Click Ad Environment".ToUpper(),
                    "Last Non-display Click Device Type".ToUpper(),
                    "Last Non-display Click Ad Environment".ToUpper()
                     ))
                    {
                        if (dr[col.ColumnName].ToString().Length > 50)
                            adjustcolumn = col.ColumnName;
                    }
                    else
                    {
                        if (dr[col.ColumnName].ToString().Length > 250)
                            adjustcolumn = col.ColumnName + dr[col.ColumnName].ToString().Length;
                    }



                    if (debug)
                    {
                        if (!_debugDt.Columns.Contains(col.ColumnName))
                        {
                            DataColumn dc = new DataColumn();
                            dc.ColumnName = col.ColumnName;
                            _debugDt.Columns.Add(dc);
                        }

                        DataRow toInsert = _debugDt.NewRow();

                        if (_debugDt.Rows.Count == 0)
                        {
                            toInsert[0] = 0;
                            _debugDt.Rows.InsertAt(toInsert, 0);
                        }

                        int count = 0;

                        if (_debugDt.Rows[0][col.ColumnName].ToString() != "")
                            count = int.Parse(_debugDt.Rows[0][col.ColumnName].ToString());

                        if (count < int.Parse(dr[col.ColumnName].ToString().Length.ToString()))
                            _debugDt.Rows[0][col.ColumnName] = int.Parse(dr[col.ColumnName].ToString().Length.ToString());
                    }
                }
            }
            #endregion 

            return dt;
        }

        private void ProcessPlacementCost()
        {
            try
            {

            }
            catch (Exception ex)
            {
                //TODO
            }
        }

        void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(String.Format("{0} Rows have been copied.", e.RowsCopied.ToString()));
        }

        /// <summary>
        /// Get company based on the advertiser and where or not the advertiser has been linked to a company in the lavidge database.
        /// </summary>
        /// <param name="advertiser"></param>
        /// <returns></returns>
        private Company GetCompany(dataTradeDeskAdvertiser advertiser)
        {
            if (advertiser.CompanyId != null)

            {
                Company com = (from m in db.Companies where m.IsActive == true select m).Where(x => x.Id == advertiser.CompanyId).FirstOrDefault();

                if (com != null)
                {
                    Data processor = new Data(com.DatabaseName, com.Login, com.Password, com.Server);
                    Processor = processor;
                    return com;
                }
            }

            return null;
        }

        private void ExportMyReport(dataTradeDeskAdvertiser advertiser, AuthenticationServiceHandler authenticationUtil, MyReportResponseData result, string downloadurl, string reportName, DateTime date)
        {
            Stream stream = null;

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(downloadurl);
            req.Headers.Add("TTD-Auth", authenticationUtil.UserSecurityToken);
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

            HttpWebRequest req1 = (HttpWebRequest)WebRequest.Create(resp.ResponseUri);
            req1.Headers.Add("TTD-Auth", authenticationUtil.UserSecurityToken);
            req1.ContentType = "application/octet-stream";
            HttpWebResponse resp1 = (HttpWebResponse)req.GetResponse();

            stream = resp1.GetResponseStream();

            string ext = "";
            string deliveredPath = result.ReportDeliveries[0].DeliveredPath;

            if (deliveredPath.Contains(".tsv"))
                ext = ".tsv";
            if (deliveredPath.Contains(".csv"))
                ext = ".csv";
            if (deliveredPath.Contains(".xlsx"))
                ext = ".xlsx";

            string dateExt = date.ToString().Replace(":", ".").Replace(@"/", ".").ToString();

            if (reportName.ToLower().Contains("history"))
                dateExt = "";

            string locations = String.Format(@"{0}\TradeDesk_{1}_{2}_{3}{4}", csvExport, advertiser.AdvertiserName, reportName.Replace("|", "-"), dateExt, ext);

            try
            {
                if (File.Exists(locations))
                    return;

                FileStream fileStream = File.Create(locations);


                byte[] b = null;

                using (MemoryStream ms = new MemoryStream())
                {
                    int count = 0;
                    do
                    {
                        byte[] buf = new byte[1024];
                        count = stream.Read(buf, 0, 1024);
                        ms.Write(buf, 0, count);
                    } while (stream.CanRead && count > 0);
                    b = ms.ToArray();
                }

                stream.Read(b, 0, b.Length);
                // Use write method to write to the file specified above
                fileStream.Write(b, 0, b.Length);
                fileStream.Close();

            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "HDReportsServiceHandler.DataTableToDatabase:" + ex.Message);
            }

            resp.Close();
            resp1.Close();

        }

        #region removeColums
        string[] removeColumns = new string[] {
            "Last Impression FCap",
            "Order Id",
            "# Non-display Clicks",
            "# Frequency Cycles and Ad Groups Where FCap is Reached",
            "# Frequency Cycles and Ad Groups w/ Activity",
            "# Frequency Cycles and Ad Groups Where FCap is Reached",
            "Path to Conversion",
            "Monetary Value",
            "Monetary Value Currency",
            "Last Impression Categories",
            "Last Impression Country",
            "Last Impression Metro",
            "First Display Click Time",
            "First Display Click Campaign Id",
            "First Display Click Campaign Name",
            "First Display Click Ad Group Id",
            "First Display Click Ad Group Name",
            "First Display Click Creative Id",
            "First Display Click Creative Name",
            "First Display Click Format",
            "Last Display Click Time",
            "Last Display Click Campaign Id",
            "Last Display Click Campaign Name",
            "Last Display Click Ad Group Id",
            "Last Display Click Ad Group Name",
            "Last Display Click Creative Id",
            "Last Display Click Creative Name",
            "Last Display Click Format",
            "First Non-display Click Time",
            "First Non-display Click Channel",
            "First Non-display Click Distribution Network",
            "First Non-display Click Distribution Match Type",
            "First Non-display Click Campaign Id",
            "First Non-display Click Campaign Name",
            "First Non-display Click Keyword Id",
            "First Non-display Click Keyword",
            "Last Non-display Click Time",
            "Last Non-display Click Channel",
            "Last Non-display Click Distribution Network",
            "Last Non-display Click Distribution Match Type",
            "Last Non-display Click Campaign Id",
            "Last Non-display Click Campaign Name",
            "Last Non-display Click Keyword Id",
            "Last Non-display Click Keyword",
            "Partner Id",
            "TD 1",
            "TD 2",
            "TD 3",
            "TD 4",
            "TD 5",
            "TD 6",
            "TD 7",
            "TD 8",
            "TD 9",
            "TD 10",
            "Full Path to Conversion",
            "First Display Click Device Type",
            "First Display Click Ad Environment",
            "Last Display Click Device Type",
            "Last Display Click Ad Environment",
            "First Non-display Click Device Type",
            "First Non-display Click Ad Environment",
            "Last Non-display Click Device Type",
            "Xdid",
            "Last Non-display Click Ad Environment"};

        #endregion

    }
}


