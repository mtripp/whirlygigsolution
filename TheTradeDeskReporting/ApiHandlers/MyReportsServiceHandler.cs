﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml;
using SolutionHelper;
using System.Data.SqlClient;
using WhirlygigSolutionHelper;
using System.Web;
using System.Data.OleDb;
using System.Globalization;

namespace TheTradeDeskReporting
{
    public class MyReportsServiceHandler
    {
        mediaTradeDeskEntities1 db = new mediaTradeDeskEntities1();
        dataTradeDeskAdvertiser[] advertisers;
        Data processor = new Data();
        dataTradeDeskAdvertiser currentAdvertiser = new dataTradeDeskAdvertiser();
        Company company;

        string tableName = "";
        string csvExportCompleted;
        bool reimportData;
        string csvExportFailed;
        string csvExportWaiting;
        string csvExport;
        bool debug;
        bool processFile = false;
        bool deleteFile = false;
        private string url;

        public MyReportsServiceHandler()
        {
            csvExportCompleted = ConfigurationManager.AppSettings["csvExportCompleted"];
            csvExportFailed = ConfigurationManager.AppSettings["csvExportFailed"];
            csvExportWaiting = ConfigurationManager.AppSettings["csvExportWaiting"];
            csvExport = ConfigurationManager.AppSettings["csvExport"];
            debug = bool.Parse(ConfigurationManager.AppSettings["debug"].ToString());
            url = ConfigurationManager.AppSettings["APIURL"] + "myreports/reportexecution/query/advertisers/";
        }


        public void RunGetFile()
        {
            int idx = 0;
            bool now = false;

            advertisers = (from m in db.dataTradeDeskAdvertisers where m.CompanyId != null && m.Active == true && m.bolMyReports == true select m).OrderByDescending(m => m.AdvertiserName).ToArray();

            foreach (dataTradeDeskAdvertiser advertiser in advertisers)
            {
                Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();

                GetCompany(advertiser);

                if (company != null)
                {

                    if (debug)
                    {
                        //if (advertiser.CompanyId == 4) // Target specific company
                        //    now = now; //   return;  // 
                        //else
                        //    continue;

                        if (advertiser.AdvertiserId.ContainsAny("1nb8046" )) // Target specific company
                            now = now;
                        else if (advertiser.AdvertiserId.ContainsAny(  "nihdd87")) // Target specific company
                            now = now;
                        else
                            continue;
                    }

                    Console.WriteLine("Current Advertiser: " + advertiser.AdvertiserName + " - '" + advertiser.AdvertiserId + "' - " + idx.ToString() + " - " + advertisers.Count().ToString());
                    GetMyReport(advertiser);
                }
                else
                    idx = idx;

                idx++;
            }
        }

        public void RunProcessFile()
        {
            try
            {
                DirectoryInfo d = new DirectoryInfo(csvExport);
                FileInfo[] infos = d.GetFiles();

                foreach (FileInfo f in infos.Where(m => m.FullName.ContainsAny("Performance SQL Server", "Conversion Details Report")))
                {
                    bool result = false;

                    try
                    {
                        string[] strReportName = f.Name.Split('_').Take(3).ToArray();

                        GetCompany(strReportName[1]);

                        if (company != null)
                        {
                            string tb = strReportName[2];

                            if (tb == "Data Elements Performance SQL Server")
                                tableName = "dataTradeDeskDataElementMyReportingData"; 
                            else if (tb == "Performance SQL Server" || tb == "Performance SQL Server History")
                                tableName = "dataTradeDeskMyReportingData"; 
                            else if (tb == "Performance SQL Server" || tb == "Performance SQL Server History")
                                tableName = "dataTradeDeskMyReportingData";
                            else if (tb.Contains("Conversion Details Report"))
                                tableName = "dataTradeDeskConversionMyReportData";


                            if (tableName == "")
                                continue;

                            DataTable dt = new DataTable();

                            if (f.Extension == ".tsv")
                                dt = CsvToDatabase.GetDataTabletFromTSVFile(f.FullName.ToString());
                            if (f.Extension == ".csv")
                                dt = CsvToDatabase.GetDataTabletFromCSVFile(f.FullName.ToString());


                            if (dt.Rows.Count != 0)
                                result = ProcessFile(dt);

                            if (result)
                                f.MoveTo(csvExportCompleted + @"\" + f.Name);
                            else if (processFile)
                            {
                                string path = csvExportCompleted + @"\" + f.Name.Replace(f.Extension, " Duplicate ") + f.Extension;
                                f.MoveTo(path);
                            }
                            else if (deleteFile)
                            {
                                string path = csvExportCompleted + @"\" + f.Name.Replace(f.Extension, " Deleted ") + System.DateTime.Now.ToString().Replace(":", " ").Replace("/", "-") + f.Extension;
                                f.MoveTo(path);
                            }
                            else
                                f.MoveTo(csvExportFailed + @"\" + f.Name);
                        }
                    }
                    catch (Exception ex)
                    {
                        SolutionHelper.Error.LogError("TradedeskMyReport.RunProcessFile 2:" + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("TradedeskMyReport.RunProcessFile:" + ex.Message);
            }
        }

        private bool ProcessFile(DataTable dt)
        {
            bool processed = false;
            DateTime today = System.DateTime.Now.Date;

            DateTime[] reports = GetReportDates(currentAdvertiser, tableName, today.AddDays(-45));

            ColumnChecker(dt);

            dt = AdvertiserChecker(dt);

            if (deleteFile)
                return false;

            DataSet ds = dt.ToDataSetDateSplit();


            foreach (DataTable datatab in ds.Tables)
            {
                DateTime tablename = DateTime.Parse(datatab.TableName.ToString());

                if (reports.Count() > 0)
                {
                    if (reports.Contains(tablename))
                    {
                        bool comparedata = CompareReportDataAgainstDatabaseData(datatab, tableName);

                        if (comparedata)
                        {
                            processFile = true;
                            continue;
                        }
                        else if (reimportData)
                        {
                            DeleteOldData(datatab);
                        }
                        else
                        {
                            processFile = false;
                            return false;
                        }

                    }
                    else if (tablename == today)
                    {
                        continue;
                    }
                    else
                    {
                        processFile = false;
                    }
                }

                if(tableName != "dataTradeDeskConversionMyReportData") // This new process and we are inserting raw data for this feed.
                    CleanData(datatab);

                Console.WriteLine("Processing MyReports for " + datatab.TableName.ToString());

                processed = ProcessMyReport(datatab, tableName);

                if (!processed)         // If at least one insert was improper then entire import needs to be reviewed
                    return processed;
            }

            return processed;
        }

        private void DeleteOldData(DataTable dt)
        {
            try
            {
                string query = String.Format(@"delete FROM [dbo].[{0}] where AsOfDate = '{1}'", tableName, dt.TableName);

                processor.RunQuery(query);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private DataTable AdvertiserChecker(DataTable dt)
        {
            try
            {
                string expression = String.Format("[Advertiser Id] = '{0}'", currentAdvertiser.AdvertiserId);

                DataRow[] foundRows = dt.Select(expression);

                DataTable _dt = dt.Clone();

                foreach (DataRow dr in foundRows)
                    _dt.Rows.Add(dr.ItemArray);

                if (_dt.Rows.Count == 0)
                    deleteFile = true;
                else
                    return _dt;
            }
            catch (Exception ex)
            {
                string except = ex.ToString();
            }

            return null;
        }

        /// <summary>
        /// Check if all the columns are created in the database for the reporting table. If the column doesnt exist it will be added.
        /// </summary>
        /// <param name="dt"></param>
        private void ColumnChecker(DataTable dt)
        {
            DateTime today = System.DateTime.Now;

            foreach (DataColumn dc in dt.Columns)
                if (dc.ColumnName.Contains("_"))
                    dc.ColumnName = dc.ColumnName.Replace("_", " ");

            string[] columnCol = GetColumnsFromDatabase(tableName);



            foreach (DataColumn dc in dt.Columns)
            {
                if (dc.ColumnName.Contains("* "))
                    dc.ColumnName = dc.ColumnName.Replace("* ", "");

                dc.ColumnName = dc.ColumnName.ToProperCase();

                if (dc.ColumnName.ToLower() == "date" || dc.ColumnName.ToLower() == "asofdate")
                {
                    dc.ColumnName = "AsOfDate";
                    continue;
                }

                if (dc.ColumnName.ToLower() == "modifieddate")
                {
                    dc.ColumnName = "ModifiedDate";
                    continue;
                }

                if (!columnCol.Contains(dc.ColumnName.ToLower().Trim()))
                {
                    AddColumn(tableName, dc.ColumnName);
                }
            }

            if (!dt.Columns.Contains("ModifiedDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                newColumn.DefaultValue = today.ToString();
                dt.Columns.Add(newColumn);
            }

            if (!dt.Columns.Contains("AsOfDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("AsOfDate", typeof(System.DateTime));
                dt.Columns.Add(newColumn);

                foreach(DataRow dr in dt.Rows)
                {
                    DateTime date = DateTime.Parse(dr["Conversion Time"].ToString());
                    dr["AsOfDate"] = date.Date;
                }

                newColumn.DefaultValue = today.ToString();
            }

            bool bolChecker = DebugHelper.CheckColumnsAgainstDatabase(processor, dt, tableName);
        }

        private string[] GetColumnsFromDatabase(string tableName)
        {
            string query = "";
            query += "DECLARE @columns varchar(8000) ";
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(8000), COLUMN_NAME) ";
            query += String.Format("FROM (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{0}') as tab ", tableName);
            query += "select @columns ";

            DataSet ds = processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString();

            string[] columnCol = columns.ToLower().Split(',').Select(x => x.Trim()).ToArray();

            return columnCol;
        }

        private DataTable GetColumnsTypesFromDatabase(DataTable _dt, string tableName)
        {
            string query = String.Format("select COLUMN_NAME,DATA_TYPE,'Data Type' AS [Column] from information_schema.columns where TABLE_CATALOG = '{1}' and TABLE_NAME = '{0}' ", tableName, processor.DatabaseName);

            DataSet ds = processor.RunQuery(query);

            DataTable dt = ds.Tables[0];
            dt = Pivot(dt, dt.Columns["COLUMN_NAME"], dt.Columns["DATA_TYPE"]);

            DataTable dtCloned = _dt.Clone();

            string lastColumn = "";

            try
            { 
                foreach (DataColumn col in dtCloned.Columns)
                {
                    lastColumn = col.ColumnName.ToString();

                    string column = dt.Rows[0][col.ColumnName].ToString();

                    if (lastColumn == "Monetary Value Currency")
                        column = column;

                    switch (column)
                    {
                        case "varchar":
                            dtCloned.Columns[col.ColumnName].DataType = typeof(String);
                            break;
                        case "bigint":
                            dtCloned.Columns[col.ColumnName].DataType = typeof(Int32);
                            break;
                        case "bit":
                            dtCloned.Columns[col.ColumnName].DataType = typeof(Boolean);
                            break;
                        case "datetime":
                            dtCloned.Columns[col.ColumnName].DataType = typeof(DateTime);
                            break;
                        case "decimal":
                            dtCloned.Columns[col.ColumnName].DataType = typeof(Double);
                            break;
                        case "int":
                            dtCloned.Columns[col.ColumnName].DataType = typeof(int);
                            break;
                    }
                }

                foreach (DataRow row in _dt.Rows)
                {
                    dtCloned.ImportRow(row);
                }

            }
            catch (Exception ex)
            {
                string exs = ex.Message;
                throw ex;
            }

            return dtCloned;
        }

        DataTable Pivot(DataTable dt, DataColumn pivotColumn, DataColumn pivotValue)
        {
            // find primary key columns 
            //(i.e. everything but pivot column and pivot value)
            DataTable temp = dt.Copy();
            temp.Columns.Remove(pivotColumn.ColumnName);
            temp.Columns.Remove(pivotValue.ColumnName);
            string[] pkColumnNames = temp.Columns.Cast<DataColumn>()
                .Select(c => c.ColumnName)
                .ToArray();

            // prep results table
            DataTable result = temp.DefaultView.ToTable(true, pkColumnNames).Copy();
            result.PrimaryKey = result.Columns.Cast<DataColumn>().ToArray();
            dt.AsEnumerable()
                .Select(r => r[pivotColumn.ColumnName].ToString())
                .Distinct().ToList()
                .ForEach(c => result.Columns.Add(c, pivotColumn.DataType));

            // load it
            foreach (DataRow row in dt.Rows)
            {
                // find row to update
                DataRow aggRow = result.Rows.Find(
                    pkColumnNames
                        .Select(c => row[c])
                        .ToArray());
                // the aggregate used here is LATEST 
                // adjust the next line if you want (SUM, MAX, etc...)

                //   aggRow[row[pivotColumn.ColumnName].ToString()] = row[pivotValue.ColumnName]; // **ADJUSTED**
                aggRow[row[0].ToString()] = row[pivotValue.ColumnName]; // **ADJUSTED**
            }

            return result;
        }

        private bool ProcessMyReport(DataTable dt, string tableName)
        {
            try
            {
                DataTable _dt = GetColumnsTypesFromDatabase(dt, tableName);

                SqlConnection conn = new SqlConnection(processor.ConnectionString);

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);

                    bulkCopy.BatchSize = 100;

                    bulkCopy.BulkCopyTimeout = 800;

                    foreach (DataColumn dc in _dt.Columns)
                    {

                        if (dc.ColumnName != "Idx")
                        {
                            SqlBulkCopyColumnMapping mapping = new SqlBulkCopyColumnMapping(dc.ColumnName, dc.ColumnName);

                            bulkCopy.ColumnMappings.Add(mapping);
                        }
                    }

                    bulkCopy.DestinationTableName = tableName;

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 20000;

                    conn.Open();

                    bulkCopy.WriteToServer(_dt);

                    return true;
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "TradedeskMyReports.ProcessMyReport:" + processor.DatabaseName);
            }

            return false;
        }

        /// <summary>
        /// This is to compare the total records and the sum of the impressions against the database. 
        /// </summary>
        /// <param name="datatab"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private bool CompareReportDataAgainstDatabaseData(DataTable datatab, string tableName)
        {
            try
            {

                string query = String.Format(@"SELECT count(*) Total, sum([Impressions]) SumImp FROM [dbo].[{0}] where AsOfDate =  '{1}' and  [Advertiser Id] = '{2}'", tableName, datatab.TableName, currentAdvertiser.AdvertiserId);

                if (tableName == "dataTradeDeskConversionMyReportData") // TODO: This is meant to reimported each time
                {

                    reimportData = true;
                    return false;
                }
                     
                DataSet ds = processor.RunQuery(query);

                if (ds != null)
                {
                    if (ds.Tables.Count != 0)
                    {
                        int _sumImp = 0;

                        int total = int.Parse(ds.Tables[0].Rows[0]["Total"].ToString());
                        int sumImp = int.Parse(ds.Tables[0].Rows[0]["SumImp"].ToString());

                        foreach (DataRow dr in datatab.Rows)
                        {
                            if (datatab.Columns.Contains("Impressions"))
                                _sumImp += int.Parse(dr["Impressions"].ToString());
                            else if (datatab.Columns.Contains("Impression Count"))
                                _sumImp += int.Parse(dr["Impression Count"].ToString());
                        }

                        int _total = datatab.Rows.Count;

                        if (total == _total && sumImp == _sumImp)
                            return true;
                    }   
                }

            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }
        private void GetMyReport(dataTradeDeskAdvertiser advertiser)
        {
            DateTime now = System.DateTime.Now.AddDays(-1);
            DateTime date = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);

            DateTime[] reports = null;
            DateTime[] reportsElement = null;
            DateTime[] reportsConversions = null;

            DateTime StartDate = date.AddDays(-7);  // DateTime.Parse("12/28/2016");  //  
            DateTime EndDate = date; //   DateTime.Parse("6/30/2016"); // 

            // if (debug)                StartDate = DateTime.Parse("9/25/2018");

            if (advertiser != null)
                reports = GetReportDates(advertiser, "dataTradeDeskMyReportingData", StartDate);
            if (advertiser.bolDataElements ?? false)
                reportsElement = GetReportDates(advertiser, "dataTradeDeskDataElementMyReportingData", StartDate);
            if (advertiser.bolConversionReport ?? false)
                reportsConversions = GetReportDates(advertiser, "dataTradeDeskConversionMyReportData", StartDate);
             
            using (AuthenticationServiceHandler authenticationUtil = new AuthenticationServiceHandler())
            {
                for (date = StartDate; date.Date <= EndDate.Date; date = date.AddDays(1))  /*    LOOP THROUGH LOGIC */
                {
                    if (date < DateTime.Parse("6/24/2017")) // This date is when we started MyReports
                        continue;  

                    try
                    {
                        Console.ForegroundColor = WhirlygigSolutionHelper.ConsoleWriter.GetRandomConsoleColor();

                        if (advertiser.bolDataElements == true && advertiser.bolConversionReport == true)  // If advertiser is supposed to have all three data sources check and make sure there is data for the date in all three tables
                        {
                            if (reports.Contains(date) && reportsElement.Contains(date) && reportsConversions.Contains(date))
                                continue;
                            if (reports.Contains(date) && reportsElement.Count() == 0 && reportsConversions.Count() == 0)
                                continue;
                        }
                        else if (advertiser.bolDataElements == true)  // If advertiser is supposed to have both data sources check and make sure there is data for the date in both tables
                        {
                            if (reports.Contains(date) && reportsElement.Contains(date))
                                continue;
                            if (reports.Contains(date) && reportsElement.Count() == 0)
                                continue;
                        }
                        else if (advertiser.bolConversionReport == true)  // If advertiser is supposed to have both data sources check and make sure there is data for the date in both tables
                        {
                            if (reports.Contains(date) && reportsConversions.Contains(date))
                                continue;
                            if (reports.Contains(date) && reportsConversions.Count() == 0)
                                continue;
                        }
                        else  // Check if advertiser has data for the date
                        {
                            if (reports.Contains(date))
                                continue;
                        }

                        MyReportResponse jsonResponse = GetResponseFromApi(advertiser, date, authenticationUtil);

                        if (jsonResponse.Result.Count() != 0)
                            ProcessMyReport(advertiser, jsonResponse, authenticationUtil, date, reports, reportsElement, reportsConversions);

                        Console.WriteLine(jsonResponse.Result.Count());
                    }
                    catch (Exception ex)
                    {
                        SolutionHelper.Error.LogError(ex, "GetMyReport:" + advertiser.AdvertiserId + " - " + date.ToString());
                    }

                }   /* LOOP THROUGH LOGIC  */
            }
        }

        /// <summary>
        /// Get response from the api using the advertiser and date
        /// </summary>
        /// <param name="advertiser"></param>
        /// <param name="date"></param>
        /// <param name="authenticationUtil"></param>
        /// <returns></returns>
        private MyReportResponse GetResponseFromApi(dataTradeDeskAdvertiser advertiser, DateTime date, AuthenticationServiceHandler authenticationUtil)
        {
            Console.WriteLine(String.Format("Processing {0}", date));

            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Headers.Add("TTD-Auth", authenticationUtil.UserSecurityToken);

            request.ContentType = "application/json; charset=utf-8";
            request.Method = "POST";

            string[] _AdvertiserId = new string[] { advertiser.AdvertiserId };

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(new
                {
                    AdvertiserIds = _AdvertiserId,
                    ExecutionSpansStartDate = date.ToString(),
                    ExecutionSpansEndDate = date.ToString(),
                    PageStartIndex = "0",
                    PageSize = "10000"
                });

                streamWriter.Write(json);
            }

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var response = (HttpWebResponse)request.GetResponse();
            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(MyReportResponse));
            object objResponse = jsonSerializer.ReadObject(response.GetResponseStream());

            MyReportResponse jsonResponse = objResponse as MyReportResponse;
            return jsonResponse;
        }

        private DateTime[] GetReportDates(dataTradeDeskAdvertiser advertiser, string tableName, DateTime date)
        {
            List<DateTime> dates = new List<DateTime>();

            string query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[{0}] where [Advertiser Id] = '{1}' and  [AsOfDate] >= '{2}'", tableName, advertiser.AdvertiserId.ToString(), date.ToString());

            DataSet ds = processor.RunQuery(query);

            if (ds != null) // Add date to list of date
                if (ds.Tables.Count != 0) // Add date to list of date
                    foreach (DataRow dr in ds.Tables[0].Rows)
                        dates.Add(DateTime.Parse(dr[0].ToString())); // Date is a non nullable date type

            return dates.ToArray();
        }

        private void ProcessMyReport(dataTradeDeskAdvertiser advertiser, MyReportResponse jsonResponse, AuthenticationServiceHandler authenticationUtil, DateTime date, DateTime[] reports, DateTime[] reportsElement, DateTime[] reportsConversions)
        {
            bool dataPerformanceReport = true; // These are meant to not try and reprocess the same import multiple times
            bool dataElementReport = true;
            bool dataElementHistoryReport = true;
            bool dataPerformanceHistoryReport = true;
            bool troubleshoot = false;

            try
            {
                MyReportResponseData[] result = jsonResponse.Result.Where(x => x.ReportScheduleName.ContainsAny("Performance SQL Server", "Conversion Details Report")).ToArray();

                if (debug)
                {
                    if (troubleshoot) // Trouble shooting: Manually change value
                    {
                        result = jsonResponse.Result.ToArray();
                    }
                }

                foreach (MyReportResponseData MyReport in result)
                {
                    string downloadurl = MyReport.ReportDeliveries[0].DownloadURL;

                    if (downloadurl == null)
                        continue;

                    if (MyReport.DisabledReason != null)
                        continue;

                    if (debug)
                    {
                        if (troubleshoot) // Trouble shooting: Manually change value
                        {
                            ExportMyReport(advertiser, authenticationUtil, MyReport, downloadurl, " Testing" + MyReport.ReportScheduleName, date);
                            continue;
                        }
                    }

                    Console.WriteLine(MyReport.ReportScheduleName);


                    if (MyReport.ReportScheduleName.Contains("Conversion Details Report"))
                    {
                        ExportMyReport(advertiser, authenticationUtil, MyReport, downloadurl, MyReport.ReportScheduleName, date);
                        continue; 
                    }

                    if (MyReport.ReportScheduleName.Contains("History | Performance SQL Server") && dataPerformanceHistoryReport)
                    {
                        if (reports.Count() == 0)
                        {
                            ExportMyReport(advertiser, authenticationUtil, MyReport, downloadurl, "Performance SQL Server History", date);
                            dataPerformanceHistoryReport = false;
                        }
                    }

                    if (MyReport.ReportScheduleName.Contains("Yesterday | Performance SQL Server") && dataPerformanceReport)
                    {
                        if (!reports.Contains(date))
                        {
                            ExportMyReport(advertiser, authenticationUtil, MyReport, downloadurl, "Performance SQL Server", date);
                            dataElementReport = false;
                        }
                    }

                    if (advertiser.bolDataElements ?? false)
                    {
                        if (MyReport.ReportScheduleName.Contains("Yesterday | Data Elements Performance SQL Server") && dataElementReport)
                        {
                            if (!reportsElement.Contains(date))
                            {
                                ExportMyReport(advertiser, authenticationUtil, MyReport, downloadurl, "Data Elements Performance SQL Server", date);
                                dataElementReport = false;
                            }
                        }
                    }

                    if (advertiser.bolDataElements ?? false)
                    {
                        if (MyReport.ReportScheduleName.Contains("History | Data Elements Performance SQL Server") && dataElementHistoryReport)
                        {
                            if (!reportsElement.Contains(date))
                            {
                                ExportMyReport(advertiser, authenticationUtil, MyReport, downloadurl, "Data Elements Performance SQL Server", date);
                                dataElementHistoryReport = false;
                            }
                        }
                    } 
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "MyReportsServiceHandler.DataTableToDatabase:" + ex.Message);
            }
        }
        private void ExportMyReport(dataTradeDeskAdvertiser advertiser, AuthenticationServiceHandler authenticationUtil, MyReportResponseData result, string downloadurl, string reportName, DateTime date)
        {
            Stream stream = null;

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(downloadurl);
            req.Headers.Add("TTD-Auth", authenticationUtil.UserSecurityToken);
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

            HttpWebRequest req1 = (HttpWebRequest)WebRequest.Create(resp.ResponseUri);
            req1.Headers.Add("TTD-Auth", authenticationUtil.UserSecurityToken);
            req1.ContentType = "application/octet-stream";
            HttpWebResponse resp1 = (HttpWebResponse)req.GetResponse();

            stream = resp1.GetResponseStream();

            string ext = "";
            string deliveredPath = result.ReportDeliveries[0].DeliveredPath;

            if (deliveredPath.Contains(".tsv"))
                ext = ".tsv";
            if (deliveredPath.Contains(".csv"))
                ext = ".csv";
            if (deliveredPath.Contains(".xlsx"))
                ext = ".xlsx";

            string dateExt = date.ToString().Replace(":", ".").Replace(@"/", ".").ToString();

            if (reportName.ToLower().Contains("history"))
                dateExt = "";

            string locations = String.Format(@"{0}\TradeDesk_{1}_{2}_{3}{4}", csvExport, advertiser.AdvertiserName, reportName.Replace("|", "-"), dateExt, ext);

            try
            {
                if (File.Exists(locations))
                    return;

                FileStream fileStream = File.Create(locations);


                byte[] b = null;

                using (MemoryStream ms = new MemoryStream())
                {
                    int count = 0;
                    do
                    {
                        byte[] buf = new byte[1024];
                        count = stream.Read(buf, 0, 1024);
                        ms.Write(buf, 0, count);
                    } while (stream.CanRead && count > 0);
                    b = ms.ToArray();
                }

                stream.Read(b, 0, b.Length);
                // Use write method to write to the file specified above
                fileStream.Write(b, 0, b.Length);
                fileStream.Close();

            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "HDReportsServiceHandler.DataTableToDatabase:" + ex.Message);
            }

            resp.Close();
            resp1.Close();
        }

        private void DataTableToDatabase(string DataSource, DataTable dt)
        {
            string modifiedDate = System.DateTime.Now.ToString();
            try
            {
                SqlConnection conn = new SqlConnection(processor.ConnectionString);

                dt = CleanData(dt);

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);

                    bulkCopy.BatchSize = 10000;
                    bulkCopy.BulkCopyTimeout = 60;

                    if (dt.Columns.Contains("Date"))
                        dt.Columns["Date"].ColumnName = "AsOfDate";

                    if (!dt.Columns.Contains("ModifiedDate"))
                    {
                        System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                        newColumn.DefaultValue = System.DateTime.Now.ToString();
                        dt.Columns.Add(newColumn);
                    }

                    #region Mappings Region
                    SqlBulkCopyColumnMapping mapping1 = new SqlBulkCopyColumnMapping("AsOfDate", "AsOfDate");
                    SqlBulkCopyColumnMapping mapping2 = new SqlBulkCopyColumnMapping("Partner Id", "Partner Id");
                    SqlBulkCopyColumnMapping mapping3 = new SqlBulkCopyColumnMapping("Advertiser Id", "Advertiser Id");
                    SqlBulkCopyColumnMapping mapping4 = new SqlBulkCopyColumnMapping("Campaign Id", "Campaign Id");
                    SqlBulkCopyColumnMapping mapping5 = new SqlBulkCopyColumnMapping("Ad Group Id", "Ad Group Id");
                    SqlBulkCopyColumnMapping mapping6 = new SqlBulkCopyColumnMapping("Ad Format", "Ad Format");
                    SqlBulkCopyColumnMapping mapping7 = new SqlBulkCopyColumnMapping("Creative Id", "Creative Id");
                    SqlBulkCopyColumnMapping mapping8 = new SqlBulkCopyColumnMapping("Frequency", "Frequency");
                    SqlBulkCopyColumnMapping mapping9 = new SqlBulkCopyColumnMapping("Ad Group Budget", "Ad Group Budget");
                    SqlBulkCopyColumnMapping mapping10 = new SqlBulkCopyColumnMapping("Ad Group Imps Budget", "Ad Group Imps Budget");
                    SqlBulkCopyColumnMapping mapping11 = new SqlBulkCopyColumnMapping("Ad Group Daily Target", "Ad Group Daily Target");
                    SqlBulkCopyColumnMapping mapping12 = new SqlBulkCopyColumnMapping("Ad Group Daily Imps Target", "Ad Group Daily Imps Target");
                    SqlBulkCopyColumnMapping mapping13 = new SqlBulkCopyColumnMapping("Ad Group Daily Cap", "Ad Group Daily Cap");
                    SqlBulkCopyColumnMapping mapping14 = new SqlBulkCopyColumnMapping("Ad Group Daily Imps Cap", "Ad Group Daily Imps Cap");
                    SqlBulkCopyColumnMapping mapping15 = new SqlBulkCopyColumnMapping("Ad Group Base Bid CPM", "Ad Group Base Bid CPM");
                    SqlBulkCopyColumnMapping mapping16 = new SqlBulkCopyColumnMapping("Bids", "Bids");
                    SqlBulkCopyColumnMapping mapping17 = new SqlBulkCopyColumnMapping("Bid Amount", "Bid Amount");
                    SqlBulkCopyColumnMapping mapping18 = new SqlBulkCopyColumnMapping("Imps", "Imps");
                    SqlBulkCopyColumnMapping mapping19 = new SqlBulkCopyColumnMapping("Clicks", "Clicks");
                    SqlBulkCopyColumnMapping mapping20 = new SqlBulkCopyColumnMapping("PC 1", "PC 1");
                    SqlBulkCopyColumnMapping mapping21 = new SqlBulkCopyColumnMapping("PC 2", "PC 2");
                    SqlBulkCopyColumnMapping mapping22 = new SqlBulkCopyColumnMapping("PC 3", "PC 3");
                    SqlBulkCopyColumnMapping mapping23 = new SqlBulkCopyColumnMapping("PC 4", "PC 4");
                    SqlBulkCopyColumnMapping mapping24 = new SqlBulkCopyColumnMapping("PC 5", "PC 5");
                    SqlBulkCopyColumnMapping mapping25 = new SqlBulkCopyColumnMapping("PC 6", "PC 6");
                    SqlBulkCopyColumnMapping mapping26 = new SqlBulkCopyColumnMapping("PI 1", "PI 1");
                    SqlBulkCopyColumnMapping mapping27 = new SqlBulkCopyColumnMapping("PI 2", "PI 2");
                    SqlBulkCopyColumnMapping mapping28 = new SqlBulkCopyColumnMapping("PI 3", "PI 3");
                    SqlBulkCopyColumnMapping mapping29 = new SqlBulkCopyColumnMapping("PI 4", "PI 4");
                    SqlBulkCopyColumnMapping mapping30 = new SqlBulkCopyColumnMapping("PI 5", "PI 5");
                    SqlBulkCopyColumnMapping mapping31 = new SqlBulkCopyColumnMapping("PI 6", "PI 6");
                    SqlBulkCopyColumnMapping mapping32 = new SqlBulkCopyColumnMapping("PC 1 Rev", "PC 1 Rev");
                    SqlBulkCopyColumnMapping mapping33 = new SqlBulkCopyColumnMapping("PC 2 Rev", "PC 2 Rev");
                    SqlBulkCopyColumnMapping mapping34 = new SqlBulkCopyColumnMapping("PC 3 Rev", "PC 3 Rev");
                    SqlBulkCopyColumnMapping mapping35 = new SqlBulkCopyColumnMapping("PC 4 Rev", "PC 4 Rev");
                    SqlBulkCopyColumnMapping mapping36 = new SqlBulkCopyColumnMapping("PC 5 Rev", "PC 5 Rev");
                    SqlBulkCopyColumnMapping mapping37 = new SqlBulkCopyColumnMapping("PC 6 Rev", "PC 6 Rev");
                    SqlBulkCopyColumnMapping mapping38 = new SqlBulkCopyColumnMapping("PI 1 Rev", "PI 1 Rev");
                    SqlBulkCopyColumnMapping mapping39 = new SqlBulkCopyColumnMapping("PI 2 Rev", "PI 2 Rev");
                    SqlBulkCopyColumnMapping mapping40 = new SqlBulkCopyColumnMapping("PI 3 Rev", "PI 3 Rev");
                    SqlBulkCopyColumnMapping mapping41 = new SqlBulkCopyColumnMapping("PI 4 Rev", "PI 4 Rev");
                    SqlBulkCopyColumnMapping mapping42 = new SqlBulkCopyColumnMapping("PI 5 Rev", "PI 5 Rev");
                    SqlBulkCopyColumnMapping mapping43 = new SqlBulkCopyColumnMapping("PI 6 Rev", "PI 6 Rev");
                    SqlBulkCopyColumnMapping mapping44 = new SqlBulkCopyColumnMapping("PC 1 Cur", "PC 1 Cur");
                    SqlBulkCopyColumnMapping mapping45 = new SqlBulkCopyColumnMapping("PC 2 Cur", "PC 2 Cur");
                    SqlBulkCopyColumnMapping mapping46 = new SqlBulkCopyColumnMapping("PC 3 Cur", "PC 3 Cur");
                    SqlBulkCopyColumnMapping mapping47 = new SqlBulkCopyColumnMapping("PC 4 Cur", "PC 4 Cur");
                    SqlBulkCopyColumnMapping mapping48 = new SqlBulkCopyColumnMapping("PC 5 Cur", "PC 5 Cur");
                    SqlBulkCopyColumnMapping mapping49 = new SqlBulkCopyColumnMapping("PC 6 Cur", "PC 6 Cur");
                    SqlBulkCopyColumnMapping mapping50 = new SqlBulkCopyColumnMapping("PI 1 Cur", "PI 1 Cur");
                    SqlBulkCopyColumnMapping mapping51 = new SqlBulkCopyColumnMapping("PI 2 Cur", "PI 2 Cur");
                    SqlBulkCopyColumnMapping mapping52 = new SqlBulkCopyColumnMapping("PI 3 Cur", "PI 3 Cur");
                    SqlBulkCopyColumnMapping mapping53 = new SqlBulkCopyColumnMapping("PI 4 Cur", "PI 4 Cur");
                    SqlBulkCopyColumnMapping mapping54 = new SqlBulkCopyColumnMapping("PI 5 Cur", "PI 5 Cur");
                    SqlBulkCopyColumnMapping mapping55 = new SqlBulkCopyColumnMapping("PI 6 Cur", "PI 6 Cur");
                    SqlBulkCopyColumnMapping mapping56 = new SqlBulkCopyColumnMapping("TTD Total Cost", "TTD Total Cost");
                    SqlBulkCopyColumnMapping mapping57 = new SqlBulkCopyColumnMapping("Partner Total Cost", "Partner Total Cost");
                    SqlBulkCopyColumnMapping mapping58 = new SqlBulkCopyColumnMapping("Advertiser Total Cost", "Advertiser Total Cost");
                    SqlBulkCopyColumnMapping mapping59 = new SqlBulkCopyColumnMapping("VideoEventCreativeView", "VideoEventCreativeView");
                    SqlBulkCopyColumnMapping mapping60 = new SqlBulkCopyColumnMapping("VideoEventStart", "VideoEventStart");
                    SqlBulkCopyColumnMapping mapping61 = new SqlBulkCopyColumnMapping("VideoEventFirstQuarter", "VideoEventFirstQuarter");
                    SqlBulkCopyColumnMapping mapping62 = new SqlBulkCopyColumnMapping("VideoEventMidpoint", "VideoEventMidpoint");
                    SqlBulkCopyColumnMapping mapping63 = new SqlBulkCopyColumnMapping("VideoEventThirdQuarter", "VideoEventThirdQuarter");
                    SqlBulkCopyColumnMapping mapping64 = new SqlBulkCopyColumnMapping("VideoEventComplete", "VideoEventComplete");
                    SqlBulkCopyColumnMapping mapping65 = new SqlBulkCopyColumnMapping("VideoEventMute", "VideoEventMute");
                    SqlBulkCopyColumnMapping mapping66 = new SqlBulkCopyColumnMapping("VideoEventUnmute", "VideoEventUnmute");
                    SqlBulkCopyColumnMapping mapping67 = new SqlBulkCopyColumnMapping("VideoEventPause", "VideoEventPause");
                    SqlBulkCopyColumnMapping mapping68 = new SqlBulkCopyColumnMapping("VideoEventResume", "VideoEventResume");
                    SqlBulkCopyColumnMapping mapping69 = new SqlBulkCopyColumnMapping("VideoEventFullscreen", "VideoEventFullscreen");
                    SqlBulkCopyColumnMapping mapping70 = new SqlBulkCopyColumnMapping("VideoEventError", "VideoEventError");
                    SqlBulkCopyColumnMapping mapping71 = new SqlBulkCopyColumnMapping("VideoEventSkip", "VideoEventSkip");
                    SqlBulkCopyColumnMapping mapping72 = new SqlBulkCopyColumnMapping("VideoEventEngagedView", "VideoEventEngagedView");
                    SqlBulkCopyColumnMapping mapping73 = new SqlBulkCopyColumnMapping("VideoEventRewind", "VideoEventRewind");
                    SqlBulkCopyColumnMapping mapping74 = new SqlBulkCopyColumnMapping("VideoEventExpand", "VideoEventExpand");
                    SqlBulkCopyColumnMapping mapping75 = new SqlBulkCopyColumnMapping("VideoEventCollapse", "VideoEventCollapse");
                    SqlBulkCopyColumnMapping mapping76 = new SqlBulkCopyColumnMapping("TWD 5 Rev", "TWD 5 Rev");
                    SqlBulkCopyColumnMapping mapping77 = new SqlBulkCopyColumnMapping("TWD 6 Rev", "TWD 6 Rev");
                    SqlBulkCopyColumnMapping mapping78 = new SqlBulkCopyColumnMapping("Deal Code", "Deal Code");
                    SqlBulkCopyColumnMapping mapping79 = new SqlBulkCopyColumnMapping("AdServer Name", "AdServer Name");
                    SqlBulkCopyColumnMapping mapping80 = new SqlBulkCopyColumnMapping("AdServer Creative Placement Id", "AdServer Creative Placement Id");
                    SqlBulkCopyColumnMapping mapping81 = new SqlBulkCopyColumnMapping("CreativeIsTrackable", "CreativeIsTrackable");
                    SqlBulkCopyColumnMapping mapping82 = new SqlBulkCopyColumnMapping("CreativeWasViewable", "CreativeWasViewable");
                    SqlBulkCopyColumnMapping mapping83 = new SqlBulkCopyColumnMapping("ModifiedDate", "Modified Date");

                    bulkCopy.ColumnMappings.Add(mapping1);
                    bulkCopy.ColumnMappings.Add(mapping2);
                    bulkCopy.ColumnMappings.Add(mapping3);
                    bulkCopy.ColumnMappings.Add(mapping4);
                    bulkCopy.ColumnMappings.Add(mapping5);
                    bulkCopy.ColumnMappings.Add(mapping9);
                    bulkCopy.ColumnMappings.Add(mapping10);
                    bulkCopy.ColumnMappings.Add(mapping11);
                    bulkCopy.ColumnMappings.Add(mapping12);
                    bulkCopy.ColumnMappings.Add(mapping13);
                    bulkCopy.ColumnMappings.Add(mapping14);
                    bulkCopy.ColumnMappings.Add(mapping15);
                    bulkCopy.ColumnMappings.Add(mapping16);
                    bulkCopy.ColumnMappings.Add(mapping17);
                    bulkCopy.ColumnMappings.Add(mapping18);
                    bulkCopy.ColumnMappings.Add(mapping19);
                    bulkCopy.ColumnMappings.Add(mapping20);
                    bulkCopy.ColumnMappings.Add(mapping21);
                    bulkCopy.ColumnMappings.Add(mapping22);
                    bulkCopy.ColumnMappings.Add(mapping23);
                    bulkCopy.ColumnMappings.Add(mapping24);
                    bulkCopy.ColumnMappings.Add(mapping25);
                    bulkCopy.ColumnMappings.Add(mapping26);
                    bulkCopy.ColumnMappings.Add(mapping27);
                    bulkCopy.ColumnMappings.Add(mapping28);
                    bulkCopy.ColumnMappings.Add(mapping29);
                    bulkCopy.ColumnMappings.Add(mapping30);
                    bulkCopy.ColumnMappings.Add(mapping31);
                    bulkCopy.ColumnMappings.Add(mapping32);
                    bulkCopy.ColumnMappings.Add(mapping33);
                    bulkCopy.ColumnMappings.Add(mapping34);
                    bulkCopy.ColumnMappings.Add(mapping35);
                    bulkCopy.ColumnMappings.Add(mapping36);
                    bulkCopy.ColumnMappings.Add(mapping37);
                    bulkCopy.ColumnMappings.Add(mapping38);
                    bulkCopy.ColumnMappings.Add(mapping39);
                    bulkCopy.ColumnMappings.Add(mapping40);
                    bulkCopy.ColumnMappings.Add(mapping41);
                    bulkCopy.ColumnMappings.Add(mapping42);
                    bulkCopy.ColumnMappings.Add(mapping43);
                    bulkCopy.ColumnMappings.Add(mapping44);
                    bulkCopy.ColumnMappings.Add(mapping45);
                    bulkCopy.ColumnMappings.Add(mapping46);
                    bulkCopy.ColumnMappings.Add(mapping47);
                    bulkCopy.ColumnMappings.Add(mapping48);
                    bulkCopy.ColumnMappings.Add(mapping49);
                    bulkCopy.ColumnMappings.Add(mapping50);
                    bulkCopy.ColumnMappings.Add(mapping51);
                    bulkCopy.ColumnMappings.Add(mapping52);
                    bulkCopy.ColumnMappings.Add(mapping53);
                    bulkCopy.ColumnMappings.Add(mapping54);
                    bulkCopy.ColumnMappings.Add(mapping55);
                    bulkCopy.ColumnMappings.Add(mapping56);
                    bulkCopy.ColumnMappings.Add(mapping57);
                    bulkCopy.ColumnMappings.Add(mapping58);
                    bulkCopy.ColumnMappings.Add(mapping59);
                    bulkCopy.ColumnMappings.Add(mapping60);
                    bulkCopy.ColumnMappings.Add(mapping61);
                    bulkCopy.ColumnMappings.Add(mapping62);
                    bulkCopy.ColumnMappings.Add(mapping63);
                    bulkCopy.ColumnMappings.Add(mapping64);
                    bulkCopy.ColumnMappings.Add(mapping65);
                    bulkCopy.ColumnMappings.Add(mapping66);
                    bulkCopy.ColumnMappings.Add(mapping67);
                    bulkCopy.ColumnMappings.Add(mapping68);
                    bulkCopy.ColumnMappings.Add(mapping69);
                    bulkCopy.ColumnMappings.Add(mapping70);
                    bulkCopy.ColumnMappings.Add(mapping71);
                    bulkCopy.ColumnMappings.Add(mapping72);
                    bulkCopy.ColumnMappings.Add(mapping73);
                    bulkCopy.ColumnMappings.Add(mapping74);
                    bulkCopy.ColumnMappings.Add(mapping75);
                    bulkCopy.ColumnMappings.Add(mapping76);
                    bulkCopy.ColumnMappings.Add(mapping77);
                    bulkCopy.ColumnMappings.Add(mapping81);
                    bulkCopy.ColumnMappings.Add(mapping82);
                    bulkCopy.ColumnMappings.Add(mapping83);

                    if (DataSource == "dataTradeDeskDataElementReportingData")
                    {
                        SqlBulkCopyColumnMapping mapping84 = new SqlBulkCopyColumnMapping("THIRD PARTY DATA BRAND NAME", "Third Party Data Brand Name");
                        SqlBulkCopyColumnMapping mapping85 = new SqlBulkCopyColumnMapping("DATA ELEMENT ID", "Data Element Id");
                        SqlBulkCopyColumnMapping mapping86 = new SqlBulkCopyColumnMapping("DATA NAME", "Data Name");
                        bulkCopy.ColumnMappings.Add(mapping84);
                        bulkCopy.ColumnMappings.Add(mapping85);
                        bulkCopy.ColumnMappings.Add(mapping86);
                    }
                    else
                    {
                        bulkCopy.ColumnMappings.Add(mapping6);
                        bulkCopy.ColumnMappings.Add(mapping7);
                        bulkCopy.ColumnMappings.Add(mapping8);
                        bulkCopy.ColumnMappings.Add(mapping78);
                        bulkCopy.ColumnMappings.Add(mapping79);
                        bulkCopy.ColumnMappings.Add(mapping80);
                    }
                    #endregion

                    bulkCopy.DestinationTableName = DataSource;

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 5000;

                    conn.Open();

                    bulkCopy.WriteToServer(dt);
                }
            }
            catch (Exception ex)
            {
                //TODO: Delete record inserted if there was an error.
                SolutionHelper.Error.LogError(ex, "HDReportsServiceHandler.DataTableToDatabase:" + processor.DatabaseName + " Modified Date: " + modifiedDate);
            }
        }

        /// <summary>
        /// Check if all the columns are created in the database for the reporting table. If the column doesnt exist it will be added.
        /// </summary>
        /// <param name="dt"></param>
        private void ColumnChecker(string datasource, DataTable dt)
        {
            string query = "";
            query += "DECLARE @columns varchar(8000) ";
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(8000), COLUMN_NAME) ";
            query += "FROM (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" + datasource + "') as tab ";
            query += "select @columns ";

            DataSet ds = processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString();

            string[] columnCol = columns.ToLower().Split(',');

            if (dt.Columns.Contains("Date"))
                dt.Columns["Date"].ColumnName = "AsOfDate";

            foreach (DataColumn dc in dt.Columns)
            {
                //  dc.ColumnName = dc.ColumnName.ToPascalCase();

                if (!columnCol.Contains(dc.ColumnName.ToLower()))
                    AddColumn(datasource, dc.ColumnName);
            }

            DateTime today = System.DateTime.Now;

            if (!dt.Columns.Contains("ModifiedDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                newColumn.DefaultValue = today.ToString("d");
                dt.Columns.Add(newColumn);
            }
        }

        /// <summary>
        /// Due to the columns needing to be dynamic this method is designed to add additional column to the table
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        private void AddColumn(string table, string column)
        {
            string queryOriginal = "ALTER TABLE {0} ADD [{1}] {2}";
            string query = queryOriginal;

            if (column.ToUpper().ContainsAny("CLICKS",
                "POST-IMPRESSION CONVERSIONS",
                "IMPRESSIONS",
                "POST IMP",
                "POST-CLICK CONVERSIONS",
                "CONVERSION",
                "TOTAL CONVERSIONS",
                "INTEGER ID",
                "VIDEO STARTED",
                "FREQUENCY",
                "VIDEO FULL PLAY",
                "BIDS",
                "PLAYER",
                "CREATIVE DURATION IN SECONDS",
                "AD SERVER CREATIVE PLACEMENT ID") && !column.ToUpper().ContainsAny("REVENUE", "COST (USD)"))
                query = string.Format(query, table, column, "[int] NULL");
            else if (column.ToUpper().ContainsAny("VIEWABILITY RATE", "%", "VIDEO FULLY PLAYED RATE", "VIDEO FULLY PLAYED RATE", "AMOUNT", "PROFIT", "COST", "CURRENCY", "REVENUE", "COST (USD)"))
                query = string.Format(query, table, column, "decimal(38, 28) NULL");
            else if (column.ToUpper().ContainsAny("CREATIVE ID", "TIMEZONE", "AD GROUP ID", "VIDEO FULLY PLAYED RATE", "VIDEO FULLY PLAYED RATE", "MEDIA TYPE", "DEVICE TYPE", "PARTNER ID", "ADVERTISER CURRENCY CODE", "PARTNER CURRENCY CODE"))
                query = string.Format(query, table, column, "[varchar](50)");
            else if (column.ToUpper().ContainsAny("REPORT HOUR"))
                query = string.Format(query, table, column, "[DateTime] NULL");
            else
                query = string.Format(query, table, column, "[varchar](250)");

            if (column.ToUpper().ContainsAny("ADVERTISER CURRENCY CODE", "PARTNER CURRENCY CODE"))
                query = string.Format(queryOriginal, table, column, "[varchar](50)");

            if (column.ToUpper().ContainsAny("Click Conversion Revenue".ToUpper(), "View Through Conversion".ToUpper()))
                query = string.Format(queryOriginal, table, column, "decimal(18, 5) NULL");

            processor.RunQuery(query);
        }

        private DataTable CleanData(DataTable dt)
        {

            #region CleanData
            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn column in dt.Columns)
                {
                    if (column.ColumnName.ToUpper().ContainsAny("Conversion Time".ToUpper(), "Conversion Time".ToUpper(), "Conversion Id".ToUpper()))
                        continue;

                    if (column.ColumnName.ToUpper().ContainsAny("CLICKS",
                        "POST-IMPRESSION CONVERSIONS",
                        "IMPRESSIONS",
                        "POST IMP",
                        "POST-CLICK CONVERSIONS",
                        "CONVERSION",
                        "TOTAL CONVERSIONS",
                        "INTEGER ID",
                        "VIDEO STARTED",
                        "FREQUENCY",
                        "VIDEO FULL PLAY",
                        "BIDS",
                        "PLAYER",
                        "Total Click + View Conversions".ToUpper(),
                        "Conversion Touch".ToUpper(),
                        "View Through Conversion Revenue".ToUpper(),
                        "View Through Conversion".ToUpper(),
                        "Click Conversion Revenue".ToUpper(),
                        "Click Conversion".ToUpper(),
                        "Not Charged Data Impressions".ToUpper(),
                        "Charged Data Impressions".ToUpper(),
                        "Clicks".ToUpper(),
                        "Impressions".ToUpper(),
                        "Creative Integer Id".ToUpper(),
                        "Campaign Integer Id".ToUpper(),
                        "Ad Server Creative Placement Id".ToUpper(),
                        "Ad Group Integer Id".ToUpper(),
                        "Advertiser Integer Id".ToUpper(),
                        "CREATIVE DURATION IN SECONDS",
                        "AD SERVER CREATIVE PLACEMENT ID") && !column.ColumnName.ToUpper().ContainsAny("REVENUE", "COST (USD)", "Conversion Device Type".ToUpper(), "CONVERSION REFERRAL URL".ToUpper()))
                    {
                        if (dr[column.ColumnName].ToString() == "")
                        {
                            dr[column.ColumnName] = 0;
                        }
                         
                        try
                        {
                            string value = dr[column.ColumnName].ToString();
                            int.Parse(dr[column.ColumnName].ToString());

                        }
                        catch (Exception ex)
                        {
                            string val = "";
                            if (company.Id == 7 && column.ColumnName == "Ad Server Creative Placement Id")
                                val = "This column exception is a varchar and doesn't require validation.";
                            else
                            {
                                val = dr[column.ColumnName].ToString();
                                throw;
                            }
                        }
                    }
                    else if (column.ColumnName.ToUpper().ContainsAny("VIEWABILITY RATE", "%", "VIDEO FULLY PLAYED RATE", "VIDEO FULLY PLAYED RATE",
                                                                                    "AMOUNT", "PROFIT", "COST", "CURRENCY", "REVENUE", "COST (USD)"))
                    {
                        if (column.ColumnName.ToUpper().ContainsAny("ADVERTISER CURRENCY CODE", "PARTNER CURRENCY CODE"))
                            continue;

                        if (dr[column.ColumnName].ToString() == "")
                        {
                            dr[column.ColumnName] = 0;
                        }
                        else
                        {
                            if (dr[column.ColumnName].ToString().Contains("E-"))
                            {
                                dr[column.ColumnName] = Decimal.Parse(dr[column.ColumnName].ToString(), System.Globalization.NumberStyles.Float).ToString();// dr[column.ColumnName].ToString();
                            }
                        }

                        try
                        {
                            Decimal.Parse(dr[column.ColumnName].ToString());
                        }
                        catch (Exception ex)
                        {
                            string val = dr[column.ColumnName].ToString();
                            // throw;
                        }
                    }


                }
            }
            #endregion

            return dt;
        }

        void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(String.Format("{0} Rows have been copied.", e.RowsCopied.ToString()));
        }

        /// <summary>
        /// Get company based on the advertiser and where or not the advertiser has been linked to a company in the lavidge database.
        /// </summary>
        /// <param name="advertiser"></param>
        /// <returns></returns>
        private void GetCompany(dataTradeDeskAdvertiser advertiser)
        {
            Reset();

            if (advertiser.CompanyId != null)
            {
                //  Company com = (from m in db.Companies where m.IsActive == true select m).Where(x => x.Id == advertiser.CompanyId).FirstOrDefault();
                Company com = (from m in db.Companies select m).Where(x => x.Id == advertiser.CompanyId).FirstOrDefault();

                if (com != null)
                {
                    Data _processor = new Data(com.DatabaseName, com.Login, com.Password, com.Server);
                    this.processor = _processor;
                    company = com;
                }
            }
        }

        /// <summary>
        /// Get company based on the advertiser and where or not the advertiser has been linked to a company in the lavidge database.
        /// </summary>
        /// <param name="advertiser"></param>
        /// <returns></returns>
        private void GetCompany(string advertiserName)
        {
            Reset();

            try
            {
                currentAdvertiser = (from m in db.dataTradeDeskAdvertisers where m.AdvertiserName == advertiserName select m).FirstOrDefault();

                Company com = (from m in db.Companies where m.IsActive == true select m).Where(x => x.Id == currentAdvertiser.CompanyId).FirstOrDefault();

                if (com != null)
                {
                    Data _processor = new Data(com.DatabaseName, com.Login, com.Password, com.Server);
                    processor = _processor;
                    company = com;
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "MYReportsServiceHandler.GetCompany:" + ex.Message);
            }
        }

        private void Reset()
        {
            company = null;
            processor = null;
            currentAdvertiser = null;
            processFile = false;
            deleteFile = false;
            tableName = "";
            reimportData = false; 
        }
    }
}


