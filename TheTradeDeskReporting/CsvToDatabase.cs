﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhirlygigSolutionHelper;
using Microsoft.VisualBasic.FileIO;

namespace TheTradeDeskReporting
{
    public class CsvToDatabase
    {
        public string _source { get; set; }
        public string _tablename { get; set; }
        public Data _processor { get; set; }
        public CsvToDatabase(string tablename, string source, Data processor)
        {
            _processor = processor;
            _tablename = tablename;
            _source = source;
        }

        public bool Run()
        {
            try
            {
                DataTable dt = GetDataTabletFromCSVFile(_source);

                using (SqlConnection dbConnection = new SqlConnection(_processor.ConnectionString))
                {
                    dbConnection.Open();
                    using (SqlBulkCopy s = new SqlBulkCopy(dbConnection))
                    {
                        s.DestinationTableName = _tablename;
                        foreach (var column in dt.Columns)
                            s.ColumnMappings.Add(column.ToString(), column.ToString());
                        s.WriteToServer(dt);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }


            return true;
        }

        public static DataTable GetDataTabletFromTSVFile(string csv_file_path)
        {
            return GetDataTabletFromFile(csv_file_path, "\t"); 
        }
        public static DataTable GetDataTabletFromCSVFile(string csv_file_path)
        {
            return GetDataTabletFromFile(csv_file_path, ",");
        }
        private static DataTable GetDataTabletFromFile(string csv_file_path, string delimiter)
        {
            DataTable csvData = new DataTable();
            try
            {
                using (TextFieldParser csvReader = new TextFieldParser(csv_file_path))
                {
                    csvReader.SetDelimiters(new string[] { delimiter });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    string[] colFields = csvReader.ReadFields();

                    foreach (string column in colFields)
                    {
                        DataColumn datecolumn = new DataColumn(column);
                        datecolumn.AllowDBNull = true;
                        csvData.Columns.Add(datecolumn);
                    }

                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();
                        //Making empty value as null
                        for (int i = 0; i < fieldData.Length; i++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = null;
                            }
                        }

                        csvData.Rows.Add(fieldData);
                    }
                }
            }
            catch (Exception ex)
            { 
                string dido = "";
            }
            return csvData;
        }
    }
}
