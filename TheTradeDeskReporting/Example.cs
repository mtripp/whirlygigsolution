﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace TheTradeDeskReporting
{
    class User
    {
        public string Login { get; set; } 
        public string Password { get; set; }
    }

    public class Example
    {

        public static void Main2()
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            using (var client = new HttpClient())
            {
                string basi = @"https://apisb.thetradedesk.com/v3/authentication";
                client.BaseAddress = new Uri(basi);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // HTTP GET
                HttpResponseMessage response = await client.GetAsync(client.BaseAddress);

                //if (response.IsSuccessStatusCode)
                //{
                //    User product = await response.Content.ReadAsAsync<User>();
                //    Console.WriteLine("{0}\t${1}\t{2}", product.Name, product.Price, product.Category);
                //}

                // HTTP POST
                var gizmo = new User() { Login = "ttd_api_doc_access@lavidge.com", Password = "^C_ipT!qhP^TKE" };

                response = await client.PostAsJsonAsync(client.BaseAddress, gizmo);

                if (response.IsSuccessStatusCode)
                {
                    Uri gizmoUrl = response.Headers.Location;
                     
                    response = await client.PutAsJsonAsync(gizmoUrl, gizmo);

                    // HTTP DELETE
                    response = await client.DeleteAsync(gizmoUrl);
                }
            }
        }
    }
}
