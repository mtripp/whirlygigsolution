﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;

namespace TheTradeDeskReporting.Models
{ 
    public class AdGroupFlightCollection
    {
        public AdGroupFlightCollection(dynamic flights)
        {
            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            string json = Json.Encode(flights);

            this.AdGroupFlights = serializer.Deserialize<List<AdGroupFlight>>(json).ToArray();
        }
        public AdGroupFlight[] AdGroupFlights { get; set; }
    }
    public class CampaignFlightCollection
    {
        public CampaignFlightCollection(dynamic flights)
        {
            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            string json = Json.Encode(flights);

            this.CampaignFlights = serializer.Deserialize<List<CampaignFlight>>(json).ToArray();
        }
        public CampaignFlight[] CampaignFlights { get; set; }
    }
    public class CampaignFlight
    {
        public string CampaignId { get; set; }
        public int CampaignFlightId { get; set; }
        public decimal DailyTargetInAdvertiserCurrency { get; set; }
        public decimal BudgetInAdvertiserCurrency { get; set; }
        public string BudgetInImpressions { get; set; }
        public string DailyTargetInImpressions { get; set; }
        public DateTime StartDateInclusiveUTC { get; set; }
        public DateTime EndDateExclusiveUTC { get; set; }
    }
    public class AdGroupFlight
    {
        public string AdGroupId { get; set; }
        public int CampaignFlightId { get; set; }
        public decimal DailyTargetInAdvertiserCurrency { get; set; }
        public decimal BudgetInAdvertiserCurrency { get; set; }
        public string BudgetInImpressions { get; set; }
        public string DailyTargetInImpressions { get; set; }
    }
}
