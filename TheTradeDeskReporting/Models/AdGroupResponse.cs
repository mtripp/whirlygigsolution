﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheTradeDeskReporting
{
    public class AdGroupResponse
    {
        public AdGroupResponseData[] RTBAttributes { get; set; }
        public AdGroupResponseData[] Result { get; set; }
        public string ResultCount { get; set; }
        public string TotalFilteredCount { get; set; }
        public string TotalUnfilteredCount { get; set; }
        public string AdvertiserId { get; set; }
        public string AdGroupId { get; set; }
        public string AdGroupName { get; set; }
        public string Description { get; set; }
        public string CampaignId { get; set; }
        public string IsEnabled { get; set; }
        public string IndustryCategoryId { get; set; }
        public string AdgroupObject { get; set; }
        public dynamic AdgroupDynamicObject { get; set; }
    }
    public class AdGroupResponseData
    {
        public string BudgetSettings { get; set; }
        public string Budget { get; set; }
        public string AdGroupId { get; set; }
        public string AdGroupName { get; set; }
        public string CampaignId { get; set; }
    }
}
