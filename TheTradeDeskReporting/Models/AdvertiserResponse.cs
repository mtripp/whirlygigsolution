﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheTradeDeskReporting
{
    public class AdvertiserResponse 
    {
        public AdvertiserResponseData[] Result { get; set; }
        public string ResultCount { get; set; }
        public string TotalFilteredCount { get; set; }
        public string TotalUnfilteredCount { get; set; }
        public string AdvertiserObject { get; set; }
        public dynamic AdvertiserDynamicObject { get; set; }
    } 
    public class AdvertiserResponseData
    {
        public string PartnerId { get; set; }
        public string AdvertiserId { get; set; }
        public string AdvertiserName { get; set; }
        public string DomainAddress { get; set; } 
        public int CompanyId { get; set; } 
        public bool Active { get; set; }
        public bool bolDataElements { get; set; }
        public bool bolMyReports { get; set; }
        public bool bolConversionReport { get; set; }
        public string Advertiser_Object { get; set; }
        public string Conversion_Success_tags { get; set; }
        public string Action_Tags { get; set; }
    }
}
