﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheTradeDeskReporting
{
    public class CampaignResponse 
    {
        public CampaignResponseData[] Result { get; set; }
        public string ResultCount { get; set; }
        public string TotalFilteredCount { get; set; }
        public string TotalUnfilteredCount { get; set; }
        public string CampaignObject { get; set; }
        public dynamic CampaignDynamicObject { get; set; }
    }
    public class CampaignResponseData
    {
        public string CampaignId { get; set; }
        public string CampaignName { get; set; } 
        public dynamic CampaignDynamicObject { get; internal set; }
        public string CampaignObject { get; internal set; }
        public string AdvertiserId { get; set; }
    }
}
