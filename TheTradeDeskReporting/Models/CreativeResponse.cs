﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheTradeDeskReporting
{
    public class CreativeResponse 
    {
        public CreativeResponseData[] Result { get; set; }
        public string ResultCount { get; set; }
        public string TotalFilteredCount { get; set; }
        public string TotalUnfilteredCount { get; set; }
        public string AdvertiserId { get; set; }
        public string CreativeId { get; set; }
        public string CreativeName { get; set; }
        public string Description { get; set; }
    }
    public class CreativeResponseData
    {
        public string CreativeId { get; set; }
        public string CreativeName { get; set; } 
    }
}
