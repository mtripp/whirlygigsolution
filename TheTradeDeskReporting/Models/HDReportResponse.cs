﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheTradeDeskReporting
{
    public class HDReportResponse
    {
        public HDReportResponseData[] Result { get; set; }
        public string ResultCount { get; set; }
        public string TotalFilteredCount { get; set; }
        public string TotalUnfilteredCount { get; set; }
    }

    public class HDReportResponseData
    {
        public string AdvertiserId { get; set; }
        public string Scope { get; set; }
        public string Type { get; set; }
        public string Duration { get; set; }
        public string DownloadUrl { get; set; }
        public string DownloadUrlExpirationDateUTC { get; set; }
    }
    public class MyReportResponse
    {
        public MyReportResponseData[] Result { get; set; }
        public string ResultCount { get; set; }
        public string TotalFilteredCount { get; set; }
        public string TotalUnfilteredCount { get; set; }
    }

    public class MyReportResponseData
    {
        public string AdvertiserId { get; set; }
        public string Scope { get; set; }
        public string Type { get; set; }
        public string ReportExecutionId { get; set; }
        public string ReportExecutionState { get; set; }
        public string LastStateChangeUTC { get; set; }
        public string DisabledReason { get; set; }
        public string Timezone { get; set; }
        public string ReportStartDateInclusive { get; set; }
        public string ReportEndDateExclusive { get; set; }
        public string ReportScheduleName { get; set; } 
        public MyReportResponseSubData[] ReportDeliveries { get; set; } 
    }
    public class MyReportResponseSubData
    {
        public string ReportDestination { get; set; }
        public string DeliveredPath { get; set; }
        public string DeliveredUTC { get; set; }
        public string DownloadURL { get; set; } 
        public string DownloadUrlExpirationDateUTC { get; set; }
    }
}
