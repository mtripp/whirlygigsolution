﻿using OpenPop.Mime;
using OpenPop.Pop3;
using SolutionHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using WhirlygigSolutionHelper;

namespace TheTradeDeskReporting
{
    public class ProcessingEmail
    {
        Data Processor = new Data();
        mediaTradeDeskEntities1 db = new mediaTradeDeskEntities1();
        Company company = new Company();

        string server;
        string accountname;
        string accountpassword;
        bool duplicate;
        bool reimportData;
        public bool completeFile;
        public bool deleteMe;
        string csvExport;

        private string tablename = "dataTradeDeskMyReportingData";

        public ProcessingEmail()
        {
            server = ConfigurationManager.AppSettings["EmailAccountServer"];
            accountname = ConfigurationManager.AppSettings["EmailAccountName"];
            accountpassword = ConfigurationManager.AppSettings["EmailAccountPassword"];
            csvExport = ConfigurationManager.AppSettings["csvExport"];
        }

        public void Run()
        {
            ProcessTDDEmailReport();
        }

        private void ProcessTDDEmailReport()
        {
            try
            {
                restart: // This goto was implemented due to a Pop3Client timeout. Connection to the mail server needs to be reauthenticated between each message

                using (Pop3Client client = new Pop3Client())
                {
                    client.Connect(server, 110, false);
                    client.Authenticate(accountname, accountpassword);

                    int messageCount = client.GetMessageCount();

                    List<Message> allMessages = new List<Message>(messageCount);

                    if (messageCount == 0)
                        return;

                    for (int i = messageCount; i > 0; i--)
                    {
                        Message message = client.GetMessage(i);
                        if (message.Headers.Subject.ContainsAny(ConfigurationManager.AppSettings["EmailSubject"].Split(',')))
                        {
                            bool result = false;
                            Reset();
                            company = GetCompany(message.Headers.Subject.ToString());

                            if (company == null)
                                continue;

                            if (bool.Parse(ConfigurationManager.AppSettings["ProcessEmails"].ToString()))
                                result = ProcessMessage(message);
                            else
                                deleteMe = true;

                            if (result || completeFile || deleteMe)
                            {
                                client.DeleteMessage(i);
                                goto restart;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("ProcessTDDEmailReport.OpenPopHandler: " + ex.Message);
            }
        }
        private void Reset()
        {
            deleteMe = false;
            completeFile = false;
            company = null;
            Processor = null;
            duplicate = false;
            reimportData = false;
        }

        private Company GetCompany(string subject)
        {
            // TODO: Switch the subject comparision to TTD

            Company[] companies = (from m in db.Companies where m.IsActive == true && m.Sizemek_Keyword != null select m).ToArray();

            foreach (Company company in companies)
            {
                if (subject.ToLower().ContainsAny(company.Sizemek_Keyword.ToLower().Split(',')))
                {
                    Data processor = new Data(company.DatabaseName, company.Login, company.Password, company.Server);

                    Processor = processor;

                    if (company.Abbreviation.ToLower().ContainsAny("fsq", "delta", "sonora"))
                        deleteMe = true;

                    return company;
                }
            }

            return null;
        }
        private bool ProcessMessage(Message message)
        {
            DataTable dt;

            try
            {
                string url = GetUrlFromDownload(message);

                if (url == "")
                    return false;

                if (message.Headers.Subject.Contains("Report Available: Conversion Details Report"))
                {
                    dt = ExtractConversionReport(url);
                    tablename = "dataTradeDeskConversionMyReportData";
                }
                else
                    dt = ExtractReport(url);

                if (dt != null)
                {
                    bool processed = ProcessTradedeskMessage(dt, tablename);

                    return processed;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "TheTradeDesk.ProcessMessage:");
            }

            return false;
        }

        private bool ProcessTradedeskMessage(DataTable dt, string tablename)
        {
            bool checkPrep = PrepTradeDeskReport(dt);

            return checkPrep;
        }

        private string[] GetColumnsFromDatabase()
        {
            string query = "";
            query += "DECLARE @columns varchar(8000) ";
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(8000), COLUMN_NAME) ";
            query += String.Format("FROM (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{0}') as tab ", tablename);
            query += "select @columns ";

            DataSet ds = Processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString();

            string[] columnCol = columns.ToLower().Split(',').Select(x => x.Trim()).ToArray();

            return columnCol;
        }
        private void ColumnChecker(DataTable dt)
        {
            try
            {
                DateTime today = System.DateTime.Now;

                string[] columnCol = GetColumnsFromDatabase();

                foreach (DataColumn dc in dt.Columns)
                {
                    if (dc.ColumnName.Contains("_"))
                        dc.ColumnName = dc.ColumnName.Replace("_", " ");
                    if (dc.ColumnName.Contains("* "))
                        dc.ColumnName = dc.ColumnName.Replace("* ", "");

                    dc.ColumnName = dc.ColumnName.ToProperCase();

                    if (dc.ColumnName.ToLower() == "date" || dc.ColumnName.ToLower() == "asofdate")
                    {
                        dc.ColumnName = "AsOfDate";
                        continue;
                    }

                    if (dc.ColumnName.ToLower() == "modifieddate")
                    {
                        dc.ColumnName = "ModifiedDate";
                        continue;
                    }

                    if (!columnCol.Contains(dc.ColumnName.ToLower().Trim()))
                        AddColumn(dc.ColumnName.ToString());
                }

                if (!dt.Columns.Contains("ModifiedDate"))
                {
                    System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                    newColumn.DefaultValue = today.ToString();
                    dt.Columns.Add(newColumn);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void AddColumn(string column)
        {
            bool processColumn = false;

            column = column.ToProperCase();

            string query = "ALTER TABLE {0} ADD [{1}] {2}";

            if (column.ToUpper().ContainsAny("Date".ToUpper()))
                query = string.Format(query, tablename, column, "[DateTime] NULL");
            if (column.ToUpper().ContainsAny("revenue".ToUpper()))
                query = string.Format(query, tablename, column, "decimal(18, 5) NULL");
            else
                query = string.Format(query, tablename, column, "[int] NULL");

            Processor.RunQuery(query);
        }

        private bool PrepTradeDeskReport(DataTable dt)
        {
            bool processed = false;

            ColumnChecker(dt);

            DateTime[] reports = GetReportDates(tablename, dt);

            DataSet ds = dt.ToDataSetDateSplit();

            foreach (DataTable datatab in ds.Tables)
            {
                DateTime _tablename = DateTime.Parse(datatab.TableName.ToString());

                reimportData = false;

                if (reports.Contains(_tablename))
                {
                    bool comparedata = CompareReportDataAgainstDatabaseData(datatab);

                    if (comparedata)
                    {
                        completeFile = true;
                        continue;
                    }
                    else if (reimportData)
                    {
                        DeleteOldData(datatab);
                    }
                    else
                    {
                        completeFile = false;
                        return false;
                    }
                }
                else
                {
                    completeFile = false;
                }


                CleanData(datatab);

                processed = ProcessTradeDeskReport(datatab);

                if (!processed)
                    return processed;
            }

            return processed;
        }

        /// <summary>
        /// Clean the data based on the column type define in the AddColumn method 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private DataTable CleanData(DataTable dt)
        {
            Console.WriteLine("TheTradeDeskReporting - CleanData: " + Processor.DatabaseName);

            int idx = 0;
            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName.ToUpper().ContainsAny("ADVERTISER COST (USD)"))
                    {
                        if (dr[col.ColumnName].ToString() == "")
                        {
                            dr[col.ColumnName] = 0;
                        }
                        else
                        {
                            if (dr[col.ColumnName].ToString().Contains("E-"))
                            {
                                string value = Decimal.Parse(dr[col.ColumnName].ToString(), System.Globalization.NumberStyles.Float).ToString();
                                dr[col.ColumnName] = value;// dr[column.ColumnName].ToString();
                            }
                        }
                    }
                }
                idx++;
            }

            //  Delete rows without a creative MIT: 10/1/2018 Per Alexis and Melanee
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = dt.Rows[i];
                if (String.IsNullOrEmpty(dr["Creative"].ToString()))
                    dr.Delete();
            }

            return dt;

        }

        /// <summary>
        /// This is to compare the total records and the sum of the impressions against the database. 
        /// </summary>
        /// <param name="datatab"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private bool CompareReportDataAgainstDatabaseData(DataTable datatab)
        {
            string imps = "";

            bool _override = false;

            try
            {
                string query = String.Format(@"SELECT count(*) Total, sum([Impressions]) SumImp FROM [dbo].[{0}] where AsOfDate =  '{1}'", tablename, datatab.TableName);

                DataSet ds = Processor.RunQuery(query);

                if (ds != null)
                {
                    if (ds.Tables.Count != 0)
                    {
                        int _sumImp = 0;
                        int total = int.Parse(ds.Tables[0].Rows[0]["Total"].ToString());
                        int sumImp = int.Parse(ds.Tables[0].Rows[0]["SumImp"].ToString());

                        foreach (DataRow dr in datatab.Rows)
                        {
                            imps = dr["Impressions"].ToString();
                            _sumImp += int.Parse(dr["Impressions"].ToString().Replace(",", "").Replace("\"", ""));
                        }

                        int _Newtotal = datatab.Rows.Count;

                        if (total == _Newtotal && sumImp == _sumImp || total == 0 && sumImp == 0)
                        {
                            if (_override)
                            {
                                reimportData = true;
                                return false;
                            }

                            return true;
                        }
                        else
                            reimportData = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }

        private void DeleteOldData(DataTable dt)
        {
            try
            {
                string query = String.Format(@"delete FROM [dbo].[{0}] where AsOfDate = '{1}'", tablename, dt.TableName);

                Processor.RunQuery(query);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private DateTime[] GetReportDates(string tableName, DataTable dt)
        {
            List<DateTime> dates = new List<DateTime>();

            string query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[{0}] where [AsOfDate] is not null", tableName);

            DataSet ds = Processor.RunQuery(query);

            if (ds != null) // Add date to list of date
                if (ds.Tables.Count != 0) // Add date to list of date
                    foreach (DataRow dr in ds.Tables[0].Rows)
                        if (dr[0].ToString() != "")
                            dates.Add(DateTime.Parse(dr[0].ToString())); // Date is a non nullable date type

            return dates.ToArray();
        }

        private bool ProcessTradeDeskReport(DataTable dt)
        {
            try
            {
                SqlConnection conn = new SqlConnection(Processor.ConnectionString);

                bool bolChecker = DebugHelper.CheckColumnsAgainstDatabase(Processor, dt, tablename);

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);

                    bulkCopy.BatchSize = 10000;
                    bulkCopy.BulkCopyTimeout = 500;

                    foreach (DataColumn dc in dt.Columns)
                    {
                        if (dc.ColumnName == "Idx")
                            continue;

                        if (dc.ColumnName == "Date")
                        {
                            SqlBulkCopyColumnMapping mapping = new SqlBulkCopyColumnMapping(dc.ColumnName, "AsOfDate");
                            bulkCopy.ColumnMappings.Add(mapping);
                        }
                        else
                        {
                            SqlBulkCopyColumnMapping mapping = new SqlBulkCopyColumnMapping(dc.ColumnName, dc.ColumnName);
                            bulkCopy.ColumnMappings.Add(mapping);
                        }
                    }

                    bulkCopy.DestinationTableName = tablename; //"dataTradeDeskMyReportingData";

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 200;
                    conn.Open();

                    bulkCopy.WriteToServer(dt);

                    return true;
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "ProcessTradeDeskReport.DataTableToDatabase:");
            }

            return false;
        }

        private void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(String.Format("{0} Rows have been copied.", e.RowsCopied.ToString()));
        }

        private DataTable ExtractConversionReport(string url)
        {
            string delimiter = ",";

            try
            {
                XmlTextReader rssReader = new XmlTextReader(url.ToString());
                XmlDocument rssDoc = new XmlDocument();

                WebRequest wrGETURL;
                wrGETURL = WebRequest.Create(url);

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                Stream objStream;
                objStream = wrGETURL.GetResponse().GetResponseStream();
                StreamReader objReader = new StreamReader(objStream, Encoding.UTF8);
                WebResponse wr = wrGETURL.GetResponse();
                Stream receiveStream = wr.GetResponseStream();
                StreamReader reader = new StreamReader(receiveStream, Encoding.UTF8);
                string content = reader.ReadToEnd();
                
                if (content != "")
                { 
                    File.WriteAllText(csvExport + @"\TradedDesk Conversion Details - " + company.CompanyName, content.ToString());

                    DataTable dt = new DataTable();

                    string[] tableData = content.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    var col = from cl in tableData[0].Split(delimiter.ToCharArray())
                              select new DataColumn(cl);
                    dt.Columns.AddRange(col.ToArray());

                    (from st in tableData.Skip(1)
                     select dt.Rows.Add(st.Split(delimiter.ToCharArray()))).ToList();

                    return dt;
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "ExtractConversionReport TDD");
            }

            return null;
        }
        private DataTable ExtractReport(string url)
        {
            string delimiter = ",";

            try
            {
                XmlTextReader rssReader = new XmlTextReader(url.ToString());
                XmlDocument rssDoc = new XmlDocument();

                WebRequest wrGETURL;
                wrGETURL = WebRequest.Create(url);

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                Stream objStream;
                objStream = wrGETURL.GetResponse().GetResponseStream();
                StreamReader objReader = new StreamReader(objStream, Encoding.UTF8);
                WebResponse wr = wrGETURL.GetResponse();
                Stream receiveStream = wr.GetResponseStream();
                StreamReader reader = new StreamReader(receiveStream, Encoding.UTF8);
                string content = reader.ReadToEnd();

                if (content != "")
                {
                    if (content.Count(f => f == '\t') > 0)
                        delimiter = "\t";

                    DataTable dt = new DataTable();

                    string[] tableData = content.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    var col = from cl in tableData[0].Split(delimiter.ToCharArray())
                              select new DataColumn(cl);
                    dt.Columns.AddRange(col.ToArray());

                    (from st in tableData.Skip(1)
                     select dt.Rows.Add(st.Split(delimiter.ToCharArray()))).ToList();

                    return dt;
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "ExtractReport TDD");
            }

            return null;
        }
        private string GetUrlFromDownload(Message message)
        {
            try
            {
                string pattern = @"\b(?:https?://|www\.)\S+\b";

                /*
                 *   \b       -matches a word boundary (spaces, periods..etc)
                 *   (?:      -define the beginning of a group, the ?: specifies not to capture the data within this group.
                 *   https?://  - Match http or https (the '?' after the "s" makes it optional)
                 *   |        -OR
                 *   www\.    -literal string, match www. (the \. means a literal ".")
                 *   )        -end group
                 *   \S+      -match a series of non-whitespace characters.
                 *   \b       -match the closing word boundary.
                 * 
                 */

                Regex r = new Regex(pattern, RegexOptions.IgnoreCase);

                string result = System.Text.Encoding.UTF8.GetString(message.FindFirstPlainTextVersion().Body);
                // Match the regular expression pattern against a text string.
                Match m = r.Match(result);

                Console.WriteLine("Url: {0}", m.Value.Replace("=3D", "="));

                // 3D are the hex digits corresponding to ='s ASCII value (61).
                string url = m.Value.Replace("=3D", "=");

                return url;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "GetUrlFromDownload TDD");
                return "";
            }
        }

    }
}
