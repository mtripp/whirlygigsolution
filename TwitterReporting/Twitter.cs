﻿using OpenPop.Mime;
using OpenPop.Pop3;
using SolutionHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhirlygigSolutionHelper;

namespace TwitterReporting
{
    public class Twitter
    {
        Data Processor = new Data();
        Company company = new Company();
        TwitterMediaEntities db = new TwitterMediaEntities();

        string server;
        string accountname;
        string accountpassword;
        private const string tableName = "dataTwitterReportingData";
        private bool deleteEmail;

        public Twitter()
        {
            server = ConfigurationManager.AppSettings["EmailAccountServer"];
            accountname = ConfigurationManager.AppSettings["EmailAccountName"];
            accountpassword = ConfigurationManager.AppSettings["EmailAccountPassword"];
        }

        public static bool ExportFile(Message msg, string csvExport)
        {
            try
            {
                List<MessagePart> attachments = msg.FindAllAttachments();

                string file = "";
                string filename = "";
                string ext = " " + System.DateTime.Now.ToString().Replace(":", "").Replace("/", "");
                 
                Stream stream = new MemoryStream(attachments[0].Body);

                DataTable dt = new DataTable();
                 
                if (attachments[0].FileName.Contains(".zip"))
                    stream = stream.ToStreamFromZip();
                if (attachments[0].FileName.Contains(".tsv"))
                {
                    filename = attachments[0].FileName.Replace(" Twitter Daily WPC","").Replace(".tsv", ext + ".tsv");
                    dt = stream.ToDataTableTsv();
                    file = dt.ToDataSetExportTsv();
                }
                else if (attachments[0].FileName.Contains(".csv"))
                {
                    filename = attachments[0].FileName.Replace(" Twitter Daily WPC", "").Replace(".csv", ext + ".csv");
                    dt = stream.ToDataTableCsv();
                    file = dt.ToDataSetExportCsv();
                }
                  
                if (dt != null)
                {
                    File.WriteAllText(csvExport + @"\Twitter Custom Report - " + filename, file.ToString());
                    return true;
                } 
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "TwitterReporting.ExportFile: " + msg.Headers.Subject.ToString());
                throw ex;
            }

            return false;
        }

        public void Process()
        {
            ProcessTwitterReport();
        }

        /// <summary>
        /// Example showing:
        ///  - how to fetch all messages from a POP3 server
        /// </summary>
        /// <param name="hostname">Hostname of the server. For example: pop3.live.com</param>
        /// <param name="port">Host port to connect to. Normally: 110 for plain POP3, 995 for SSL POP3</param>
        /// <param name="useSsl">Whether or not to use SSL to connect to server</param>
        /// <param name="username">Username of the user on the server</param>
        /// <param name="password">Password of the user on the server</param>
        ///  http://hpop.sourceforge.net/
        /// <returns>All Messages on the POP3 server</returns>
        private void ProcessTwitterReport()
        {
            string status = "";

            try
            {
                restart: // This goto was implemented due to a Pop3Client timeout. Connection to the mail server needs to be reauthenticated between each message

                do // LOOP AND REFRESH POP3 CLIENT
                {
                    DateTime startTime = System.DateTime.Now;

                    status = "Keep running";
                    Console.WriteLine(status);

                    using (Pop3Client client = new Pop3Client())
                    {
                        client.Connect(server, 110, false);
                        client.Authenticate(accountname, accountpassword);

                        int messageCount = client.GetMessageCount();

                        List<Message> allMessages = new List<Message>(messageCount);

                        if (messageCount == 0)
                            return;

                        for (int i = messageCount; i > 0; i--)
                        {
                            if (i == 1)
                                status = "Stop"; // Once the last email passes status should remain "Stop"
                            else
                                status = "Keep running";

                            Console.WriteLine("Processing " + i + " of " + messageCount);

                            Reset();

                            Message message = client.GetMessage(i);

                            if (message.Headers.Subject == null)
                                continue;

                            if (message.Headers.Subject.ContainsAny(ConfigurationManager.AppSettings["EmailSubject"].Split(',')))
                            {
                                bool result = false;

                                result = ProcessData(message);

                                if (result || deleteEmail)
                                {
                                    client.DeleteMessage(i);
                                    goto restart;
                                }
                            }

                            TimeSpan span = System.DateTime.Now - startTime;

                            if (span.TotalMinutes > 3)
                                break;
                        }
                    }
                } while (status == "Keep running"); // DONT STOP UNLESS ERROR
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("TwitterReporting.OpenPopHandler: " + ex.Message.ToString());
            }
        }

        private bool ProcessData(Message message)
        {
            bool result = false;

            Console.WriteLine("Processing: " + message.Headers.Subject.ToString());

            try
            {
                company = GetCompany(message.Headers.Subject.ToString());

                if (company != null)
                {
                    result = ProcessMessage(message);

                    Console.WriteLine("Finished: " + message.Headers.Subject.ToString());

                    return result;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in processing: " + message.Headers.Subject.ToString() + ex.Message.ToString());
                SolutionHelper.Error.LogError(ex, "TwitterReporting.ProcessAnalytics:");
            }

            return result;
        }

        /// <summary>
        /// Extract Datatable from Message's attachment
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private bool ProcessMessage(Message message)
        {
            try
            {
                DataTable dt = ExtractDatatable(message);

                if (dt.Columns[0].ColumnName.ToString().ToLower().Contains("data was updated last"))
                {
                    deleteEmail = true;
                    return false;
                }

                bool processed = ProcessDatatable(dt);

                return processed;
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "TwitterReporting.ProcessMessage:");
            }

            return false;
        }

        private static DataTable ExtractDatatable(Message message, String filename = "")
        {
            List<MessagePart> attachments = message.FindAllAttachments();

            Stream stream = new MemoryStream(attachments[0].Body);

            DataTable dt = new DataTable();

            if (attachments[0].FileName.Contains(".zip"))
                stream = stream.ToStreamFromZip();
            if (attachments[0].FileName.Contains(".tsv"))
                dt = stream.ToDataTableTsv();
            else if (attachments[0].FileName.Contains(".csv"))
                dt = stream.ToDataTableCsv();
            return dt;
        }

        public bool ProcessDatatable(DataTable dt)
        {
            bool processed = false;
            DateTime today = System.DateTime.Now.Date;
            DateTime[] reports = GetReportDates(tableName);

            ColumnChecker(dt, tableName);

            if (BadDataChecker(dt))
                return false;

            if (NoDataChecker(dt))
                return false;

            DataSet ds = dt.ToDataSetDateSplit();

            foreach (DataTable datatab in ds.Tables)
            {
                DateTime tablename = DateTime.Parse(datatab.TableName.ToString());

                if (reports.Contains(tablename))
                {
                    bool comparedata = CompareReportDataAgainstDatabaseData(datatab);

                    if (comparedata)
                        deleteEmail = true;
                    else
                    {
                        deleteEmail = false;
                        return false;
                    }

                    continue;
                }
                else if (tablename == today)
                {
                    continue;
                }
                else
                {
                    deleteEmail = false;
                }

                CleanData(datatab);



                processed = ProcessTwitterDatatable(datatab, tableName);

                if (!processed)
                    return processed;
            }

            return processed;
        }

        /// <summary>
        /// Process datatable using Sql Bulk Insert
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private bool ProcessTwitterDatatable(DataTable dt, string tableName)
        {
            try
            {
                SqlConnection conn = new SqlConnection(Processor.ConnectionString);

                using (conn)
                {
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn.ConnectionString, SqlBulkCopyOptions.KeepNulls);

                    bulkCopy.BatchSize = 10000;
                    bulkCopy.BulkCopyTimeout = 500;

                    foreach (DataColumn dc in dt.Columns)
                    {
                        SqlBulkCopyColumnMapping mapping = new SqlBulkCopyColumnMapping(dc.ColumnName, dc.ColumnName);

                        bulkCopy.ColumnMappings.Add(mapping);
                    }

                    bulkCopy.DestinationTableName = tableName;

                    bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                    bulkCopy.NotifyAfter = 2000;
                    conn.Open();

                    bulkCopy.WriteToServer(dt);

                    return true;
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError(ex, "TwitterProcessing.DataTableToDatabase:" + Processor.DatabaseName);
            }

            return false;
        }

        #region FunctionMethods

        /// <summary>
        /// This is to compare the total records and the sum of the impressions against the database. 
        /// </summary>
        /// <param name="datatab"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private bool CompareReportDataAgainstDatabaseData(DataTable datatab)
        {
            try
            {

                string query = String.Format(@"SELECT count(*) Total, sum([Impressions]) SumImp FROM [dbo].[{0}] where AsOfDate =  '{1}'", tableName, datatab.TableName);

                DataSet ds = Processor.RunQuery(query);

                if (ds != null)
                {
                    if (ds.Tables.Count != 0)
                    {
                        int _sumImp = 0;
                        int total = int.Parse(ds.Tables[0].Rows[0]["Total"].ToString());
                        int sumImp = int.Parse(ds.Tables[0].Rows[0]["SumImp"].ToString());

                        foreach (DataRow dr in datatab.Rows)
                        {
                            if (dr["Impressions"].ToString() == null)
                                _sumImp += int.Parse(dr["Impressions"].ToString());
                        }

                        int _total = datatab.Rows.Count;

                        if (total == _total && sumImp == _sumImp)
                            return true;
                    }
                }

            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }

        private Company GetCompany(string _companyString)
        {
            Company[] companys = (from m in db.Companies where m.IsActive == true select m).ToArray();

            foreach (Company company in companys)
            {
                if (company.Twitter_Keyword == null)
                    continue;

                if (_companyString.ToLower().ContainsAny(company.Twitter_Keyword.ToLower().Split(',')))
                {
                    Data processor = new Data();
                    processor.DatabaseName = company.DatabaseName;
                    processor.UserName = company.Login;
                    processor.Password = company.Password;
                    processor.Server = company.Server;

                    Processor = processor;
                    return company;
                }
            }

            return null;
        }

        private void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(String.Format("{0} Rows have been copied.", e.RowsCopied.ToString()));
        }

        /// <summary>
        /// Check if all the columns are created in the database for the reporting table. If the column doesnt exist it will be added.
        /// </summary>
        /// <param name="dt"></param>
        private void ColumnChecker(DataTable dt, string tableName)
        {
            string[] columnCol = GetColumnsFromDatabase(tableName);

            foreach (DataColumn dc in dt.Columns)
            {
                if (dc.ColumnName.Contains("* "))
                    dc.ColumnName = dc.ColumnName.Replace("* ", "");

                dc.ColumnName = dc.ColumnName.ToProperCase();

                if (dc.ColumnName.ToLower() == "date")
                {
                    dc.ColumnName = "AsOfDate";
                    continue;
                }

                if (!columnCol.Contains(dc.ColumnName.ToLower()))
                {

                    AddColumn(tableName, dc.ColumnName);
                }
            }

            DateTime today = System.DateTime.Now;

            if (!dt.Columns.Contains("ModifiedDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                newColumn.DefaultValue = today.ToString();
                dt.Columns.Add(newColumn);
            }

            bool bolChecker = DebugHelper.CheckColumnsAgainstDatabase(Processor, dt, tableName);
        }

        private string[] GetColumnsFromDatabase(string tableName)
        {
            string query = "";
            query += "DECLARE @columns varchar(8000) ";
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(8000), COLUMN_NAME) ";
            query += String.Format("FROM (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{0}') as tab ", tableName);
            query += "select @columns ";

            DataSet ds = Processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString();

            string[] columnCol = columns.ToLower().Split(',');

            return columnCol;
        }

        private DateTime[] GetReportDates(string tableName)
        {
            List<DateTime> dates = new List<DateTime>();

            string query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[{0}] where [AsOfDate] is not null", tableName);

            DataSet ds = Processor.RunQuery(query);

            if (ds != null) // Add date to list of date
                if (ds.Tables.Count != 0) // Add date to list of date
                    foreach (DataRow dr in ds.Tables[0].Rows)
                        dates.Add(DateTime.Parse(dr[0].ToString())); // Date is a non nullable date type

            return dates.ToArray();
        }

        /// <summary>
        /// Due to the columns needing to be dynamic this method is designed to add additional column to the table
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        private void AddColumn(string table, string column)
        {
            column = column.ToProperCase();

            string query = "ALTER TABLE {0} ADD [{1}] {2}";

            if (column.ToUpper().ContainsAny("CLICKS", "IMPRESSIONS", "CONVERSIONS"))
            {
                if (!column.ToUpper().ContainsAny("COST"))
                    query = string.Format(query, table, column, "[int] NULL");
            }

            if (column.ToUpper().ContainsAny("RATE", "COST", "CPC", "CTR"))
                query = string.Format(query, table, column, "decimal(21, 4) NULL");

            if (column.ToUpper().ContainsAny("TIME"))
                query = string.Format(query, table, column, "datetime NULL");

            if (query == "ALTER TABLE {0} ADD [{1}] {2}")
                query = string.Format(query, table, column, "[nvarchar](250)");

            Processor.RunQuery(query);
        }

        /// <summary>
        /// This method is to confirm that this datatable has actual meaningful data in it.
        /// </summary>
        /// <param name="dt"></param>


        /// <summary>
        /// This method checks for bad data within a file.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private bool BadDataChecker(DataTable dt)
        {
            bool isTableEmpty = true;

            foreach (DataRow dr in dt.Rows)
            {
                string record = dr[1].ToString();
                if (dr[1].ToString() != "ERROR")
                {
                    isTableEmpty = false;
                    break;
                }
            }

            if (isTableEmpty)
            {
                deleteEmail = true;
                return true;
            }
            else
            {
                deleteEmail = false;
                return false;
            }
        }

        /// <summary>
        /// This method checks if a well structured file contains data that is not useful
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private bool NoDataChecker(DataTable dt)
        {
            int values = 0;

            foreach (DataRow dr in dt.Rows)
            {
                string impressions = dr["Impressions"].ToString();
                string clicks = dr["Clicks"].ToString();

                if (!impressions.ContainsAny("ERROR", "NULL"))
                    values += int.Parse(impressions);
                if (!clicks.ContainsAny("ERROR", "NULL"))
                    values += int.Parse(clicks);
            }

            if (values == 0)
            {
                deleteEmail = true;
                return true;
            }
            else
            {
                deleteEmail = false;
                return false;
            }
        }

        /// <summary>
        /// Clean the data based on the column type define in the AddColumn method 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private DataTable CleanData(DataTable dt)
        {
            int idx = 0;
            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName.ToUpper().ContainsAny("CLICKS", "IMPRESSIONS", "CPC", "CTR", "CONVERSIONS"))
                    {
                        if (String.IsNullOrEmpty(dr[col.ColumnName].ToString()) || dr[col.ColumnName].ToString().ToUpper() == "NULL" || dr[col.ColumnName].ToString().ToUpper() == "ERROR")
                            dr[col.ColumnName] = 0;

                        if (dr[col.ColumnName].ToString().Contains(","))
                            dr[col.ColumnName] = dr[col.ColumnName].ToString().Replace(",", "");
                    }

                    if (col.ColumnName.ToUpper().ContainsAny("RATE", "COST"))
                    {
                        if (String.IsNullOrEmpty(dr[col.ColumnName].ToString()) || dr[col.ColumnName].ToString().ToUpper() == "NULL" || dr[col.ColumnName].ToString().ToUpper() == "ERROR")
                            dr[col.ColumnName] = "0.00";
                        if (dr[col.ColumnName].ToString().Contains(","))
                            dr[col.ColumnName] = dr[col.ColumnName].ToString().Replace(",", "");
                        if (dr[col.ColumnName].ToString().Contains("%"))
                            dr[col.ColumnName] = dr[col.ColumnName].ToString().Replace("%", "");
                    }

                    if (col.ColumnName.ToUpper().ContainsAny("TIME"))
                    {
                        if (String.IsNullOrEmpty(dr[col.ColumnName].ToString()) || dr[col.ColumnName].ToString() == "NULL")
                            dr[col.ColumnName] = "1901-01-01 00:00:00.000";
                    }
                }
                idx++;
            }
            return dt;
        }

        private void Reset()
        {
            company = null;
            Processor = null;
            deleteEmail = false;
        }

        #endregion

    }
}
