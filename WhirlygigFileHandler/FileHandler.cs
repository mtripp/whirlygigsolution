﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 

namespace WhirlygigFileHandler
{
    public class FileHandler
    {
        string csvExportCompleted;
        string csvExportFailed;
        string csvExportWaiting;
        string csvExportProcessing;
        string csvExport;
        string csvEmails;
        bool debug; 

        public FileHandler()
        {
            debug = bool.Parse(ConfigurationManager.AppSettings["debug"].ToString());
            csvExport = ConfigurationManager.AppSettings["csvExport"];
            csvEmails = ConfigurationManager.AppSettings["csvEmails"];
            csvExportWaiting = ConfigurationManager.AppSettings["csvExportWaiting"];
            csvExportCompleted = ConfigurationManager.AppSettings["csvExportCompleted"];
            csvExportProcessing = ConfigurationManager.AppSettings["csvExportProcessing"];
            csvExportFailed = ConfigurationManager.AppSettings["csvExportFailed"];
        }

        public void Run()
        {
            try
            {
                DirectoryInfo d = new DirectoryInfo(csvExport);
                FileInfo file = d.GetFiles().OrderBy(m => m.CreationTime).FirstOrDefault();
                try
                {
                    file.MoveTo(csvExportWaiting + @"\" + file.Name);
                    ProcessFile(file);
                }
                catch (Exception ex)
                { 
                    file.MoveTo(csvExportFailed + @"\" + file.Name);
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("WhirlygigFileHandler.FileProcessor.Run: " + ex.Message);
            }
        }

        private void ProcessFile(FileInfo file)
        {
            try
            {
                if (file.FullName.Contains("Sizmek Analytics Report"))
                {
                    SizmekMDXReporting.ReportingData Sizmek_Analytics = new SizmekMDXReporting.ReportingData();
                }
                else if (file.FullName.Contains("Sizmek Daily Search"))
                {
                    SizmekMDXReporting.SearchReportingData Sizmek_Analytics = new SizmekMDXReporting.SearchReportingData();
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("EmailExtractor.ExtractFiles: " + ex.Message);
            }
        }
    }
}
