﻿using OpenPop.Mime;
using SolutionHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhirlygigSolutionHelper;

namespace WhirlygigFileProcessor
{
    public class DownloadConversantFiles
    {
        private mediaWhirlygigEntities db = new mediaWhirlygigEntities();
        public Data Processor = new Data();

        public Company company;
        string csvExportCompleted;
        string csvExportFailed;
        string csvExportWaiting;
        string csvExportProcessing;
        string csvExport;
        string csvEmails;
        bool debug;

        public DownloadConversantFiles()
        {
            debug = bool.Parse(ConfigurationManager.AppSettings["debug"].ToString());

            csvExport = ConfigurationManager.AppSettings["csvExport"];
            csvEmails = ConfigurationManager.AppSettings["csvEmails"];
            csvExportWaiting = ConfigurationManager.AppSettings["csvExportWaiting"];
            csvExportCompleted = ConfigurationManager.AppSettings["csvExportCompleted"];
            csvExportProcessing = ConfigurationManager.AppSettings["csvExportProcessing"];
            csvExportFailed = ConfigurationManager.AppSettings["csvExportFailed"];
        }

        public bool ExtractFile(Message msg)
        {
            try
            {
                return ConversantReporting.ConversantReport.ExportFile(msg, csvExport);
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("WhirlygigFileProcessor.DownloadConversantFiles.DownloadFile: " + ex.Message);
            }

            return false;
        }
    }
}
