﻿using OpenPop.Mime;
using SolutionHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhirlygigSolutionHelper;
using static WhirlygigFileProcessor.Enums;

namespace WhirlygigFileProcessor
{
    public class DownloadSizmekFiles
    {
        private mediaWhirlygigEntities db = new mediaWhirlygigEntities();
        public Data Processor = new Data();

        public Company company;
        string csvExportCompleted;
        string csvExportFailed;
        string csvExportWaiting;
        string csvExportProcessing;
        string csvExport;
        string csvEmails;
        bool debug;

        public DownloadSizmekFiles()
        {
            debug = bool.Parse(ConfigurationManager.AppSettings["debug"].ToString());

            csvExport = ConfigurationManager.AppSettings["csvExport"];
            csvEmails = ConfigurationManager.AppSettings["csvEmails"];
            csvExportWaiting = ConfigurationManager.AppSettings["csvExportWaiting"];
            csvExportCompleted = ConfigurationManager.AppSettings["csvExportCompleted"];
            csvExportProcessing = ConfigurationManager.AppSettings["csvExportProcessing"];
            csvExportFailed = ConfigurationManager.AppSettings["csvExportFailed"];
        }

        public bool ExtractFile(SizmekTypes type, Message msg)
        {
            switch (type)
            {
                case SizmekTypes.Analytics:
                    return AnalyticsDownloader(msg);
                case SizmekTypes.Custom:
                    return CustomsDownloader(msg);
                case SizmekTypes.PathToConversion:
                    return PathToConversionDownloader(msg);
                case SizmekTypes.DailySearch:
                    return DailySearchDownloader(msg);
                case SizmekTypes.Unique:
                    return DailyUniqueDownloader(msg);
            }

            return false;
        }

        private bool DailyUniqueDownloader(Message msg)
        {
            try
            {
                return SizmekMDXReporting.EmailProcessing.UniquesReportingData.ExportReport(msg, csvExport);
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("WhirlygigFileProcessor.Extraction_Methods.DailyUniqueDownloader: " + ex.Message);
            }

            return false;
        }

        private bool DailySearchDownloader(Message msg)
        {
            try
            {
                return SizmekMDXReporting.EmailProcessing.SearchReportingData.ExportReport(msg, csvExport);
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("WhirlygigFileProcessor.Extraction_Methods.DailySearchDownloader: " + ex.Message);
            }

            return false;
        }

        private bool PathToConversionDownloader(Message msg)
        {
            try
            {
                return SizmekMDXReporting.EmailProcessing.PathToConversionProcessing.ExportReport(msg, csvExport);
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("WhirlygigFileProcessor.Extraction_Methods.PathToConversionDownloader: " + ex.Message);
            }

            return false;
        }

        private bool AnalyticsDownloader(Message msg)
        {
            try
            {
                return SizmekMDXReporting.ReportingData.ExportFile(msg, csvExport);
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("WhirlygigFileProcessor.Extraction_Methods.AnalyticsDownloader: " + ex.Message);
            }

            return false;
        }

        private bool CustomsDownloader(Message msg)
        {
            try
            {
                return SizmekMDXReporting.CustomReportingData.ExportFile(msg, csvExport);
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("WhirlygigFileProcessor.Extraction_Methods.CustomsDownloader: " + ex.Message);
            }

            return false;
        }
        #region FunctionalMethods

        #endregion
         
    } 
}
