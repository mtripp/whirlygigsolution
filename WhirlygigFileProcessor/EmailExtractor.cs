﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenPop.Mime;
using OpenPop.Pop3;
using System.IO;
using SolutionHelper;
using WhirlygigFileProcessor;
using static WhirlygigFileProcessor.Enums;
using WhirlygigSolutionHelper;

namespace WhirlygigFileProcessor
{
    public class EmailExtractor
    {
        string server;
        string emailaddress;
        string accountpassword;
        string csvExportCompleted;
        string csvExportFailed;
        string csvExportWaiting;
        string csvExportProcessing;
        string csvExport;
        string csvEmails;
        bool debug;
        string[] unzippedFiles = null;

        public EmailExtractor()
        {
            debug = bool.Parse(ConfigurationManager.AppSettings["debug"].ToString());

            server = ConfigurationManager.AppSettings["EmailAccountServer"];
            emailaddress = ConfigurationManager.AppSettings["EmailAddress"];
            accountpassword = ConfigurationManager.AppSettings["EmailAccountPassword"];

            csvExport = ConfigurationManager.AppSettings["csvExport"];
            csvEmails = ConfigurationManager.AppSettings["csvEmails"];
            csvExportWaiting = ConfigurationManager.AppSettings["csvExportWaiting"];
            csvExportCompleted = ConfigurationManager.AppSettings["csvExportCompleted"];
            csvExportProcessing = ConfigurationManager.AppSettings["csvExportProcessing"];
            csvExportFailed = ConfigurationManager.AppSettings["csvExportFailed"];
        }

        public void Run()
        {
            ExportEmails();
            ExtractFiles();  //http://blog.danskingdom.com/limit-the-number-of-c-tasks-that-run-in-parallel/
            UnzipFiles();
            // https://www.thoughtco.com/multi-threading-in-c-with-tasks-958372
        }

        private static void ExportEmails()
        {
            ExportEmails();
        }

        private void UnzipFiles()
        {
            FileDecompressor un = new FileDecompressor();
            un.UnzipFiles();
        }
         
        /// <summary>
        /// Load
        /// </summary>
        private void ExtractFiles()
        {
            int idx = 0;

            try
            {
                DirectoryInfo d = new DirectoryInfo(csvEmails);
                FileInfo[] infos = d.GetFiles();

                foreach (FileInfo file in infos)
                {
                    idx++;

                    if (!file.FullName.ContainsAny(".eml"))
                        continue;

                    Message message = Message.Load(file); // Load message from file
                    string subject = message.Headers.Subject;

                    var result = false;

                    if (subject.ContainsAny(ConfigurationManager.AppSettings["SizmekEmailSubject_Analytics"].Split(',')) && (!subject.Contains("Search") && (!subject.Contains("Unique"))))
                    {
                        DownloadSizmekFiles extractor = new DownloadSizmekFiles();
                        result = extractor.ExtractFile(SizmekTypes.Analytics, message);
                    }
                    else if (subject.ContainsAny(ConfigurationManager.AppSettings["SizmekEmailSubject_CustomReport"].Split(',')))
                    {
                        DownloadSizmekFiles extractor = new DownloadSizmekFiles();
                        result = extractor.ExtractFile(SizmekTypes.Custom, message);
                    }
                    else if (subject.ContainsAny(ConfigurationManager.AppSettings["SizmekEmailSubject_PathToConversion"].Split(',')))
                    {
                        DownloadSizmekFiles extractor = new DownloadSizmekFiles();
                        result = extractor.ExtractFile(SizmekTypes.PathToConversion, message);
                    }
                    else if (subject.ContainsAny(ConfigurationManager.AppSettings["SizmekEmailSubject_DailySearch"].Split(',')))
                    {
                        DownloadSizmekFiles extractor = new DownloadSizmekFiles();
                        result = extractor.ExtractFile(SizmekTypes.DailySearch, message);
                    }
                    else if (subject.ContainsAny(ConfigurationManager.AppSettings["SizmekEmailSubject_Uniques"].Split(',')))
                    {
                        DownloadSizmekFiles extractor = new DownloadSizmekFiles();
                        result = extractor.ExtractFile(SizmekTypes.Unique, message);
                    }
                    else if (subject.ContainsAny(ConfigurationManager.AppSettings["TwitterEmailSubject"].Split(',')))
                    {
                        DownloadTwitterFiles extractor = new DownloadTwitterFiles();
                        result = extractor.ExtractFile(message);
                    }
                    else if (subject.ContainsAny(ConfigurationManager.AppSettings["LinkedInEmailSubject"].Split(',')))
                    {
                        DownloadLinkedInFiles extractor = new DownloadLinkedInFiles();
                        result = extractor.ExtractFile(message);
                    }
                    else if (subject.ContainsAny(ConfigurationManager.AppSettings["AdWordsEmailSubject"].Split(',')))
                    {
                        DownloadAdwordsFiles extractor = new DownloadAdwordsFiles();
                        result = extractor.ExtractFile(message);
                    }
                    else if (subject.ContainsAny(ConfigurationManager.AppSettings["FacebookEmailSubject"].Split(',')))
                    {
                        DownloadFacebookFiles extractor = new DownloadFacebookFiles();
                        result = extractor.ExtractFile(message);
                    }
                    else if (subject.ContainsAny(ConfigurationManager.AppSettings["ConversantEmailSubject"].Split(',')))
                    {
                        DownloadConversantFiles extractor = new DownloadConversantFiles();
                        result = extractor.ExtractFile(message);
                    }
                    else if (subject.ContainsAny(ConfigurationManager.AppSettings["BingEmailSubject"].Split(',')))
                    {
                        DownloadBingFiles extractor = new DownloadBingFiles();
                        result = extractor.ExtractFile(message);
                    }
                    else
                        continue;
                     
                    string path = @"\" + file.Name.Replace(file.Extension, " ") + System.DateTime.Now.ToString().ReplaceAny("",":", ".", "/", "AM", "PM") + file.Extension;

                    if (result)
                        path = csvExportCompleted + path;
                    else
                        path = csvExportFailed + path;


                    int count = path.Length;

                    if (count > 250)
                        path = path.Substring(0, 250) + file.Extension;

                    file.MoveTo(path);
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("EmailExtractor.ExtractFiles: " + ex.Message);
            }
        }

    }
}
