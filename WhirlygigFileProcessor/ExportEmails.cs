﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenPop.Mime;
using OpenPop.Pop3;
using System.IO;
using SolutionHelper;
using WhirlygigFileProcessor;
using static WhirlygigFileProcessor.Enums;
using WhirlygigSolutionHelper;

namespace WhirlygigFileProcessor
{
    public class ExportEmails
    {
        string server;
        string emailaddress;
        string accountpassword;
        string csvExportCompleted;
        string csvExportFailed;
        string csvExportWaiting;
        string csvExportProcessing;
        string csvExport;
        string csvEmails;
        bool debug;

        /// <summary>
        /// Example showing:
        ///     - how to fetch all messages from a POP3 server
        /// </summary>
        /// <param name="hostname">Hostname of the server. For example: pop3.live.com</param>
        /// <param name="port">Host port to connect to. Normally: 110 for plain POP3, 995 for SSL POP3</param>
        /// <param name="useSsl">Whether or not to use SSL to connect to server</param>
        /// <param name="username">Username of the user on the server</param>
        /// <param name="password">Password of the user on the server</param>
        ///  http://hpop.sourceforge.net/
        /// <returns>All Messages on the POP3 server</returns>
        public void RunExportEmails()
        {
            DateTime startTime = System.DateTime.Now;

            try
            {
                using (Pop3Client client = new Pop3Client())
                {
                    client.Connect(server, 110, false);
                    client.Authenticate(emailaddress, accountpassword);

                    int messageCount = client.GetMessageCount();

                    if (client.GetMessageCount() != 0)
                    {
                        for (int i = 1; i <= messageCount; i++)
                        {
                            Message message = client.GetMessage(i);
                            Console.WriteLine("Processing " + i + " of " + messageCount + ": " + message.Headers.Subject + " " + i.ToString() + " " + message.Headers.Date.ToString());

                            string fileName = csvEmails + @"\" + message.Headers.Subject.ToString().Replace("|", "").Replace(":", " ") + " " + i.ToString() + " .eml";
                            fileName = fileName.Replace(".eml", System.DateTime.Now.ToString().Replace("/", ".").Replace(":", ".") + ".eml");

                            int count = fileName.Length;

                            if (count > 250)
                                fileName = fileName.Substring(0, 246) + ".eml";

                            FileInfo f = new FileInfo(fileName);
                            message.Save(f);

                            if (!debug)
                                client.DeleteMessage(i);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("WhirlygigFileProcessor.ExportEmails.RunExportEmails: " + ex.Message);
            }
        }
    }
}
