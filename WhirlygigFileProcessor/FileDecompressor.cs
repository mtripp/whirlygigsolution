﻿using SolutionHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhirlygigSolutionHelper;

namespace WhirlygigFileProcessor
{
    public class FileDecompressor
    {
        string csvExportCompleted;
        string csvExportFailed;
        string csvExportWaiting;
        string csvExportProcessing;
        string csvExport;
        string csvEmails;

        bool debug;
        string[] unzippedFiles = null;

        public FileDecompressor()
        { 
            debug = bool.Parse(ConfigurationManager.AppSettings["debug"].ToString());
            csvExport = ConfigurationManager.AppSettings["csvExport"];
            csvEmails = ConfigurationManager.AppSettings["csvEmails"];
            csvExportWaiting = ConfigurationManager.AppSettings["csvExportWaiting"];
            csvExportCompleted = ConfigurationManager.AppSettings["csvExportCompleted"];
            csvExportProcessing = ConfigurationManager.AppSettings["csvExportProcessing"];
            csvExportFailed = ConfigurationManager.AppSettings["csvExportFailed"];
        }

        public void UnzipFiles()
        {
            try
            {
                Unzip();

                DeleteUnzippedFiles();
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("SizmekMDXReporting.FileProcessing.RunProcessingFiles: " + ex.Message);
            }
        }

        private void DeleteUnzippedFiles()
        {
            foreach (string str in unzippedFiles)
            {
                FileInfo file = new FileInfo(str);

                file.Delete();
            }

            unzippedFiles = null;
        }

        private void Unzip()
        {
            unzippedFiles = null;

            try
            {
                DirectoryInfo d = new DirectoryInfo(csvExport);
                FileInfo[] infos = d.GetFiles();
                List<string> _unzippedFiles = new List<string>();

                foreach (FileInfo f in infos.Where(m => m.Extension.Contains(".zip")))
                {
                    string prefix = "";

                    Stream fs = File.OpenRead(f.FullName);
                    Streem str = new Streem();
                    str.Stream = fs;

                    str.ToStreamFromZip(csvExport, prefix);

                    fs.Close();
                    _unzippedFiles.Add(f.FullName);
                }

                unzippedFiles = _unzippedFiles.ToArray();
            }
            catch (Exception ex)
            {
                SolutionHelper.Error.LogError("WhirlygigFileProcessor.FileDecompressor: " + ex.Message);
            }
        }
    }
}
