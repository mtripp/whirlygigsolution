﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Exchange.WebServices.Data;

namespace WhirlygigFileProcessor
{
    class Program
    {
        static void Main(string[] args)
        {
            EmailExtractor extractor = new EmailExtractor();
            extractor.Run();
        } 
    }
}
