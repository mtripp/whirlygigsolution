﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.VisualBasic.FileIO;
using SolutionHelper;
using System.Data.OleDb;

namespace WhirlygigSolutionHelper
{
    public class CsvToDatabase
    {
        public string _source { get; set; }
        public string _tablename { get; set; }
        public bool _duplicate { get; set; }
        public Data _processor { get; set; }

        public CsvToDatabase(string tablename, string source, Data processor)
        {
            _processor = processor;
            _tablename = tablename;
            _source = source;
        }

        public bool Run()
        {
            // TODO: This needs to be moved into SizmekMDXReporting

            DataTable dts = new DataTable();
            //   DateTime[] reports = GetReportDates(_tablename);
            DateTime today = System.DateTime.Now.Date;

            string currentTable = "";

            try
            {
                DataTable dt = new DataTable();

                Console.WriteLine("Converting table : " + System.DateTime.Now);

                dt = GetDataTabletFromCSVTSVFile(_source);

                Console.WriteLine("Checking columns : " + System.DateTime.Now);

                ColumnChecker(dt, "dataSizmekDataFeedReportingData");

                DataSet ds = dt.ToDataSetDateSplit();

                foreach (DataTable datatab in ds.Tables)
                {
                    currentTable = datatab.TableName;

                    DateTime tablename = DateTime.Parse(datatab.TableName.ToString());

                    if (GetReportDate(datatab.TableName.ToString()))
                    {
                        bool comparedata = CompareReportDataAgainstDatabaseData(datatab, _tablename);

                        if (comparedata)
                            _duplicate = true;
                        else
                        {
                            _duplicate = false;
                            return false;
                        }

                        continue;
                    }
                    else if (tablename == today)
                    {
                        continue;
                    }
                    else
                        _duplicate = false;


                    Console.WriteLine(String.Format("Cleaning data '{0}' : ", datatab.TableName) + System.DateTime.Now);
                    CleanDataFeed(datatab);

                    Console.WriteLine("Inserting data : " + System.DateTime.Now);
                    bool result = ProcessBulkCopy(datatab);

                    if (result)
                        if (_processor.DatabaseName == "mediaArizonaStateUniversity")
                            ProcessGeoSummaryData(datatab, "dataSizmekDataFeedReportingData");
                }

                return true;
            }
            catch (Exception ex)
            {
                string result = ex.Message;
                SolutionHelper.Error.LogError(ex, "CsvToDatabase.DataTableToDatabase:" + _processor.DatabaseName + " Current Table: " + currentTable + " - " + ex.Message);
            }

            return false;
        }

        private DateTime[] GetDates()
        {
            return GetReportDates(_tablename);
        }

        private bool ProcessGeoSummaryData(DataTable datatab, string tableName)
        {
            string query = String.Format("EXEC [dbo].[spUpdateSizmekServerGeo] @Date = '{0}' , @Table = '{1}'", datatab.TableName, tableName);

            try
            {
                _processor.RunQuery(query);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }

            return true;
        }


        private bool ProcessBulkCopy(DataTable dt)
        {
            try
            {
                using (SqlConnection dbConnection = new SqlConnection(_processor.ConnectionString))
                {
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(dbConnection))
                    {
                        bulkCopy.BulkCopyTimeout = 1500;

                        bulkCopy.DestinationTableName = _tablename;
                        foreach (var column in dt.Columns)
                            bulkCopy.ColumnMappings.Add(column.ToString(), column.ToString());

                        bulkCopy.DestinationTableName = _tablename;

                        bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);

                        bulkCopy.NotifyAfter = 20000;
                        dbConnection.Open();

                        bulkCopy.WriteToServer(dt);

                        return true;
                    }
                }

            }
            catch (Exception ex)
            {
                Error.LogError(ex, "ProcessBulkCopy:" + _processor.DatabaseName);
            }

            return false;
        }
        private DateTime[] GetReportDates(string tableName)
        {
            List<DateTime> dates = new List<DateTime>();

            string query = String.Format(@"select distinct [AsOfDate] FROM [dbo].[{0}] where [AsOfDate] is not null", tableName);

            DataSet ds = _processor.RunQuery(query);

            if (ds != null) // Add date to list of date
                if (ds.Tables.Count != 0) // Add date to list of date
                    foreach (DataRow dr in ds.Tables[0].Rows)
                        dates.Add(DateTime.Parse(dr[0].ToString())); // Date is a non nullable date type

            return dates.ToArray();
        }

        private bool GetReportDate(string date)
        {
            List<DateTime> dates = new List<DateTime>();

            string query = String.Format(@"select top 1 AsOfDate FROM [dbo].[{0}] where AsOfDate = '{1}'", _tablename, date);

            DataSet ds = _processor.RunQuery(query);

            if (ds != null) // Add date to list of date
                if (ds.Tables.Count != 0) // Add date to list of date
                    foreach (DataRow dr in ds.Tables[0].Rows)
                        return true;


            return false;
        }

        /// <summary>
        /// This is to compare the total records and the sum of the impressions against the database. 
        /// </summary>
        /// <param name="datatab"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private bool CompareReportDataAgainstDatabaseData(DataTable datatab, string tableName)
        {

            try
            {

                string query = String.Format(@"SELECT count(*) Total, sum([Impressions]) SumImp FROM [dbo].[{0}] where AsOfDate =  '{1}'", tableName, datatab.TableName);

                DataSet ds = _processor.RunQuery(query);

                if (ds != null)
                {
                    if (ds.Tables.Count != 0)
                    {
                        int _sumImp = 0;
                        int total = int.Parse(ds.Tables[0].Rows[0]["Total"].ToString());
                        int sumImp = int.Parse(ds.Tables[0].Rows[0]["SumImp"].ToString());

                        foreach (DataRow dr in datatab.Rows)
                        {
                            double impress = double.Parse(dr["Impressions"].ToString().Replace(",", "").Replace("\"", ""));
                            int imps = (int)impress;

                            _sumImp += imps;
                            // _sumImp += int.Parse(dr["Impressions"].ToString().Replace(",", "").Replace("\"", ""));
                        }

                        int _total = datatab.Rows.Count;

                        if (total == _total && sumImp == _sumImp)
                            return true;
                    }
                }

            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }


        private void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(String.Format("{0} Rows have been copied.", e.RowsCopied.ToString()));
        }

        /// <summary>
        /// Clean the data based on the column type define in the AddColumn method 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private DataTable CleanDataFeed(DataTable dt)
        {
            int idx = 0;
            int idxPerMinute = 0;
            DateTime start = DateTime.UtcNow;
            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName.ToUpper().ContainsAny("CLICKS",
                       "POST-IMPRESSION CONVERSIONS",
                       "POST IMP",
                       "IMPRESSIONS",
                       "UNIQUE USERS",
                       "POST-CLICK CONVERSIONS",
                       "POST CLICK CON",
                       "TOTAL CONVERSIONS",
                       "TOTALCON",
                       "VIDEO STARTED",
                       "VIEWABLE IMPRESSIONS",
                       "VIDEO FULL PLAY",
                       "VIDEO PLAYED 25",
                       "VIDEO PLAYED 50",
                       "VIDEO PLAYED 75"))
                    {
                        string apr = dr[col.ColumnName].ToString();

                        dr[col.ColumnName] = decimal.ToInt32(decimal.Parse(apr));
                    }
                }


                idx++;
                idxPerMinute++;

                if (start.AddSeconds(+30) < DateTime.UtcNow)
                {
                    Console.WriteLine(String.Format("Processing {0} of {1}     -      " + idxPerMinute + " Per Minute", idx, dt.Rows.Count));
                    start = DateTime.UtcNow;
                    idxPerMinute = 0;
                }
            }
            return dt;

        }

        /// <summary>
        /// Check if all the columns are created in the database for the reporting table. If the column doesnt exist it will be added.
        /// </summary>
        /// <param name="dt"></param>
        private void ColumnChecker(DataTable dt, string tableName)
        {
            foreach (DataColumn dc in dt.Columns)
                if (dc.ColumnName.Contains("_"))
                    dc.ColumnName = dc.ColumnName.Replace("_", " ");

            string[] columnCol = GetColumnsFromDatabase(tableName);

            foreach (DataColumn dc in dt.Columns)
            {
                if (dc.ColumnName.Contains("* "))
                    dc.ColumnName = dc.ColumnName.Replace("* ", "");

                dc.ColumnName = dc.ColumnName.ToProperCase();

                if (dc.ColumnName.ToLower() == "date")
                {
                    dc.ColumnName = "AsOfDate";
                    continue;
                }

                if (!columnCol.Contains(dc.ColumnName.ToLower()))
                {
                    AddColumnDataFeed(tableName, dc.ColumnName);
                }
            }

            DateTime today = System.DateTime.Now;

            if (!dt.Columns.Contains("ModifiedDate"))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn("ModifiedDate", typeof(System.DateTime));
                newColumn.DefaultValue = today.ToString();
                dt.Columns.Add(newColumn);
            }

            bool bolChecker = DebugHelper.CheckColumnsAgainstDatabase(_processor, dt, "dataSizmekDataFeedReportingData");
        }

        /// <summary>
        /// Due to the columns needing to be dynamic this method is designed to add additional column to the table
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        private void AddColumnDataFeed(string table, string column)
        {
            column = column.ToProperCase();

            string query = "ALTER TABLE {0} ADD [{1}] {2}";

            if (column.ToUpper().ContainsAny("CLICKS",
                "POST-IMPRESSION CONVERSIONS",
                "POST IMP",
                "POST-CLICK CONVERSIONS",
                "POST CLICK CON",
                "TOTAL CONVERSIONS",
                "TOTALCON",
                "VIDEO STARTED",
                "VIDEO FULL PLAY"))
                query = string.Format(query, table, column, "[int] NULL");
            else if (column.ToUpper().ContainsAny("VIEWABILITY RATE", "VIDEO FULLY PLAYED RATE"))
                query = string.Format(query, table, column, "decimal(21, 8) NULL");
            else if (column.ToUpper().ContainsAny("VIDEO PLAYED 25", "VIDEO PLAYED 50", "VIDEO PLAYED 75"))
                query = string.Format(query, table, column, "[int] NULL");
            else
                query = string.Format(query, table, column, "[nvarchar](250)");

            _processor.RunQuery(query);
        }


        private string[] GetColumnsFromDatabase(string tableName)
        {
            string query = "";
            query += "DECLARE @columns varchar(MAX) ";
            query += "SELECT @columns =  COALESCE(@columns  + ',', '') + CONVERT(varchar(MAX), COLUMN_NAME) ";
            query += String.Format("FROM (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{0}') as tab ", tableName);
            query += "select @columns ";

            DataSet ds = _processor.RunQuery(query);

            string columns = ds.Tables[0].Rows[0][0].ToString();

            string[] columnCol = columns.ToLower().Split(',').Select(x => x.Trim()).ToArray();

            return columnCol;
        }

        public static DataTable GetDataTabletFromCSVTSVFile(string csv_file_path)
        {
            if (csv_file_path.Contains(".csv"))
                return GetDataTabletFromFile(csv_file_path, "\t");
            if (csv_file_path.Contains(".tsv"))
                return GetDataTabletFromFile(csv_file_path, "\t");
            if (csv_file_path.Contains(".xls"))
                return GetDataTabletXls(csv_file_path);
            else
                return null;
        }

        private static DataTable GetDataTabletXls(string csv_file_path)
        {
            try
            {
                string sheetName = "Sheet1";

                if (csv_file_path.Contains("Search_Overview"))
                    sheetName = "Search_Overview_FlatExcel";

                DataTable table = new DataTable();
                string strConn = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1;TypeGuessRows=0;ImportMixedTypes=Text\"", csv_file_path);

                using (OleDbConnection dbConnection = new OleDbConnection(strConn))
                {
                    using (OleDbDataAdapter dbAdapter = new OleDbDataAdapter(String.Format("SELECT * FROM [{0}$] WHERE [F4] <> ''", sheetName), dbConnection)) //rename sheet if required!
                        dbAdapter.Fill(table);
                    int rows = table.Rows.Count;
                }

                try
                {
                    DataTable dt_Parsed = new DataTable();
                    String str_Connection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + csv_file_path + ";Extended Properties=\"Excel 12.0;HDR=YES;IMEX=1;\"";

                    // CREATED DB CONNECTION OBJECT TO READ FILE
                    using (System.Data.OleDb.OleDbConnection oledb_Connection = new System.Data.OleDb.OleDbConnection(str_Connection))
                    {
                        // OPEN DB CONNECTION
                        oledb_Connection.Open();

                        // READS FILE DB SCHEMA
                        System.Data.DataTable dt_Schema = oledb_Connection.GetSchema("COLUMNS");

                        // CHECKS THAT DB SCHEMA IS NOT NULL
                        if (dt_Schema != null)
                        {
                            // CREATES DB COMMAND TO READ FILE
                            System.Data.OleDb.OleDbCommand odbc_Command = new System.Data.OleDb.OleDbCommand(String.Format("SELECT * FROM [{0}] WHERE [F4] <> ''",
                                dt_Schema.Rows[15]["TABLE_NAME"].ToString()), oledb_Connection);

                            // SETS DB COMMAND TYPE
                            odbc_Command.CommandType = CommandType.Text;

                            // PARSES FILE ACCORDINGLY
                            new System.Data.OleDb.OleDbDataAdapter(odbc_Command).Fill(dt_Parsed);
                        }

                        // CLOSE DB CONNECTION
                        oledb_Connection.Close();
                    }
                }
                catch (Exception)
                {
                }
                 
                //var connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0};Extended Properties='Excel 8.0;IMEX=1;HDR=NO;TypeGuessRows=0;ImportMixedTypes=Text'", csv_file_path);

                //OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + csv_file_path + @";Extended Properties=""Excel 8.0;IMEX=1;HDR=NO;TypeGuessRows=0;ImportMixedTypes=Text""");

                var connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0; data source={0};Extended Properties='Excel 12.0;IMEX=1;HDR=YES;TypeGuessRows=0;ImportMixedTypes=Text'", csv_file_path);

                //  OleDbConnection connection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + csv_file_path + @";Extended Properties=""Excel 8.0;IMEX=0;HDR=YES;TypeGuessRows=0;ImportMixedTypes=Text""");

                OleDbConnection connection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + csv_file_path + @";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;'");

                OleDbCommand cmd = new OleDbCommand(String.Format("SELECT * FROM [{0}$] WHERE [F4] <> 'Date'", sheetName), connection);
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);


                DataSet ds = new DataSet();
                ds.Tables.Add("xlsImport", "Excel");
                da.Fill(ds, "xlsImport");

                DataTable dt = ds.Tables[0];

                return dt;
            }
            catch (Exception ex)
            {

            }

            return null;
        }

        public static DataTable GetDataTabletFromFile(string csv_file_path, string delimeter)
        {
            DataTable csvData = new DataTable();
            try
            {
                using (TextFieldParser csvReader = new TextFieldParser(csv_file_path))
                {
                    csvReader.SetDelimiters(new string[] { delimeter });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    string[] colFields = csvReader.ReadFields();

                    int donthit = 7;
                    if (!colFields.Contains(",,,,,,,,,,,,,,,,")  ) 
                    {

                        if (colFields.Count() == 1)
                        {
                            if (colFields[0].ToString().Contains(","))
                            {
                                csvReader.SetDelimiters(new string[] { "," });
                                colFields = colFields[0].Split(',');
                            }
                            else if (colFields[0].ToString().Contains("\t"))
                            {
                                csvReader.SetDelimiters(new string[] { "\t" });
                                colFields = colFields[0].Split('\t');
                            }
                        }

                        foreach (string column in colFields)
                        {
                            DataColumn datecolumn = new DataColumn(column);
                            datecolumn.AllowDBNull = true;
                            csvData.Columns.Add(datecolumn);
                        }
                        while (!csvReader.EndOfData)
                        {
                            string[] fieldData = csvReader.ReadFields();
                            //Making empty value as null
                            for (int i = 0; i < fieldData.Length; i++)
                            {
                                if (fieldData[i] == "")
                                {
                                    fieldData[i] = null;
                                }
                            }
                            csvData.Rows.Add(fieldData);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                string dido = "";
            }
            return csvData;
        }

    }
}
