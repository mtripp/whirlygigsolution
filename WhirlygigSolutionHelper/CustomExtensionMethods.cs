﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Globalization;
using Ionic.Zip;
using WhirlygigSolutionHelper;
using Excel;
using System.Configuration;
using System.Dynamic;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace SolutionHelper
{
    public static class CustomExtensionMethods
    {
        public static DataTable ToDataTable(Stream stream, delimiter del, int skip = 0)
        {
            DataTable oDataTable = null;
            string[] ColumnNames = null;
            string[] oStreamDataValues = null;


            int skipRowCount = -1;
            int RowCount = 0;
            string strColumn = "";

            bool converted = false;

            try
            {
                //initialising a StreamReader type variable and will pass the file location
                StreamReader oStreamReader = new StreamReader(stream);

                //using while loop read the stream data till end
                while (!oStreamReader.EndOfStream)
                {
                    String oStreamRowData = oStreamReader.ReadLine().Trim();
                    String oStreamRowDataOriginal = oStreamRowData;

                    oStreamRowData = oStreamRowDataOriginal;
                    skipRowCount++;

                    if (skipRowCount < skip)
                        continue;

                    if (oStreamRowData == "")
                        continue;

                    if (oStreamRowData.ToLower() == strColumn.ToLower())
                        continue;

                    retry:
                     
                    if (oStreamRowData == "Error: The access token is invalid since the user hasn't engaged the app in longer than 90 days.")
                        throw new Exception(oStreamRowData);

                    if (oStreamRowData.Length > 0)
                    {
                        if (del == delimiter.comma)
                        {
                            if (converted)
                                if (RowCount != 0)  // This check is to insure you are targeting data not the header row
                                {
                                    oStreamRowData = ScrubTsvToCsv(oStreamRowData); 
                                }

                            oStreamDataValues = oStreamRowData.Split(',');

                            bool test = false;

                            if (test) // For testing purposes
                                if (String.IsNullOrWhiteSpace(oStreamDataValues[51].ToString()))
                                {
                                    string value = oStreamDataValues[51].ToString();
                                    Console.WriteLine(value);

                                }
                        }
                        else if (del == delimiter.tab)
                        {
                            if (oStreamRowData.Contains("\t\t"))
                                oStreamRowData = oStreamRowData.Replace("\t\t", "\tNULL\t");

                            if (oStreamRowData.Contains("\t\t")) // Needs a double pass
                                oStreamRowData = oStreamRowData.Replace("\t\t", "\tNULL\t");

                            oStreamDataValues = oStreamRowData.Split('\t');

                            if (RowCount != 0)
                            {
                                if (oStreamDataValues.Count() != ColumnNames.Count())
                                {
                                    do
                                    {
                                        List<string> list = oStreamDataValues.ToList();
                                        list.Add("ERROR");
                                        oStreamDataValues = list.ToArray();
                                    } while (oStreamDataValues.Count() != ColumnNames.Count());
                                }
                            }


                            /// Occasionally Sizmek sends files that are marked as .tsv but are really comma seperated. This converts them
                            if (oStreamDataValues.Count() == 1)
                            {
                                string[] firstRow = oStreamDataValues[0].ToString().Split(',');
                                if (oStreamDataValues.Count() < firstRow.Count())
                                {
                                    converted = true;
                                    del = delimiter.comma;
                                    goto retry;
                                }
                            }
                        }

                        //Bcoz the first row contains column names, we will poluate 
                        //the column name by
                        //reading the first row and RowCount -0 will be true only once
                        if (RowCount == 0)
                        {
                            #region BuildColumns
                            strColumn = oStreamRowData.ToLower();

                            if (del == delimiter.comma)
                                ColumnNames = oStreamRowData.Split(',');
                            else if (del == delimiter.tab)
                                ColumnNames = oStreamRowData.Split('\t');

                            oDataTable = new DataTable();

                            //using foreach looping through all the column names
                            foreach (string csvcolumn in ColumnNames)
                            {
                                string col = csvcolumn;

                                if (oDataTable.Columns.Contains(csvcolumn))
                                    col = col + " (Duplicate)";

                                DataColumn oDataColumn = new DataColumn(col.ToUpper(), typeof(string));

                                //setting the default value of empty.string to newly created column
                                oDataColumn.DefaultValue = string.Empty;

                                //adding the newly created column to the table
                                oDataTable.Columns.Add(oDataColumn);
                            }
                            #endregion
                        }
                        else
                        {
                            //creates a w with the same schema as of the oDataTable            
                            DataRow oDataRow = oDataTable.NewRow();

                            if (oStreamDataValues[0].ToString().ContainsAny(ConfigurationManager.AppSettings["RowsToAvoidWhenImporting"].Split(',')))
                                continue;


                            //using foreach looping through all the column names
                            for (int i = 0; i < ColumnNames.Length; i++)
                            {
                                oDataRow[ColumnNames[i]] = oStreamDataValues[i] == null ? string.Empty : oStreamDataValues[i].ToString();
                            }

                            //adding the newly created row with data to the oDataTable       
                            oDataTable.Rows.Add(oDataRow);
                        }

                        RowCount++;
                    }
                }

                //close the oStreamReader object
                oStreamReader.Close();
                //release all the resources used by the oStreamReader object
                oStreamReader.Dispose();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return oDataTable;
        }

        private static string ScrubTsvToCsv(string value)
        {
            if (value.Contains("\""))
            {
                string pattern = @"(""[^"",]+),([^""]+"")";

                MatchCollection mc = Regex.Matches(value, pattern);

                foreach (Match m in mc)
                { 
                    string replacement  = m.Value.ToString();
                    string newValue = m.Value.ToString().Replace(",", "").Replace("\"", "");

                    value = value.Replace(replacement, newValue);
                }


            }

            return value;
        }

        public static DataTable ToDataTableCsv(this Stream stream, int skip = 0)
        {
            return ToDataTable(stream, delimiter.comma, skip);
        }

        public static DataTable ToDataTableTsv(this Stream stream, int skip = 0)
        {
            return ToDataTable(stream, delimiter.tab, skip);
        }
        public static DataTable ToDataTableFromZip(this Stream stream, int skip = 0)
        {
            Streem newStreem = new Streem();

            byte[] b = null;

            using (stream)
            using (MemoryStream ms = new MemoryStream())
            {
                int count = 0;
                do
                {
                    byte[] buf = new byte[1024];
                    count = stream.Read(buf, 0, 1024);
                    ms.Write(buf, 0, count);
                } while (stream.CanRead && count > 0);
                b = ms.ToArray();
            }

            Stream newStream = new MemoryStream(b);

            using (newStream)
            using (ZipFile zip = ZipFile.Read(newStream))
            {
                foreach (ZipEntry e in zip)
                {
                    if (e.FileName.ContainsAny(".csv", ".tsv", ".xls", ".xslx"))
                    {
                        MemoryStream tempS = new MemoryStream();
                        e.Extract(tempS);

                        Dictionary<string, byte[]> files = new Dictionary<string, byte[]>();
                        files.Add("csvFile", tempS.ToArray());
                        Stream bytee = new MemoryStream(tempS.ToArray());

                        Streem _newStreem = new Streem { Name = e.FileName, Stream = bytee };
                        newStreem = _newStreem;
                    }
                }
            }

            if (newStreem.Name.Contains(".csv"))
                return ToDataTable(stream, delimiter.comma, skip);

            else if (newStreem.Name.Contains(".tsv"))
            {
                try
                {
                    return ToDataTable(stream, delimiter.tab, skip);
                    throw new Exception();
                }
                catch (Exception ex)
                {

                    using (stream)
                    using (MemoryStream ms = new MemoryStream())
                    {
                        int count = 0;
                        do
                        {
                            byte[] buf = new byte[1024];
                            count = stream.Read(buf, 0, 1024);
                            ms.Write(buf, 0, count);
                        } while (stream.CanRead && count > 0);
                        b = ms.ToArray();
                    }

                    throw;
                }
            }
            else
                return null;
        }

        public static DataTable ToDataTableXls(this Stream stream)
        {
            DataTable dt = new DataTable();

            try
            {
                ExcelReader er = new ExcelReader();
                dt = er.Read(stream);

                if (dt == null)      // Excel spreadsheet could be missforned to you may need to export and fill datatable
                {
                    string csvExport = ConfigurationManager.AppSettings["csvExport"];
                    csvExport += String.Format(@"\tempprocessingfile_{0}.xls", System.DateTime.Now.ToString().Replace(":", " ").Replace("/", "-"));
                    using (stream)
                    {
                        var fileStream = new FileStream(csvExport, FileMode.Create, FileAccess.Write);
                        // stream.CopyTo(fileStream);
                        fileStream.Dispose();
                    }

                }

            }
            catch (Exception ex)
            {
                string haha = ex.Message;
                throw ex;
            }



            return dt;
        }

        /// <summary>
        /// Split data table by "AsOfDate" into a dataset with each set of records
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DataSet ToDataSetDateSplit(this DataTable dt)
        {
            DataSet ds = new DataSet();

            if (dt == null || dt.Rows.Count == 0)
                return null;

            List<DataTable> result = new List<DataTable>();

            try
            {
                // Rename column to OLD:
                dt.Columns["AsOfDate"].ColumnName = "AsOfDate_OLD";
                // Add column with new type:
                dt.Columns.Add("AsOfDate", typeof(DateTime));
                // copy data from old column to new column with new type:
                foreach (DataRow dr in dt.Rows)
                {
                    string colName = dr["AsOfDate_OLD"].ToString();

                    DateTime dateTime;

                    if (!DateTime.TryParse(colName, out dateTime))
                    {
                        continue;
                    }
                    else
                        dr["AsOfDate"] = System.Convert.ToDateTime(dr["AsOfDate_OLD"]);
                }
                // remove "OLD" column
                dt.Columns.Remove("AsOfDate_OLD");

                DataView dv = dt.DefaultView;
                dv.Sort = "AsOfDate DESC";
                dt = dv.ToTable();
            }
            catch (Exception ex)
            {

            }

            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = dt.Rows[i];
                if (dr["AsOfDate"].ToString() == "")
                    dr.Delete();
            }

            try
            {
                result = dt.AsEnumerable()
                .GroupBy(row => row.Field<DateTime>("AsOfDate"))
                .Select(g => g.CopyToDataTable())
                .ToList();
            }
            catch (Exception)
            {
                result = dt.AsEnumerable()
                .GroupBy(row => row.Field<string>("AsOfDate"))
                .Select(g => g.CopyToDataTable())
                .ToList();
            }

            foreach (DataTable _dt in result)
            {
                _dt.TableName = _dt.Rows[0]["AsOfDate"].ToString();
                ds.Tables.Add(_dt);
            }

            return ds;
        }

        /// <summary>
        /// ExportCsv
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToDataSetExportTsv(this DataTable dt)
        {
            try
            {
                return GetTextFromDataTable(dt, "\t");
            }
            catch (Exception ex)
            {
                return "";
            }

            return "";
        }

        /// <summary>
        /// ExportCsv
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static bool ToDataSetExportTsv(this DataTable dt, string location)
        {
            try
            {
                string file = GetTextFromDataTable(dt, "\t");

                File.WriteAllText(location + @"\" + dt.TableName, file.ToString());
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// ExportCsv
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToDataSetExportCsv(this DataTable dt)
        {
            try
            {
                return GetTextFromDataTable(dt, @","); //TODO TEST
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        private static string GetTextFromDataTable(DataTable dataTable, string delimeter)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(string.Join(delimeter, dataTable.Columns.Cast<DataColumn>().Select(arg => arg.ColumnName)));
            foreach (DataRow dataRow in dataTable.Rows)
                stringBuilder.AppendLine(string.Join(delimeter, dataRow.ItemArray.Select(arg => arg.ToString())));
            return stringBuilder.ToString();
        }

        public static void ToFileFromStream(this Stream stream, string filename)
        {
            using (Stream file = File.Create(filename))
            {
                CopyStream(stream, file);
            }
        }

        /// <summary>
        /// Copies the contents of input to output. Doesn't close either stream.
        /// </summary>
        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }
        }

        public static Stream ToStreamFromZip(this Stream stream)
        {
            MemoryStream data = new MemoryStream();

            Dictionary<string, byte[]> files = new Dictionary<string, byte[]>();

            MemoryStream tempS = new MemoryStream();

            using (ZipFile zip = ZipFile.Read(stream))
            {
                foreach (ZipEntry e in zip)
                {
                    if (e.FileName.ContainsAny(".csv", ".tsv", ".xls", ".xlsx"))
                    {
                        e.Extract(tempS);

                        files.Add("csvFile", tempS.ToArray());

                        break;
                    }
                }
            }

            Stream bytee = new MemoryStream(tempS.ToArray());

            return bytee;
        }
        public static Stream ToStreamFromExcel(this Stream stream)
        {
            try
            {
                IExcelDataReader reader = ExcelReaderFactory.CreateOpenXmlReader(stream);

                reader.IsFirstRowAsColumnNames = true;

                DataSet result = reader.AsDataSet();

                MemoryStream data = new MemoryStream();

                Dictionary<string, byte[]> files = new Dictionary<string, byte[]>();

                MemoryStream tempS = new MemoryStream();

                using (ZipFile zip = ZipFile.Read(stream))
                {
                    foreach (ZipEntry e in zip)
                    {
                        if (e.FileName.ContainsAny(".csv", ".tsv"))
                        {
                            e.Extract(tempS);

                            files.Add("csvFile", tempS.ToArray());

                            break;
                        }
                    }
                }

                Stream bytee = new MemoryStream(tempS.ToArray());

                return bytee;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public static Stream ToStreamFromZip(this Streem streem)
        {
            MemoryStream data = new MemoryStream();

            Dictionary<string, byte[]> files = new Dictionary<string, byte[]>();

            MemoryStream tempS = new MemoryStream();

            using (ZipFile zip = ZipFile.Read(streem.Stream))
            {
                foreach (ZipEntry e in zip)
                {
                    if (e.FileName.ContainsAny(".csv", ".tsv", ".xls", ".xlsx"))
                    {
                        e.Extract(tempS);

                        files.Add("csvFile", tempS.ToArray());

                        streem.Name = e.FileName;

                        break;
                    }
                }
            }

            Stream bytee = new MemoryStream(tempS.ToArray());
            streem.Stream = bytee;

            return bytee;
        }

        public static Streem[] ToStreamFromZip(this Streem streem, bool NoExport)
        {
            List<Streem> streems = new List<Streem>();

            using (ZipFile zip = ZipFile.Read(streem.Stream))
            {
                foreach (ZipEntry e in zip)
                {
                    if (e.FileName.ContainsAny(".csv", ".tsv"))
                    {
                        MemoryStream tempS = new MemoryStream();
                        e.Extract(tempS);

                        Dictionary<string, byte[]> files = new Dictionary<string, byte[]>();
                        files.Add("csvFile", tempS.ToArray());
                        Stream bytee = new MemoryStream(tempS.ToArray());

                        Streem newStreem = new Streem { Name = e.FileName, Stream = bytee };

                        streems.Add(newStreem);
                    }
                }
            }

            return streems.ToArray();
        }

        public static string ToStreamFromZip(this Streem stream, string csvExport = "", string prefix = "")
        {
            MemoryStream data = new MemoryStream();

            Dictionary<string, byte[]> files = new Dictionary<string, byte[]>();

            MemoryStream tempS = new MemoryStream();

            string _file = "";
            string filePath = "";

            using (ZipFile zip = ZipFile.Read(stream.Stream))
            {
                foreach (ZipEntry e in zip)
                {
                    if (prefix != "")
                        _file = csvExport + @"\" + prefix + e.FileName;
                    else
                        _file = csvExport + @"\" + e.FileName;

                    if (e.FileName.ContainsAny(".csv", ".tsv", ".xls"))
                    {
                        try
                        {

                            e.Extract(tempS);
                        }
                        catch (Exception ex)
                        {
                            //TODO: REGRESSION TESTING NEEDED
                        }
                        files.Add("csvFile", tempS.ToArray());

                        stream.Name = e.FileName;

                        Stream bytee = new MemoryStream(tempS.ToArray());

                        filePath = Path.Combine(csvExport, _file);

                        FileStream Stream = new FileStream(filePath, FileMode.Create);
                        BinaryWriter BinaryStream = new BinaryWriter(Stream);
                        BinaryStream.Write(tempS.ToArray());
                        BinaryStream.Close();

                        break;
                    }
                }
            }

            return filePath;
        }

        public static void ToStreamFromZip(this Streem streem, string csvExport = "")
        {
            Streem newStreem = new Streem();

            byte[] b = null;

            using (streem.Stream)
            using (MemoryStream ms = new MemoryStream())
            {
                int count = 0;
                do
                {
                    byte[] buf = new byte[1024];
                    count = streem.Stream.Read(buf, 0, 1024);
                    ms.Write(buf, 0, count);
                } while (streem.Stream.CanRead && count > 0);
                b = ms.ToArray();
            }

            Stream newStream = new MemoryStream(b);

            using (newStream)
            using (ZipFile zip = ZipFile.Read(newStream))
            {
                foreach (ZipEntry e in zip)
                {
                    int idx = 0;

                    if (e.FileName.ContainsAny(".csv", ".tsv", ".xls", ".xslx"))
                    {
                        csvExport += e.FileName;
                        streem.Name = e.FileName;

                        restart: // MIT - 7.17.2018: Adding logic to append a version marker to the export file if the filename already exists

                        try
                        {
                            if (idx > 5)
                                throw new Exception("ToStramFromZip File Creation Error: " + csvExport); // This is to prevent process from creating infinite files.

                            if (File.Exists(csvExport))
                            {
                                string ext = System.IO.Path.GetExtension(csvExport);
                                csvExport = csvExport.Replace(ext, " (" + idx.ToString() + ")" + ext);
                                idx++;

                                goto restart;
                            }

                        }
                        catch (Exception ex)
                        {
                            SolutionHelper.Error.LogError(ex, "ToStreamFromZip: " + csvExport.ToString());
                            // TODO: This needs to be properly tested before throwing a fatal error.
                        }

                        MemoryStream tempS = new MemoryStream();
                        e.Extract(tempS);

                        Dictionary<string, byte[]> files = new Dictionary<string, byte[]>();
                        files.Add("csvFile", tempS.ToArray());
                        Stream bytee = new MemoryStream(tempS.ToArray());

                        string filePath = Path.Combine(csvExport, csvExport);

                        FileStream Stream = new FileStream(filePath, FileMode.Create);
                        BinaryWriter BinaryStream = new BinaryWriter(Stream);
                        BinaryStream.Write(tempS.ToArray());
                        BinaryStream.Close();
                    }
                }
            }
        }

        public static void ToStreamFromZip(this FileInfo f, string csvExport = "")
        {


            string outputDirectory = "";
            ZipFile zip1 = ZipFile.Read(f.FullName);
            Directory.CreateDirectory(outputDirectory);
            foreach (ZipEntry e in zip1)
            {
                // check if you want to extract e or not
                if (e.FileName == "TheFileToExtract")
                    e.Extract(outputDirectory, ExtractExistingFileAction.OverwriteSilently);
            }


            FileStream fs = f.Create();

            Stream stream = fs.ToStreamFromZip();


            using (ZipFile zip = ZipFile.Read(stream))
            {
                foreach (ZipEntry e in zip)
                {
                    if (e.FileName.ContainsAny(".csv", ".tsv", ".xls", ".xslx"))
                    {
                        csvExport += e.FileName;

                        MemoryStream tempS = new MemoryStream();
                        e.Extract(tempS);

                        Dictionary<string, byte[]> files = new Dictionary<string, byte[]>();
                        files.Add("csvFile", tempS.ToArray());
                        Stream bytee = new MemoryStream(tempS.ToArray());

                        string filePath = Path.Combine(csvExport, csvExport);

                        FileStream Stream = new FileStream(filePath, FileMode.Create);
                        BinaryWriter BinaryStream = new BinaryWriter(Stream);
                        BinaryStream.Write(tempS.ToArray());
                        BinaryStream.Close();
                    }
                }
            }

        }

        // all params are optional
        public static void ChangeDatabase(this DbContext source, WhirlygigSolutionHelper.Data data = null)
        {
            try
            {
                // use the const name if it's not null, otherwise
                // using the convention of connection string = EF contextname
                // grab the type name and we're done
                var configNameEf = string.IsNullOrEmpty(data.ConnectionString)
                    ? source.GetType().Name
                    : data.ConnectionString;

                // add a reference to System.Configuration
                var entityCnxStringBuilder = new System.Data.Entity.Core.EntityClient.EntityConnectionStringBuilder
                    (System.Configuration.ConfigurationManager
                        .ConnectionStrings[configNameEf].ConnectionString);

                // init the sqlbuilder with the full EF connectionstring cargo
                var sqlCnxStringBuilder = new SqlConnectionStringBuilder
                    (entityCnxStringBuilder.ProviderConnectionString);

                // only populate parameters with values if added
                if (!string.IsNullOrEmpty(data.DatabaseName))
                    sqlCnxStringBuilder.InitialCatalog = data.DatabaseName;
                //if (!string.IsNullOrEmpty(dataSource))
                //    sqlCnxStringBuilder.DataSource = dataSource;
                if (!string.IsNullOrEmpty(data.UserName))
                    sqlCnxStringBuilder.UserID = data.UserName;
                if (!string.IsNullOrEmpty(data.Password))
                    sqlCnxStringBuilder.Password = data.Password;

                // set the integrated security status
                sqlCnxStringBuilder.IntegratedSecurity = true;

                // now flip the properties that were changed
                source.Database.Connection.ConnectionString
                    = sqlCnxStringBuilder.ConnectionString;
            }
            catch (Exception ex)
            {
                string TODO = "";
            }
        }

        /// <summary>
        /// Determine if a string contains any of the string parameters that are passed to it.
        /// </summary>
        /// <param name="haystack"></param>
        /// <param name="needles"></param>
        /// <returns></returns>
        public static bool ContainsAny(this string haystack, params string[] needles)
        {
            foreach (string needle in needles)
            {
                if (haystack.Contains(needle))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Replace all instances with one character.
        /// </summary>
        /// <param name="repCharacters">Replacement character</param>
        /// <param name="newCharacters"></param>
        /// <returns></returns>
        public static string ReplaceAny(this string origString, string repCharacter, params string[] oldCharacters)
        {  
            foreach (string oldCharacter in oldCharacters)
            {
                origString = origString.Replace(oldCharacter, repCharacter);
            }

            return origString;
        }

        /// <summary>
        /// Reverse String.Format formula
        /// </summary> 
        /// <returns></returns>
        public static string Format(this string the_string, params string[] _strings)
        {
            string _format = String.Format(the_string, _strings);

            return _format;
        }

        /// <summary>
        /// Reverse String.Format formula
        /// </summary> 
        /// <returns></returns>
        public static string Format(this string the_string, string _string)
        {
            string _format = String.Format(the_string, _string);

            return _format;
        }
         
        // Convert the string to Pascal case.
        public static string ToPascalCase(this string the_string)
        {
            the_string = the_string.ToLower();

            // If there are 0 or 1 characters, just return the string.
            if (the_string == null) return the_string;
            if (the_string.Length < 2) return the_string.ToUpper();

            // Split the string into words.
            string[] words = the_string.Split(
                new char[] { },
                StringSplitOptions.RemoveEmptyEntries);

            // Combine the words.
            string result = "";
            foreach (string word in words)
            {
                result +=
                    word.Substring(0, 1).ToUpper() +
                    word.Substring(1);
            }

            return result;
        }
        // Capitalize the first character and add a space before
        // each capitalized letter (except the first character).
        public static string ToProperCase(this string the_string)
        {

            string title = the_string.ToLower();

            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            title = textInfo.ToTitleCase(title); //War And Peace 

            return title;
        }

        public static void Convert<T>(this DataColumn column, Func<object, T> conversion)
        {
            foreach (DataRow row in column.Table.Rows)
            {
                row[column] = conversion(row[column]);
            }
        }
        public enum delimiter { tab, comma, xls, xslx }
    }
}
