﻿using SolutionHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhirlygigSolutionHelper
{
    public class Data
    {
        public string DatabaseName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConnectionString { get; set; }
        public string EntityFrameworkConnectionString { get; set; }
        public string Server { get; set; }

        private const string _Server = "172.16.8.201";
        private string conStr = "Server={3};Database={0};User ID={1};Password={2};Trusted_Connection=False;Connection Timeout=1600";
        private string conEntity = "data source={3};initial catalog={0};persist security info=True;user id={1};password={2};MultipleActiveResultSets=True;App=EntityFramework";

        public Data()
        {

        }

        public Data(string database, string user, string password, string server)
        {
            DatabaseName = database;
            UserName = user;
            Password = password;
            Server = server;

            ConnectionString = String.Format(conStr, DatabaseName, UserName, Password, Server);
            EntityFrameworkConnectionString = String.Format(conEntity, DatabaseName, UserName, Password, Server);
        }

        public DataSet RunQuery(string strQuery)
        {
            DataSet ds = new DataSet();

            if (strQuery.Trim() != string.Empty)
            {
                if (Server == "") 
                    Server = _Server;
                 
                ConnectionString = String.Format(conStr, DatabaseName, UserName, Password, Server);

                SqlConnection conn = new SqlConnection(ConnectionString);
            
                try
                {
                    SqlDataAdapter sda = new SqlDataAdapter(strQuery, conn);
                    conn.Open();
                    sda.Fill(ds); 
                    conn.Close();
                    return ds;
                }
                catch (Exception ex)
                {
                    if (conn.State != ConnectionState.Closed)
                    {
                        conn.Close();
                    }
                            
                    Error.LogError(ex, "RunQuery qry:" + strQuery);

                    throw;
                }
            }
            return ds;
        }
    }

}
