﻿using SolutionHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhirlygigSolutionHelper
{
    public class DebugHelper
    {

        /// <summary>
        /// This is for debugging any issues with Columns in the datatable not matching the database 
        /// </summary>
        /// <param name="dt"></param>
        public static bool CheckColumnsAgainstDatabase(Data Processor, DataTable dtOriginal, string TableName)
        {
            try
            {
                DataTable dt = dtOriginal;

                bool RedoColumns = false;
                int idx = 0;
                string currentValue = "";

                if (dt.TableName == "")
                    dt.TableName = TableName;

                // Add row index for easier trouble shooting
                if (!dt.Columns.Contains("Idx"))
                {
                    DataColumn column = new DataColumn();
                    column.DataType = System.Type.GetType("System.Int32");
                    column.ColumnName = "Idx";

                    dt.Columns.Add(column);
                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                        dt.Rows[i][column] = i + 1;
                }

                try
                {
                    string query = "Select * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{0}'".Format(TableName.Split());

                    DataSet ds = Processor.RunQuery(query);

                    string[] columnCol = ds.Tables[0].AsEnumerable().Select(r => r.Field<string>("COLUMN_NAME")).ToArray();   // columns.Split(',');

                    List<string> columnDt = columnCol.OfType<string>().ToList();

                    List<string> finalCollection = new List<string>();
                    finalCollection = columnDt;

                    if (RedoColumns) // This is meant to be a manual process. Debug only
                    {
                        if (bool.Parse(ConfigurationManager.AppSettings["debug"].ToString()))
                        {
                            foreach (string colName in columnCol)
                            {

                                if (colName != colName.ToProperCase())
                                {
                                    if (!colName.ContainsAny("AsOfDate", "ModifiedDate", "ActiveRcd"))
                                    {
                                        string properName = colName.ToProperCase();
                                        query = String.Format("EXEC sp_rename '[dbo].[{0}].[{1}]', '{2}', 'COLUMN';", TableName, colName, properName);

                                        Processor.RunQuery(query);
                                    }
                                }
                            }
                        }
                    }


                    // The goal of this loop is to help determine if there is a wrong datatype in the database. 
                    foreach (DataColumn dc in dt.Columns)
                    {

                        string columnName = dc.ColumnName;

                        if (dc.ColumnName == "Idx")
                            continue;

                        DataRow foundRow = ds.Tables[0].Select("COLUMN_NAME = '" + columnName + "'").First();
                        var dataType = foundRow[7].ToString();

                        Console.WriteLine(columnName);
                        //  var col = dt.Select(columnName);

                        try
                        {

                            if (dc.ColumnName == "AsOfDate")
                                columnName = columnName;


                            switch (dataType)
                            {
                                case "bigint":
                                    {
                                        var _col = dt.Select("Conversion Time", "'Conversion Time' ASC");

                                    }
                                    break;
                                case "bit":
                                    {

                                    }
                                    break;
                                case "char":
                                    {

                                    }
                                    break;
                                case "date":
                                case "datetime":
                                case "smalldatetime":
                                    {

                                    }
                                    break;
                                case "decimal":
                                case "float":
                                    {

                                    }
                                    break;
                                case "int":
                                    {

                                    }
                                    break;
                                case "nvarchar":
                                case "varchar":
                                    {
                                        int maxValue = int.Parse(foundRow[8].ToString());

                                        //        string[] columndCol = dt.AsEnumerable().Select(r => r.Field<string>(dc.ColumnName).ToString()).ToArray();   // columns.Split(',');
                                        foreach (DataRow dr in dt.Rows)
                                        {
                                            currentValue = dr[dc.ColumnName].ToString();

                                            if (maxValue > -1)
                                                if (dr[dc.ColumnName].ToString().Length > maxValue)
                                                    throw new Exception(String.Format("Row {0} exceeds column '{1}': {2} of {3}'", dr["Idx"].ToString(), dc.ColumnName.ToString(), dr[dc.ColumnName].ToString().Length, maxValue));

                                        }
                                    }
                                    break;
                                case "smallint":
                                    {

                                    }
                                    break;
                                case "tinyint":
                                    {

                                    }
                                    break;
                                default:
                                    {

                                    }
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            SolutionHelper.Error.LogError("WhirlygigSolutionHelper.DebugHelper.CheckColumnsAgainstDatabase - " + ex.Message);
                            return false;
                        }
                        string finish = "SF";
                    }

                    foreach (string _column in columnDt)
                    {
                        if (!_column.ContainsAny("AsOfDate", "ModifiedDate", "ActiveRcd"))
                            if (_column != _column.ToProperCase())
                                throw new Exception();

                        if (!columnCol.Contains(_column))
                        {
                            foreach (DataColumn dc in dt.Columns)
                            {
                                if (dc.ColumnName == _column)
                                {
                                    dc.ColumnName = dc.ColumnName.Trim();
                                }
                            }

                            finalCollection.Remove(_column);
                        }
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
