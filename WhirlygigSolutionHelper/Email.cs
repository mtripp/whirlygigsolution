﻿using System;
using System.Configuration;
using System.Xml;
using System.Web;
using System.Text;
using System.Data;
using System.Collections;
using System.IO;
using System.Net.Mail;
using SolutionHelper;
using System.Net.Mime;
using WhirlygigSolutionHelper;

namespace SolutionHelper
{

    /// <summary>
    /// Summary description for Email.
    /// </summary>
    public class Email
    {
        public static void SendEmail(string strTo, string strCC, string strBCC, string strFrom, string strBody, string strSubject)
        {
            try
            {
                string server = ConfigurationManager.AppSettings["SmtpServer"];

                MailMessage message = new MailMessage(strFrom, strTo);

                if (strBody.Contains("<email>"))
                    message.IsBodyHtml = true;
                 
                message.Subject = strSubject;
                message.Body = strBody;

                SmtpClient client = new SmtpClient(server);

                client.UseDefaultCredentials = true;

                client.Send(message);
            }
            catch (Exception ex)
            {
                Error.LogError("SendEmail Error:" + strTo + "|Body:" + strBody + " -- " + ex.Message);
                throw new Exception("Error sending email. " + ex.Message);
            }
        }
        public static void SendEmail(string strTo, string strCC, string strBCC, string strFrom, string strBody, string strSubject, Streem _attachment)
        {
            try
            {
                using (_attachment.Stream)
                {
                    using (System.IO.StreamWriter writer = new System.IO.StreamWriter(_attachment.Stream))
                    {
                        string server = ConfigurationManager.AppSettings["SmtpServer"];

                        MailMessage message = new MailMessage(strFrom, strTo);

                        message.Subject = strSubject;
                        message.Body = strBody;

                        if (_attachment != null)  //Add a new attachment to the E-mail message, using the correct MIME type
                        {
                            Attachment attachment = new Attachment(_attachment.Stream, new ContentType("text/csv"));
                            attachment.Name = _attachment.Name;
                            message.Attachments.Add(attachment);
                        }

                        SmtpClient client = new SmtpClient(server);

                        client.UseDefaultCredentials = true;

                        client.Send(message);
                    }
                }
            }
            catch (Exception ex)
            {
                Error.LogError(ex, "SendEmail:" + strTo + "|Body:" + strBody);
                throw new Exception("Error sending email. " + ex.Message);
            }
        }
    }
}









