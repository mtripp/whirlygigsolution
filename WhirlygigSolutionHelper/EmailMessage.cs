//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WhirlygigSolutionHelper
{
    using System;
    using System.Collections.Generic;
    
    public partial class EmailMessage
    {
        public string MessageId { get; set; }
        public Nullable<System.DateTime> DateSent { get; set; }
        public string Date { get; set; }
        public string Subject { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<long> CompanyId { get; set; }
        public string TableName { get; set; }
        public string ErrorMessage { get; set; }
        public Nullable<bool> IsProcessed { get; set; }
        public string FileName { get; set; }
        public string Notes { get; set; }
    
        public virtual Company Company { get; set; }
    }
}
