﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SolutionHelper;

namespace SolutionHelper
{
    /// <summary>
    /// Summary description for Error.
    /// </summary>
    public class Error
    {
        public Error()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static void Log(Exception ex)
        {
            try
            {
                LogError(ex, "");
                //				sw = File.AppendText(ConfigurationManager.AppSettings["error.file"]);
                //				sw.WriteLine(DateTime.Now.ToString() + ": " + ex.Message.ToString());
                //				sw.Close();
            }
            catch
            {

            }
        }

        public static void LogError(string str)
        {
            LogError(str, true);
        }
        public static void LogError(string str, bool email = true)
        {
            try
            {
                StreamWriter sw = File.AppendText(ConfigurationManager.AppSettings["error.file"]);
                try
                {
                    sw.WriteLine("");
                    sw.WriteLine(DateTime.Now + "-" + str);
                    sw.Close();

                    if (email && bool.Parse(ConfigurationManager.AppSettings["error.email"]))
                    {
                        SendError(DateTime.Now + "-" + str);
                    }
                }
                catch { }
                finally
                {
                    sw.Close();
                }
            }
            catch { }
        }

        public static void LogError(Exception ex, string extraData, bool email = true)
        {
            try
            {
                if (ex.Message.ToLower() == "Thread was being aborted.".ToLower())
                    return;
                StreamWriter sw = File.AppendText(ConfigurationManager.AppSettings["error.file"]);
                try
                {
                    sw.WriteLine("");
                    sw.WriteLine(DateTime.Now);
                    if (ex.Message != null)
                        sw.WriteLine("message: " + ex.Message.ToString());
                    if (ex.StackTrace != null)
                        sw.WriteLine("stack trace: " + ex.StackTrace.ToString());
                    if (ex.Source != null)
                        sw.WriteLine("source: " + ex.Source.ToString());
                    if (ex.HelpLink != null)
                        sw.WriteLine("help link: " + ex.HelpLink.ToString());
                    if (extraData.Trim().Length > 0)
                    {
                        sw.WriteLine("extraData: ");
                        string[] ed = extraData.Split(",".ToCharArray());
                        foreach (string s in ed)
                        {
                            sw.WriteLine(s);
                        }
                    }
                    Exception inner = ex.InnerException;
                    while (inner != null)
                    {
                        int level = 0;
                        sw.WriteLine("inner exception " + level.ToString() + ": ");
                        if (inner.Message != null)
                            sw.WriteLine("\tmessage: " + inner.Message.ToString());
                        if (inner.StackTrace != null)
                            sw.WriteLine("\tstacktrace: " + inner.StackTrace.ToString());
                        if (inner.Source != null)
                            sw.WriteLine("\tsource: " + inner.Source.ToString());
                        if (inner.HelpLink != null)
                            sw.WriteLine("\tsource: " + inner.HelpLink.ToString());

                        inner = inner.InnerException;
                        level++;
                    }
                     
                    sw.Close();

                    if (email && bool.Parse(ConfigurationManager.AppSettings["error.email"]))
                    {
                        SendError(DateTime.Now + "-" + ex.Message + Environment.NewLine + extraData);
                    }

                }
                catch (Exception ex2)
                {
                    LogError("THIS ERROR OCCURED IN THE ERROR LOG: " + ex2.Message.ToString(),false);
                }
                finally
                {
                    sw.Close();
                }
            }
            catch { }
        }

        private static void SendError(string str)
        {
            string strTo = ConfigurationManager.AppSettings["errorTo"];
            bool debug = bool.Parse( ConfigurationManager.AppSettings["debug"]);

            if (debug)
                Email.SendEmail(strTo, "", "", "info@lavidge.com", str, "Error Log");
            else
                Email.SendEmail(strTo, "", "", "info@lavidge.com", str, "Prodcution Error Log");
        }

    }
}
