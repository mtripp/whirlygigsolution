﻿using Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhirlygigSolutionHelper
{
    public class ExcelReader
    {

        public DataTable Read(Stream stream)
        {
            try
            { 
         //            IExcelDataReader reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                IExcelDataReader reader = ExcelReaderFactory.CreateBinaryReader(stream);
                reader.IsFirstRowAsColumnNames = true;
                DataSet result = reader.AsDataSet();

                int intRow = 14; 

                DataTable dt = result.Tables[0];

                for (int counter = 0; intRow > 0; intRow--)
                {
                    dt.Rows[counter].Delete();
                    counter++;
                }

                dt.AcceptChanges();

                string removeColumns = ""; 

                foreach (DataColumn col in dt.Columns)
                {
                    string colName = dt.Rows[0][col].ToString();

                    if (colName == "")
                        removeColumns += col.ColumnName + ",";
                    else
                    {
                        if (colName == "S\0e\0a\0r\0c\0h\0 ")
                            dt.Columns[col.ColumnName.ToString()].ColumnName = "Search Engine";
                        if (colName == "C\0a\0m\0p\0a\0i\0g")
                            dt.Columns[col.ColumnName.ToString()].ColumnName = "Campaign Name";
                        if (colName == "A\0d\0 \0G\0")
                            dt.Columns[col.ColumnName.ToString()].ColumnName = "Ad Group";
                        if (colName == "D\0a\0")
                            dt.Columns[col.ColumnName.ToString()].ColumnName = "Date";
                        if (colName == "T\0o\0t\0a\0l\0 \0")
                            dt.Columns[col.ColumnName.ToString()].ColumnName = "Total Clicks";
                        if (colName == "S\0e\0r\0v\0e\0d\0 \0I\0m\0")
                            dt.Columns[col.ColumnName.ToString()].ColumnName = "Served Impressions";
                        if (colName == "C\0T\0")
                            dt.Columns[col.ColumnName.ToString()].ColumnName = "CTR";
                        if (colName == "A\0v\0e\0r\0a\0g\0e\0 \0")
                            dt.Columns[col.ColumnName.ToString()].ColumnName = "Average Position";
                        if (colName == "R\0O\0")
                            dt.Columns[col.ColumnName.ToString()].ColumnName = "ROI";
                        if (colName == "R\0O\0A\0S\0")
                            dt.Columns[col.ColumnName.ToString()].ColumnName = "ROAS ($)";
                        if (colName == "T\0o\0t\0a\0l\0 \0C\0o\0n")
                            dt.Columns[col.ColumnName.ToString()].ColumnName = "Total Conversions";
                        if (colName == "C\0l\0i\0c\0k\0 \0t\0o\0 \0C\0o\0n\0")
                            dt.Columns[col.ColumnName.ToString()].ColumnName = "Click to Conversion Rate";
                        if (colName == "C\0o\0n\0v\0e\0r\0s\0i\0o\0n\0s\0")
                            dt.Columns[col.ColumnName.ToString()].ColumnName = "Conversions Revenue ($)";
                        if (colName == "T\0o\0t\0a\0l\0 \0M\0e\0d\0i\0")
                            dt.Columns[col.ColumnName.ToString()].ColumnName = "Total Media Cost ($)";
                        if (colName == "T\0o\0t\0a\0l\0 \0P\0r\0")
                            dt.Columns[col.ColumnName.ToString()].ColumnName = "Total Profit ($)";
                        if (colName == "A\0v\0e\0r\0a\0g\0e\0 \0C\0o\0s\0t\0 \0")
                            dt.Columns[col.ColumnName.ToString()].ColumnName = "Average Cost per Click($)";
                        if (colName == "A\0v\0e\0r\0a\0g")
                            dt.Columns[col.ColumnName.ToString()].ColumnName = "Average CPM";
                        if (colName == "A\0v\0e\0r\0a\0g\0e\0 \0C\0o\0s\0t\0 \0p\0e\0r")
                            dt.Columns[col.ColumnName.ToString()].ColumnName = "Average Cost per Conversion ($)";
                        if (colName == "P\0a\0p\0e\0r\0l\0e\0s\0s\0 \0-\0 \0G\0o\0 \0P\0a\0p\0e\0r\0l\0e\0s\0s\0 \0C")
                            dt.Columns[col.ColumnName.ToString()].ColumnName = "Paperless - Go Paperless Clicks -Total Conversions";
                        if (colName == "P\0a\0p\0e\0r\0l\0e\0s\0s\0 \0-\0 \0G\0o\0 \0P\0a\0p\0e\0r\0l\0e\0s\0s\0 \0C\0l\0i\0c\0")                                                          
                         dt.Columns[col.ColumnName.ToString()].ColumnName = "Paperless - Go Paperless Clicks -Click to Conversion Rate";
                        if (colName == "P\0a\0p\0e\0r\0l\0e\0s\0s\0 \0-\0 \0P\0a\0g\0e\0 \0V\0i\0s\0i\0t")                                                          
                         dt.Columns[col.ColumnName.ToString()].ColumnName = "Paperless - Page Visits - Total Conversions";
                        if (colName == "P\0a\0p\0e\0r\0l\0e\0s\0s\0 \0-\0 \0P\0a\0g\0e\0 \0V\0i\0s\0i\0t\0s\0 \0-\0")                                                          
                         dt.Columns[col.ColumnName.ToString()].ColumnName = "Paperless - Page Visits - Click to Conversion Rate";
                        if (colName == "S\0o\0l\0u\0t\0i\0o\0n\0s\0 \0F\0o\0r\0 \0B\0u\0s\0i\0n\0e\0s\0s\0 \0-\0 \0L\0a\0n\0d\0i\0n\0g\0")                                                          
                        dt.Columns[col.ColumnName.ToString()].ColumnName = "Solutions For Business - Landing Page Visits -Total Conversions";
                        if (colName == "S\0o\0l\0u\0t\0i\0o\0n\0s\0 \0F\0o\0r\0 \0B\0u\0s\0i\0n\0e\0s\0s\0 \0-\0 \0L\0a\0n\0d\0i\0n\0g\0 \0P\0a\0g")                                                          
                         dt.Columns[col.ColumnName.ToString()].ColumnName = "Solutions For Business - Landing Page Visits -Click to Conversion Rate";
                        if (colName == "S\0o\0l\0u\0t\0i\0o\0n\0s\0 \0F\0o\0r\0 \0B\0u\0s\0i\0n\0e\0s\0s\0 \0-\0 \0R\0e\0t\0")                                                          
                         dt.Columns[col.ColumnName.ToString()].ColumnName = "Solutions For Business - Retargeting - Total Conversions";
                        if (colName == "S\0o\0l\0u\0t\0i\0o\0n\0s\0 \0F\0o\0r\0 \0B\0u\0s\0i\0n\0e\0s\0s\0 \0-\0 \0R\0e\0t\0a\0r\0g\0e")                                                          
                        dt.Columns[col.ColumnName.ToString()].ColumnName = "Solutions For Business - Retargeting - Click to Conversion Rate";
                        if (colName == "S\0o\0l\0u\0t\0i\0o\0n\0s\0 \0F\0o\0r\0 \0B\0u\0s\0i\0n\0e\0s\0s\0 \0-\0 \0V")                                                          
                         dt.Columns[col.ColumnName.ToString()].ColumnName = "Solutions For Business - Visits - Total Conversions";
                        if (colName == "S\0o\0l\0u\0t\0i\0o\0n\0s\0 \0F\0o\0r\0 \0B\0u\0s\0i\0n\0e\0s\0s\0 \0-\0 \0V\0i\0s\0i\0")                                                          
                            dt.Columns[col.ColumnName.ToString()].ColumnName = "Solutions For Business - Visits - Click to Conversion Rate"; 
                    }
                }

                dt.Rows[0].Delete();

                dt.AcceptChanges();

                return dt;
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
            return null;
        }

        public void Read()
        {
            string location = @"C:\Users\mtripp\Downloads\APS_APS_-_Influencer_Campaign_Unique_Metrics_Summary_161353.xls";

            FileStream fs = File.Open(location, FileMode.Open, FileAccess.Read);
            IExcelDataReader reader = ExcelReaderFactory.CreateBinaryReader(fs);

            reader.IsFirstRowAsColumnNames = true;
            DataSet result = reader.AsDataSet();
            int intRow = 15;
            int intCurrent = 20;

            DataTable dt = result.Tables[0];

            for (int counter = 0; intRow > 0; intRow--)
            {
                dt.Rows[counter].Delete();
                counter++;
            }

            dt.AcceptChanges();

            dt.Rows[1].Delete();
            dt.Rows[2].Delete();

            dt.AcceptChanges();

            for (int counter = 0; intCurrent > 0; intCurrent--)
            {
                dt.Columns.RemoveAt(0);
                dt.AcceptChanges();
                counter++;
            }

            string removeColumns = "";
            string coln = "";

            foreach (DataColumn col in dt.Columns)
            {
                string colName = dt.Rows[0][col].ToString();
              //  coln = colName;
                if (colName == "")
                    removeColumns += col.ColumnName + ",";
                else
                    dt.Columns[col.ColumnName.ToString()].ColumnName = colName;
            }

            dt.Rows[0].Delete();

            dt.AcceptChanges();

            foreach (string col in removeColumns.Split(','))
            {
                if (col != "")
                    dt.Columns.Remove(col);
            }

            int lastRow = 0;
            int idx = 0;

            foreach (DataRow dr in dt.Rows)
            {
                if (dr[2].ToString() != "")
                {
                    lastRow = idx;
                }
                else
                {
                    dr[0] = dt.Rows[lastRow][0];
                    dr[1] = dt.Rows[lastRow][1];
                    dr[2] = dt.Rows[lastRow][2];
                    dr[3] = dt.Rows[lastRow][3];
                    dr[4] = dt.Rows[lastRow][4];
                    dr[5] = dt.Rows[lastRow][5];
                }

                idx++;
            }

            var table = dt.AsEnumerable()
                  .Where(r => r.Field<string>(coln) != "")
                  .CopyToDataTable();
        }
    }
}
