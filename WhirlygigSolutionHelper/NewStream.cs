﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SolutionHelper.CustomExtensionMethods;

namespace WhirlygigSolutionHelper
{
    public class Streem
    {
        public string Name { get; set; }
        public Stream Stream { get; set; }

        public DataTable ToDataTable()
        {
            DataTable dt = new DataTable();

            delimiter del = new delimiter();

            if (this.Name.Contains(".tsv"))
                del = delimiter.tab;
            else if (this.Name.Contains(".csv"))
                del = delimiter.comma;

            return SolutionHelper.CustomExtensionMethods.ToDataTable( Stream, del, 0);
        }
    }
}
