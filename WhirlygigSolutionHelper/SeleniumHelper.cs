﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
/// <summary>
/// Documentation:
/// 
/// Configure Firefox to download to a specific place
/// http://kb.mozillazine.org/File_types_and_download_actions
/// </summary>
namespace WhirlygigSolutionHelper
{
    public class SeleniumHelper
    {
        public static bool GetLinkedInReport(string accountNumber)
        {
            try
            {
                //  IWebDriver dv = new FirefoxDriver();
                IWebDriver dv = new ChromeDriver();
                // accountNumber = "505914064";

                dv.Url = String.Format(@"https://www.linkedin.com/start/join?trk=login_reg_redirect&session_redirect=https%3A%2F%2Fwww.linkedin.com%2Fad%2Faccounts%2F{0}", accountNumber);

                //dv.Url = @"https://www.linkedin.com/uas/login?session_redirect=https%3A%2F%2Fwww.linkedin.com%2Fad%2Faccounts

                dv.FindElement(By.CssSelector(@".sign-in-link")).Click();

                Thread.Sleep(1000);

                dv.FindElement(By.CssSelector(@"#session_key-login")).Clear();
                dv.FindElement(By.CssSelector(@"#session_key-login")).SendKeys(ConfigurationManager.AppSettings["LinkedLogin"]);
                dv.FindElement(By.CssSelector(@"#session_password-login")).Clear();
                dv.FindElement(By.CssSelector(@"#session_password-login")).SendKeys(ConfigurationManager.AppSettings["LinkedPassword"]);

                Thread.Sleep(500);

                dv.FindElement(By.CssSelector(@"#btn-primary")).Click();

                Thread.Sleep(50);

                // dv.FindElement(By.CssSelector(@".account-505914064")).Click(); 

                Thread.Sleep(2000);

                ////For daily pull uncomment
                //dv.FindElement(By.CssSelector(@".selected-dropdown-date")).Click();
                //dv.FindElement(By.CssSelector(@"ul.drop:nth-child(4) > li:nth-child(2)")).Click();

                try
                {
                    IJavaScriptExecutor js = dv as IJavaScriptExecutor;

                    string title = (string)js.ExecuteScript("$('#dialog-mask').remove();");
                    Thread.Sleep(1000);
                    string title2 = (string)js.ExecuteScript("$('#download-metrics').click();");
                }
                catch (Exception)
                {

                }

                Thread.Sleep(100);
                    //try
                    //{

                    //dv.FindElement(By.CssSelector(@"#download-metrics")).Click();
                    //dv.FindElement(By.XPath("$('#download-metrics').click()")).Click();
                    //}
                    //catch (Exception)
                    //{ 
                    //}
                Thread.Sleep(100);
                dv.FindElement(By.CssSelector(@".report-type-select")).Click();
                Thread.Sleep(100);
                dv.FindElement(By.CssSelector(@".report-type-select > option:nth-child(2)")).Click();
                Thread.Sleep(100);
                dv.FindElement(By.CssSelector(@".download")).Click();


                Thread.Sleep(2000);

                dv.Quit();  
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}
